begin

	dayRange = (/"00-04","05-09","10-14"/)
	
	member = "12"
	
	res         = "f09"
	member_stub = "DYNAMO_"+member+"_"

  	num_t = dimsizes(dayRange)
;===================================================================================
;===================================================================================
ofile = "/Users/whannah/DYNAMO/Hindcast/data/"+member_stub+".batch_file.txt"
lines = ""
cr = inttochar(10) 
do t = 0,num_t-1
  ;--------------------------------
  ; Write lines for ftp batch file
  ;--------------------------------
  case_name = member_stub+dayRange(t)
  SRC =           "/Users/whannah/DYNAMO/Hindcast/data/"+case_name+"/"
  DST = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/"+case_name+"/"
  
  lines = lines+"put  "+SRC+"*  "+DST+cr
  ;--------------------------------
  ;--------------------------------
end do

	asciiwrite(ofile,lines)
	
	print("")
	system("sftp -b "+ofile+"  keto.atmos.colostate.edu")
	print("")
;===================================================================================
;===================================================================================
end

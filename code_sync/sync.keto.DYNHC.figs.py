#!/usr/bin/env python
#=========================================================================================
# 	This script transfers VAPOR data from the keto server to a local directory	
#
#    Dec, 2013	Walter Hannah 		Colorado State University
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

DST_DIR = "~/Research/DYNAMO/Hindcast/"

SRC_DIR = "/home/whannah/Research/DYNAMO/Hindcast/"

cmd = "rsync -uavt "

#---------------------------------------
#---------------------------------------
file_type = ".png"

os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+"*"+file_type+"  "+DST_DIR)

dir = "figs_GMS"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_budget"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_climo"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_hov"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_Q"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_drift"
os.system(cmd+" keto.atmos.colostate.edu:"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

#=========================================================================================
#=========================================================================================

#! /bin/bash

	SRC='/maloney-scratch/whannah/DYNAMO/Hindcast/figures'
	DST='/Users/whannah/DYNAMO/Hindcast/figures'
	
	MCH='keto.atmos.colostate.edu'
	USR='whannah'

rsync  $USR@$MCH:$SRC/RMM.ts.*			$DST
rsync  $USR@$MCH:$SRC/RMM.ts.alt.* 		$DST
rsync  $USR@$MCH:$SRC/DYNAMO.HC.hov.*	$DST
rsync  $USR@$MCH:$SRC/DYNAMO.HC.hov.*	$DST

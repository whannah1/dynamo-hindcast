#!/usr/bin/env python
#=========================================================================================
#   This script transfers local files to the Miami "weather" server
#
#    Dec, 2014  Walter Hannah       University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

SRC_DIR = "~/Research/DYNAMO/Hindcast/data/"

#DST_DIR = "~/Research/DYNAMO/Hindcast/"

DST_DIR = "/data2/whannah/DYNAMO/Hindcast/data/"

cmd = "rsync -uravPt "

server = "weather.rsmas.miami.edu"

os.system(cmd+" "+SRC_DIR+"  "+server+":"+DST_DIR+"")

#=========================================================================================
#=========================================================================================
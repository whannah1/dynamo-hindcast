#!/usr/bin/env python
#=========================================================================================
# 	This script transfers VAPOR data from the keto server to a local directory	
#
#    Dec, 2013	Walter Hannah 		Colorado State University
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

server = "echidna.rsmas.miami.edu"

DST_DIR = "~/Research/DYNAMO/Hindcast/"

SRC_DIR = "~/Research/DYNAMO/Hindcast/"

cmd = "rsync -uavt "+server+": "

#---------------------------------------
#---------------------------------------
file_type = ".png"

dir = "code_drift"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_drift"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

exit()

os.system(cmd+SRC_DIR+"*"+file_type+"  "+DST_DIR)

dir = "figs_GMS"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_budget"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_climo"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_hov"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_Q"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

dir = "figs_drift"
os.system(cmd+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")

#=========================================================================================
#=========================================================================================

#!/usr/bin/env python
#========================================================================================================
# 	This script transfers repackaged DYNAMO Hindcast data from the keto server to a local directory	
#
#    Jul, 2014	Walter Hannah 		University of Miami
#========================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================
#========================================================================================================

DST_DIR = "~/Research/DYNAMO/Hindcast/data/repacked/"

SRC_DIR = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/repacked/"
#SRC_DIR = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/"

cmd 	= "rsync -uavt "

days	= ["00-04","05-09"]
#days	= ["00-04"]
	
mem		= ["90","09","EC"]

var	 	= ["U"]

res     = "09"
#memStub = "DYNAMO_"+str(mem)+"_"

#========================================================================================================
#========================================================================================================
num_v	= len(var)
num_m 	= len(mem)
num_t 	= len(days)

for v in range(num_v):
	for m in range(num_m):
		for t in range(num_t):
			case_name = "DYNAMO_"+mem[m]+"_"+days[t]
			
			SRC = SRC_DIR+case_name+"/*."+var[v]+".*nc"
			DST = DST_DIR+case_name+"/"

			os.system(cmd+" keto.atmos.colostate.edu:"+SRC+"  "+DST)

#========================================================================================================
#========================================================================================================

#!/usr/bin/env python
#=========================================================================================
# 	This script transfers figure files from the keto server to a local directory	
#
#    Feb, 2014	Walter Hannah 		Colorado State University
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

SRC_DIR = "/Users/whannah/DYNAMO/Hindcast/"

DST_DIR = "/home/whannah/Research/DYNAMO/Hindcast/"

cmd = "rsync -uavt "

#---------------------------------------
#---------------------------------------
file_type = ".ncl"

server = "keto.atmos.colostate.edu"

os.system(cmd+" "+SRC_DIR+"code_budget/*"	+file_type+"  "+server+":"+DST_DIR+"code_budget/")
os.system(cmd+" "+SRC_DIR+"code_climo/*"	+file_type+"  "+server+":"+DST_DIR+"code_climo/")
os.system(cmd+" "+SRC_DIR+"code_GMS/*"		+file_type+"  "+server+":"+DST_DIR+"code_GMS/")
os.system(cmd+" "+SRC_DIR+"code_hov/*"		+file_type+"  "+server+":"+DST_DIR+"code_hov/")
os.system(cmd+" "+SRC_DIR+"code_bin/*"		+file_type+"  "+server+":"+DST_DIR+"code_bin/")
os.system(cmd+" "+SRC_DIR+"code_Q/*"		+file_type+"  "+server+":"+DST_DIR+"code_Q/")

#---------------------------------------
#---------------------------------------


#=========================================================================================
#=========================================================================================

#!/usr/bin/env python
#========================================================================================================
# 	This script transfers repackaged DYNAMO Hindcast data from the keto server to a local directory	
#
#    Jul, 2014	Walter Hannah 		University of Miami
#========================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================
#========================================================================================================

#DST_DIR = "~/Data/DYNAMO/Hindcast/data/raw/"
DST_DIR = "~/Research/DYNAMO/Hindcast/data/raw/"

SRC_DIR = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/raw/"

cmd 	= "rsync -uavt "

yrmndy = 20110000 + np.array([1001,1006,1011,1016,1021,1026,1031,1105,1110,1115,1120,1125,1130,1205,1210,1215])
	
mem		= ["90","09"]

var	 	= ["RMM"]

res     = "f09"
#memStub = "DYNAMO_"+str(mem)+"_"

#========================================================================================================
#========================================================================================================
num_v	= len(var)
num_m 	= len(mem)
num_t 	= len(yrmndy)

for v in range(num_v):
	for m in range(num_m):
		for t in range(num_t):
			case_name = "DYNAMO_"+mem[m]+"_"+res+"_"+str(yrmndy[t])
			
			SRC = SRC_DIR+case_name+"/*."+var[v]+".*nc"
			DST = DST_DIR+case_name+"/"

			os.system(cmd+" keto.atmos.colostate.edu:"+SRC+"  "+DST)

#========================================================================================================
#========================================================================================================

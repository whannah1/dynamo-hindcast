#!/usr/bin/env python
#=========================================================================================
#   This script transfers local files to the Miami "weather" server
#
#    Dec, 2014  Walter Hannah       University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

SRC_DIR = "~/Research/DYNAMO/Hindcast/"

DST_DIR = "~/Research/DYNAMO/Hindcast/"

cmd = "rsync -uravt "

server = "weather.rsmas.miami.edu"

#=========================================================================================
#=========================================================================================

#os.system(cmd+" --exclude 'data' "+SRC_DIR+"  "+server+":"+DST_DIR+"")

os.system(cmd+" "+SRC_DIR+"code_*"+"  "+server+":"+DST_DIR+"")
#os.system(cmd+" "+SRC_DIR+"figs_*"+"  "+server+":"+DST_DIR+"")

#file_type = "*.ncl"
#os.system(cmd+" "+SRC_DIR+"code_repack/"+file_type+"  "+server+":"+DST_DIR+"code_repack/")
#os.system(cmd+" "+SRC_DIR+"code_budget/"+file_type+"  "+server+":"+DST_DIR+"code_budget/")
#os.system(cmd+" "+SRC_DIR+"code_climo/"    +file_type+"  "+server+":"+DST_DIR+"code_climo/")
#os.system(cmd+" "+SRC_DIR+"code_GMS/"  +file_type+"  "+server+":"+DST_DIR+"code_GMS/")
#os.system(cmd+" "+SRC_DIR+"code_hov/"  +file_type+"  "+server+":"+DST_DIR+"code_hov/")
#os.system(cmd+" "+SRC_DIR+"code_bin/"  +file_type+"  "+server+":"+DST_DIR+"code_bin/")
#os.system(cmd+" "+SRC_DIR+"code_Q/"        +file_type+"  "+server+":"+DST_DIR+"code_Q/")

#=========================================================================================
#=========================================================================================
load "$NCARG_ROOT/custom_functions.ncl"
begin
;===================================================================================
;===================================================================================
    ;member = (/"09","12","13"/)
    member = "13"
    num_m = dimsizes(member)
    
    log_dir = "~/Research/DYNAMO/Hindcast/logs/"
    cdir    = "~/Research/DYNAMO/Hindcast/code_repack/"

    do m = 0,num_m-1
        
        printline()

        tlog = log_dir+"repack.var.v2."+member(m)+".out"
        print("  "+tlog)
        system("ncl imem='"+member(m)+"' "+cdir+"repack.var.v2.ncl > "+tlog)

        tlog = log_dir+"repack.sfc.v2."+member(m)+".out"
        print("  "+tlog)
        system("ncl imem='"+member(m)+"' "+cdir+"repack.sfc.v2.ncl > "+tlog)
        ;system("ncl imem='"+member(m)+"' "+cdir+"repack.var.v2.ncl ")
        
    end do
;===================================================================================
;===================================================================================
printline()
end

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin
	case = (/"LSF"/)
	
	sa = "nsa"
	
	rd1 = (/00,05,10/)
	rds = sprinti("%0.2i",rd1)
	rde = sprinti("%0.2i",rd1+5-1)
	oname = "DYNAMO_"+case+"_"+sa+"_"+rds+"-"+rde
	
	
	
	;var = (/"Q1_theta","Q2"/)
	var = "DSE"

;===================================================================================
; Coordinates
;===================================================================================
  num_c = dimsizes(case)
  num_v = dimsizes(var)
  num_r = dimsizes(rd1)
  lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,\
          600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,70./)
  lev = lev(::-1)
  if sa.eq."nsa" then
    lat = 3.5
  end if
  if sa.eq."ssa" then
    lat = -3.5
  end if
  lon = 75.
  num_lat = dimsizes(lat)
  num_lon = dimsizes(lon)
  num_lev = dimsizes(lev)  
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do r = 0,num_r-1
  ofile = "/Users/whannah/DYNAMO/Hindcast/data/"+oname(r)+"/"+oname(r)+"."+var(v)+".nc"
  ;=======================================
  ; Repack Hindcast simulations
  ;=======================================
  num_t = (15-rd1(r)/5)*5*4
  time = new((/num_t/),double)
    time = ispan(0,(num_t-1)*6,6)
    time!0 = "time"
    time&time = time
    time@units = "hours since 2011-10-"+sprinti("%0.2i",1+rd1(r))
  V = new((/num_t,num_lev,num_lat,num_lon/),float)
    V!0 = "time"
    V!1 = "lev"
    V!2 = "lat"
    V!3 = "lon"
    V&time = time
    V&lev = lev
    V&lat = lat
    V&lon = lon
  ;----------------------------------------------------------------------
  new_dir = "/Users/whannah/DYNAMO/Hindcast/data/"+oname(r)+"/"
  if .not.isfilepresent(new_dir) then
    system("mkdir "+new_dir)
  end if
  ;-------------------------------------
  if isfilepresent(ofile) then
    system("rm "+ofile) 
  end if
  ;-------------------------------------
  outfile = addfile(ofile,"c")
  outfile->$var(v)$ = V
  outfile->lat = lat
  outfile->lon = lon
  outfile->lev = lev
  ;-------------------------------------
do c = 0,num_c-1
    ;=======================================
    ; Load simulation data
    ;=======================================
      ifile = "/Users/whannah/DYNAMO/"+case+"/"+sa+"."+var(v)+".nc"
      infile = addfile(ifile,"r") 
      print("	var = "+var(v)+"	type:"+typeof(infile->$var(v)$))
      t1 = (rd1(r))*4
      t2 = (rd1(r))*4+num_t-1
      outfile->$var(v)$ = (/ infile->$var(v)$(t1:t2,:,:,:) /)
    ;=======================================
    ;=======================================
    delete([/time,V/])
end do
;===================================================================================
;===================================================================================

  	print("")
  	print("  "+ofile)
  	print("")
end do
end do
;===================================================================================
;===================================================================================
end

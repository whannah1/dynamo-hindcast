load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    HCdatadir = "/var/run/media/whannah/Helios/DYNAMO/Hindcast/data/"

    ;odir = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/"
    odir = "~/Data/DYNAMO/Hindcast/repacked/"

    yr = 2011
    mn = (/10,10,10,10,10,10,10,11,11,11,11,11,11,12,12,12/)
    dy = (/01,06,11,16,21,26,31,05,10,15,20,25,30,05,10,15/)
    ;mn = (/10/)
    ;dy = (/11/)
    yrmndy = toint( yr*10000 + mn*100 + dy )

    ;imem = 13
    member = sprinti("%0.2i",imem)

    case = (/"DYNAMO_"+member+"_f09_"/)+yrmndy

    rd1 = (/00,05/)
    rds = sprinti("%0.2i",rd1)
    rde = sprinti("%0.2i",rd1+5-1)
    oname = "DYNAMO_"+member+"_"+rds+"-"+rde

    ;var = (/"FLUT","PRECT","PS"/)
    var = "FLUT"

    mkplot = False

;===================================================================================
; Coordinates
;===================================================================================
    num_c = dimsizes(case)
    num_v = dimsizes(var)
    num_r = dimsizes(rd1)
    lat1 = -90.
    lat2 =  90.
    lon1 =   0.
    lon2 = 360.
    opt  = True
    opt@lat1 = lat1
    opt@lat2 = lat2
    opt@lon1 = lon1
    opt@lon2 = lon2
    qlev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,70./)
    lev = 0.
    lat = LoadHClat(case(0),opt)
    lon = LoadHClon(case(0),opt)
    num_lat = dimsizes(lat)
    num_lon = dimsizes(lon)
    num_t = num_c*5*4
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do r = 0,num_r-1
  ofile = odir+oname(r)+"/"+oname(r)+"."+var(v)+".nc"
  ;=======================================
  ; Repack Hindcast simulations
  ;=======================================
  time = new((/num_t/),double)
    time = ispan(0,(num_t-1)*6,6)
    time!0 = "time"
    time&time = time
    time@units = "hours since 2011-10-"+sprinti("%0.2i",1+rd1(r))
  V = new((/num_t,num_lat,num_lon/),float)
    V!0 = "time"
    V!1 = "lat"
    V!2 = "lon"
    V&time = time
    V&lat = lat
    V&lon = lon
  ;----------------------------------------------------------------------
  new_dir = odir+oname(r)+"/"
  if .not.isfilepresent(new_dir) then
    system("mkdir "+new_dir)
  end if
  ;-------------------------------------
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  ;-------------------------------------
  outfile = addfile(ofile,"c")
  outfile->$var(v)$ = V
  outfile->lat = lat
  outfile->lon = lon
  ;-------------------------------------
do c = 0,num_c-1
    print("c: "+c+" "+yrmndy(c))
    ;=======================================
    ; Load simulation data
    ;=======================================
        print(" var = "+var(v))
        if var(v).eq."PRECT" then
            PRECC = LoadHC(case(c),"PRECC",lev,opt)
            PRECL = LoadHC(case(c),"PRECL",lev,opt)
            X = PRECC+PRECL
        end if

        if var(v).eq."CWV" then
            Q  = LoadHC4D(case(c),"Q",qlev,opt)
            Ps = LoadHC(case(c),"PS",lev,opt)
            P  = conform(Q,qlev,1)*100.
            copy_VarCoords(Q,P)
            dP = calc_dP(P,Ps)
            X  = dim_sum_n(Q*dP/g,1)
            delete([/Q,dP,P,Ps/])
        end if

        if .not.isvar("X") then
            X = LoadHC(case(c),var(v),lev,opt)
        end if

        outfile->$var(v)$(c*5*4:(c+1)*5*4-1,:,:) = (/ X(rd1(r)*4:(rd1(r)+5)*4-1,:,:) /)
        delete(X)
    ;=======================================
    ;=======================================
end do
;===================================================================================
;===================================================================================

    print("")
    print("  "+ofile)
    print("")
end do
end do
;===================================================================================
;===================================================================================
end
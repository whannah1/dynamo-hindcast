load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin	
	
	member = (/"ERAi","90","09"/)
	case_name = (/"ERAi","SP-CAM","ZM_1.0","ZM_0.2"/)
	mkrs = (/16,12,4,6,15/)

	;member = (/"ERAi","12","09","13"/)
	;case_name = (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
	;mkrs = (/16,4,6,15/)
	
	;member    = (/"ERAi","12"/)
	;case_name = (/"ERAi","ZM_2.0"/)
	;mkrs = (/16,4,6,15/)

	fig_type = "png"
	
	lstart = dimsizes(member)-2				; # of obs datasets (i.e. "line start")	
	
	sa = "nsa"
	
	;rd1 = (/00/)
	rd1 = (/00,05/)
	clrs = (/"orangered","deepskyblue"/)

	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  fig_file = odir+"GMS.scatter.bin.v1"

	nsmooth = 3
	
	anom = False
	
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  
  top_lev = 150.
  
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
    bin_min = -950
    bin_max =  150
    bin_spc =  100
    xbin = ispan(toint(bin_min),toint(bin_max),toint(bin_spc))
  	num_bin = dimsizes(xbin)
  	bdim = (/num_m,4,num_bin/)
  	bin_val = new(bdim,float)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)
  	bin_tct = new(bdim,float)
  
  tval = 1.960		; 95% confidence (2 tail) 
  ;dof  = bin_cnt 
  ;dof  = 75.
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  	res = True
  	res@gsnDraw 				= False
  	res@gsnFrame 			= False
  	res@tmXTOn				= False
  	res@tmYROn 				= False
  	res@tmXBMinorOn			= False
  	res@tmYLMinorOn			= False
  	res@gsnLeftString 		= ""
  	res@gsnLeftStringFontHeightF	= 0.02
;===================================================================================
;===================================================================================
	X    = new((/num_r,num_m/),float)
	xdim = (/num_r,num_m,4/)
	Y    = new(xdim,float)
	Yup  = new(xdim,float)
    Ydn  = new(xdim,float)
do r = 0,num_r-1
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1(r))+"-"+sprinti("%0.2i",rd1(r)+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  infile = addfile(idir+"data/"+oname+"/"+oname+".budget.MSE.nc","R")
  LHFLX  = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX  = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR  = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  VdelH  = (infile->VdelH(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  infile = addfile(idir+"data/"+oname+"/"+oname+".PS.nc","R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".DSE.nc","R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    MSEvi = conform(H,dim_sum_n(H*dP/g,1),(/0,2,3/))
    COLDP = conform(H,dim_sum_n(  dP/g,1),(/0,2,3/))
    MSEvi = (/MSEvi/COLDP/cpd/)
    do i = 0,nsmooth-1 
  	  MSEvi = smooth(MSEvi) 
  	end do
    ;MSEvi = (/MSEvi - conform(MSEvi,dim_avg_n(MSEvi,0),(/1,2,3/))/)
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    WdHdp = dim_sum_n(W*dHdp*dP/g,1)
    SRC   = COLQR + SHFLX + LHFLX
    iSRC  = SRC
    SRC   = (/ run_wgt_mean_2D( SRC ,lat,lon) /)
    W     = (/ run_wgt_mean_2D_alt(W   ,lat,lon) /)
    dHdp  = (/ run_wgt_mean_2D_alt(dHdp,lat,lon) /)
    ;---------------------------------------------------
    ;---------------------------------------------------
  	do v = 0,3
  	  if v.eq.0 then iVy  = run_wgt_mean_2D( -1.*VdelH       ,lat,lon) end if
  	  if v.eq.1 then iVy  = run_wgt_mean_2D( -1.*WdHdp       ,lat,lon) end if
  	  if v.eq.2 then iVy  = run_wgt_mean_2D( -1.*WdHdp+VdelH ,lat,lon) end if
  	  if v.eq.3 then iVy  = run_wgt_mean_2D( -1.*WdHdp+iSRC   ,lat,lon) end if
  	  if v.eq.0 then  Vy  = -1.*VdelH 		end if
  	  if v.eq.1 then  Vy  = -1.*W*dHdp 		end if
  	  if v.eq.2 then  Vy  = -1.*W*dHdp  		end if
  	  if v.eq.3 then  Vy  = -1.*W*dHdp   	end if
  	  nd = dimsizes(dimsizes(Vy))
  	  Vx  = run_wgt_mean_2D( -1.*WdSdp     ,lat,lon)
      ; Old Method (much cleaner, but inconsistent with other figures)
      ;do b = 0,num_bin-1
      ;  bin_bot = bin_min + bin_spc*(b  )
      ;  bin_top = bin_min + bin_spc*(b+1)
      ;  condition = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      ;  tmp = where(condition,Vy,Vy@_FillValue)
      ;  if all(ismissing(tmp)) then
      ;    print("!!! ALL TMP VALUES ARE MISSING !!!    bin_bot: "+bin_bot+"  bin_top: "+bin_top)
      ;  else
      ;    bin_val(m,v,b) =    avg(tmp)
      ;    bin_std(m,v,b) = stddev(tmp)
      ;    bin_cnt(m,v,b) = num(condition)
      ;    bin_tct(m,v,b) = max(dim_num_n(condition,0))
      ;  end if
      ;  delete([/tmp,condition/])
      ;end do
      do b = 0,num_bin-1
        bin_bot = bin_min + bin_spc*(b  )
        bin_top = bin_min + bin_spc*(b+1)
        cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
        tmp2  = where(cond2,iVy,iVy@_FillValue)
        if nd.eq.4 then
         cond3 = conform(Vy,cond2,(/0,2,3/))
         tmp3  = where(cond3, Vy, Vy@_FillValue)
        end if
        tSRC  = where(cond2,SRC,SRC@_FillValue)
        if v.ne.3 then tSRC = 0. end if
        if all(ismissing(tmp2)) then
          print("!!! ALL TMP VALUES ARE MISSING !!!    bin_bot: "+bin_bot+"  bin_top: "+bin_top)
        else
          if nd.eq.4 then
           atmp = dim_avg_n(dim_avg_n(tmp3,(/2,3/)),0)
           bin_val(m,v,b) = sum( dP1/g*atmp ) + avg(tSRC)
          else
           atmp = avg(tmp2)
           bin_val(m,v,b) = atmp
          end if
          bin_std(m,v,b) = stddev(tmp2)
          bin_cnt(m,v,b) = num(cond2)
          ;bin_tct(m,v,b) = avg(dim_num_n(cond2,0))/4. * avg(dim_num_n(cond2,(/1,2/)))
       
          ; Estimate Degrees of Freedom
          nt = dimsizes(cond2(:,0,0))
          tct = new(nt,float)
          tct = 0
          tfac = 4
          sfac = 5
          do t = 0,nt/tfac-1
          do y = 0,dimsizes(cond2(0,:,0))/sfac-1
          do x = 0,dimsizes(cond2(0,0,:))/sfac-1
            tt = (/ t*tfac , t*tfac+tfac-1 /)
            yy = (/ y*sfac , y*sfac+sfac-1 /)
            xx = (/ x*sfac , x*sfac+sfac-1 /)
            if num(cond2(tt(0):tt(1),yy(0):yy(1),xx(0):xx(1))).ge.1 then
              tct(t) = tct(t) + 1
            end if
          end do
          end do
          end do
          bin_tct(m,v,b) = sum(tct)
          delete([/tct/])
        
        end if
        delete([/tmp2,cond2,atmp,tSRC/])
        if nd.eq.4 then delete([/tmp3,cond3/]) end if
      end do
      delete([/Vx,Vy,iVy/])
      bin_cnt(m,v,:) = bin_cnt(m,v,:)/sum(bin_cnt(m,v,:)) *100.
    end do
    print("		bin: "+xbin+"	"+bin_tct(m,0,:))
    print("")
	;----------------------------------------------------------------------
	;----------------------------------------------------------------------
	dof = bin_tct
	dof = where(dof.gt.1,dof,dof@_FillValue)
	X(r,m)   = stddev(MSEvi)
	do v = 0,3
	  Y(r,m,v) = sum( ( bin_cnt(m,v,:) * bin_val(m,v,:)/xbin ) /sum(bin_cnt(m,v,:)) )
	  ;Yup(r,m,v) = Y(r,m,v) + avg( tval*bin_std(m,v,:)/sqrt(dof(m,v,:)-1.) /xbin )
      ;Ydn(r,m,v) = Y(r,m,v) - avg( tval*bin_std(m,v,:)/sqrt(dof(m,v,:)-1.) /xbin )
      Yup(r,m,v) = Y(r,m,v) + sum( ( bin_cnt(m,v,:) * (tval*bin_std(m,v,:)/sqrt(dof(m,v,:)-1.)) /xbin ) /sum(bin_cnt(m,v,:)) )
      Ydn(r,m,v) = Y(r,m,v) - sum( ( bin_cnt(m,v,:) * (tval*bin_std(m,v,:)/sqrt(dof(m,v,:)-1.)) /xbin ) /sum(bin_cnt(m,v,:)) )
	end do
	
;print(Ydn+"	"+Y+"	"+Yup+"	"+dof)

    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
      delete([/H,S,W,dP,dP1,WdHdp,WdSdp,dHdp,dSdp,MSEvi,COLDP,lat,lon/])
      delete([/VdelH,SRC,iSRC,COLQR,SHFLX,LHFLX/])
end do
end do	
;===================================================================================
;===================================================================================
	Xs  = "MSE (K)"
	Y1s = "HGMS"
	Y2s = "VGMS"
	Y3s = "Total GMS"
	Y4s = "Eff. VGMS"

  	res@xyLineColor = "white"
  	res@xyDashPattern = 0
	
	res1 = res
	res2 = res
	res3 = res
	res4 = res

		res1@gsnLeftString	= "HGMS"				;Y1s
		res2@gsnLeftString 	= "VGMS"				;Y2s
		res3@gsnLeftString 	= "Total GMS"	;Y3s
		res4@gsnLeftString 	= "Effective VGMS"	;Y4s
		
		sigma = "~F8~s~F21~"
		opstr = ""
		
		res1@tiXAxisString 	= sigma+Xs
		res2@tiXAxisString 	= sigma+Xs
		res3@tiXAxisString 	= sigma+Xs
		res4@tiXAxisString 	= sigma+Xs
		res1@tiYAxisString 	= opstr+Y1s
		res2@tiYAxisString 	= opstr+Y2s
		res3@tiYAxisString 	= opstr+Y3s
		res4@tiYAxisString 	= opstr+Y4s
		
		resconf = res
 		resconf@xyMarkLineMode 		= "Markers"
 		resconf@xyMarker 			= 2
 		;resconf@xyMonoMarkerColor 	= True
 		resconf@xyMarkerSizeF		= 0.01
 		resconf@xyMarkerThicknessF	= 2.
 		resconfup = resconf
 		resconfdn = resconf
 		resconfup@xyMarker			= 7
 		resconfdn@xyMarker			= 8
 		resconfup@xyMarkerColors 	= clrs
 		resconfdn@xyMarkerColors		= clrs
;===================================================================================
; Plot GMS
;===================================================================================
    ; Create blank plots
    tX = (/min(X),min(X),max(X),max(X)/)
	  plot(0) = gsn_csm_xy(wks,tX,(/min(Ydn(:,:,0)),max(Ydn(:,:,0)),min(Yup(:,:,0)),max(Yup(:,:,0))/),res1)
  	plot(1) = gsn_csm_xy(wks,tX,(/min(Ydn(:,:,1)),max(Ydn(:,:,1)),min(Yup(:,:,1)),max(Yup(:,:,1))/),res2)
  	plot(2) = gsn_csm_xy(wks,tX,(/min(Ydn(:,:,2)),max(Ydn(:,:,2)),min(Yup(:,:,2)),max(Yup(:,:,2))/),res3)
  	plot(3) = gsn_csm_xy(wks,tX,(/min(Ydn(:,:,3)),max(Ydn(:,:,3)),min(Yup(:,:,3)),max(Yup(:,:,3))/),res4)
  ;---------------------------------------------------------------------
  ; Overlay lines for each lead time
  ;---------------------------------------------------------------------
  	  res@xyLineColors = clrs
  	  res@xyLineThicknessF	= 2.
  	do v = 0,3
  	  overlay(plot(v),gsn_csm_xy(wks,X(:,lstart:),Y(:,lstart:,v),res))
  	end do
  ;---------------------------------------------------------------------
  ; Add error bars
  ;---------------------------------------------------------------------
  		eres = res
  		eres@xyDashPattern				= 0
  		eres@xyLineThicknessF			= 1.
  		eres@xyMonoLineColor				= True
  	do v = 0,3
  	  overlay(plot(v) , gsn_csm_xy(wks,X,Yup(:,:,v),resconfup))
      overlay(plot(v) , gsn_csm_xy(wks,X,Ydn(:,:,v),resconfdn))
      do m = 0,num_m-1
      do r = 0,num_r-1
        eres@xyLineColor	 = clrs(r)
        overlay(plot(v) , gsn_csm_xy(wks,(/X(r,m),X(r,m)/),(/Ydn(r,m,v),Yup(r,m,v)/),eres))
      end do
  	  end do
  	end do
  ;---------------------------------------------------------------------
  ; add zero line
  ;---------------------------------------------------------------------
  	lx = (/-1e8,1e8/)
  	ly = (/0.,0./)
  		lres = res
  		lres@xyMonoLineColor		= True
  		lres@xyLineColor 		= "grey"
  		lres@xyDashPattern 		= 1
  		lres@xyLineThicknessF	= 1
  	do v = 0,3
  	  overlay(plot(v),gsn_csm_xy(wks,lx,ly,lres))
  	end do
 ;---------------------------------------------------------------------
 ; add markers
 ;---------------------------------------------------------------------
  	dum = new((/4,num_r,num_m/),graphic)
  	
  	delete([/res1,res2,res3,res4/])
  	
  	gres = True
  	gres@gsMarkerThicknessF 	= 2.0
  	gres@gsMarkerSizeF 		= 0.02
  	do m = 0,num_m-1
 	do r = 0,num_r-1
 		gres@gsMarkerIndex  	= mkrs(m)
 		gres@gsMarkerColor 	= clrs(r)
 	  do v = 0,3
 	    dum(v,r,m) = gsn_add_polymarker(wks,plot(v),X(r,m),Y(r,m,v),gres)
 	  end do
  	end do
  	end do
  ;---------------------------------------------------------------------
  ; combine panel plots
  ;---------------------------------------------------------------------
  		pres = True
  		pres@gsnFrame       	= False
  		;pres@gsnPanelBottom 	= 0.18 
    		;pres@txString = lat1+":"+lat2+"N / "+lon1+":"+lon2+"E"
    		pres@amJust   = "TopLeft"
    		pres@gsnPanelFigureStrings= (/"a)","b)","c)","c)","e)","f)","g)","h)"/) 
    		pres@gsnPanelXWhiteSpacePercent = 2
    		pres@gsnPanelYWhiteSpacePercent = 5

	plot(2) = plot@_FillValue

    ;gsn_panel(wks,plot(:1),(/1,2/),pres)
    gsn_panel(wks,plot,(/2,2/),pres)
  ;---------------------------------------------------------------------
  ; Add legends
  ;---------------------------------------------------------------------
  legend1 = create "Legend" legendClass wks 
    "vpXF"                      	: 0.13
    "vpYF"                      	: 0.45
    "vpWidthF"                  	: 0.2   
    "vpHeightF"                 	: 0.15   
    "lgPerimOn"                 	: True   
    "lgItemCount"               	: num_m
    "lgLabelStrings"            	: case_name
    "lgItemType"					: "Markers"
    "lgLabelsOn"                	: True     
    "lgMarkerIndexes"		    	: mkrs   
    "lgMarkerSizeF"				: 0.02
    "lgLabelFontHeightF"        	: 0.015    
    "lgMarkerColor"				: "black"
    "lgMonoLineLabelFontColor"  	: True                  
  end create
  draw(legend1)
  
  legend2 = create "Legend" legendClass wks 
    "vpXF"                      	: 0.13
    "vpYF"                      	: 0.25
    "vpWidthF"                  	: 0.3   
    "vpHeightF"                 	: 0.07
    "lgPerimOn"                 	: True   
    "lgItemCount"               	: num_r
    "lgLabelStrings"            	: "  "+(/"00-04","05-09","10-14"/)+" day lead"
    "lgItemType"					: "Lines"
    "lgLineLabelsOn"				: False
    "lgLineColors"				: clrs
    "lgLineThicknessF"			: 30.
    "lgBoxMinorExtentF"			: 0.05
    "lgMonoDashIndex"			: True
    "lgDashIndex"				: 0
    "lgLabelsOn"                	: True     
    "lgLabelFontHeightF"        	: 0.015    
    "lgMonoLineLabelFontColor"  	: True                  
  end create
  draw(legend2)
  ;---------------------------------------------------------------------
  ; Finalize plot
  ;---------------------------------------------------------------------
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end


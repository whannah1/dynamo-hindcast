; This script plots a series of bar charts to display
; the contributions of the numerator and denominator 
; to the change in VGMS between two datasets
; using the identity dG = dN/D - dD*N*D^-2
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin	
	member    = (/"12","13"/)
	case_name = (/"ZM_0.2","ZM_2.0"/)
	
	member = member(::-1)
	case_name = case_name(::-1)

	fig_type = "x11"
	
	rd1 = (/00/)
	
	;clrs = (/"black","red","blue","green"/)
	clrs = (/1,13,14,15/)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  fig_file = odir+"GMS.components.v2.alt"
	
	top_lev = 200.
;===================================================================================
;===================================================================================
	num_p = 3
	num_b = (/4,4,4,4/)					; # bars per plot
	num_m = dimsizes(member)
	xdim = (/num_p,max(num_b),num_m/)
	X = new(xdim,float)
	Y = new(xdim,float)
	gsz = 0.4
	bsz = (gsz*2.)/(num_b)  ;*1.5
	
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"default")
  plot = new((/4,max(num_b)/),graphic)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame 		= False
  	res@tmXTOn			= False
  	res@tmYROn 			= True
  	res@gsnXYBarChart	= True
  	res@gsnScale 		= True
  	res@gsnYRefLine		= 0.
  	res@gsnLeftStringFontHeightF	= 0.02
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ;ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  ;infile = addfile(ifile,"R")
  ;LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  ;SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  ;COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".PS.nc"
  infile = addfile(ifile,"R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".MSE.nc"
  infile = addfile(ifile,"R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".DSE.nc"
  infile = addfile(ifile,"R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".OMEGA.nc"
  infile = addfile(ifile,"R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  Wa = conform(W,dim_avg_n(dim_avg_n(W,(/2,3/)),0),1)
  Ha = conform(H,dim_avg_n(dim_avg_n(H,(/2,3/)),0),1)
  Wp = W - Wa
  Hp = H - Ha
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  MSEvi = infile->MSEvi(:,{lat1:lat2},{lon1:lon2})
  MSEvi = dim_rmvmean_n(MSEvi,0)
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  delete([/lat,lon/])
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP    = calc_dp(P,Ps)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    dHpdp = new(dimsizes(H),float)
    dHadp = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHpdp(:,1:num_lev-2,:,:) = (Hp(:,0:num_lev-3,:,:) - Hp(:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHadp(:,1:num_lev-2,:,:) = (Ha(:,0:num_lev-3,:,:) - Ha(:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)    
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ;===================================================================================
    WdSdp = new(dimsizes(MSEvi),float)
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    
    WdHdp = new(dimsizes(WdSdp),float)
	WpdHp = new(dimsizes(WdSdp),float)
	WadHa = new(dimsizes(WdSdp),float) 
	WdHdp = dim_sum_n(W *dHdp *dP/g,1)
    	WpdHp = dim_sum_n(Wp*dHpdp*dP/g,1) 
    	WadHa = dim_sum_n(Wa*dHadp*dP/g,1)
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    DTH = 10.
    TH  = 0.
    cond0 = (abs(WdSdp).ge.DTH)
    cond1 = (MSEvi.lt.(-TH*stddev(MSEvi))) .and. cond0
    cond2 = (MSEvi.gt.( TH*stddev(MSEvi))) .and. cond0
	
	do p = 0,num_p-1
      X(p,:,m) = ispan(0,num_b(p)-1,1);m+fspan(-gsz+bsz(p)/2.,gsz-bsz(p)/2.,num_b(p))
    end do
	
	D = dim_avg_n(WdSdp,0)
	D0 = avg( where(D.ge.10.,D,D@_FillValue)  )
	
	N0 = dim_avg_n( WdHdp ,0)
	N1 = dim_avg_n( WadHa ,0)
	N2 = dim_avg_n( WpdHp ,0)
	
	if m.eq.0 then
	  D0o = D0
	  N0o = N0
	  N1o = N1
	  N2o = N2
	end if
	
	dN0 = N0-N0o
	dD0 = D0-D0o
	dG0 = N0/D0 - N0o/D0o
	
	dN1 = N1-N1o
	dG1 = N1/D0 - N1o/D0o
	
	dN2 = N2-N2o
	dG2 = N2/D0 - N2o/D0o
	
    Y(0,0,m) = avg(dG0)
    Y(0,1,m) = avg(dN0/D0o - dD0*N0o/(D0o^2))
    Y(0,2,m) = avg(dN0/D0o)
    Y(0,3,m) = avg(-dD0*N0o/(D0o^2))
    
    Y(1,0,m) = avg(dG1)
    Y(1,1,m) = avg(dN1/D0o - dD0*N1o/(D0o^2))
    Y(1,2,m) = avg(dN1/D0o)
    Y(1,3,m) = avg(-dD0*N1o/(D0o^2))
    
    Y(2,0,m) = avg(dG2)
    Y(2,1,m) = avg(dN2/D0o - dD0*N2o/(D0o^2))
    Y(2,2,m) = avg(dN2/D0o)
    Y(2,3,m) = avg(-dD0*N2o/(D0o^2))
    
    delete([/D,D0,N0,N1,N2,dN0,dN1,dN2,dD0,dG1,dG2/])
    
    fmt = "%6.2f"
    print(case_name(m)+"		"+sprintf(fmt,Y(0,0,m))+"		"+sprintf(fmt,Y(0,1,m))+"		"+sprintf(fmt,Y(0,2,m))+"		"+sprintf(fmt,Y(0,3,m)))

	;if all(SRC.eq.0) then lstr = "Anomalous VGMS" else lstr = "Anomalous eff. VGMS" end if
	lstr = new(3,string)
	lstr(0) = "~F134~u~F21~VGMS (Total)"
	lstr(1) = "~F134~u~F21~VGMS from mean fields"
	lstr(2) = "~F134~u~F21~VGMS from eddy flux"

      delete([/H,S,W,Hp,Wp,Wa,Ha,dP/])
      delete([/dHdp,dSdp,dHadp,dHpdp,WdSdp,WdHdp,WpdHp,WadHa/])
      delete([/MSEvi,cond0,cond1,cond2/])
      ;delete([/SRC,COLQR,SHFLX,LHFLX/])
end do
;===================================================================================
; Plot GMS
;===================================================================================
  		res@trXMinF =  -0.5
  		res@trXMaxF =  num_b-0.5
  		res@tmXBMode = "Explicit"
  		res@tmXBValues = X(0,:,0)
  		res@tmXBLabels = (/"dG","dG est.","dN/D","-dD*N/(D^2)"/)
  		
  		;res@gsnLeftString = "Difference from ERAi"
  		
  		res@vpWidthF = 0.4
  		res@trYMinF  = min(Y)-stddev(Y)*0.2
  		res@trYMaxF  = max(Y)+stddev(Y)*0.2
 		
 		lres = res
  		mres = res 
  		res@gsnXYBarChart			= True
  		res@gsnScale 				= True
  		res@gsnYRefLine				= 0.
 		
  do p = 0,num_p-1
  		tres = res
  		tres@gsnLeftString	= lstr(p)
  		;tres@trYMinF = min(Y(p,:,1:))-stddev(Y)*0.2
 		;tres@trYMaxF = max((/ max(Y(p,:,1:))+stddev(Y)*0.2 ,0./))
 		tres@gsnXYBarChartBarWidth 	= gsz;bsz(p)
  do b = 0,num_b(p)-1
  		tres@gsnXYBarChartColors2 = clrs(b)
    plot(p,b) = gsn_csm_xy(wks,X(p,b,1),Y(p,b,1),tres)    
    if b.ge.1 then
      overlay(plot(p,0),plot(p,b))
    end if
  end do
  delete(tres)
  end do
  ;---------------------------------------------------------------------
  ; Add legend
  ;---------------------------------------------------------------------
  	 lbres                    = True          ; labelbar only resources
     lbres@vpWidthF           = 0.1           ; labelbar width
     lbres@vpHeightF          = 0.1           ; labelbar height
     lbres@lbBoxMajorExtentF  = 0.5           ; puts space between color boxes
     lbres@lbMonoFillPattern  = True          ; Solid fill pattern
     lbres@lbLabelFontHeightF = 0.015         ; font height. default is small
     lbres@lbLabelJust        = "TopLeft" 
     lbres@lbPerimOn          = False
  	;lbres@lbBoxMajorExtentF  = 0.75          ; puts space between color boxes
  	lbres@lbFillColors       = clrs
  		
  		
  		labels = (/"Anomalous VGMS (Column MSE < -"+TH+")","Anomalous VGMS (Column MSE > "+TH+")"/)
  	;gsn_labelbar_ndc(wks,dimsizes(labels),labels,0.32,0.345,lbres)
  ;---------------------------------------------------------------------
  ; Panel plots
  ;---------------------------------------------------------------------
    	pres = True 
  	pres@gsnFrame       	= False
  	;pres@gsnPanelBottom 	= 0.1 
    ;pres@amJust   = "TopLeft"
    ;pres@gsnPanelFigureStringsFontHeightF = 0.01
    	;pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 

	;plot(2:3,0) = (/plot@_FillValue,plot(2,0)/)

  gsn_panel(wks,plot(:num_p-1,0),(/1,3/),pres)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end

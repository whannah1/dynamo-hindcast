; similar to v4 but only plots dH/dp
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin	
	
	;member = (/"ERAi","13","09","12"/)
	;case_name = (/"ERAi","ZM_0.2","ZM_1.0","ZM_2.0"/)
	
	member = (/"ERAi","09","90"/)
	case_name = (/"ERAi","ZM_1.0","SP-CAM"/)

	fig_type = "png"
	
	sa = "nsa"
	
	rd1 = (/00/)

	clr = (/"black","green","blue","red" /)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	lat1 = -10.
	lat2 =  10.
	lon1 =  60.
	lon2 =  90.
	fig_file = odir+"GMS.bin.profile.v5."+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
	
	top_lev = 150.
;===================================================================================
;===================================================================================
	num_r = 3
	num_m = dimsizes(member)

  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
	
	nbv = 1

	;bin_min = -950
    ;bin_max =  150
    bin_spc =  100
    bin_min = -950
    bin_max =  150
    xbin = ispan(toint(bin_min),toint(bin_max),toint(bin_spc))
  	num_bin = dimsizes(xbin)
  	bdim = (/num_m,nbv,num_bin/)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)
  	bin_tct = new(bdim,float)
  
    bin_val = new((/num_m,nbv,num_bin,num_lev/),float)
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"BlueRed")
  plot = new((/2/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@tmXTOn						= False
  	res@tmYROn 						= False
  	res@gsnRightString				= ""
  	res@gsnLeftString				= ""
  	res@gsnLeftStringFontHeightF 	= 0.02
  	res@gsnRightStringFontHeightF 	= 0.02
  	res@tiXAxisFontHeightF			= 0.02
  	res@tiYAxisFontHeightF			= 0.02
  	res@tmXBMinorOn					= False
    	res@tmYLMinorOn					= False
  	res@vpHeightF					= 0.4
  	;res@xyLineThicknessF				= 3.
  	res@trYReverse					= True
  	lres = res
  	
  	lres@xyDashPattern				= 0
  	lres@xyLineThicknessF			= 1.
  	lres@xyLineColor					= "black"
;===================================================================================
;===================================================================================
slp = new(num_m,float)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  infile = addfile(idir+"data/"+oname+"/"+oname+".PS.nc","R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".DSE.nc","R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP  = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    WdHdp = dim_sum_n(W*dHdp*dP/g,1)
    
    SRC = COLQR + SHFLX + LHFLX
    delete([/COLQR,SHFLX,LHFLX/])
    SRC = run_wgt_mean_2D( SRC ,lat,lon)
    
    W    = (/ run_wgt_mean_2D_alt(W   ,lat,lon) /)
    dHdp = (/ run_wgt_mean_2D_alt(dHdp,lat,lon) /)
    
    ;---------------------------------------------------
    ; Binning algorithm
    ;---------------------------------------------------
  	do v = 0,nbv-1
    Vx  = run_wgt_mean_2D( -1.*WdSdp ,lat,lon)
    ;if v.eq.0 then Vy = W    end if
  	if v.eq.0 then Vy = dHdp end if
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      cond3 = conform(Vy,cond2,(/0,2,3/))
      tmp   = where(cond3, Vy, Vy@_FillValue)
      if all(cond2.eq.False) then
        print("!!! BIN IS EMPTY !!!  bin_bot: "+bin_bot+"  bin_top: "+bin_top)
      else
        bin_val(m,v,b,:) = dim_avg_n(dim_avg_n(tmp,(/2,3/)),0)
        bin_cnt(m,v,b) = num(cond2)
        bin_tct(m,v,b) = max(dim_num_n(cond2,0))
      end if
      delete([/tmp,cond2,cond3/])
    end do
    delete([/Vx,Vy/])
    bin_cnt(m,v,:) = bin_cnt(m,v,:)/sum(bin_cnt(m,v,:)) *100.
    end do
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
      delete([/H,S,W,dP,WdHdp,WdSdp,SRC/])
      delete([/dHdp,dSdp/])
      delete([/lat,lon/])
end do	
;===================================================================================
; Create plot 
;===================================================================================
		res@cnLinesOn				= False
		res@cnFillOn					= True
    		res@cnLevelSelectionMode		= "ExplicitLevels"
    		;res@cnLevels 		= tofloat(ispan(-30,30,3))*1e-2
    		res@cnLevels			= tofloat(ispan(-30,30,5))/100.
    		
    		xbin@long_name = "-W*dS/dp"
    		xbin@units = "W m~S~-2~N~"
    		
    		bin_val!0 = "case"
    		bin_val!1 = "var"
    		bin_val!2 = "bin"
    		bin_val!3 = "lev"
    		bin_val&bin = xbin
    		bin_val&lev = lev
    		
  olev = ispan(toint(max(lev)),toint(min(lev)),50)
  
  tmp0 = linint1_n_Wrap(lev, bin_val(case|(num_m-1),var|0,lev|:,bin|:) ,False,olev,0,0)
    		
  do m = 0,num_m-2
  		;res@gsnRightString 	= "Omega [m/s]"
  		res@gsnRightString 	= "dH/dp [K/hPa]"
  		res@gsnLeftString 	= case_name(num_m-1)+" - "+case_name(m)
  		res@tiYAxisString	= "Pressure [hPa]"
  		tmp = linint1_n_Wrap(lev, bin_val(case|m,var|0,lev|:,bin|:) ,False,olev,0,0)
  		;tmp = linint1_n     (lev, bin_val(case|3,var|0,lev|:,bin|:)-bin_val(case|m,var|0,lev|:,bin|:) ,False,olev,0,0)
  		tmp = (/ tmp0 - tmp /)
    plot(m) = gsn_csm_contour(wks,tmp,res)
    delete([/tmp/])
  end do
 
  ;x1 = (/-1.,1./)*1e8
  ;x0 = x1*0
  ;do p = 0,nbv-1
  ;  overlay( plot(p) , gsn_csm_xy(wks,x1,x0,lres) )
  ;  overlay( plot(p) , gsn_csm_xy(wks,x0,x1,lres) )
  ;end do
  ;delete([/x0,x1/])
;===================================================================================
; Panel plots
;===================================================================================
  	pres = True 
  	pres@gsnFrame       						= False
  	pres@amJust   							= "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF 	= 0.01
    	pres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)"/) 
    	
  gsn_panel(wks,plot,(/1,2/),pres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"  			   : False
    "vpXF"                     : 0.7
    "vpYF"                     : 0.7
    "vpWidthF"                 : 0.2
    "vpHeightF"                : 0.15
    "lgPerimOn"                : True
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True
    "lgLineLabelsOn"           : False
    "lgLabelFontHeightF"       : 0.025
    "lgDashIndexes"            : (/0,0,0,0/)
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True
  end create
  ;draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


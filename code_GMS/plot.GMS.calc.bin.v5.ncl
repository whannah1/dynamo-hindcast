; Calculates the VGMS using a weighted mean over data that 
; has been binned by the DSE export. The data is initially 
; processed with a spatial running mean
; v5 is similar to v2, but it is made to explain the contribution
; to the change in GMS relative to the control by omega and dHdp
; v5 also uses the method in v3, in which the DSE import distribution 
; from the first case is used to eliminate effects of changes to the
; VGMS denominator 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
;----------------------------------------------
;----------------------------------------------
function run_wgt_mean_2D (q,lat,lon)
local wgt,nwgt,qout,i,j,k,nlat,nlon
begin
	nlat = dimsizes(lat)
	nlon = dimsizes(lon)
	ntim = dimsizes(q(:,0,0))
	qout = new(dimsizes(q),float)
	nwgt = dimsizes(lat({-2.:2.}))
  	wgt  = new((/ntim,nwgt,nwgt/),float)
  	wgt  = 1./(nwgt^2.)
  	do j = nwgt/2,nlat-1-nwgt/2
  	do i = nwgt/2,nlon-1-nwgt/2
  	  qout(:,j,i) = dim_avg_n(q(:,j-nwgt/2:j+nwgt/2,i-nwgt/2:i+nwgt/2),(/1,2/))
  	end do
  	end do
  	opt = 0
  	;qout = runave_n(qout,4,opt,0)
  	return qout
end
;----------------------------------------------
;----------------------------------------------
function run_wgt_mean_2D_alt (q,lat,lon)
local wgt,nwgt,qout,i,j,k,nlat,nlon
begin
	nlat = dimsizes(lat)
	nlon = dimsizes(lon)
	ntim = dimsizes(q(:,0,0,0))
	nlev = dimsizes(q(0,:,0,0))
	qout = new(dimsizes(q),float)
	nwgt = dimsizes(lat({-2.:2.}))
  	wgt  = new((/ntim,nlev,nwgt,nwgt/),float)
  	wgt  = 1./(nwgt^2.)
  	do j = nwgt/2,nlat-1-nwgt/2
  	do i = nwgt/2,nlon-1-nwgt/2
  	  qout(:,:,j,i) = dim_avg_n(q(:,:,j-nwgt/2:j+nwgt/2,i-nwgt/2:i+nwgt/2),(/2,3/))
  	end do
  	end do
  	opt = 0
  	;qout = runave_n(qout,4,opt,0)
  	return qout
end
;----------------------------------------------
;----------------------------------------------
begin	
	
	;member = (/"13","12"/)
	;case_name = (/"ZM_0.2","ZM_2.0"/)
	
	member = (/"ERAi","12"/)
	case_name = (/"ERAi","ZM_2.0"/)

	fig_type = "png"
	
	sa = "nsa"
	
	rd1 = (/00/)

	;clr = (/"black","green","blue","red" /)
	clr = (/"green","red" /)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  fig_file = odir+"GMS.calc.bin.v5."+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
	
	top_lev = 150.

;===================================================================================
;===================================================================================
	num_r = 3
	num_m = dimsizes(member)

	bin_min = -950
    bin_max =  150
    bin_spc =  100
    xbin = ispan(toint(bin_min),toint(bin_max),toint(bin_spc))
  	num_bin = dimsizes(xbin)
  	nbv = 4
  	bdim = (/num_m,nbv,num_bin/)
  	bin_val = new(bdim,float)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)

  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"default")
  plot = new((/4/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@tmXTOn						= False
  	res@tmYROn 						= False
  	res@gsnRightString				= ""
  	res@gsnLeftString				= ""
  	res@gsnLeftStringFontHeightF 	= 0.02
  	res@gsnRightStringFontHeightF 	= 0.02
  	res@tiXAxisFontHeightF			= 0.02
  	res@tiYAxisFontHeightF			= 0.02
  	;res@vpHeightF					= 0.4
  	res@xyLineThicknessF				= 3.
  	lres = res
  	
  	lres@xyDashPattern				= 0
  	lres@xyLineThicknessF			= 1.
  	lres@xyLineColor					= "black"
;===================================================================================
;===================================================================================
slp = new(num_m,float)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  infile = addfile(idir+"data/"+oname+"/"+oname+".PS.nc","R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".DSE.nc","R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP  = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    MSEvi = conform(H,dim_sum_n(H*dP/g,1),(/0,2,3/))
    MSEvi = (/MSEvi - conform(MSEvi,dim_avg_n(MSEvi,0),(/1,2,3/))/)
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    WdHdp = dim_sum_n(W*dHdp*dP/g,1)
    
    SRC = COLQR + SHFLX + LHFLX
    SRC = run_wgt_mean_2D( SRC ,lat,lon)
    
    Vy1 = run_wgt_mean_2D_alt(W   ,lat,lon) 
    Vy2 = run_wgt_mean_2D_alt(dHdp,lat,lon) 
    
    ;Vy1 = W   
    ;Vy2 = dHdp
    
    Vx  = run_wgt_mean_2D( -1.*WdSdp     ,lat,lon)
    ;Vx  = -1.*WdSdp 
    ;---------------------------------------------------
    ; Binning algorithm
    ;---------------------------------------------------
    if m.eq.0 then vtmp01 = new((/nbv,num_bin,num_lev/),float) end if
    if m.eq.0 then vtmp02 = new((/nbv,num_bin,num_lev/),float) end if
  	do v = 0,nbv-1
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      cond3  = conform(Vy1,cond2,(/0,2,3/))
      tmp1 = where(cond3,Vy1,Vy1@_FillValue)
      tmp2 = where(cond3,Vy2,Vy2@_FillValue)
      tSRC = where(cond2,SRC,SRC@_FillValue)
      if all(ismissing(tmp1)) then
        print("!!! ALL TMP VALUES ARE MISSING !!!  bin_bot: "+bin_bot+"  bin_top: "+bin_top)
      else
        vtmp1 = dim_avg_n(dim_avg_n(tmp1,(/2,3/)),0)
        vtmp2 = dim_avg_n(dim_avg_n(tmp2,(/2,3/)),0)
        if m.eq.0 then vtmp01(v,b,:) = vtmp1  end if
        if m.eq.0 then vtmp02(v,b,:) = vtmp2  end if

        if v.eq.0 then bin_val(m,v,b) = -1.*sum( dP1/g*  vtmp1               * vtmp2                )    end if
        if v.eq.1 then bin_val(m,v,b) = -1.*sum( dP1/g*  vtmp01(v,b,:)       *(vtmp2-vtmp02(v,b,:)) )    end if
        if v.eq.2 then bin_val(m,v,b) = -1.*sum( dP1/g* (vtmp1-vtmp01(v,b,:))* vtmp02(v,b,:)        )    end if
        if v.eq.3 then bin_val(m,v,b) = -1.*sum( dP1/g* (vtmp1-vtmp01(v,b,:))*(vtmp2-vtmp02(v,b,:)) )    end if
        
        bin_cnt(m,v,b) = num(cond2)
        delete([/vtmp1,vtmp2/])  
      end if
      delete([/tmp1,tmp2,tSRC,cond2,cond3/])
    end do
    end do
    delete([/Vx,Vy1,Vy2/])
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
      delete([/H,S,W,dP,WdHdp,WdSdp/])
      delete([/SRC,COLQR,SHFLX,LHFLX/])
      delete([/dHdp,dSdp,MSEvi/])
      delete([/lat,lon/])
end do	

	do m = 0,num_m-1
	do v = 0,nbv-1
	  bin_cnt(m,v,:) = bin_cnt(m,v,:)/sum(bin_cnt(m,v,:)) *100.
	  bin_cnt(m,v,:) = bin_cnt(0,v,:)
	end do
	end do
	
;===================================================================================
; Create plot 
;===================================================================================    
    ;GMS = new((/num_m,2/),float)
    ;do m = 0,num_m-1
    ;  GMS(m,0) = sum( ( bin_cnt(m,0,:) * bin_val(m,0,:)/xbin ) /sum(bin_cnt(m,0,:)) )
    ;  GMS(m,1) = sum( ( bin_cnt(m,1,:) * bin_val(m,1,:)/xbin ) /sum(bin_cnt(m,1,:)) )
    ;end do
    ;print(GMS)
    
    ;GMS = sum( ( bin_cnt(0,0,:) * (bin_val(1,0,:)-bin_val(0,0,:))/xbin ) /sum(bin_cnt(0,0,:)) )
    GMS0 = sum( ( bin_cnt(0,0,:) * bin_val(0,0,:)/xbin ) /sum(bin_cnt(0,0,:)) )
    GMS1 = sum( ( bin_cnt(1,0,:) * bin_val(1,0,:)/xbin ) /sum(bin_cnt(1,0,:)) )
    GMS  = GMS1 - GMS0 
    

      
    dGMS = new((/3/),float)  
    m = 1
    dGMS(0) = sum( ( bin_cnt(m,1,:) * bin_val(m,1,:)/xbin ) /sum(bin_cnt(m,1,:)) )
    dGMS(1) = sum( ( bin_cnt(m,2,:) * bin_val(m,2,:)/xbin ) /sum(bin_cnt(m,2,:)) )
    dGMS(2) = sum( ( bin_cnt(m,3,:) * bin_val(m,3,:)/xbin ) /sum(bin_cnt(m,3,:)) )
    
    RESIDUAL = GMS - sum(dGMS)
    ;RESIDUAL = sum( ( bin_cnt(0,0,:) * ( bin_val(0,0,:)-bin_val(1,0,:)+dim_sum_n(bin_val(1,1:,:),0) )/xbin ) /sum(bin_cnt(0,0,:)) )
    print("")
    print("GMS       		: "+GMS)
    print("sum(dGMS) 		: "+sum(dGMS))
    print("sum(dGMS(0,2)) 	: "+sum(dGMS((/0,2/))))
    print("residual  		: "+RESIDUAL)
    print("")
    		
    		res4 = res
    		
    		;res@xyLineColors 		= clr
    		res@xyMonoDashPattern	= True
    		res@xyDashPattern 		= 0
    		res@xyMarkLineMode		= "MarkLines"
    		res@xyMarker				= 16
    		res@xyMarkerSizeF		= 0.005
    		res@xyMarkerColor		= "black"
    		res@tmXBMinorOn			= False
    		res@tmYLMinorOn			= False
    		
    		res1 = res
    		res1@gsnLeftString			= "-W*dH/dp change from "+case_name(0)+" to "+case_name(1)
    		res1@gsnRightString 			= ""
    		res1@tiXAxisString			= "-W*dS/dp  [W m~S~-2~N~]"
    		res1@tiYAxisString			= "[W m~S~-2~N~]"
    		res1@trXMaxF					= max(xbin)
    		res1@trXMinF					= min(xbin)
    		;res1@xyMonoLineColor			= True
    		res1@xyLineColor 			= "black"
    		
    		;res1@trYMaxF		=  200.
    		;res1@trYMinF		= -200.
    		
    		res1a = res1
    		res1b = res1
    		
    		res1a@xyLineColors 		= (/"red","green","blue"/)
    		res1a@xyMarkerColors		= res1a@xyLineColors
    		res1a@xyDashPatterns		= (/1,2,3/)
    		;res1a@xyMonoMarker		= False
    		;res1a@xyMarkers			= (/4,5,6/)
    		
    		res1b@xyLineColor 		= "black"
    		
  plot(0) = gsn_csm_xy(wks,xbin,(bin_val(1,0,:)-bin_val(0,0,:))/xbin,res1)
          
          do v = 1,nbv-1
            bin_val(1,v,:) = (/ bin_val(1,v,:) / xbin /)
          end do
          
  overlay(plot(0) , gsn_csm_xy(wks,xbin,bin_val(1,1:,:),res1a))
  overlay(plot(0) , gsn_csm_xy(wks,xbin,dim_sum_n(bin_val(1,1:,:),0),res1b))
    		
    		num_r = 1
		gsz = 0.2
		bsz = (gsz*2.)/(num_r+1)  *0.6
    		;barx = ispan(1,num_m,1) 
    		barx = 1
    		res4@vpHieghtF				= 0.4
    		res4@gsnLeftString			= "VGMS change from "+case_name(0)+" to "+case_name(1)
    		res4@gsnRightString 			= ""
    		res4@gsnXYBarChart			= True
  		res4@gsnScale 				= True
  		res4@gsnYRefLine				= 0.
 		res4@gsnXYBarChartBarWidth 	= bsz
    		res4@tmXBMode 				= "Explicit"
  		res4@tmXBValues 				= 1.+(/-3.,-1.,1.,3./)*bsz/2.
  		d = "~F3~D~F~"
  		res4@tmXBLabels 				= (/"dGMS","w*"+d+"h",""+d+"w*h",""+d+"w*"+d+"h"/)
  		res4@trYMinF 				= -0.15
 		res4@trYMaxF 				=  0.03
 		res4@trXMinF					= min(barx)-bsz*2.5
 		res4@trXMaxF					= max(barx)+bsz*2.5
  		res4@gsnXYBarChart			= True
  		res4@gsnScale 				= True
  		res4@gsnYRefLine				= 0.
 		res4@gsnXYBarChartBarWidth 	= bsz*0.6
    

      
      res4@gsnXYBarChartColors2 	= "black"
  plot(1) = gsn_csm_xy(wks,barx-3.*bsz/2.,GMS,res4)
      
      res4a = res4
      res4b = res4
      res4c = res4
      res4a@gsnXYBarChartColors2  	= "red"
      res4b@gsnXYBarChartColors2  	= "green"
      res4c@gsnXYBarChartColors2  	= "blue"
      
      tplot1 = gsn_csm_xy(wks,barx-1.*bsz/2.,dGMS(0),res4a)
      tplot2 = gsn_csm_xy(wks,barx+1.*bsz/2.,dGMS(1),res4b)
      tplot3 = gsn_csm_xy(wks,barx+3.*bsz/2.,dGMS(2),res4c)
      
      overlay(plot(1) , tplot1 )
      overlay(plot(1) , tplot2 )
      overlay(plot(1) , tplot3 )
      

    
    x1 = (/-1.,1./)*1e8
    x0 = x1*0
    overlay( plot(0) , gsn_csm_xy(wks,x1,x0,lres) )
    overlay( plot(0) , gsn_csm_xy(wks,x0,x1,lres) )
    delete([/x0,x1/])
;===================================================================================
; Panel plots
;===================================================================================
  	pres = True 
  	pres@gsnFrame       						= False
  	;pres@amJust   							= "TopLeft"
    ;pres@gsnPanelFigureStringsFontHeightF 	= 0.01
    	;pres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)"/) 
    	
  gsn_panel(wks,plot(0:1),(/2,1/),pres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"  			   : False
    "vpXF"                     : 0.25
    "vpYF"                     : 0.68
    "vpWidthF"                 : 0.15
    "vpHeightF"                : 0.1
    "lgPerimOn"                : True
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True
    "lgLineLabelsOn"           : False
    "lgLabelFontHeightF"       : 0.012
    "lgDashIndexes"            : (/0,0,0,0,0,0,0/)
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True
  end create

  ;draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;===================================================================================
;===================================================================================
end


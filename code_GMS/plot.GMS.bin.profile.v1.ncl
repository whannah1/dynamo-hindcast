; Calculates the average
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin	
	
	member = (/"ERAi","13","09","12"/)
	case_name = (/"ERAi","ZM_0.2","ZM_1.0","ZM_2.0"/)

	fig_type = "png"
	
	sa = "nsa"
	
	rd1 = (/00/)

	clr = (/"black","green","blue","red" /)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	lat1 = -10.
	lat2 =  10.
	lon1 =  60.
	lon2 =  90.
	fig_file = odir+"GMS.bin.profile.v1a."+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
	
	top_lev = 150.
;===================================================================================
;===================================================================================
	num_r = 3
	num_m = dimsizes(member)

  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
	
	nbv = 4

	;bin_min = -950
    ;bin_max =  150
    bin_spc =  100
    bin_min1 = -250
    bin_max1 =  -50
    bin_min2 =   50
    bin_max2 =  250
    xbin = ispan(toint(bin_min1),toint(bin_max1),toint(bin_spc))
  	num_bin = dimsizes(xbin)
  	bdim = (/num_m,nbv,num_bin/)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)
  	bin_tct = new(bdim,float)
  
    bin_val = new((/num_m,nbv,num_bin,num_lev/),float)
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new((/nbv/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@tmXTOn						= False
  	res@tmYROn 						= False
  	res@gsnRightString				= ""
  	res@gsnLeftString				= ""
  	res@gsnLeftStringFontHeightF 	= 0.02
  	res@gsnRightStringFontHeightF 	= 0.02
  	res@tiXAxisFontHeightF			= 0.02
  	res@tiYAxisFontHeightF			= 0.02
  	res@tmXBMinorOn					= False
    	res@tmYLMinorOn					= False
  	;res@vpHeightF					= 0.4
  	res@xyLineThicknessF				= 3.
  	res@trYReverse					= True
  	lres = res
  	
  	lres@xyDashPattern				= 0
  	lres@xyLineThicknessF			= 1.
  	lres@xyLineColor					= "black"
;===================================================================================
;===================================================================================
slp = new(num_m,float)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  infile = addfile(idir+"data/"+oname+"/"+oname+".PS.nc","R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".DSE.nc","R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP  = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    WdHdp = dim_sum_n(W*dHdp*dP/g,1)
    
    SRC = COLQR + SHFLX + LHFLX
    delete([/COLQR,SHFLX,LHFLX/])
    SRC = run_wgt_mean_2D( SRC ,lat,lon)
    
    W    = (/ run_wgt_mean_2D_alt(W   ,lat,lon) /)
    dHdp = (/ run_wgt_mean_2D_alt(dHdp,lat,lon) /)
    
    ;---------------------------------------------------
    ; Binning algorithm
    ;---------------------------------------------------
  	do v = 0,nbv-1
    Vx  = run_wgt_mean_2D( -1.*WdSdp ,lat,lon)
    if v.eq.0 then Vy = W    end if
    if v.eq.2 then Vy = W    end if
  	if v.eq.1 then Vy = dHdp end if
  	if v.eq.3 then Vy = dHdp end if
  	if v.lt.2 then bin_min = bin_min1    end if
    if v.lt.2 then bin_max = bin_max1    end if
  	if v.ge.2 then bin_min = bin_min2    end if
    if v.ge.2 then bin_max = bin_max2    end if
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      cond3 = conform(Vy,cond2,(/0,2,3/))
      tmp   = where(cond3, Vy, Vy@_FillValue)
      if all(cond2.eq.False) then
        print("!!! BIN IS EMPTY !!!  bin_bot: "+bin_bot+"  bin_top: "+bin_top)
      else
        bin_val(m,v,b,:) = dim_avg_n(dim_avg_n(tmp,(/2,3/)),0)
        bin_cnt(m,v,b) = num(cond2)
        bin_tct(m,v,b) = max(dim_num_n(cond2,0))
      end if
      delete([/tmp,cond2,cond3/])
    end do
    delete([/Vx,Vy/])
    bin_cnt(m,v,:) = bin_cnt(m,v,:)/sum(bin_cnt(m,v,:)) *100.
    end do
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
      delete([/H,S,W,dP,WdHdp,WdSdp,SRC/])
      delete([/dHdp,dSdp/])
      delete([/lat,lon/])
end do	
;===================================================================================
; Create plot 
;===================================================================================
	var_str = new(nbv,string)
	var_unt = new(nbv,string)
	var_str(0) = "Omega"
	var_str(1) = "dH/dp"
	var_str(2) = var_str(0)
	var_str(3) = var_str(1)
	
	var_unt(0) = "[m/s]"
	var_unt(1) = "[K/hPa]"
	var_unt(2) = var_unt(0)
	var_unt(3) = var_unt(1)

    		res@xyLineColors 		= clr
    		res@xyMonoDashPattern	= True
    		
  do v = 0,nbv-1
  		res@gsnLeftString 	= var_str(v)
  		res@tiXAxisString	= var_unt(v)
  		res@tiYAxisString	= "Pressure [hPa]"
  		res@gsnRightString 	= ""
    if v.lt.2 then res@gsnRightString = bin_min1+" to "+bin_max1+" W m~S~-2~N~" end if
    if v.ge.2 then res@gsnRightString = bin_min2+" to "+bin_max2+" W m~S~-2~N~" end if
    ;plot(v) = gsn_csm_xy(wks,dim_avg_n_Wrap(bin_val(:,v,:,:),1),lev,res)
    tcnt = conform(bin_val(:,v,:,:),bin_cnt(:,v,:),(/0,1/))
    tmp = dim_sum_n(bin_val(:,v,:,:)*tcnt,1)/dim_sum_n(tcnt,1)
    plot(v) = gsn_csm_xy(wks,tmp,lev,res)
  end do
  
  ;print(dim_sum_n(dim_avg_n_Wrap(bin_val(:,0,:,:),1),1))
 
  x1 = (/-1.,1./)*1e8
  x0 = x1*0
  do p = 0,nbv-1
    overlay( plot(p) , gsn_csm_xy(wks,x1,x0,lres) )
    overlay( plot(p) , gsn_csm_xy(wks,x0,x1,lres) )
  end do
  delete([/x0,x1/])
;===================================================================================
; Panel plots
;===================================================================================
  	pres = True 
  	pres@gsnFrame       						= False
  	pres@amJust   							= "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF 	= 0.01
    	pres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)"/) 
    	
  gsn_panel(wks,plot,(/2,2/),pres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"  			   : False
    ;"vpXF"                     : 0.7
    ;"vpYF"                     : 0.7
    ;"vpWidthF"                 : 0.2	; one panel
    ;"vpHeightF"                : 0.15
    ;"vpXF"                     : 0.8 	; two panel
    ;"vpYF"                     : 0.6
    "vpXF"                     : 0.8	 	; four panel
    "vpYF"                     : 0.8
    "vpWidthF"                 : 0.1
    "vpHeightF"                : 0.08
    "lgPerimOn"                : True
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True
    "lgLineLabelsOn"           : False
    ;"lgLabelFontHeightF"       : 0.025
    "lgLabelFontHeightF"       : 0.015
    "lgDashIndexes"            : (/0,0,0,0/)
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;===================================================================================
;===================================================================================
end


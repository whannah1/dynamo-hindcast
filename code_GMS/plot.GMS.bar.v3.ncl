; Similar to version 2, but considers enhanced vs suppressed 
; phases of the MJO based on column MSE anomaly
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
;------------------------------------------------------------------------------------------------
function calc_dp (P,Ps)
local dP,num_lev,Ps3D,Tvals,Cvals,Bvals
begin
  num_lev = dimsizes(P&lev)
  Tvals = ispan(2,num_lev-1,1)
  Cvals = ispan(1,num_lev-2,1)
  Bvals = ispan(0,num_lev-3,1)
  Ps3D = conform(P,Ps,(/0,2,3/))
  dP = new(dimsizes(P),float)
  dP(:,Cvals,:,:) = (P(:,Bvals,:,:)+P(:,Cvals,:,:))/2. \
                   -(P(:,Cvals,:,:)+P(:,Tvals,:,:))/2.
  dP(:,0,:,:)  = where( Ps.gt.P(:,0,:,:) , Ps - (P(:,0,:,:)+P(:,1,:,:))/2. , dP@_FillValue)
  dP(:,Cvals,:,:) = where( (Ps3D(:,Cvals,:,:).gt.P(:,Cvals,:,:)).and.	\
                           (Ps3D(:,Cvals,:,:).lt.P(:,Bvals,:,:)), 	\
                            Ps3D(:,Cvals,:,:)-(P(:,Cvals,:,:)+P(:,Tvals,:,:))/2.,dP(:,Cvals,:,:) )
  return(dP)
end
;------------------------------------------------------------------------------------------------
begin	

	member = (/"ERAi","13","09","12"/)
	case_name = (/"ERAi","ZM_0.2","ZM_1.0","ZM_2.0"/)

	fig_type = "png"
	
	sa = "nsa"
	
	rd1 = (/00/)
	
	;clrs = (/"red","blue","green"/)
	clrs = (/"black","green","red","blue"/)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  fig_file = odir+"GMS.bar.v3"
	
	top_lev = 200.
;===================================================================================
;===================================================================================
	num_r = 4
	num_m = dimsizes(member)
	xdim = (/num_r,num_m/)
	X = new(xdim,float)
	Y = new(xdim,float)
	gsz = 0.3
	bsz = (gsz*2.)/(num_r)  ;*1.5
	
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(2,graphic)
  	res = True
  	res@gsnDraw 			= True
  	res@gsnFrame 		= False
  	res@tmXTOn			= False
  	res@tmYROn 			= True
  	res@gsnXYBarChart	= True
  	res@gsnScale 		= True
  	res@gsnYRefLine		= 0.
  	;res@gsnLeftStringFontHeightF	= 0.02  	
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".PS.nc"
  infile = addfile(ifile,"R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".MSE.nc"
  infile = addfile(ifile,"R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".DSE.nc"
  infile = addfile(ifile,"R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".OMEGA.nc"
  infile = addfile(ifile,"R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  Wa = conform(W,dim_avg_n(dim_avg_n(W,(/2,3/)),0),1)
  Ha = conform(H,dim_avg_n(dim_avg_n(H,(/2,3/)),0),1)
  Wp = W - Wa
  Hp = H - Ha
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  delete([/lat,lon/])
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P    = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP   = calc_dp(P,Ps)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    dHpdp = new(dimsizes(H),float)
    dHadp = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHpdp(:,1:num_lev-2,:,:) = (Hp(:,0:num_lev-3,:,:) - Hp(:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHadp(:,1:num_lev-2,:,:) = (Ha(:,0:num_lev-3,:,:) - Ha(:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)    
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ;===================================================================================
    MSEvi = dim_sum_n(H*dP/g,1)
    MSEsd = stddev(MSEvi)
    MSEav = conform(MSEvi,dim_avg_n(MSEvi,0),(/1,2/))
    MSEvi = (/ MSEvi - MSEav /)
        
    WdSdp = new(dimsizes(MSEvi),float)
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    
    WdHdp = new(dimsizes(WdSdp),float)
	WpdHp = new(dimsizes(WdSdp),float)
	WadHa = new(dimsizes(WdSdp),float) 
	WdHdp = dim_sum_n(W *dHdp *dP/g,1)
    	WpdHp = dim_sum_n(Wp*dHpdp*dP/g,1) 
    	WadHa = dim_sum_n(Wa*dHadp*dP/g,1)
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    cond0 = MSEvi.lt.( 1.*MSEsd)
    cond1 = MSEvi.gt.( 1.*MSEsd)
    
  	D  = avg( where( abs(WdSdp).ge.10.           ,WdSdp	,WdSdp@_FillValue) )
  	D0 = avg( where((abs(WdSdp).ge.10.).and.cond0,WdSdp ,WdSdp@_FillValue) )
  	D1 = avg( where((abs(WdSdp).ge.10.).and.cond1,WdSdp ,WdSdp@_FillValue) )
    
print(case_name(m)+"	"+D+"	"+D0+"	"+D1)
    
    SRC = 0.;COLQR + SHFLX + LHFLX

    ;VGMS1  = avg( WdHdp - SRC 				) /D
    ;VGMS2  = avg( WadHa - avg(SRC)			) /D
    ;VGMS3  = avg( WpdHp - ( SRC-avg(SRC) )	) /D
    ;VGMS4  = (VGMS2+VGMS3)
  
    ;print(case_name(m)  +"		"	+sprintf("%+6.4f",VGMS1)	+"		"	+sprintf("%+6.4f",VGMS4)			\
    ;                    +"		( "	+sprintf("%+8.6f",VGMS1-VGMS4)+" )"	\
    ;                    +"		"	+sprintf("%+6.4f",VGMS2)	+"		"	+sprintf("%+6.4f",VGMS3)			 )		
    
    X(:,m) = m+fspan(-gsz+bsz/2.,gsz-bsz/2.,num_r)
    	
    Y(0,m) = avg( WdHdp - SRC 				) /D
    Y(1,m) = avg( WadHa - avg(SRC)			) /D
    Y(2,m) = avg( where(cond0, WpdHp - ( SRC-avg(SRC) ) ,WpdHp@_FillValue)  ) /D0
    Y(3,m) = avg( where(cond1, WpdHp - ( SRC-avg(SRC) ) ,WpdHp@_FillValue)  ) /D1
	
	if all(SRC).eq.0 then lstr = "VGMS" else lstr = "eff. VGMS" end if
	
      delete([/H,S,W,Hp,Wp,Wa,Ha,dP/])
      delete([/SRC,COLQR,SHFLX,LHFLX/])
      delete([/dHdp,dSdp,dHadp,dHpdp,WdSdp,WdHdp,WpdHp,WadHa/])
      delete([/MSEvi,MSEav,MSEsd,cond0,cond1,D0,D1/])
end do
;===================================================================================
; Plot GMS
;===================================================================================
  		res@trXMinF =  -0.5
  		res@trXMaxF =  num_m-0.5
  		res@gsnLeftString	= lstr
  		res@tmXBMode = "Explicit"
  		res@tmXBValues = dim_avg_n(X,0)
  		res@tmXBLabels = case_name
 		res@gsnXYBarChartBarWidth = bsz
 		
 		res@trYMinF = min(Y(:,1:))-stddev(Y)*0.2
 		res@trYMaxF = max(Y(:,1:))+stddev(Y)*0.2
 		
  do r = 0,num_r-1
 		res@gsnXYBarChartColors2 = clrs(r)
    plot(r) = gsn_csm_xy(wks,X(r,:),Y(r,:),res)
  end do
  ;---------------------------------------------------------------------
  ; Add legend
  ;---------------------------------------------------------------------
  tstr = " _~H-16~~V-21~w d~B~p~N~ _~H-16~~V-21~h" 
  legend2 = create "Legend" legendClass wks 
    "vpXF"                      	: 0.2
    "vpYF"                      	: 0.12
    "vpWidthF"                  	: 0.3   
    "vpHeightF"                 	: 0.1   
    "lgPerimOn"                 	: True   
    "lgItemCount"               	: num_r
    "lgLabelStrings"         	: "  "+(/"VGMS","VGMS bar","VGMS' Dry Phase","VGMS' Wet Phase"/)
    "lgItemType"					: "Lines"
    "lgLineLabelsOn"				: False
    "lgLineColors"				: clrs
    "lgLineThicknessF"			: 30.
    "lgBoxMinorExtentF"			: 0.05
    "lgMonoDashIndex"			: True
    "lgDashIndex"				: 0
    "lgLabelsOn"                	: True     
    "lgLabelFontHeightF"        	: 0.03    
    "lgMonoLineLabelFontColor"  	: True                  
  end create
  draw(legend2)
  ;---------------------------------------------------------------------
  ; Finalize plot
  ;---------------------------------------------------------------------
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"

begin
	;member = (/"13","12"/)
	;case_name = (/"ZM_0.2","ZM_2.0"/)
	;member = (/"ERAi","13","12"/)
	;case_name = (/"ERAi","ZM_0.2","ZM_2.0"/)
	member = (/"ERAi"/)
	case_name = (/"ERAi"/)
	
	rd1 = (/00/)
	
	fig_type = "x11"

	lat1 = -10.
	lat2 =  10.
	lon1 =  40.
	lon2 = 180.
	
	nsmooth = 5
	
	fvar1 = "MSE"
	fvar2 = "DSE"
	
	anom = False
	
	num_p = 3
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_GMS/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
  fig_file = odir+"GMS.climo"
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"ncl_default")
  plot = new(num_p*num_m,graphic)
  	res = True
  	res@gsnDraw 					= False
  	res@gsnFrame 				= False
  	;res@gsnRightString 			= ""
  	res@mpCenterLonF 			= 180.
	res@mpLimitMode 				= "LatLon"
	res@mpMinLatF 				= lat1
	res@mpMaxLatF 				= lat2
	res@mpMinLonF 				= lon1
	res@mpMaxLonF 				= lon2
  	res@gsnAddCyclic 			= False
  	;res@lbAutoManage				= False
  	;res@lbTitleFontHeightF			= 0.02
  	res@lbLabelFontHeightF 			= 0.02
  	res@gsnLeftStringFontHeightF		= 0.02
  	res@gsnRightStringFontHeightF	= 0.02
  	res@gsnSpreadColors 			= True
  	res@cnFillOn 				= True
  	res@cnLinesOn 				= False
  	res@cnLineLabelsOn 			= False
  	res@lbLabelBarOn 			= True
  	res@lbTitlePosition 			= "Bottom"
  	res@lbTitleFontHeightF		= 0.01
  	res@tmXBLabelFontHeightF		= 0.
  	res@tmYLLabelFontHeightF		= 0.
;===================================================================================
;===================================================================================
do r = 0,num_r-1
do m = 0,num_m-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
;===================================================================================
;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget."+fvar1+".nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  t1    = 0
  time  = infile->time(t1::4)
  MSEvi = avg4to1(infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) )
  COLDP = avg4to1(infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) )
  ;VdelH = avg4to1(infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) )
  ;udHdx = avg4to1(infile->udHdx(t1:,{lat1:lat2},{lon1:lon2}) )
  ;vdHdy = avg4to1(infile->vdHdy(t1:,{lat1:lat2},{lon1:lon2}) )
  WdHdp = avg4to1(infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) )
  COLQR = avg4to1(infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) )
  LHFLX = avg4to1(infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = avg4to1(infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) )
  MSEvi = (/MSEvi/COLDP/cpd/)
  
  ifile = idir+"data/"+oname+"/"+oname+".budget."+fvar2+".nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  WdSdp = avg4to1(infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) )	
  PRECT = infile->PRECT(t1:,{lat1:lat2},{lon1:lon2})
  
	PRECT = PRECT*(24.*3600.)/1000.
  	
  	if True
  	do i = 0,nsmooth-1
  	 MSEvi = smooth(MSEvi)
	end do
  	end if
  	if anom then MSEvi = dim_rmvmean_n(MSEvi,0) end if
  
  SRC  = (/ COLQR+LHFLX+SHFLX /)
;===================================================================================
;===================================================================================
    	D = dim_avg_n(WdSdp,(/0/))
    	aD  = D
  	D = where(abs(D).lt.10.,D@_FillValue,D)
  
printVarSummary(WdHdp)
printVarSummary(D)
  
  VGMS  = dim_avg_n(WdHdp,(/0/))/D
  eVGMS = dim_avg_n(WdHdp-SRC,(/0/))/D

  aP = dim_avg_n_Wrap(PRECT,0)  	
  
  	copy_VarCoords(aP,VGMS)
  	copy_VarCoords(aP,eVGMS)
  	copy_VarCoords(aP,aD)
;===================================================================================
; Create plot
;===================================================================================
  	res@cnLevelSelectionMode = "ManualLevels"
  	if anom then
  	res@cnMinLevelValF  = -3.
  	res@cnMaxLevelValF  =  3.
  	else
  	res@cnMinLevelValF  = 328.
  	res@cnMaxLevelValF  = 340.
  	end if
  	res@cnLevelSpacingF =   1.
  	
  	res@gsnRightString = case_name(m)
  	
  	res@gsnLeftString = "Mean MSEvi"
  	res@lbTitleString = "W/m^2"
  plot(0*2+m) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(MSEvi,(/0/)),res)
  
  
  ;	res1 = res
 ; 	res1@cnLevelSelectionMode = "ExplicitLevels"
 ; 	res1@cnLevels = (/-400.,-300.,-200.,-150.,-100.,-50.,-20.,-10.,0.,10.,20.,50.,100.,150.,200.,300.,400./)
 ; 	res1@gsnLeftString = "Mean Normalization Factor"
 ; 	res1@lbTitleString = "W/m^2"
 ; plot(1*num_m+m) = gsn_csm_contour_map(wks,aD,res1)
 ;   delete(res1)

  ;	res2 = res
 ; 	res2@cnLevelSelectionMode = "ExplicitLevels"
 ; 	res2@cnLevels = (/0.5,1.,2.,4.,6.,8.,12.,16.,20.,24.,28.,36./)
 ; 	res2@gsnLeftString = "PRECT"
 ; 	res2@lbTitleString = "mm/day"
 ; plot(2*num_m+m) = gsn_csm_contour_map(wks,aP,res2)
 ;   delete(res2)
  
  	res3 = res
  	res3@cnLevelSelectionMode = "ExplicitLevels"
  	res3@cnLevels = (/-2.,-1.,-0.8,-0.4,-0.2,-0.1,0.,0.1,0.2,0.4,0.8,1.,2./)
  	res3@gsnLeftString = "Vert. GMS"
  	res3@lbTitleString = "W/m^2"
  plot(1*num_m+m) = gsn_csm_contour_map(wks,VGMS,res3)
  	
  	res4 = res3
  	res4@gsnLeftString = "Eff. Vert. GMS"
  	res4@lbTitleString = "W/m^2"
  plot(2*num_m+m) = gsn_csm_contour_map(wks,eVGMS,res4)
  
  delete([/MSEvi,COLDP,WdSdp,PRECT,WdSdp,COLQR,LHFLX,SHFLX,WdHdp,SRC,D,aD,VGMS,eVGMS,aP/])
  	
end do  	
;===================================================================================
;===================================================================================
  	pres = True
    	;pres@txString = oname;+"  "+lat1+":"+lat2+"N / "+lon1+":"+lon2+"E"
    	pres@amJust   = "TopLeft"
    	pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 
    	;pres@gsnPanelYWhiteSpacePercent = 10


  gsn_panel(wks,plot,(/num_p,num_m/),pres)
  ;gsn_panel(wks,plot(0),(/1,1/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end do
end


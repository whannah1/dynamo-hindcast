; Calculates and plots the distribution of the GMS denomintor
; used in calculating the GMS and components
; v2 is similar to v1, but regrids the data to match the
; first case to test if the different in resolution makes
; any noteworthy differences in the distributions
; (p.s. it doesn't matter)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin   
        
    member = (/"ERAi","90","09"/)
    case_name = (/"ERAi","SP-CAM","ZM_1.0"/)

    fig_type = "png"
    
    rd1 = (/00/)

    ;clr = (/"black","green","blue","red" /)
    clr = (/"black","purple","blue","green" /)
    
    idir = "/Users/whannah/DYNAMO/Hindcast/"
    odir = "/Users/whannah/DYNAMO/Hindcast/"
    
      lat1 = -10.
      lat2 =  10.
      lon1 =  60.
      lon2 =  90.
      fig_file = odir+"GMS.calc.dist.v2"

    top_lev = 150.

;===================================================================================
;===================================================================================
    num_r = dimsizes(rd1)
    num_m = dimsizes(member)

    bin_min = -950
    bin_max =  250
    bin_spc =  100
    xbin = ispan(toint(bin_min),toint(bin_max),toint(bin_spc))
    num_bin = dimsizes(xbin)
    bdim = (/num_m,2,num_bin/)
    bin_val = new(bdim,float)
    bin_std = new(bdim,float)
    bin_cnt = new(bdim,float)
    bin_tct = new(bdim,float)

  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"default")
  plot = new((/num_r/),graphic)
    res = True
    res@gsnDraw                         = False
    res@gsnFrame                    = False
    res@tmXTOn                      = False
    res@tmYROn                      = False
    res@gsnRightString              = ""
    res@gsnLeftString               = ""
    res@gsnLeftStringFontHeightF    = 0.02
    res@gsnRightStringFontHeightF   = 0.02
    res@tiXAxisFontHeightF          = 0.02
    res@tiYAxisFontHeightF          = 0.02
    res@vpHeightF                   = 0.4
    res@tmXBMinorOn                 = False
    res@tmYLMinorOn                 = False

    res@trYMaxF                     = 25.
    res@trYMinF                     =  0.

    lres = res
    
    res@xyLineThicknessF                = 3.
;===================================================================================
;===================================================================================
do r = 0,num_r-1
do m = 0,num_m-1
  pname = sprinti("%0.2i",rd1(r))+"-"+sprinti("%0.2i",rd1(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+pname
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".PS.nc"
  infile = addfile(ifile,"R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".DSE.nc"
  infile = addfile(ifile,"R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".OMEGA.nc"
  infile = addfile(ifile,"R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})  
  ;--------------------------------------------------
  H = S
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP  = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    WdSdp = dim_sum_n(W*dSdp*dP/g,1) 
    ;cdSdp = dim_sum_n(dSdp*dP/g,1) 
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================

    if m.eq.0 then
      lat0 = lat
      lon0 = lon
    end if
    
    tWdSdp = WdSdp
    delete(WdSdp)
    WdSdp = linint2_Wrap(lon,lat,tWdSdp,False,lon0,lat0,0)
    delete(tWdSdp)
    

    do v = 0,1
    Vx  = run_wgt_mean_2D( -1.*WdSdp     ,lat0,lon0)
    ;Vx  = run_wgt_mean_2D( -1.*cdSdp     ,lat,lon)
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      bin_cnt(m,v,b) = num(cond2)
      delete([/cond2/])
    end do
    delete([/Vx/])
    end do
    delete([/S,H,W,dSdp,WdSdp,dP/])
    delete([/lat,lon/])
end do  
;===================================================================================
; Create plot 
;===================================================================================    
            res@xyLineColors        = clr
            res@xyMonoDashPattern   = True
            res@xyDashPattern       = 0
            res@xyMarkLineMode      = "MarkLines"
            res@xyMarker            = 16
            res@xyMarkerColors      = clr
            
            
            res@gsnLeftString          = "Freq. of Occurrence"
            res@gsnRightString         = pname
            res@tiXAxisString          = "<-~F33~w~F21~*ds/dp>  [W m~S~-2~N~]";"-W*dS/dp  [W m~S~-2~N~]"
            res@tiYAxisString          = "%"
            res@trXMaxF                    = max(xbin)
            res@trXMinF                    = min(xbin)

            res@trYMaxF   = 25
            
    pbin_cnt = bin_cnt
    do m = 0,num_m-1
      pbin_cnt(m,0,:) = pbin_cnt(m,0,:)/sum(pbin_cnt(m,0,:)) *100.
      pbin_cnt(m,1,:) = pbin_cnt(m,1,:)/sum(pbin_cnt(m,1,:)) *100.
    end do
            
    plot(r) = gsn_csm_xy(wks,xbin,pbin_cnt(:,0,:),res)
    
        
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                    = "red"

    
    tval = 1.960    ; 95% confidence (2 tail) 
    ;dof = bin_cnt*0.01
    dof = bin_tct;*0.01
    ;dof = 75.
    dof = where(dof.gt.1,dof,dof@_FillValue)

    
      
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                    = "black"
    
    x1 = (/-1.,1./)*1e8
    x0 = x1*0
    overlay( plot(r) , gsn_csm_xy(wks,x1,x0,lres) )
    overlay( plot(r) , gsn_csm_xy(wks,x0,x1,lres) )
    delete([/x0,x1/])
  end do
  ;===================================================================================
  ;===================================================================================
  ;---------------------------------------------------------------------
  ; Panel plot
  ;---------------------------------------------------------------------
    pres = True 
    pres@gsnFrame                               = False
    pres@amJust                             = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF   = 0.01
        pres@gsnPanelFigureStrings              = (/"a","b","c","d"/) 
  
  gsn_panel(wks,plot,(/1,num_r/),pres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"                 : False
    "vpXF"                     : 0.1
    "vpYF"                     : 0.62
    "vpWidthF"                 : 0.15
    "vpHeightF"                : 0.1
    "lgPerimOn"                : True
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True
    "lgLineLabelsOn"           : False
    "lgLabelFontHeightF"       : 0.012
    "lgDashIndexes"            : (/0,0,0,0,0,0,0/)
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True
  end create

  draw(legend)
  frame(wks)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
    
;===================================================================================
;===================================================================================
end


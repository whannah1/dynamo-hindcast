; Calculates the VGMS using a weighted mean over data that 
; has been binned by the DSE export. The data is initially 
; processed with a spatial running mean
; v5 is similar to v2, but it is made to explain the contribution
; to the change in GMS relative to the control by omega and dHdp
; v5 also uses the method in v3, in which the DSE import distribution 
; from the first case is used to eliminate effects of changes to the
; VGMS denominator 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin	
	
	;member = (/"13","12"/)
	;case_name = (/"ZM_0.2","ZM_2.0"/)
	
	member = (/"ERAi","13","09","12"/)
	case_name = (/"ERAi","ZM_0.2","ZM_1.0","ZM_2.0"/)

	fig_type = "png"
	
	sa = "nsa"
	
	rd1 = (/00/)

	;clr = (/"black","green","blue","red" /)
	clr = (/"green","red" /)
	
	idir = "/Users/whannah/DYNAMO/Hindcast/"
  	odir = "/Users/whannah/DYNAMO/Hindcast/"
	
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  fig_file = odir+"GMS.calc.bin.v6."+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
	
	top_lev = 150.

;===================================================================================
;===================================================================================
	num_r = 3
	num_m = dimsizes(member)

	bin_min = -950
    bin_max =  250
    bin_spc =  100
    xbin = ispan(toint(bin_min),toint(bin_max),toint(bin_spc))
  	num_bin = dimsizes(xbin)
  	nbv = 3
  	bdim = (/num_m,nbv,num_bin/)
  	bin_val = new(bdim,float)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)

  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev) 
  
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"default")
  plot = new((/4/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@tmXTOn						= False
  	res@tmYROn 						= False
  	res@gsnRightString				= ""
  	res@gsnLeftString				= ""
  	res@gsnLeftStringFontHeightF 	= 0.02
  	res@gsnRightStringFontHeightF 	= 0.02
  	res@tiXAxisFontHeightF			= 0.02
  	res@tiYAxisFontHeightF			= 0.02
  	res@tmXBMinorOn					= False
    	res@tmYLMinorOn					= False
  	res@vpHeightF					= 0.4
  	res@xyLineThicknessF				= 3.
  	lres = res
  	
  	lres@xyDashPattern				= 0
  	lres@xyLineThicknessF			= 1.
  	lres@xyLineColor					= "black"
;===================================================================================
;===================================================================================
slp = new(num_m,float)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+sprinti("%0.2i",rd1)+"-"+sprinti("%0.2i",rd1+5-1)
  ;===================================================================================
  ; Load Data
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  LHFLX = (infile->LHFLX(:,{lat1:lat2},{lon1:lon2}) )
  SHFLX = (infile->SHFLX(:,{lat1:lat2},{lon1:lon2}) )
  COLQR = (infile->COLQR(:,{lat1:lat2},{lon1:lon2}) )
  ;--------------------------------------------------
  infile = addfile(idir+"data/"+oname+"/"+oname+".PS.nc","R")
  Ps = infile->PS(:,{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
  H = infile->MSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".DSE.nc","R")
  S = infile->DSE(:,{:top_lev},{lat1:lat2},{lon1:lon2}) 
  infile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
  W = infile->OMEGA(:,{:top_lev},{lat1:lat2},{lon1:lon2})
  ;--------------------------------------------------
  lat = H&lat
  lon = H&lon
  lat!0 = "lat"
  lon!0 = "lon"
  lat&lat = lat
  lon&lon = lon
  ;===================================================================================
  ; Calculate budget terms
  ;===================================================================================
    P  = conform(H,lev*100.,1)
    copy_VarCoords(H,P)
    dP  = calc_dp(P,Ps)
    dP1 = dP(0,:,0,0)
    dSdp  = new(dimsizes(H),float)
    dHdp  = new(dimsizes(H),float)
    altdP = new(dimsizes(H),float)
    altdP(:,1:num_lev-2,:,:) =   P(:,0:num_lev-3,:,:) - P (:,2:num_lev-1,:,:)
    dSdp (:,1:num_lev-2,:,:) = ( S(:,0:num_lev-3,:,:) - S (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    dHdp (:,1:num_lev-2,:,:) = ( H(:,0:num_lev-3,:,:) - H (:,2:num_lev-1,:,:) ) / altdP(:,1:num_lev-2,:,:)
    MSEvi = conform(H,dim_sum_n(H*dP/g,1),(/0,2,3/))
    MSEvi = (/MSEvi - conform(MSEvi,dim_avg_n(MSEvi,0),(/1,2,3/))/)
      delete([/Ps,P,altdP/])
  ;===================================================================================
  ; Calculate GMS
  ;===================================================================================
    WdSdp = dim_sum_n(W*dSdp*dP/g,1)
    WdHdp = dim_sum_n(W*dHdp*dP/g,1)
    
    SRC = COLQR + SHFLX + LHFLX
    SRC = run_wgt_mean_2D( SRC ,lat,lon)
    
    Vy1 = run_wgt_mean_2D_alt(W   ,lat,lon) 
    Vy2 = run_wgt_mean_2D_alt(dHdp,lat,lon) 
    
    ;Vy1 = W   
    ;Vy2 = dHdp
    
    Vx  = run_wgt_mean_2D( -1.*WdSdp     ,lat,lon)
    ;Vx  = -1.*WdSdp 
    ;---------------------------------------------------
    ; Binning algorithm
    ;---------------------------------------------------
  	do v = 0,nbv-1
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      cond2 = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      cond3  = conform(Vy1,cond2,(/0,2,3/))
      tmp1 = where(cond3,Vy1,Vy1@_FillValue)
      tmp2 = where(cond3,Vy2,Vy2@_FillValue)
      tSRC = where(cond2,SRC,SRC@_FillValue)
      if all(ismissing(tmp1)) then
        print("!!! ALL TMP VALUES ARE MISSING !!!  bin_bot: "+bin_bot+"  bin_top: "+bin_top)
      else
        if v.eq.0 then 
          vtmp = dim_avg_n(dim_avg_n(tmp1*tmp2,(/2,3/)),0)
          bin_val(m,v,b) = -1.*sum( dP1/g*vtmp )
          delete([/vtmp/])  
        end if
        if v.eq.1 then 
          vtmp1 = dim_avg_n(dim_avg_n(tmp1,(/2,3/)),0)
          vtmp2 = dim_avg_n(dim_avg_n(tmp2,(/2,3/)),0)
          bin_val(m,v,b) = -1.*sum( dP1/g*vtmp1*vtmp2 )
          delete([/vtmp1,vtmp2/])  
        end if
        if v.eq.2 then 
          atmp1 = conform(tmp1,dim_avg_n(dim_avg_n(tmp1,(/2,3/)),0),1)
          atmp2 = conform(tmp2,dim_avg_n(dim_avg_n(tmp2,(/2,3/)),0),1)
          vtmp = dim_avg_n(dim_avg_n((tmp1-atmp1)*(tmp2-atmp2),(/2,3/)),0)
          bin_val(m,v,b) = -1.*sum( dP1/g*vtmp )
          delete([/atmp1,atmp2,vtmp/])
        end if
        
        bin_cnt(m,v,b) = num(cond2)
        
      end if
      delete([/tmp1,tmp2,tSRC,cond2,cond3/])
    end do
    end do
    delete([/Vx,Vy1,Vy2/])
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
      delete([/H,S,W,dP,WdHdp,WdSdp/])
      delete([/SRC,COLQR,SHFLX,LHFLX/])
      delete([/dHdp,dSdp,MSEvi/])
      delete([/lat,lon/])
end do	

	do m = 0,num_m-1
	do v = 0,nbv-1
	  bin_cnt(m,v,:) = bin_cnt(m,v,:)/sum(bin_cnt(m,v,:)) *100.
	  bin_cnt(m,v,:) = bin_cnt(0,v,:)
	end do
	end do
	
;===================================================================================
; Create plot 
;===================================================================================    
    ;GMS = new((/num_m,2/),float)
    ;do m = 0,num_m-1
    ;  GMS(m,0) = sum( ( bin_cnt(m,0,:) * bin_val(m,0,:)/xbin ) /sum(bin_cnt(m,0,:)) )
    ;  GMS(m,1) = sum( ( bin_cnt(m,1,:) * bin_val(m,1,:)/xbin ) /sum(bin_cnt(m,1,:)) )
    ;end do
    ;print(GMS)
    
    cxbin = conform(bin_val(:,0,:),xbin,1)
    
    GMSt = dim_sum_n( bin_cnt(:,0,:) * bin_val(:,0,:)/cxbin ,1) / dim_sum_n( bin_cnt(:,0,:) ,1)
    GMSb = dim_sum_n( bin_cnt(:,1,:) * bin_val(:,1,:)/cxbin ,1) / dim_sum_n( bin_cnt(:,1,:) ,1)
    GMSp = dim_sum_n( bin_cnt(:,2,:) * bin_val(:,2,:)/cxbin ,1) / dim_sum_n( bin_cnt(:,2,:) ,1)
    
    
    RESIDUAL = GMSt - ( GMSb + GMSp )

    print("")
    print("GMSt		GMSb		GMSp		RESIDUAL")
    print(GMSt+"		"+GMSb+"		"+GMSp+"		"+RESIDUAL)
    print("")
    		
    		res4 = res
    		
    		;res@xyLineColors 		= clr
    		;res@xyMonoDashPattern	= True
    		;res@xyDashPattern 		= 0
    		res@xyMarkLineMode		= "MarkLines"
    		res@xyMarker				= 16
    		res@xyMarkerSizeF		= 0.005
    		res@xyMarkerColor		= "black"
    		
    		res1 = res
    		res1@gsnRightString 			= ""
    		res1@tiXAxisString			= "-W*dS/dp  [W m~S~-2~N~]"
    		res1@tiYAxisString			= "[W m~S~-2~N~]"
    		res1@trXMaxF					= max(xbin)
    		res1@trXMinF					= min(xbin)
    		res1@xyMonoLineColor			= True
    		res1@xyLineColor 			= "black"
    		
    		res1@trYMaxF		=   50.
    		res1@trYMinF		= -150.
    		
    		res1a = res1
    		res1b = res1
    		
    		res1@gsnLeftString			= "-W*dH/dp (Total)"
    		res1a@gsnLeftString			= "-W*dH/dp (Mean)"
    		res1b@gsnLeftString			= "-W*dH/dp (Eddy Flux)"
    		
    		res1a@xyLineColor 		= "blue"
    		res1b@xyLineColor 		= "red"
    		
    		;res1a@xyMonoDashPattern	= True
    		;res1a@xyDashPatterns		= 1
    		;res1a@xyMonoMarker		= False
    		;res1a@xyMarkers			= (/4,5,6/)
    		
    		
    		;res1b@xyMonoDashPattern	= True
    		;res1b@xyDashPatterns		= 1
    		
  plot(0) = gsn_csm_xy(wks,xbin,bin_val(:,0,:),res1)
  plot(1) = gsn_csm_xy(wks,xbin,bin_val(:,1,:),res1a)
  plot(2) = gsn_csm_xy(wks,xbin,bin_val(:,2,:),res1b)
  
  ;overlay(plot(0) , gsn_csm_xy(wks,xbin,bin_val(:,1,:),res1a))
  ;overlay(plot(0) , gsn_csm_xy(wks,xbin,bin_val(:,2,:),res1b))
    		
    		num_r = 3
		gsz = 0.2
		bsz = (gsz*2.)/(num_r+1)  ;*2.
    		barx = ispan(1,num_m,1) 
    		;barx = 1
    		res4@gsnLeftString			= "VGMS and Components"
    		res4@gsnRightString 			= ""
    		res4@gsnXYBarChart			= True
  		res4@gsnScale 				= True
  		res4@gsnYRefLine				= 0.
 		res4@gsnXYBarChartBarWidth 	= bsz
    		res4@tmXBMode 				= "Explicit"
    		res4@tmXBValues 				= barx
  		res4@tmXBLabels   			= case_name
  		res4@trYMinF 				= -0.1
 		res4@trYMaxF 				=  0.1
 		res4@trXMinF					= min(barx)-bsz*3.
 		res4@trXMaxF					= max(barx)+bsz*3.
  		res4@gsnXYBarChart			= True
  		res4@gsnScale 				= True
  		res4@gsnYRefLine				= 0.
 		res4@gsnXYBarChartBarWidth 	= bsz
    

      res4a = res4
      res4b = res4
      res4@gsnXYBarChartColors2 		= "black"
      res4a@gsnXYBarChartColors2  	= "blue"
      res4b@gsnXYBarChartColors2  	= "red"
      
          plot(3) = gsn_csm_xy(wks,barx-bsz*3./2.,GMSt,res4)
          tplot1  = gsn_csm_xy(wks,barx-bsz*0./2.,GMSb,res4a)
          tplot2  = gsn_csm_xy(wks,barx+bsz*3./2.,GMSp,res4b)
          
  overlay(plot(3) , tplot1)
  overlay(plot(3) , tplot2)
    
    x1 = (/-1.,1./)*1e8
    x0 = x1*0
    do p = 0,2
      overlay( plot(p) , gsn_csm_xy(wks,x1,x0,lres) )
      overlay( plot(p) , gsn_csm_xy(wks,x0,x1,lres) )
    end do
    delete([/x0,x1/])
;===================================================================================
; Panel plots
;===================================================================================
  	pres = True 
  	pres@gsnFrame       						= False
  	pres@amJust   							= "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF 	= 0.01
    	pres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)"/) 
    	
  gsn_panel(wks,plot,(/2,2/),pres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"  			   : False
    "vpXF"                     : 0.2
    "vpYF"                     : 0.35
    "vpWidthF"                 : 0.15
    "vpHeightF"                : 0.1
    "lgPerimOn"                : True
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True
    "lgLineLabelsOn"           : False
    "lgLabelFontHeightF"       : 0.012
    "lgDashIndexes"            : (/0,1,2,3/)
    "lgLineThicknessF"         : 3.
    ;"lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;===================================================================================
;===================================================================================
end


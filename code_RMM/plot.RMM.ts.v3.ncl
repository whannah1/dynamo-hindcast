load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
;load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
begin

    member    = (/"90","09"/)
    clr = (/"blue","red","black"/)
    
    nt = 10
    ;nt = 15

    num_m = dimsizes(member)
    
    use_alt = False
    
    fig_type = "png"
    
  odir = "~/DYNAMO/Hindcast/"
  fig_file = odir+"RMM.ts.v3"
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new((/num_m,4/),graphic)
;===================================================================================
;===================================================================================
  case_name = new(dimsizes(member)+1,string)
  case_name(:num_m-1) = where(member.eq."90","SP-CAM",case_name(:num_m-1))
  case_name(:num_m-1) = where(member.eq."09","CAM5  ",case_name(:num_m-1))
  case_name(num_m) = "Obs"
  
do m = 0,num_m-1    

    case_stub = "DYNAMO_"+member(m)+"_f09"
    
    yr = 2011
    
    if True then
      mndy = (/1001,1006,1011,1016,1021,1026,1031,1105,1110,1115,1120,1125,1130,1205,1210,1215/)
      ;mndy = (/1001/)
      yrmndy = toint( yr*10000 + mndy )
    else
      mn = 10
      if mn.eq.10 then
         dy = (/01,06,11,16,21,26,31/)
      end if
      if mn.eq.11 then
        dy = (/05,10,15,20,25,30/)
      end if
      yrmndy = toint( yr*10000 + mn*100 + dy )
    end if
    case = case_stub+"_"+yrmndy
    
    ;odir = "/maloney-scratch/whannah/DYNAMO/Hindcast/figures/"
    ;if use_alt then
    ;  fig_file = odir+"RMM.ts.winds_only."+case_stub
    ;else
    ;  fig_file = odir+"RMM.ts."+case_stub
    ;end if

    pi = 3.14159
;===================================================================================
; Load Obs RMM
;===================================================================================
  ;ifile = "/maloney-scratch/whannah/DYNAMO/RMM_DYNAMO.txt"
  ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.txt"
  data = asciiread(ifile,(/31+30+31,7/),"float")
    oyr = data(:,0)
    omn = data(:,1)
    ody = data(:,2)
    oRMM1 = data(:,3)
    oRMM2 = data(:,4)
    oRMM1@mn = omn
    oAMP = sqrt(oRMM1^2.+oRMM2^2.)
    oPHS = atan2(oRMM2,oRMM1)
    num_t = dimsizes(oAMP)
    ;otime = fspan(1,num_t,num_t)
    time = (tofloat(ispan(0,num_t-1,1)))
        time@units = "days since "+yr(0)+"-10-01";+mn(0)+"-"+dy(0)+""
;===================================================================================
; Load Hindcast RMM
;===================================================================================
num_c = dimsizes(case)
mRMM1 = new((/num_c,nt/),float)
mRMM2 = new((/num_c,nt/),float)
mAMP  = new((/num_c,nt/),float)
mPHS  = new((/num_c,nt/),float)
do c = 0,num_c-1
  if use_alt then
    ifile = "~/DYNAMO/Hindcast/data/"+case(c)+"/"+case(c)+".RMM.120_retained.wind_only.nc"
  else
    ifile = "~/DYNAMO/Hindcast/data/"+case(c)+"/"+case(c)+".RMM.120_retained.nc"
  end if
  infile = addfile(ifile,"r")
  mRMM1(c,:) = (/ infile->RMM1(:nt-1) /)
  mRMM2(c,:) = (/ infile->RMM2(:nt-1) /)
  mAMP (c,:) = (mRMM1(c,:)^2.+mRMM2(c,:)^2.)^0.5
  mPHS (c,:) = atan2(mRMM2(c,:),mRMM1(c,:))
end do
;===================================================================================
;===================================================================================
    t  = fspan(0.,2.*pi,100)
    r  = fspan(1.,6.,2)
    xx = fspan(-10.,10.,2)
    theta = fspan(-pi,pi,9)
  ;-------------------------------------------------------------
  ; Set up workspace
  ;-------------------------------------------------------------
  ;wks = gsn_open_wks(fig_type,fig_file)
  ;plot = new(4,graphic)
    res = True
    res@gsnDraw  = False
    res@gsnFrame = False
    ;res@trXMaxF = num_t
    res@trXMaxF = 31+30+25
    res@tmXBLabelFontHeightF        = 0.015
    res@tmYLLabelFontHeightF        = 0.015
    res@tiXAxisFontHeightF          = 0.02
    res@tiYAxisFontHeightF          = 0.02
    res@tiXAxisOn                   = False
    res@tmYLMinorOn                 = False
    res@tmYROn                      = False
    res@tmXTOn                      = False
    res@tmXBLabelFontHeightF        = 0.02
    res@tmYLLabelFontHeightF        = 0.02
    res@tmXBLabelAngleF             = -45.
    res@tmXBMajorOutwardLengthF     = 0.0
    res@tmYLMajorOutwardLengthF     = 0.0
    res@tmXBMinorOutwardLengthF     = 0.0
    res@tmYLMinorOutwardLengthF     = 0.0
    res@vpHeightF                   = 0.3
    res@gsnCenterString             = ""
    res@gsnCenterStringFontHeightF = 0.02
    lres = res
    ;-----------------------------------
    mres = res
    mres@xyMarkLineMode     = "Markers"
    mres@xyMarkerSizeF      = 0.015
    mres@xyMarkerThicknessF = 2.
    mres@xyMarker           = 14
    ;-----------------------------------
    cdtime = cd_calendar(time,2)
    tres = True
    tres@ttmFormat = "%D%c"
    tres@ttmAxis = "XB"
    tres@ttmMajorStride = 10
    time_axis_labels(time,res,tres)
  ;-------------------------------------------------------------
  ; obs RMM
  ;-------------------------------------------------------------
  ;if m.eq.0 then

        res@gsnLeftString           = case_name(m)
        
        res@xyDashPattern           = 0
        res@xyLineThicknessF        = 3.
        res@xyLineColor             = "black"
        res@tiYAxisString           = "Amplitude"
        ;res@gsnCenterString        = "RMM Amplitude"
        res@gsnRightString          = "RMM Amplitude"
        res@tmXBLabelsOn            = False
        res@trYMaxF                 = 5.;max((/max(oAMP),max(mAMP)/))
        res@trYMinF                 = 0.
      plot(m,0) = gsn_csm_xy(wks,time,oAMP,res)
      
        res@tiYAxisString           = "RMM1"
        ;res@gsnCenterString        = "RMM1"
        res@gsnRightString          = "RMM1"
        res@tmXBLabelsOn            = False
        res@trYMaxF                 =  4.;max((/max(oRMM1),max(mRMM1)/))
        res@trYMinF                 = -4.;min((/min(oRMM1),min(mRMM1)/))
        res@tmYLMinorOn             = True
      plot(m,1) = gsn_csm_xy(wks,time,oRMM1,res)
      
        res@tiYAxisString           = "RMM2"
        ;res@gsnCenterString        = "RMM2"
        res@gsnRightString          = "RMM2"
        res@tmXBLabelsOn            = True
        res@trYMaxF                 =  4.;max((/max(oRMM2),max(mRMM2)/))
        res@trYMinF                 = -4.;min((/min(oRMM2),min(mRMM2)/))
        res@tmYLMinorOn             = True
      plot(m,3) = gsn_csm_xy(wks,time,oRMM2,res)
        
        tres = res
        tres@tiYAxisString          = "Phase"
        ;tres@gsnCenterString       = "RMM Phase"
        res@gsnRightString          = "RMM Phase"
        tres@tmXBLabelsOn           = True
        tres@trYMinF                = -pi
        tres@trYMaxF                = pi
        tres@tmYLMode               = "Explicit"
        tres@tmYLValues             = (/-pi,-pi/2,0,pi/2,pi/)
        tres@tmYLLabels             = (/"-~F8~p~N~","-~F8~p~N~/2","0","~F8~p~N~/2","~F8~p~N~"/)
      plot(m,2) = gsn_csm_xy(wks,time,oPHS,tres)
      
  ;end if
  ;-------------------------------------------------------------
  ; Overlay model RMM
  ;-------------------------------------------------------------
    lres@gsnCenterString        = ""
    lres@gsnRightString         = ""
    lres@gsnLeftString          = ""
    res@gsnCenterString         = ""
    res@gsnRightString          = ""
    res@gsnLeftString           = ""
    mres@gsnCenterString        = ""
    mres@gsnRightString         = ""
    mres@gsnLeftString          = ""
    do c = 0,num_c-1
      lres@xyLineThicknessF     = 3.
      lres@xyLineColor          = clr(m)
      lres@xyDashPattern        = 0
      overlay(plot(m,0),gsn_csm_xy(wks,time(c*5:c*5+nt-1),mAMP (c,:),lres))
      overlay(plot(m,2),gsn_csm_xy(wks,time(c*5:c*5+nt-1),mPHS (c,:),lres))
      overlay(plot(m,1),gsn_csm_xy(wks,time(c*5:c*5+nt-1),mRMM1(c,:),lres))
      overlay(plot(m,3),gsn_csm_xy(wks,time(c*5:c*5+nt-1),mRMM2(c,:),lres))
      mres@xyMarkerColor      = "black"
      overlay(plot(m,0),gsn_csm_xy(wks,(/time(c*5),time(c*5)/),(/mAMP(c,0),mAMP (c,0)/),mres))
      overlay(plot(m,2),gsn_csm_xy(wks,(/time(c*5),time(c*5)/),(/mPHS(c,0),mPHS (c,0)/),mres))
      overlay(plot(m,1),gsn_csm_xy(wks,(/time(c*5),time(c*5)/),(/mRMM1(c,0),mRMM1(c,0)/),mres))
      overlay(plot(m,3),gsn_csm_xy(wks,(/time(c*5),time(c*5)/),(/mRMM2(c,0),mRMM2(c,0)/),mres))
    end do
      lres@xyLineThicknessF = 1.
      lres@xyLineColor      = "black"
      lres@xyDashPattern        = 1
      overlay(plot(m,1),gsn_csm_xy(wks,time,time*0.,lres))
      overlay(plot(m,3),gsn_csm_xy(wks,time,time*0.,lres))
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
end do 
    
    pres = True
    ;pres@gsnPanelBottom            = .1
    ;pres@gsnPanelTop               = .9
    ;pres@txString                  = case_stub
    pres@gsnPanelXWhiteSpacePercent = 3.
    pres@gsnFrame                   = False
    pres@amJust                     = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF = 0.015
    pres@gsnPanelFigureStrings      = (/"a","b","c","d"/) 
    
    pplot = new(4,graphic)
    pplot(0) = plot(0,1)
    pplot(1) = plot(1,1)
    pplot(2) = plot(0,3)
    pplot(3) = plot(1,3)

  gsn_panel(wks,pplot,(/2,2/),pres)
  
    legend = create "Legend" legendClass wks 
    "lgAutoManage"             : False
    ;"vpXF"                     : 0.375
    ;"vpYF"                     : 0.2
    "vpXF"                      : 0.82
    "vpYF"                      : 0.47
    "vpWidthF"                  : 0.1  
    "vpHeightF"                 : 0.07   
    "lgPerimOn"                 : True   
    "lgItemCount"               : num_m+1
    "lgLabelStrings"            : case_name
    "lgLabelsOn"                : True     
    "lgLineLabelsOn"            : False     
    "lgLabelFontHeightF"        : 0.015
    "lgMonoDashIndex"           : True
    "lgDashIndex"               : 0
    "lgLineThicknessF"          : 3.
    "lgLineColors"              : clr
    "lgMonoLineLabelFontColor"  : True                  
  end create
  
  draw(legend)
  frame(wks)

    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
;end do
end


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin

	case      = (/"ERAi","90"/)
	case_name = (/"ERAi","SP-CAM"/)
	
	;case      = (/"90"/)
	;case_name = (/"SP-CAM"/)
	
	;case      = (/"ERAi"/)
	;case_name = (/"ERAi"/)
	
	rd    = (/00/)
	
	fig_type = "png"
	
	qvar = "Q1"

	lat1 = -5.
	lat2 =  5.
	lon1 = 60.
	lon2 = 70.
	
	fig_file = "~/DYNAMO/Hindcast/RMM.comp.v1"
;===================================================================================
; Load Obs RMM
;===================================================================================
  nt = 16*5
  ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.txt" 
  data = asciiread(ifile,(/31+30+31,7/),"float")
	oyr  = data(:nt-1,0)
	omn  = data(:nt-1,1)
	ody  = data(:nt-1,2)
	RMM1 = data(:nt-1,3)
	RMM2 = data(:nt-1,4)
	RMM1@mn = omn
    AMP = sqrt(RMM1^2+RMM2^2)
    pi = 3.14159
    PHS = round( (atan2(RMM2,RMM1)+pi)/(2.*pi) *8. ,3)%8
    PHS = (PHS+4)%8
;===================================================================================
;===================================================================================
  num_c = dimsizes(case)
  num_r = dimsizes(rd)
  lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150./) 
  olev = ispan(toint(max(lev)),toint(min(lev)),50)
  num_lev  = dimsizes(lev)
  num_olev = dimsizes(olev)
  ;-------------------------------------------------------------
  ; Set up workspace
  ;-------------------------------------------------------------
  wks  = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"BlueRed")
  plot = new(num_c*num_r,graphic)
  	res = True
  	res@gsnDraw  = False
  	res@gsnFrame = False
  	res@tmXBLabelFontHeightF 		= 0.015
  	res@tmYLLabelFontHeightF 		= 0.015
  	res@tiXAxisString 				= "RMM Phase"
  	;res@tiYAxisString 				= "RMM2"
  	res@tiXAxisFontHeightF 			= 0.015
  	res@tiYAxisFontHeightF 			= 0.015
  	res@tmXBMinorOn 					= False
  	res@tmYLMinorOn 					= False
  	res@tmXTOn 						= False
  	res@tmYROn						= False
  	res@gsnLeftStringFontHeightF		= 0.02
  	res@gsnRightStringFontHeightF	= 0.02
  	res@gsnRightString 				= ""
  	res@gsnLeftString  				= ""
  	res@trXReverse					= True
  	res@trYReverse					= True
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd(r))
  rde = sprinti("%0.2i",rd(r)+5-1)
  if rd(r).eq.0 then rdstr = "00-04 day lead" end if
  if rd(r).eq.5 then rdstr = "05-09 day lead" end if
  ;-------------------------------------------------------------
  ; Load Hindcast data
  ;-------------------------------------------------------------
  W = new((/num_c,num_olev,8/),float)
  U = new((/num_c,num_olev,8/),float)
  Q = new((/num_c,num_olev,8/),float)
  do c = 0,num_c-1
  	oname = "DYNAMO_"+case(c)+"_"+rds+"-"+rde
    Wfile = addfile("~/DYNAMO/Hindcast/data/"+oname+"/"+oname+".OMEGA.nc","r")
    Ufile = addfile("~/DYNAMO/Hindcast/data/"+oname+"/"+oname+".U.nc","r")
    Qfile = addfile("~/DYNAMO/Hindcast/data/"+oname+"/"+oname+"."+qvar+".nc","r")
    ;iW = dim_rmvmean_n_Wrap(Wfile->OMEGA		,0)
    ;iU = dim_rmvmean_n_Wrap(Ufile->U			,0)
    ;iQ = dim_rmvmean_n_Wrap(Qfile->$qvar$	,0)
    ;tW = linint1_n_Wrap(lev, avg4to1(dim_avg_n( iW(:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1)
    ;tU = linint1_n_Wrap(lev, avg4to1(dim_avg_n( iU(:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1)
    ;tQ = linint1_n_Wrap(lev, avg4to1(dim_avg_n( iQ(:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1)
    tW = dim_rmvmean_n_Wrap(linint1_n_Wrap(lev, avg4to1(dim_avg_n( Wfile->OMEGA (:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1) ,0)
    tU = dim_rmvmean_n_Wrap(linint1_n_Wrap(lev, avg4to1(dim_avg_n( Ufile->U     (:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1) ,0)
    tQ = dim_rmvmean_n_Wrap(linint1_n_Wrap(lev, avg4to1(dim_avg_n( Qfile->$qvar$(:,{lev},{lat1:lat2},{lon1:lon2}),(/2,3/)))  ,False,olev,0,1) ,0)
    do p = 0,7
      condition = conform(tW,PHS.eq.p,0)
      W(c,:,p) = dim_avg_n(where(condition,tW,tW@_FillValue),0)
      U(c,:,p) = dim_avg_n(where(condition,tU,tU@_FillValue),0)
      Q(c,:,p) = dim_avg_n(where(condition,tQ,tQ@_FillValue),0)
    end do
    ;delete([/iW,iU,iQ/])
    delete([/tW,tU,tQ,condition/])
  end do
  
  
  cc = 0
  fmt = "%4.2f"
  do p = 0,7
  str = ""
  do k = 0,num_olev-1
    str = str+"  "+sprintf(fmt,U(cc,k,p))
  end do
  print(""+str)
  end do
  print("")
  do p = 0,7
  str = ""
  do k = 0,num_olev-1
    str = str+"  "+sprintf(fmt,W(cc,k,p))
  end do
  print(""+str)
  end do
  print("")
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
    W!0 = "case"
    W!1 = "lev"
    W!2 = "phs"
    W&lev = olev
    W&phs = ispan(1,8,1)
    copy_VarCoords(W,U)
    copy_VarCoords(W,Q)
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
  		cres = res
  		vres = res
  		cres@gsnSpreadColors 			= True
  		cres@cnFillOn 				= True
  		cres@cnLinesOn 				= False
  		cres@cnLevelSelectionMode 	= "ExplicitLevels"
  		
  		if qvar.eq."Q1" then cres@cnLevels = ispan(-40,40,8)/1000. end if
  		;if qvar.eq."Q" then cres@cnLevels = ispan(-40,40,8)/1000. end if

  		;vres@vcFillArrowsOn           = True
  		vres@vcLineArrowThicknessF    =  2.0
  		;vres@vcMinFracLengthF         = 0.33
  		vres@vcMinMagnitudeF          = 1.
  		vres@vcMonoFillArrowFillColor = True
  		;vres@vcMonoLineArrowColor     = False
  		vres@vcRefLengthF             = 0.03
  		vres@vcRefMagnitudeF          = 5.0
  		;vres@vcRefAnnoOrthogonalPosF  = -0.12
 		;vres@vcRefAnnoParallelPosF    =  0.997
  		;vres@vcRefAnnoFontHeightF     = 0.015
  	
  	do c = 0,num_c-1
  		cres@gsnRightString = rdstr
  		cres@gsnLeftString  = case_name(c)
  	  ;plot(c+num_c*r) = gsn_csm_vector(wks,U(c,:,:),W(c,:,:)*100.,vres)
  	  ;plot(c+num_c*r) = gsn_csm_contour(wks,Q(c,:,:),cres)
  	  ;overlay(plot(c+num_c*r),gsn_csm_vector(wks,U(c,:,:),W(c,:,:)*100.,vres))
  	  
  	  plot(c+num_c*r) = gsn_csm_contour(wks,Q(c,:,:)-Q(0,:,:),cres)
  	  overlay(plot(c+num_c*r),gsn_csm_vector(wks,U(c,:,:)-U(0,:,:),(W(c,:,:)-W(0,:,:))*100.,vres))
  	end do
  
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
end do

		pnlres = True 
  		pnlres@gsnFrame       					= False
  		;pnlres@gsnPanelBottom       				= 0.15
  		pnlres@amJust   							= "TopLeft"
    		pnlres@gsnPanelFigureStringsFontHeightF 	= 0.015
    		pnlres@gsnPanelFigureStrings				= (/"a","b","c","d"/) 

  gsn_panel(wks,plot,(/num_r,num_c/),pnlres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"				: False
    ;"vpXF"                     	: 0.4
    ;"vpYF"                     	: 0.12
    "vpXF"                     	: 0.62
    "vpYF"                     	: 0.52-0.1
    "vpWidthF"                 	: 0.1
    "vpHeightF"                	: 0.1
    "lgPerimOn"                	: True
    "lgItemCount"              	: num_c+1
    "lgLabelStrings"           	: case_name
    "lgLabelsOn"               	: True
    "lgLineLabelsOn"           	: False
    "lgLabelFontHeightF"       	: 0.01
    "lgMonoLineThickness"		: False
    "lgLineThicknesses"         	: (/3.,3.,3.,5./)
    ;"lgLineColors"             	: clr
    "lgDashIndexes"				: ispan(0,num_c+1,1)*0
    "lgMonoLineLabelFontColor" 	: True
    "lgBoxBackground"			: "white"
    "lgPerimFillColor"			: "white"
    "lgPerimFill"				: "SolidFill"
  end create

  ;draw(legend)
  frame(wks)

	print("")
	print("	"+fig_file+"."+fig_type)
	print("")
	
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    top_dir = "~/Data/DYNAMO/"
    HCdatadir = "/var/run/media/whannah/Helios/DYNAMO/Hindcast/data/"

    yr = 2011
    mn = (/10,10,10,10,10,10,10,11,11,11,11,11,11,12,12,12/)
    dy = (/01,06,11,16,21,26,31,05,10,15,20,25,30,05,10,15/)


    yrmndy = toint( yr*10000 + mn*100 + dy )

    ;member = "09"
    member = sprinti("%0.2i",imem)

    case = (/"DYNAMO_"+member+"_f09_"/)+yrmndy

    lat1 = -15.
    lat2 =  15.
    lon1 =   0.
    lon2 = 360.

    num_h = 3		; number of harmonics for seasonal cycle

    recalc = True
    mkplot = False

    sigma1 = sqrt(55.43719)
    sigma2 = sqrt(52.64146)

    wind_only = False
;===================================================================================
; Coordinates
;===================================================================================
    opt  = True
    opt@lat1 = lat1
    opt@lat2 = lat2
    opt@lon1 = lon1
    opt@lon2 = lon2 
    	
    num_c = dimsizes(case)

    lat = LoadHClat(case(0),opt)
    lon = LoadHClon(case(0),opt)

    num_t   = 20
    time    = (tofloat(ispan(0,num_t-1,1))*(24.))/24.
    time@units = "days since "+yr(0)+"-"+mn(0)+"-"+dy(0)+""

    ifile  = top_dir+"NCEP/NCEP.U850.1980-2011.nc"
    infile = addfile(ifile,"r")
    olat = infile->lat({lat1:lat2})
    olon = infile->lon({lon1:lon2})
    num_lat = dimsizes(olat)
    num_lon = dimsizes(olon)
;===================================================================================
; Load obs data
;===================================================================================
    ifile = top_dir+"NCEP/NCEP.RMM.seasonal_cycle.1980-2010.nc"
    infile = addfile(ifile,"r")
    HU850 = dim_sum_n_Wrap(infile->HU850(:num_h-1,:,{lat1:lat2},{lon1:lon2}),0)
    HU200 = dim_sum_n_Wrap(infile->HU200(:num_h-1,:,{lat1:lat2},{lon1:lon2}),0)
    ;HOLR  =   infile->HOLR (:num_h-1,:,{lat1:lat2},{lon1:lon2}),0)
    HOLR  = dim_sum_n_Wrap(infile->HOLR (:num_h-1,:,{lat1:lat2},{lon1:lon2}),0)
    ;-----------------------------------------------------------------------------
    ifile = top_dir+"NCEP/NCEP.RMM.climo.1980-2010.nc"
    infile = addfile(ifile,"r")
    meanU850 = conform(HU850, infile->U850({lat1:lat2},{lon1:lon2}) ,(/1,2/))
    meanU200 = conform(HU200, infile->U200({lat1:lat2},{lon1:lon2}) ,(/1,2/))
    meanOLR  = conform(HOLR , infile->OLR ({lat1:lat2},{lon1:lon2}) ,(/1,2/))
    ;-----------------------------------------------------------------------------
    t1 = (yr-1980)*365*4
    t2 = t1+365*4-1
    print("  loading NCEP U850")
    infile = addfile(top_dir+"NCEP/NCEP.U850.1980-2011.nc","r")
    itmp = infile->U(t1:t2,{lat1:lat2},{lon1:lon2})
    ncepU850 = ( itmp(0::4,:,:) +itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:) )/4.
    delete([/itmp/])
    ;----------------------------------------
    print("  loading NCEP U200")
    infile = addfile(top_dir+"NCEP/NCEP.U200.1980-2011.nc","r")
    itmp = infile->U(t1:t2,{lat1:lat2},{lon1:lon2})
    ncepU200 = ( itmp(0::4,:,:) +itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:) )/4.
    delete([/itmp/])
    ;----------------------------------------
    t1 = (yr-1980)*365
    t2 = t1+365-1
    print("  loading NOAA OLR")
    infile = addfile(top_dir+"OLR/olr.daily.1980-2011.nc","r")
    ncepOLR = infile->OLR(t1:t2,{lat1:lat2},{lon1:lon2})
;===================================================================================
; Load historical RMM data for normalizing (std.dev.)
;===================================================================================  
    data = asciiread("~/Research/DYNAMO/RMM/RMM_DYNAMO.alt.txt",(/30+31+30+31,7/),"float")    
    oRMM1 = data(30:,3)
    oRMM2 = data(30:,4)
    delete([/data/])
;===================================================================================
; Caclulate RMM
;===================================================================================
h0 = 31+28+31+30+31+30+31+31+30
do c = 0,num_c-1
  new_dir = top_dir+"Hindcast/raw/"+case(c)+"/"
  if .not.fileexists(new_dir) then system("mkdir "+new_dir) end if
  if recalc then
    if mn(c).eq.10
      hd1 = h0 +dy(c)-1
    end if
    if mn(c).eq.11
      hd1 = h0 +31+dy(c)-1
    end if
    if mn(c).eq.12
      hd1 = h0 +31+30+dy(c)-1
    end if
    hdvals = ispan(hd1,hd1+num_t-1,1)%365
    ;================================================================
    ; Load simulation data
    ;================================================================
      print("  loading hindcast simulations...(U850)")
      mU850  = area_hi2lores_Wrap(lon,lat, block_avg(LoadHC(case(c),"U"   ,850.,opt),4) ,True,1.,olon,olat,False)
      print("  loading hindcast simulations...(U200)")
      mU200  = area_hi2lores_Wrap(lon,lat, block_avg(LoadHC(case(c),"U"   ,200.,opt),4) ,True,1.,olon,olat,False)
      print("  loading hindcast simulations...(OLR)")
      mOLR   = area_hi2lores_Wrap(lon,lat, block_avg(LoadHC(case(c),"FLUT",-99.,opt),4) ,True,1.,olon,olat,False)
      print("  done.")
    ;================================================================
    ; Remove mean
    ;================================================================
      mU850(:num_t-1,:,:) = mU850(:num_t-1,:,:) - (/ meanU850(:num_t-1,:,:) /)
      mU200(:num_t-1,:,:) = mU200(:num_t-1,:,:) - (/ meanU200(:num_t-1,:,:) /)
      mOLR (:num_t-1,:,:) = mOLR (:num_t-1,:,:) - (/ meanOLR (:num_t-1,:,:) /)
      ;----------------------------------------
      ncepU850 = ncepU850 - (/ meanU850 /)
      ncepU200 = ncepU200 - (/ meanU200 /)
      ncepOLR  = ncepOLR  - (/ meanOLR  /)
    ;================================================================
    ; Remove observed climatological seaonal cycle
    ;================================================================
      print("  Removing seasonal cycle...")
      mU850(:num_t-1,:,:) = (/ mU850(:num_t-1,:,:) - HU850(hdvals,:,:) /)
      mU200(:num_t-1,:,:) = (/ mU200(:num_t-1,:,:) - HU200(hdvals,:,:) /)
      mOLR (:num_t-1,:,:) = (/ mOLR (:num_t-1,:,:) - HOLR (hdvals,:,:) /)
      ;----------------------------------------
      ncepU850 = (/ ncepU850 - HU850 /)
      ncepU200 = (/ ncepU200 - HU200 /)
      ncepOLR  = (/ ncepOLR  - HOLR  /)
      print("  done.")
    ;================================================================
    ; Remove 120-mean
    ;================================================================
      ;mlen = 120
      ;do t = 0,20-1
      ;  ;mU850(t,:,:) = (/ mU850(t,:,:) - ( dim_sum_n(mU850(:t,:,:),0)+dim_sum_n(ncepU850(hd1-mlen+t:hd1,:,:),0) )/120. /)
      ;  ;mU200(t,:,:) = (/ mU200(t,:,:) - ( dim_sum_n(mU200(:t,:,:),0)+dim_sum_n(ncepU200(hd1-mlen+t:hd1,:,:),0) )/120. /)
      ;  ;mOLR (t,:,:) = (/ mOLR (t,:,:) - ( dim_sum_n(mOLR (:t,:,:),0)+dim_sum_n(ncepOLR (hd1-mlen+t:hd1,:,:),0) )/120. /)
      ;  mU850(t,:,:) = (/ mU850(t,:,:) - dim_avg_n(ncepU850(hd1+t-mlen:hd1+t,:,:),0) /)
      ;  mU200(t,:,:) = (/ mU200(t,:,:) - dim_avg_n(ncepU200(hd1+t-mlen:hd1+t,:,:),0) /)
      ;  mOLR (t,:,:) = (/ mOLR (t,:,:) - dim_avg_n(ncepOLR (hd1+t-mlen:hd1+t,:,:),0) /)
      ;end do
      if False
	      mlen = 120
	      t  = 0
	      U1 = mU850(t,:,:)
	      U2 = U1
	      U3 = U1
	      U4 = U1
	      U2 = (/ mU850(t,:,:) - ( dim_sum_n(mU850(:t,:,:),0)+dim_sum_n(ncepU850(hd1-mlen+t:hd1,:,:),0) )/120. /)
	      U3 = ( dim_sum_n(mU850(:t,:,:),0)+dim_sum_n(ncepU850(hd1-mlen+t:hd1,:,:),0) )/120.
	      UT = (/ ncepU850 + HU850 /)
	      U4 =   dim_avg_n(UT(hd1-mlen+t:hd1,:,:),0)
	      
	      wks = gsn_open_wks("png","test.RMM")
	      plot = new(4,graphic)
	      		res = True	
	      		res@gsnDraw 		= False
	  		res@gsnFrame 		= False
	  		res@gsnSpreadColors 	= True
	  		res@cnFillOn 		= True
	  		res@cnLinesOn 		= False
	  		res@mpCenterLonF 	= 180.
			res@mpLimitMode 	= "LatLon"
			res@mpMinLatF 		= -30.
			res@mpMaxLatF 		=  30.
	  		res@gsnAddCyclic 	= False
	  		res@lbLabelFontHeightF	= 0.015
	      plot(0) = gsn_csm_contour_map(wks,U1,res)
	      plot(1) = gsn_csm_contour_map(wks,U2,res)
	      plot(2) = gsn_csm_contour_map(wks,U3,res)
	      plot(3) = gsn_csm_contour_map(wks,U4,res)
	      gsn_panel(wks,plot,(/dimsizes(plot),1/),False)
	      exit
      end if
    ;================================================================
    ; Average Across the Equator
    ;================================================================
      eU850 = dim_avg_n(mU850(:num_t-1,:,:),1) /  1.81
      eU200 = dim_avg_n(mU200(:num_t-1,:,:),1) /  4.81
      eOLR  = dim_avg_n(mOLR (:num_t-1,:,:),1) / 15.1
        delete([/mOLR,mU850,mU200/])
    ;================================================================
    ; Write temporary model RMM file
    ;================================================================
    ofile = top_dir+"Hindcast/raw/"+case(c)+"/temp_RMM."+case(c)+".nc"
    ;ofile = hc_dir+case(c)+"/temp_RMM."+case(c)+".nc"
    if isfilepresent(ofile) then
      system("rm "+ofile)
    end if
    outfile = addfile(ofile,"c")
    outfile->eU850 = eU850
    outfile->eU200 = eU200
    outfile->eOLR  = eOLR
  else
    ifile = top_dir+"Hindcast/raw/"+case(c)+"/temp_RMM."+case(c)+".nc"
    ;ifile = hc_dir+case(c)+"/temp_RMM."+case(c)+".nc"
    infile = addfile(ifile,"r")
    eU850 = infile->eU850
    eU200 = infile->eU200
    eOLR  = infile->eOLR
  end if
  ;================================================================
  ; Create Vector for projection
  ;================================================================
    V = new((/num_t,3*num_lon/),float)
    
    if .not.wind_only then
     i = 0
     V(:,i*num_lon:(i+1)*num_lon-1) = (/ eOLR /)
    end if
    
    i = 1
    V(:,i*num_lon:(i+1)*num_lon-1) = (/ eU850 /)
    
    i = 2
    V(:,i*num_lon:(i+1)*num_lon-1) = (/ eU200 /)
      ;delete([/eOLR,eU200,eU850/])
  ;================================================================
  ; Project onto EOFs
  ;================================================================
  ifile = "~/Research/DYNAMO/Hindcast/RMM_EOF_Structures.txt"
  data = asciiread(ifile,23+144*2*3,"float")
  EOF1 = data(23::2) 
  EOF2 = data(24::2) 
    delete([/data/])
	
	  if False then
	  wks = gsn_open_wks("x11","test")
	  plot = new(4,graphic)
	  	res = True
	  	res@gsnDraw = False
	  	res@gsnFrame = False
	  plot(0) = gsn_csm_y(wks,(/V(0,:)*EOF1,V(0,:)*EOF2/),res)
	  plot(2) = gsn_csm_y(wks,(/EOF1,EOF2/),res)
	  i = 1
	  x1 = 144*i
	  x2 = 144*(i+1)-1
	  plot(1) = gsn_csm_y(wks,(/V(0,x1:x2)*EOF1(x1:x2),V(0,x1:x2)*EOF2(x1:x2)/),res)
	  i = 2
	  x1 = 144*i
	  x2 = 144*(i+1)-1
	  plot(3) = gsn_csm_y(wks,(/V(0,x1:x2)*EOF1(x1:x2),V(0,x1:x2)*EOF2(x1:x2)/),res)
	  gsn_panel(wks,plot,(/2,2/),False)
	  ;exit
	  end if
	  

  
  RMM1 = dim_sum_n(V*conform(V,EOF1,1),1) /sigma1
  RMM2 = dim_sum_n(V*conform(V,EOF2,1),1) /sigma2

    delete([/V/])  
  ;================================================================
  ;================================================================
  print("")
  
  	if isvar("tmp") then
  	  delete(tmp)
  	end if 
  	t = 1
  
    mS =  RMM1(t)^2.+ RMM2(t)^2.
    oS = oRMM1(t)^2.+oRMM2(t)^2.
  	
  	tmp = eOLR(t,:)
  	i = 0
  	x1 = num_lon*i
    x2 = num_lon*(i+1)-1
  	A1 = sum( (/RMM1(t),RMM2(t)/)*(/ sum(tmp*EOF1(x1:x2))/sigma1 , sum(tmp*EOF2(x1:x2))/sigma2 /) )
  	tmp = eU850(t,:)
  	i = 1
  	x1 = num_lon*i
    x2 = num_lon*(i+1)-1
  	A2 = sum( (/RMM1(t),RMM2(t)/)*(/ sum(tmp*EOF1(x1:x2))/sigma1 , sum(tmp*EOF2(x1:x2))/sigma2 /) )
  	tmp = eU200(t,:)
  	i = 2
  	x1 = num_lon*i
    x2 = num_lon*(i+1)-1
  	A3 = sum( (/RMM1(t),RMM2(t)/)*(/ sum(tmp*EOF1(x1:x2))/sigma1 , sum(tmp*EOF2(x1:x2))/sigma2 /) ) 
  
  
  pr = 5
  fmt = "%4.4f"
  print("")
  print("OLR:  "+sprintf(fmt,(A1/mS))+"		"+sprintf(fmt,(A1/oS))+"	(0.1213689)"+"		"+sprintf(fmt,A1)+"		"+sprintf(fmt,(oS*0.1213689)))
  print("U850: "+sprintf(fmt,(A2/mS))+"		"+sprintf(fmt,(A2/oS))+"	(0.2738290)"+"		"+sprintf(fmt,A2)+"		"+sprintf(fmt,(oS*0.2738290)))
  print("U200: "+sprintf(fmt,(A3/mS))+"		"+sprintf(fmt,(A3/oS))+"	(0.6048022)"+"		"+sprintf(fmt,A3)+"		"+sprintf(fmt,(oS*0.6048022)))
  print("")
  print("mAMP^2	"+mS)
  print("oAMP^2	"+oS)
  
  print("")
  print("	"+sqrt(RMM1(:num_t-1)^2.+ RMM2(:num_t-1)^2.)+"		"+sqrt(oRMM1(:num_t-1)^2.+oRMM2(:num_t-1)^2.))

  print("")
  mSt =  RMM1^2.+ RMM2^2.
    i = 2
  	x1 = 144*i
    x2 = 144*(i+1)-1
    do t = 0,num_t-1
      a1 = sum( RMM1(t)*(eU200(t,:)*EOF1(x1:x2))/sigma1 )
      a2 = sum( RMM2(t)*(eU200(t,:)*EOF2(x1:x2))/sigma2 )
      print("	U200	 contribution:	"+((a1+a2)/mSt(t)))
    end do
    
    delete([/tmp,A1,A2,A3,x1,x2/])
  
  ;================================================================
  ; plotting code for sanity check
  ;================================================================
	if mkplot then
	  ot1 = (mn(c)-10)*31+dy(c)-1
	  ot2 = (mn(c)-10)*31+dy(c)-1+num_t-1
	  wks = gsn_open_wks("x11","test")
	  plot = new(4,graphic)
		res = True
		res@gsnDraw = False
		res@gsnFrame = False
	  oAMP = sqrt(oRMM1(ot1:ot2)^2.+oRMM2(ot1:ot2)^2.)
	  mAMP = sqrt( RMM1  ^2.+ RMM2  ^2.)
	  oPHS = atan2(oRMM2(ot1:ot2),oRMM1(ot1:ot2))
	  mPHS = atan2( RMM2  , RMM1)
	  plot(0) = gsn_csm_y(wks,(/oAMP,mAMP/),res)
	  plot(1) = gsn_csm_y(wks,(/oPHS,mPHS/),res)
	  plot(2) = gsn_csm_y(wks,(/oRMM1(ot1:ot2),RMM1/),res)
	  plot(3) = gsn_csm_y(wks,(/oRMM2(ot1:ot2),RMM2/),res)
	  gsn_panel(wks,plot,(/2,2/),False)
	  exit
	end if
  ;================================================================
  ; Write model RMM to file
  ;================================================================  
  if wind_only then
    ofile = top_dir+"Hindcast/raw/"+case(c)+"/"+case(c)+".RMM.120_retained.wind_only.nc"
    ;ofile = hc_dir+case(c)+"/"+case(c)+".RMM.120_retained.wind_only.nc"
  else
    ofile = top_dir+"Hindcast/raw/"+case(c)+"/"+case(c)+".RMM.120_retained.nc"
    ;ofile = hc_dir+case(c)+"/"+case(c)+".RMM.120_retained.nc"
  end if

  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")
  outfile->RMM1 = RMM1
  outfile->RMM2 = RMM2
  
  	print("")
  	print("  "+ofile)
  	print("")
  
    delete([/RMM1,RMM2/])
    delete([/eOLR,eU200,eU850/])
  
end do
;===================================================================================
;===================================================================================
end

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin

	case      = (/"09","90"/)
	case_name = (/"ZM_1.0","SP-CAM","RMM"/)

	clr = (/"blue","purple","black"/)

	;rd    = (/00,05/)
	;rdstr = (/"00-04 day lead","05-09 day lead"/)
	
	rd    = 00
	;rd    = 05
	
	if rd.eq.0 then rdstr = "00-04 day lead" end if
	if rd.eq.5 then rdstr = "05-09 day lead" end if
	
	idir = "~/DYNAMO/Hindcast/"
	odir = "~/DYNAMO/Hindcast/"
	fig_type = "png"
	
	wind_only = False
	olr_only  = False
	
	mn = (/10,11/)
	mnstr = (/"01 Oct - 31 Oct","10 Nov - 8 Dec"/)

	pi = 3.14159
	
	fig_file = odir+"RMM.phase.v4"
	if rd.eq.0 then fig_file = fig_file+".00-04" end if
	if rd.eq.5 then fig_file = fig_file+".05-09" end if
	if wind_only then fig_file = fig_file+".wind_only" end if
	if olr_only  then fig_file = fig_file+".olr_only"  end if
;===================================================================================
; Load Obs RMM
;===================================================================================
  if (.not.wind_only).and.(.not.olr_only) then ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.txt" end if
  if     wind_only then ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.wind_only.txt" end if
  if     olr_only  then ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.olr_only.txt"  end if
  data = asciiread(ifile,(/31+30+31,7/),"float")
	oyr = data(:,0)
	omn = data(:,1)
	ody = data(:,2)
	RMM1 = data(:,3)
	RMM2 = data(:,4)
	RMM1@mn = omn
	
print(sqrt(RMM1^2+RMM2^2))
exit
;===================================================================================
;===================================================================================
  num_c = dimsizes(case)
  num_r = dimsizes(rd)
  num_m = dimsizes(mn)
  num_t = 16*5
  ;-------------------------------------------------------------
  ; Set up workspace
  ;-------------------------------------------------------------
  wks  = gsn_open_wks(fig_type,fig_file)
  plot = new(num_m*num_r,graphic)
  	res = True
  	res@gsnDraw  = False
  	res@gsnFrame = False
  	res@trXMinF  = -4.
  	res@trXMaxF  =  4.
  	res@trYMinF  = -4.
  	res@trYMaxF  =  4.
  	res@tmXBLabelFontHeightF = 0.015
  	res@tmYLLabelFontHeightF = 0.015
  	res@tiXAxisString 	= "RMM1"
  	res@tiYAxisString 	= "RMM2"
  	res@tiXAxisFontHeightF 	= 0.015
  	res@tiYAxisFontHeightF 	= 0.015
  	res@tmXBMinorOn = False
  	res@tmYLMinorOn = False
  	res@tmXTOn 		= False
  	res@tmYROn		= False
  	cres = res
  	cres@gsnLeftStringFontHeightF	= 0.02
  	cres@gsnRightStringFontHeightF	= 0.02
do r = 0,num_r-1
  ;-------------------------------------------------------------
  ; Load Hindcast RMM
  ;-------------------------------------------------------------
  hRMM1 = new((/num_c,num_t/),float)
  hRMM2 = new((/num_c,num_t/),float)
  do c = 0,num_c-1
    oname = "DYNAMO_"+case(c)+"_"+sprinti("%0.2i",rd(r))+"-"+sprinti("%0.2i",rd(r)+5-1)
    ifile = idir+"data/"+oname+"/"+oname+".RMM."
    if wind_only then ifile = ifile+"wind_only." end if
    if olr_only  then ifile = ifile+"olr_only."  end if
    ifile = ifile+"nc"
    print(""+ifile)
    infile = addfile(ifile,"r")
    hRMM1(c,:) = (/ infile->RMM1 /)
    hRMM2(c,:) = (/ infile->RMM2 /)
  end do
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
	the  	= fspan(0.,2.*pi,100)
	rad 		= fspan(1.,6.,2)
	xx 		= fspan(-10.,10.,2)
	theta 	= fspan(-pi,pi,9)
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
do m = 0,num_m-1
  p = m+r*num_m
    if mn(m).eq.10 then t1 =  0 end if
  	if mn(m).eq.10 then t2 = 30 end if
  	if mn(m).eq.11 then t1 = 31+10 end if
  	if mn(m).eq.11 then t2 = 61+8 end if
  ;-------------------------------------------------------------
  ; Plot basic phase space
  ;-------------------------------------------------------------
  	cres@gsnDraw = False
  	cres@gsnFrame = False
  	cres@xyLineColor = "black"
  	cres@xyDashPattern = 0
  	cres@gsnLeftString  = mnstr(m)
  	cres@gsnRightString = rdstr(r)
  plot(p) = gsn_csm_xy(wks,cos(the),sin(the),cres)
  	cres@gsnLeftString  = ""
  	cres@gsnRightString = ""
    cres@xyLineColor = "grey"
  	overlay(plot(p),gsn_csm_xy(wks,2.*cos(the),2.*sin(the),cres))
  	overlay(plot(p),gsn_csm_xy(wks,3.*cos(the),3.*sin(the),cres))
  	overlay(plot(p),gsn_csm_xy(wks,4.*cos(the),4.*sin(the),cres))
  	  ;delete(the)
  	cres@xyLineColor = "black"
  	cres@xyDashPattern = 2
  	do a = 0,dimsizes(theta)-1
  	  overlay(plot(p),gsn_csm_xy(wks,rad*cos(theta(a)),rad*sin(theta(a)),cres))
  	end do
  	txres               = True
  	txres@txFontHeightF = 0.015
  	R = 4.
  	offset = pi/8.
  	do a = 0,dimsizes(theta)-2
  	  angle = theta(a)+offset
  	  dum = gsn_add_text(wks,plot(p),a+1,R*cos(angle),R*sin(angle),txres)
  	end do
  ;-------------------------------------------------------------
  ; Overlay obs RMM
  ;-------------------------------------------------------------
  	res@xyDashPattern 		= 0
  	res@xyLineThicknessF 	= 5.
  	res@xyLineColor 			= "black"
    ;vals = ind(omn.eq.mn(m)) + rd(r)
    ;overlay(plot(p),gsn_csm_xy(wks,RMM1(vals),RMM2(vals),res))
    ;delete(vals)
    overlay(plot(p),gsn_csm_xy(wks,RMM1(t1+rd(r):t2+rd(r)),RMM2(t1+rd(r):t2+rd(r)),res))
  ;-------------------------------------------------------------
  ; Overlay model RMM
  ;-------------------------------------------------------------
  	mres = res
  	mres@xyMarkLineMode 		= "Markers"
  	mres@xyMarkerThicknessF	= 1.
  	mres@xyMarkerSizeF		= 0.01
  	res@xyDashPattern 		= 0
  	res@xyLineThicknessF 	= 3.  	
  	do c = 0,num_c-1
  	  res@xyLineColor	  	= clr(c)
  	  overlay(plot(p),gsn_csm_xy(wks,hRMM1(c,t1:t2),hRMM2(c,t1:t2),res))
  	  mres@xyMarkerColors 	= res@xyLineColor
  	  mres@xyMarker			= 14
  	  overlay(plot(p),gsn_csm_xy(wks,hRMM1(c,t1)*(/1,1/),hRMM2(c,t1)*(/1,1/),mres))
  	  mres@xyMarker			= 15
  	  overlay(plot(p),gsn_csm_xy(wks,hRMM1(c,t2)*(/1,1/),hRMM2(c,t2)*(/1,1/),mres))
  	end do
  ;-------------------------------------------------------------
  ;  Overlay position marker
  ;-------------------------------------------------------------
  	pres = True
  	pres@gsnDraw = False
  	pres@gsnFrame = False
  	pres@xyMarkLineMode 		= "Markers"
  	pres@xyMarkerColor 		= "black"
  	pres@xyMarkerThicknessF	= 2.
  	pres@xyMarkerSizeF		= 0.02
  	pres@xyMarker				= 14
    overlay(plot(p),gsn_csm_xy(wks,RMM1(t1+rd(r))*(/1,1/),RMM2(t1+rd(r))*(/1,1/),pres))
    pres@xyMarker				= 15
    ;if mn(m).eq.11 then t2 = t2-1 end if
    overlay(plot(p),gsn_csm_xy(wks,RMM1(t2+rd(r))*(/1,1/),RMM2(t2+rd(r))*(/1,1/),pres))
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
  
end do
end do

		pnlres = True 
  		pnlres@gsnFrame       					= False
  		;pnlres@gsnPanelBottom       				= 0.15
  		pnlres@amJust   							= "TopLeft"
    		pnlres@gsnPanelFigureStringsFontHeightF 	= 0.015
    		pnlres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)"/) 

  gsn_panel(wks,plot,(/num_r,num_m/),pnlres)
  
  legend = create "Legend" legendClass wks
    "lgAutoManage"				: False
    ;"vpXF"                     	: 0.4
    ;"vpYF"                     	: 0.12
    "vpXF"                     	: 0.62
    "vpYF"                     	: 0.52-0.1
    "vpWidthF"                 	: 0.1
    "vpHeightF"                	: 0.1
    "lgPerimOn"                	: True
    "lgItemCount"              	: num_c+1
    "lgLabelStrings"           	: case_name
    "lgLabelsOn"               	: True
    "lgLineLabelsOn"           	: False
    "lgLabelFontHeightF"       	: 0.01
    "lgMonoLineThickness"		: False
    "lgLineThicknesses"         	: (/3.,3.,3.,5./)
    "lgLineColors"             	: clr
    "lgDashIndexes"				: ispan(0,num_c+1,1)*0
    "lgMonoLineLabelFontColor" 	: True
    "lgBoxBackground"			: "white"
    "lgPerimFillColor"			: "white"
    "lgPerimFill"				: "SolidFill"
  end create

  draw(legend)
  frame(wks)

	print("")
	print("	"+fig_file+"."+fig_type)
	print("")
	
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
	
  delete([/res,mres,txres/])
;===================================================================================
;===================================================================================
end

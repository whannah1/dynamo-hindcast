load "$NCARG_ROOT/custom_functions.ncl"
begin
	;member = (/"09","12","13"/)
    member = (/"12"/)
	num_m = dimsizes(member)
	
    log_dir = "~/Research/DYNAMO/Hindcast/logs/"
    cdir    = "~/Research/DYNAMO/Hindcast/code_RMM/"

    ;---------------------------------------
    ;---------------------------------------
    do m = 0,num_m-1
        printline()
        
        tlog = log_dir+"RMM."+member(m)+".out"
        print("  "+tlog)
        system("ncl imem='"+member(m)+"' "+cdir+"mk.RMM.ncl                 > "+tlog)
        
        ;tlog = log_dir+"RMM.dc."+member(m)+".out"
        ;print("  "+tlog)
        ;system("ncl imem='"+member(m)+"' "+cdir+"mk.RMM.drift_corrected.ncl ");"> "+tlog)
    end do
    ;---------------------------------------
    ;---------------------------------------
    printline()

end


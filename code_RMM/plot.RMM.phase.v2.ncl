load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
;load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"

begin
	member = (/"09","90"/)
	
	yr = 2011
	mn = 11
	dy = 15
	
	wind_only = False
	

	yrmndy = toint( yr*10000 + mn*100 + dy )
	case_stub = "DYNAMO_"+member+"_f09"

	
	odir = "~/DYNAMO/Hindcast/figures/"
	fig_type = "x11"

	pi = 3.14159
;===================================================================================
; Load Obs RMM
;===================================================================================
  ;ifile = "~/DYNAMO/RMM_DYNAMO.txt"
  ifile = "~/DYNAMO/RMM_DYNAMO.w-inter-ann.txt"
  data = asciiread(ifile,(/31+30+31,7/),"float")
	oyr = data(:,0)
	omn = data(:,1)
	ody = data(:,2)
	RMM1 = data(:,3)
	RMM2 = data(:,4)
	RMM1@mn = omn
;===================================================================================
;===================================================================================
num_d = dimsizes(yrmndy)
do d = 0,num_d-1
  case = case_stub+"_"+yrmndy(d)
  if wind_only then
    fig_file = odir+"RMM.phase.alt.wind_only."+yrmndy(d)
  else
    fig_file = odir+"RMM.phase.alt."+yrmndy(d)
  end if
  num_c = dimsizes(case)
  hRMM1 = new((/num_c,10/),float)
  hRMM2 = new((/num_c,10/),float)
  ;-------------------------------------------------------------
  ; Load Hindcast RMM
  ;-------------------------------------------------------------
  do c = 0,num_c-1
    if wind_only then
      ifile = "~/DYNAMO/Hindcast/data/"+case(c)+"/"+case(c)+".RMM.120_retained.wind_only.nc"
    else
      ifile = "~/DYNAMO/Hindcast/data/"+case(c)+"/"+case(c)+".RMM.120_retained.nc"
    end if
    infile = addfile(ifile,"r")
    hRMM1(c,:) = (/ infile->RMM1(:9) /)
    hRMM2(c,:) = (/ infile->RMM2(:9) /)
  end do
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
	t  = fspan(0.,2.*pi,100)
	r  = fspan(1.,6.,2)
	xx = fspan(-10.,10.,2)
	theta = fspan(-pi,pi,9)
  ;-------------------------------------------------------------
  ; Set up workspace
  ;-------------------------------------------------------------
  wks = gsn_open_wks(fig_type,fig_file)
  	res = True
  	res@gsnDraw  = False
  	res@gsnFrame = False
  	res@trXMinF  = -4.
  	res@trXMaxF  =  4.
  	res@trYMinF  = -4.
  	res@trYMaxF  =  4.
  	res@tmXBLabelFontHeightF = 0.015
  	res@tmYLLabelFontHeightF = 0.015
  	res@tiXAxisString 	= "RMM1"
  	res@tiYAxisString 	= "RMM2"
  	res@tiXAxisFontHeightF 	= 0.015
  	res@tiYAxisFontHeightF 	= 0.015
  	res@tmXBMinorOn = False
  	res@tmYLMinorOn = False
  	
  	
  ;-------------------------------------------------------------
  ; Plot basic phase space
  ;-------------------------------------------------------------
  plot = gsn_csm_xy(wks,cos(t),sin(t),res)
  	res@xyLineColor = "grey"
  	overlay(plot,gsn_csm_xy(wks,2.*cos(t),2.*sin(t),res))
  	overlay(plot,gsn_csm_xy(wks,3.*cos(t),3.*sin(t),res))
  	overlay(plot,gsn_csm_xy(wks,4.*cos(t),4.*sin(t),res))
  	  delete(t)
  	res@xyLineColor = "black"
  	res@xyDashPattern = 2
  	do a = 0,dimsizes(theta)-1
  	  overlay(plot,gsn_csm_xy(wks,r*cos(theta(a)),r*sin(theta(a)),res))
  	end do
  	txres               = True
  	txres@txFontHeightF = 0.015
  	R = 4.
  	offset = pi/8.
  	do a = 0,dimsizes(theta)-2
  	  angle = theta(a)+offset
  	  dum = gsn_add_text(wks,plot,a+1,R*cos(angle),R*sin(angle),txres)
  	end do
  ;-------------------------------------------------------------
  ; Overlay obs RMM
  ;-------------------------------------------------------------
  	res@xyDashPattern 	= 0
  	res@xyLineThicknessF 	= 2.5
  ;color = (/"red","blue","green"/)		
  do m = mn,mn
    	;res@xyMarkerColors = color;(m-10)
  	;res@xyLineColors   = color;(m-10)
    vals = ind(omn.eq.m)
    overlay(plot,gsn_csm_xy(wks,RMM1(vals),RMM2(vals),res))
    if m.lt.12 then
     vals = vals+1
     overlay(plot,gsn_csm_xy(wks,RMM1(vals),RMM2(vals),res))
    end if
    delete(vals)
  end do
  ;-------------------------------------------------------------
  ; Overlay model RMM
  ;-------------------------------------------------------------
  	mres = res
  	mres@xyMarkLineMode 	= "Markers"
  	mres@xyMarkerThicknessF	= 1.
  	mres@xyMarkerSizeF	= 0.01
  	res@xyDashPattern 	= 0
  	res@xyLineThicknessF 	= 1.
  	color = (/"blue","red","green"/)
  	do c = 0,num_c-1
  	  res@xyLineColor	  = color(c)
  	  overlay(plot,gsn_csm_xy(wks,hRMM1(c,:),hRMM2(c,:),res))
  	  mres@xyMarkerColors 	= res@xyLineColor
  	  mres@xyMarker		= 14
  	  overlay(plot,gsn_csm_xy(wks,hRMM1(c,0)*(/1,1/),hRMM2(c,0)*(/1,1/),mres))
  	  mres@xyMarker		= 15
  	  overlay(plot,gsn_csm_xy(wks,hRMM1(c,9)*(/1,1/),hRMM2(c,9)*(/1,1/),mres))
  	end do
  ;-------------------------------------------------------------
  ;  Overlay position marker
  ;-------------------------------------------------------------
  	res@xyMarkLineMode 	= "Markers"
  	res@xyMarkerColor 	= "black"
  	res@xyMarker		= 14
  	res@xyMarkerThicknessF	= 2.
  	res@xyMarkerSizeF	= 0.02
  tt = (mn-10)*31+dy(d)-1
    overlay(plot,gsn_csm_xy(wks,RMM1(tt)*(/1,1/),RMM2(tt)*(/1,1/),res))
  ;-------------------------------------------------------------
  ;-------------------------------------------------------------
  	gres = True
  	gres@lgLineColors           = (/"black","blue","red"/)
  	gres@pmLegendDisplayMode    = "Always" 
  	gres@lgPerimOn              = True
  	gres@lgLabelFontHeightF     = 0.1
  	gres@vpWidthF           = 0.2                   ; width of legend (NDC)
 	gres@vpHeightF          = 0.1                   ; height of legend (NDC)
	gres@lgDashIndexes      = (/0,0,0/)
	gres@lgLineThicknesses  = (/2.5,1.,1./)
	  	
  gsn_legend_ndc(wks,3,(/"Obs","Control","w/Low-Level Ent."/),0.7,0.15,gres)
  
  draw(plot)
  frame(wks)

	print("")
	print("	"+fig_file+"."+fig_type)
  delete([/res,mres,txres/])
end do
print("")
;===================================================================================
;===================================================================================
end

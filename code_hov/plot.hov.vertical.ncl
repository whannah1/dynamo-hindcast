load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"

begin	
	;member = (/"09","13"/)
	member = (/"09","13"/)
	
	rd1 = (/00,05,10/)
	;rd1 = (/05/)
	
	fig_type = "png"

	lat1 = -7.
	lat2 =  7.
	lon1 = 70.
	lon2 = 80.
	
	nsmooth = 0
	
	anom = True
;===================================================================================
;===================================================================================
  idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
  odir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
num_r = dimsizes(rd1)
num_m = dimsizes(member)
do m = 0,num_m-1
do r = 0,num_r-1
  
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  if anom then
    fig_file = odir+"hov.vertical.anom."+oname
  else
    fig_file = odir+"hov.vertical."+oname
  end if
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"ncl_default")
  plot = new(4,graphic)
  	res = True
  	res@gsnDraw = False
  	res@gsnFrame = False
  	res@vpWidthF = 0.4
  	res@gsnRightString = ""
  	lres = res
  	
  	res@gsnSpreadColors = True
  	res@cnFillOn = True
  	res@cnLinesOn = False
  	res@cnLineLabelsOn = False
  	res@tmYLLabelAngleF = 45.
  	;res@trYMaxF = tofloat(num_day)
  	;res@trYMinF = 0.
  	res@lbLabelBarOn = True
  	;res@tiYAxisString = ""
  	res@lbTitlePosition = "bottom"
  	res@gsnLeftStringFontHeightF = 0.02
;===================================================================================
;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  t1    = 0
  time  = infile->time(t1::4)
  MSEvi = avg4to1(dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1/)))

  	if True
  	do i = 0,nsmooth-1
  	 MSEvi = smooth(MSEvi)
  	 COLDP = smooth(COLDP)
  	 MSEDT = smooth(MSEDT)
  	 VdelH = smooth(VdelH)
  	 udHdx = smooth(udHdx)
  	 vdHdy = smooth(vdHdy)
  	 WdHdp = smooth(WdHdp)
  	 COLQR = smooth(COLQR)
  	 LHFLX = smooth(LHFLX)
  	 SHFLX = smooth(SHFLX)
  	 DSEvi = smooth(DSEvi)
  	 DSEDT = smooth(DSEDT)
  	 VdelS = smooth(VdelS)
  	 WdSdp = smooth(WdSdp)
  	 PRECT = smooth(PRECT)
	end do
  	end if
  	if anom
  	 MSEvi = dim_rmvmean_n(MSEvi,0)
  	 COLDP = dim_rmvmean_n(COLDP,0)
  	 MSEDT = dim_rmvmean_n(MSEDT,0)
  	 VdelH = dim_rmvmean_n(VdelH,0)
  	 udHdx = dim_rmvmean_n(udHdx,0)
  	 vdHdy = dim_rmvmean_n(vdHdy,0)
  	 WdHdp = dim_rmvmean_n(WdHdp,0)
  	 COLQR = dim_rmvmean_n(COLQR,0)
  	 LHFLX = dim_rmvmean_n(LHFLX,0)
  	 SHFLX = dim_rmvmean_n(SHFLX,0)
  	end if
  	
  
  SFC_FLX = LHFLX
  SFC_FLX = (/SFC_FLX+SHFLX/)
  
  F = COLQR
  F = (/F+SFC_FLX/)
  
  ;RTEND = VdelH
  ;RWdHp = VdelH
  ;RESID = VdelH
  ;RTEND = (/-WdHdp-VdelH+COLQR+SFC_FLX		 /)
  ;RWdHp =-(/-MSEDT-VdelH+COLQR+SFC_FLX		 /)
  ;RESID = (/ MSEDT+VdelH+WdHdp-COLQR-SFC_FLX	 /)
  ;RTEND@long_name = "MSEDT as residual"
  ;RWdHp@long_name = "WdHdp as residual"
  ;RESID@long_name = "MSE Budget Residual"
  
  VdelH = -VdelH
  udHdx = -udHdx
  vdHdy = -vdHdy
  WdHdp = -WdHdp
	
	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
	MSEDT@_FillValue = VdelH@_FillValue
  
  MSEvi@long_name   = "Column Avg. MSE"
  MSEDT@long_name   = "Total MSE Tendency"
  VdelH@long_name   = "Horizontal MSE import (-V*del[h])"
  udHdx@long_name   = "-u*dh/dx"
  vdHdy@long_name   = "-v*dh/dy"
  WdHdp@long_name   = "Vertical MSE Import (-W*dh/dp)"
  COLQR@long_name   = "QR"
  F@long_name  	    = "MSE Forcing"
  SFC_FLX@long_name = "Surface Fluxes"
  
  PRECT@long_name   = "Total Precipitation"
  
  PRECT = (/PRECT/1000./Lv *24.*3600./)
  printMinMax(PRECT,True)

;===================================================================================
; Create plot
;===================================================================================
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat = "%D%c"
  	tres@ttmAxis = "YL"
  	time_axis_labels(time,res,tres)
  	;-----------------------------------
  	res@cnLevelSelectionMode = "ManualLevels"
  	if anom then
  	res@cnMinLevelValF  = -5.
  	res@cnMaxLevelValF  =  5.
  	else
  	res@cnMinLevelValF  = 330.
  	res@cnMaxLevelValF  = 342.
  	end if
  	res@cnLevelSpacingF =   1.
  	res@lbTitleString = "K"
  plot(0) = gsn_csm_contour(wks,MSEvi,res)
  	res@cnLevelSelectionMode = "ManualLevels"
  	if anom
  	res@cnMinLevelValF  = -150.
  	res@cnMaxLevelValF  =  150.
  	else
  	res@cnMinLevelValF  = -200.
  	res@cnMaxLevelValF  =  200.
  	end if
  	res@cnLevelSpacingF =   20.
  	res@lbTitleString = "W m~S~-~S~2"
  plot(1) = gsn_csm_contour(wks,SFC_FLX,res)
  plot(2) = gsn_csm_contour(wks,COLQR,res)
  	res@cnLevelSelectionMode = "AutomaticLevels"
  	res@lbTitleString = "mm/day"
  plot(3) = gsn_csm_contour(wks,PRECT,res)
  
  ;plot(1) = gsn_csm_contour(wks,VdelH,res)
  ;plot(2) = gsn_csm_contour(wks,udHdx,res)
  ;plot(3) = gsn_csm_contour(wks,vdHdy,res)
  ;plot(4) = gsn_csm_contour(wks,MSEDT,res)
  ;plot(5) = gsn_csm_contour(wks,WdHdp,res)
  ;plot(6) = gsn_csm_contour(wks,COLQR,res)
  ;plot(7) = gsn_csm_contour(wks,SFC_FLX,res)
  	
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	
  
  sl = -1./(7. *3600./111000.)
  do p = 0,dimsizes(plot)-1
    lres@xyDashPattern		= 1
    xx = (/50.,160./)
    yy = (xx(0)-xx)*sl + time(20-t1/4-rd1(r))
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    yy = (xx(0)-xx)*sl + time(31+18-t1/4-rd1(r))
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    ;yy = (xx(0)-xx)*sl + time(30+31+30+13-t1/4)
    ;overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    lres@xyDashPattern		= 2
    xx = (/1.,1./)*75.
    yy = (/min(time)-5.*24.,max(time)+5.*24./)
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
  end do
;===================================================================================
;===================================================================================
  	pres = True
    	pres@txString = oname+"	"+lat1+":"+lat2+"N / "+lon1+":"+lon2+"E"
    	pres@amJust   = "TopLeft"
    	if member(m).eq."09" then
    	  ;pres@gsnPanelFigureStrings= (/"e)","f)","g)","h)"/) 
    	  pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)"/) 
    	end if
    	if member(m).eq."13" then
    	  ;pres@gsnPanelFigureStrings= (/"i)","j)","k)","l)"/)
    	  pres@gsnPanelFigureStrings= (/"e)","f)","g)","h)"/)  
    	end if
  	

  gsn_panel(wks,plot,(/1,4/),pres)
  ;gsn_panel(wks,plot(0),(/1,1/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end do
end do
end

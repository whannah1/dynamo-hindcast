load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"

begin
	;member = (/"02","03","04","05"/)
	member = (/"09"/)
	num_m = dimsizes(member)
	
	fig_type = "png"
	
	;mvar = (/"U","U","FLUT"/)
	;ovar = (/"U","U","OLR"/)
	;lev = (/200.,850.,0./)
	
	mvar = (/"DTCOND"/)
	;ovar = (/"precip"/)
	lev  = (/850./)
	
	yr = 2011
	mn = 11
	
	if mn.eq.10 then
	  dy = (/01,06,11,16,21,26,31/)
	  ;dy = 01
	end if
	if mn.eq.11 then
	  ;dy = (/05,10,15,20,25,30/)
	  dy = 15
	end if
	
	idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
	odir = "/maloney-scratch/whannah/DYNAMO/Hindcast/figures/"
	
do m = 0,num_m-1
	case_stub = "DYNAMO_"+member(m)+"_f09"
	
	yrmndy = toint( yr*10000 + mn*100 + dy )
	yrmn   = toint( yr*100   + mn )
	case = case_stub+"_"+yrmndy	
	bstr = new(dimsizes(yrmndy),string)
	bstr = ""
	clb = "clb = 0.0"+bstr
	mu  = "mu  = 0.4"+bstr

	;fig_file = odir+"DYNAMO.HC.hov.multi_vars."+yrmn+"."+case_stub
	fig_file = odir+"DYNAMO.HC.hov."+mvar(0)+"."+yrmn+"."+case_stub
	
	lat1 = -5.
	lat2 =  5.
	lon1 = 0.
	lon2 = 180.
	
	olat1 = lat1
	olat2 = lat2

	num_c = dimsizes(case)
	num_v = dimsizes(mvar)
	num_day = 20+(num_c-1)*5
;===================================================================================
;===================================================================================
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  plot = new((num_c)*num_v,graphic)
  	res = True
  	res@gsnDraw = False
  	res@gsnFrame = False
  	res@gsnSpreadColors = True
  	res@cnFillOn = True
  	res@cnLinesOn = False
  	res@vpWidthF = 0.3
  	res@gsnRightString = ""
  	res@cnLineLabelsOn = False
  	res@tmYLLabelAngleF = 45.
  	res@trYMaxF = tofloat(num_day)
  	res@trYMinF = 0.
  	res@lbLabelBarOn = True
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do c = 0,num_c-1
  ;===========================================================
  ; Coordinate variables
  ;===========================================================
  if (c.eq.0) then
    tmp = LoadHC(case(c),mvar(v),lev(v),opt)
    lat = tmp&lat
    lon = tmp&lon
    num_lat = dimsizes(lat)
    num_lon = dimsizes(lon)
    num_t = num_day*4
    time = (tofloat(ispan(0,num_t-1,1))*6.)/24.
    time@units = "days since "+yr(0)+"-"+mn(0)+"-"+dy(0)+""
    num_t = dimsizes(time)
    mV = new((/num_c,20*4,num_lon/),float)
      mV!0 = "case"
      mV!1 = "time"
      mV!2 = "lon"
      mV&lon = lon
      delete(tmp) 
  end if
  ;===========================================================
  ; Load hindcast data
  ;===========================================================
  mV(c,:,:) = dim_avg_n( LoadHC(case(c),mvar(v),lev(v),opt) ,1) *24.*3600.
end do
  
  if mvar(v).eq."PRECC" then
    mV = mV*60.*60.*24.*1000.
    mV@units = "mm/day"
  end if
 
;===================================================================================
; Create plot
;===================================================================================
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat = "%D%c"
  	tres@ttmAxis = "YL"
  	time_axis_labels(time,res,tres)
  	;-----------------------------------
  	res@tiYAxisString = ""
  	;-----------------------------------
  	;if num_d.eq.4 then
   	;  res@gsnLeftString = ""+mvar(v)+lev(v)
   	;else
   	  res@gsnLeftString = ""+mvar(v)
   	;end if
  ;------------------
  ; plot model
  ;------------------
  do c = 0,num_c-1
  	res@tiYAxisString  = ""
     tmp = mV(c,:,:)
     offset = (dy(c)-dy(0))*4
     tmp&time= time(offset:offset+20*4-1)
     p = c+v*(num_c)
     plot(p) = gsn_csm_contour(wks,tmp,res)
       delete(tmp)
    print("p = "+p+"	c = "+c+"	v = "+v)
  end do
    delete([/mV,time,cdtime,offset/]) 
end do
;===================================================================================
;===================================================================================
  	pres = True
    	pres@txString = case_stub;"clb:"+clb(0)+"  mu:"+mu(0)+"    lat = "+lat1+":"+lat2+"~C~"

  gsn_panel(wks,plot,(/num_v,num_c+1/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;===================================================================================
;===================================================================================
end do
end


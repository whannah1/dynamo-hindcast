load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"

begin
	;member = (/"02","03","04","05"/)
	member = (/"12"/)
	num_m = dimsizes(member)
	
	fig_type = "png"
	
	;mvar = (/"U","U","FLUT"/)
	;ovar = (/"U","U","OLR"/)
	;lev = (/200.,850.,0./)
	
	mvar = (/"PRECC"/)
	ovar = (/"precip"/)
	lev  = (/0./)
	
	yr = 2011
	mn = 11
	
	if mn.eq.10 then
	  dy = (/01,06,11,16,21,26,31/)
	  ;dy = 01
	end if
	if mn.eq.11 then
	  dy = (/05,10,15,20,25,30/)
	  ;dy = 15
	end if
	
	idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
	odir = "/maloney-scratch/whannah/DYNAMO/Hindcast/figures/"
	
do m = 0,num_m-1
	case_stub = "DYNAMO_"+member(m)+"_f09"
	
	yrmndy = toint( yr*10000 + mn*100 + dy )
	yrmn   = toint( yr*100   + mn )
	case = case_stub+"_"+yrmndy	
	bstr = new(dimsizes(yrmndy),string)
	bstr = ""
	clb = "clb = 0.0"+bstr
	mu  = "mu  = 0.4"+bstr

	;fig_file = odir+"DYNAMO.HC.hov.multi_vars."+yrmn+"."+case_stub
	fig_file = odir+"DYNAMO.HC.hov."+ovar(0)+"."+yrmn+"."+case_stub
	
	lat1 = -5.
	lat2 =  5.
	lon1 = 0.
	lon2 = 180.
	
	olat1 = lat1
	olat2 = lat2

	num_c = dimsizes(case)
	num_v = dimsizes(mvar)
	num_day = 20+(num_c-1)*5
;===================================================================================
;===================================================================================
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  plot = new((num_c+1)*num_v,graphic)
  	res = True
  	res@gsnDraw = False
  	res@gsnFrame = False
  	res@gsnSpreadColors = True
  	res@cnFillOn = True
  	res@cnLinesOn = False
  	res@vpWidthF = 0.3
  	res@gsnRightString = ""
  	res@cnLineLabelsOn = False
  	res@tmYLLabelAngleF = 45.
  	res@trYMaxF = tofloat(num_day)
  	res@trYMinF = 0.
  	res@lbLabelBarOn = True
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do c = 0,num_c-1
  ;===========================================================
  ; Coordinate variables
  ;===========================================================
  if (c.eq.0) then
    tmp = LoadHC(case(c),mvar(v),lev(v),opt)
    lat = tmp&lat
    lon = tmp&lon
    num_lat = dimsizes(lat)
    num_lon = dimsizes(lon)
    num_t = num_day*4
    time = (tofloat(ispan(0,num_t-1,1))*6.)/24.
    time@units = "days since "+yr(0)+"-"+mn(0)+"-"+dy(0)+""
    num_t = dimsizes(time)
    mV = new((/num_c,20*4,num_lon/),float)
      mV!0 = "case"
      mV!1 = "time"
      mV!2 = "lon"
      mV&lon = lon
      delete(tmp) 
  end if
  ;===========================================================
  ; Load hindcast data
  ;===========================================================
  mV(c,:,:) = dim_avg_n( LoadHC(case(c),mvar(v),lev(v),opt) ,1)
end do
  
  if mvar(v).eq."PRECC" then
    mV = mV*60.*60.*24.*1000.
    mV@units = "mm/day"
  end if
 
;===================================================================================
; Load observed data
;===================================================================================
	if isvar("ifile") then
	  delete(ifile)
	end if
	if ovar(v).eq."precip" then
	  ifile = "/maloney-scratch/whannah/DYNAMO/TRMM/TRMM.DYNAMO.6hour.nc"
	end if
	if ovar(v).eq."OLR" then
	  ifile = "/maloney-scratch/whannah/DYNAMO/OLR/OLR.DYNAMO.daily.nc"
	end if
	
	if .not.isvar("ifile") then
	  ifile = "/maloney-scratch/whannah/DYNAMO/ERAi/DYNAMO_data/6hour.DYNAMO.ERAi."+ovar(v)+".nc"
	end if
	
		if mn(0).eq.10 then
		  t1 = (dy(0)-1)*4
		end if
		if mn(0).eq.11 then
		  t1 = (31+dy(0)-1)*4
		end if
		if mn(0).eq.12 then
		  t1 = (31+30+dy(0)-1)*4
		end if
	
  infile = addfile(ifile,"r")
  num_d = dimsizes(dimsizes(infile->$ovar(v)$))
    if isvar("olon") then
      delete(olon)
    end if
  olon = infile->lon({lon1:lon2})
  olat = infile->lat({olat1:olat2})
    if isvar("tmp") then
      delete(tmp)
    end if
  ;------------------------------------------------------------
  ;------------------------------------------------------------
  if ovar(v).eq."OLR" then    
    tmp = new((/num_t,dimsizes(olat),dimsizes(olon)/),float)
    tmp(::4,:,:) = infile->$ovar(v)$(t1/4:t1/4+num_t/4-1,{olat1:olat2},{lon1:lon2})
    do h = 1,3
      tmp(h:num_t-4-1:4,:,:) = tmp(0:num_t-4-1:4,:,:) + h*(tmp(4::4,:,:) - tmp(0:num_t-4-1:4,:,:))/4
    end do
    tmp(num_t-4:,:,:) = conform(tmp(num_t-4:,:,:),tmp(num_t-4-1,:,:),(/1,2/))
    tmp2 = linint2_Wrap(olon,olat,tmp,True,lon,lat,0)
    delete(olon)
    olon = lon
;printVarSummary(tmp)
;printVarSummary(tmp2)
  ;------------------------------------------------------------
  else
  ;------------------------------------------------------------
    if num_d.eq.3 then
      tmp = infile->$ovar(v)$(t1:t1+num_t-1,{olat1:olat2},{lon1:lon2})
    else
      tmp = infile->$ovar(v)$(t1:t1+num_t-1,{toint(lev(v))},{olat1:olat2},{lon1:lon2})
    end if

    if ovar(v).eq."precip" then
      tmp2 = area_hi2lores_Wrap(olon,olat,tmp,True,1.,lon,lat,False)
;printVarSummary(lon)
;printVarSummary(olon)
;printVarSummary(lat)
;printVarSummary(olat)
;printMinMax(tmp,True)
;printMinMax(tmp2,True)
;exit
    else
      tmp2 = tmp
    end if
  end if
  ;------------------------------------------------------------
    
  oV = dim_avg_n( tmp2 ,1)
    delete([/tmp,tmp2/])

printVarSummary(oV)
printVarSummary(lon)
printVarSummary(olon)
;print("	c = "+c+"	v = "+v+"	"+ovar(v))

    oV!0 = "time"
    oV!1 = "lon"
    oV&time = time
    oV&lon  = lon
;===================================================================================
; Create plot
;===================================================================================
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat = "%D%c"
  	tres@ttmAxis = "YL"
  	time_axis_labels(time,res,tres)
  	;-----------------------------------
  	if mvar(v).eq."PRECC" then
  	 res@cnLevelSelectionMode = "ManualLevels"
  	 res@cnMinLevelValF  = 2.
  	 res@cnMaxLevelValF  = 40.
  	 res@cnLevelSpacingF = 2.
  	end if
  	if ovar(v).eq."OLR" then
  	 res@cnLevelSelectionMode = "ManualLevels"
  	 res@cnMinLevelValF  = 160.
  	 res@cnMaxLevelValF  = 280.
  	 res@cnLevelSpacingF =  10.
  	end if
  	if any(mvar(v).eq.(/"U","V"/)) then
  	 gsn_define_colormap(wks,"BlueYellowRed")
  	 res@cnLevelSelectionMode = "ManualLevels"
  	 if lev(v).gt.500. then
  	  res@cnMinLevelValF  = -8.
  	  res@cnMaxLevelValF  =  8.
  	  res@cnLevelSpacingF =  1.
  	 else
  	  res@cnMinLevelValF  = -16.
  	  res@cnMaxLevelValF  =  16.
  	  res@cnLevelSpacingF =  2.
  	 end if
  	end if
	if mvar(v).eq."OMEGA" then
	 gsn_define_colormap(wks,"BlueYellowRed")
  	 res@cnLevelSelectionMode = "ManualLevels"
  	 res@cnMinLevelValF  = -0.2
  	 res@cnMaxLevelValF  =  0.2
  	 res@cnLevelSpacingF =  0.04
  	end if
  	;-----------------------------------
  	res@tiYAxisString = ""
  	;if ovar(v).eq."precip" then
  	;  res@gsnLeftString = "TRMM"
  	;else
  	;  res@gsnLeftString = "ERAi"
  	;end if
  	;-----------------------------------
  	if num_d.eq.4 then
   	  res@gsnLeftString = ""+mvar(v)+lev(v)
   	else
   	  res@gsnLeftString = ""+mvar(v)
   	end if
  ;------------------
  ; plot obs
  ;------------------
  p = v*(num_c+1)
  print("p = "+p+"	c = "+c+"	v = "+v)
  plot(p) = gsn_csm_contour(wks,oV,res)
  ;------------------
  ; plot model
  ;------------------
  do c = 0,num_c-1
  	res@tiYAxisString  = ""
     tmp = mV(c,:,:)
     offset = (dy(c)-dy(0))*4
     tmp&time= time(offset:offset+20*4-1)
     p = c+1+v*(num_c+1)
     plot(p) = gsn_csm_contour(wks,tmp,res)
       delete(tmp)
    print("p = "+p+"	c = "+c+"	v = "+v)
  end do
    delete([/mV,oV,time,cdtime,offset,olat,olon/]) 
end do
;===================================================================================
;===================================================================================
  	pres = True
    	pres@txString = case_stub;"clb:"+clb(0)+"  mu:"+mu(0)+"    lat = "+lat1+":"+lat2+"~C~"

  gsn_panel(wks,plot,(/num_v,num_c+1/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
;===================================================================================
;===================================================================================
end do
end


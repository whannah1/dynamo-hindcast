load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin	
	
	member    = (/"13","12"/)
	case_name = (/"ZM_0.2","ZM_2.0"/)
	
	;member    = (/"ERAi","12"/)
	;case_name = (/"ERAi","ZM_2.0"/)
	
	clr = (/"black","blue","red"/)
	dsh = (/0,16,0,0/)
	
	rd1 = (/00/)
	
	fig_type = "png"

	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  
	top_lev = 150.
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_climo/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev@units = "hPa"
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev)
  num_t   = (31+30+15+4)*4
  xdim = (/num_m,num_t,num_lev/)
  WdH   = new(xdim,float)
  WbdHb = new(xdim,float)
  WpdHp = new(xdim,float)
  Tvals = ispan(2,num_lev-1,1)
  Cvals = ispan(1,num_lev-2,1)
  Bvals = ispan(0,num_lev-3,1)
;===================================================================================
;===================================================================================
  fig_file = odir+"climo.profile.anomaly.v5"
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(6,graphic)
  
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame 		= False
  	res@tmXTOn 			= False
  	res@gsnRightString 	= ""
  	res@trYMinF			= 150.
  	;--------------------------------------
  	lres = res
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 2
  	;--------------------------------------
  	;res@xyLineColors					= clr
  	res@xyDashPattern				= 0
  	res@xyLineThicknessF				= 3.0
  	res@tmYLLabelAngleF 				= 45.
  	res@gsnLeftStringFontHeightF 	= 0.03
  	;res@gsnCenterStringFontHeightF 	= 0.03
  	res@trYReverse = True
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)       +"_"+rds+"-"+rde
  t1 = 0
  if rds.eq."00" then t2 = (31+30+14)*4-1 		end if
  if rds.eq."05" then t2 = (31+30+14)*4-1-5 		end if
  if rds.eq."10" then t2 = (31+30+14)*4-1-10		end if
    ;-------------------------------------------------------------------------
    Hfile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R")
    Wfile = addfile(idir+"data/"+oname+"/"+oname+".OMEGA.nc","R")
    H = Hfile->MSE  (t1:,{:top_lev},{lat1:lat2},{lon1:lon2})
    W = Wfile->OMEGA(t1:,{:top_lev},{lat1:lat2},{lon1:lon2})
    Sfile = addfile(idir+"data/"+oname+"/"+oname+".budget.DSE.nc","R")
    iD = dim_avg_n(Sfile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}),(/0/)) *-1.
    tD = where(abs(iD).ge.10.,iD,iD@_FillValue)
    D  = conform(H,tD,(/2,3/)) 
    ;-------------------------------------------------------------------------
    ifile = idir+"data/"+oname+"/"+oname+".PS.nc"
    infile = addfile(ifile,"R")
    Ps = infile->PS(:,{lat1:lat2},{lon1:lon2})
    if m.eq.0 then
     P  = conform(H,lev*100.,1)
     P!1 = "lev"
     P&lev = lev
     dP = calc_dp(P,Ps)
     altdP = new(dimsizes(P),float)
     altdP(:,Cvals,:,:) = P(:,Bvals,:,:)-P(:,Tvals,:,:)
    end if
    ;-------------------------------------------------------------------------
    dH = new(dimsizes(H),float)
    ;dH(:,Cvals,:,:) = (H(:,Bvals,:,:)-H(:,Tvals,:,:))/conform(H(:,Cvals,:,:),dP(Cvals),1)
    dH(:,Cvals,:,:) = (H(:,Bvals,:,:)-H(:,Tvals,:,:))/altdP(:,Cvals,:,:)
    Wb  = conform(H,dim_avg_n_Wrap(W ,0),(/1,2,3/))
	dHb = conform(H,dim_avg_n_Wrap(dH,0),(/1,2,3/))
	Wp  = dim_rmvmean_n_Wrap(W ,0)
	dHp = dim_rmvmean_n_Wrap(dH,0)

	G = avg( dim_avg_n( dim_sum_n( -W*dH*dP/g ,(/1/)) ,(/0/)) /tD )
	print(member(m)+"	"+G)

    WdH  (m,:,:) = dim_avg_n( W *dH /D ,(/2,3/))
    WbdHb(m,:,:) = dim_avg_n( Wb*dHb/D ,(/2,3/))
    WpdHp(m,:,:) = dim_avg_n( Wp*dHp/D ,(/2,3/))
    ;-------------------------------------------------------------------------
      delete([/iD,tD,D,H,W,Wb,Wp,dHb,dHp/])
end do

 		;############################################################
 		;############################################################
 		    lev1 = 600.
			lev2 = 500.
			adP = dim_avg_n(dim_avg_n(dP,(/2,3/)),0)
			tdP = conform(WdH(:,0,:),adP,(/1/))
			tdP!1 = "lev"
			tdP&lev = lev
			
			WdH!2 = "lev"
			WdH&lev = lev
			copy_VarCoords(WdH,WbdHb)
			copy_VarCoords(WdH,WpdHp)

			V0 = dim_sum_n(dim_avg_n(-WdH  ,1)	 *tdP/g,1)
			V1 = dim_sum_n(dim_avg_n(-WbdHb,1)	 *tdP/g,1)
			V2 = dim_sum_n(dim_avg_n(-WpdHp,1)	 *tdP/g,1)
			V3tot = V0(1) - V0(0)
			V4tot = V1(1) - V1(0)
			V5tot = V2(1) - V2(0)
			
			V0 = dim_sum_n(dim_avg_n(-WdH  (:,:,{lev2:}),1)	 *tdP(:,{lev2:})/g,1)
			V1 = dim_sum_n(dim_avg_n(-WbdHb(:,:,{lev2:}),1)	 *tdP(:,{lev2:})/g,1)
			V2 = dim_sum_n(dim_avg_n(-WpdHp(:,:,{lev2:}),1)	 *tdP(:,{lev2:})/g,1)
			V3top = V0(1) - V0(0)
			V4top = V1(1) - V1(0)
			V5top = V2(1) - V2(0)
			
			V0 = dim_sum_n(dim_avg_n(-WdH  (:,:,{:lev1}),1)	 *tdP(:,{:lev1})/g,1)
			V1 = dim_sum_n(dim_avg_n(-WbdHb(:,:,{:lev1}),1)	 *tdP(:,{:lev1})/g,1)
			V2 = dim_sum_n(dim_avg_n(-WpdHp(:,:,{:lev1}),1)	 *tdP(:,{:lev1})/g,1)
			V3bot = V0(1) - V0(0)
			V4bot = V1(1) - V1(0)
			V5bot = V2(1) - V2(0)

			fmt = "%6.3f"
			print("")
			print("DIFFERENCE:")
			print("")
			print("total = top + bottom")
			print("")
			
			print("Total : "+sprintf(fmt,V3tot)	+"  "+sprintf(fmt,V3top)	+"  "+sprintf(fmt,V3bot) )
			print("")
			print("Total : "+sprintf(fmt,V4tot)	+"  "+sprintf(fmt,V4top)	+"  "+sprintf(fmt,V4bot) )
			print("")
			print("Total : "+sprintf(fmt,V5tot)	+"  "+sprintf(fmt,V5top)	+"  "+sprintf(fmt,V5bot) )
			print("")
			delete([/V0,V1,V2/])
		;############################################################
		;############################################################
;===================================================================================
; Create plot
;===================================================================================
	cnst = 1e3
	res@tiXAxisString = "[W m~S~-2~N~ x10~S~3~N~] / [W m~S~-2~N~]"
	
    		res1 = res
    		res2 = res
    		res3 = res
    		res4 = res
    		res5 = res
    		res6 = res
		
		res1@gsnLeftString = "Normalized -W dH/dp"
		res2@gsnLeftString = "Normalized -Wbar dHbar/dp"
		res3@gsnLeftString = "Normalized -W' dH'/dp"
		
		res1@xyDashPatterns	= dsh
		res2@xyDashPatterns	= dsh
		res3@xyDashPatterns	= dsh
		
		res4@gsnLeftString = res1@gsnLeftString+" Difference"
		res5@gsnLeftString = res2@gsnLeftString+" Difference"
		res6@gsnLeftString = res3@gsnLeftString+" Difference"
		
		res4@xyLineColors = clr
		res5@xyLineColors = clr
		res6@xyLineColors = clr
	;---------------------------------------------------------------
	;---------------------------------------------------------------
	V0 = dim_avg_n(-WdH  ,1)*cnst
	V3 = V0 - conform(V0,V0(0,:),(/1/))	
	res1@trXMinF = min((/V0,V3/))
    res1@trXMaxF = max((/V0,V3/))
	p = 0	
    plot(p) = gsn_csm_xy(wks,V0,lev,res1)
    
    plot(3) = gsn_csm_xy(wks,(/V3(1,:)/),lev,res4)
    ;---------------------------------------------------------------
	;---------------------------------------------------------------
	V1 = dim_avg_n(-WbdHb,1)*cnst
	V4 = V1 - conform(V1,V1(0,:),(/1/))	
    res2@trXMinF = min((/V1,V4/))
    res2@trXMaxF = max((/V1,V4/))
    p = 1
    plot(p) = gsn_csm_xy(wks,V1,lev,res2)
    
    plot(4) = gsn_csm_xy(wks,(/V4(1,:)/),lev,res5)
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
	V2 = dim_avg_n(-WpdHp,1)*cnst
	V5 = V2 - conform(V2,V2(0,:),(/1/))	
			
    res3@trXMinF = min((/V0,V1,V2,V3,V4,V5/))
    res3@trXMaxF = max((/V0,V1,V2,V3,V4,V5/))
    p = 2
    plot(p) = gsn_csm_xy(wks,V2,lev,res3)
        
    plot(5) = gsn_csm_xy(wks,(/V5(1,:)/),lev,res6)
;===================================================================================
;===================================================================================
end do

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lev*0.,lev,lres))
    overlay(plot(p),gsn_csm_xy(wks,(/-1e3,1e3/),(/1.,1./)*500.,lres))
  end do
  
  	pres = True
  	cr = inttochar(10) 
  	pres@gsnFrame       	= False
  	;pres@gsnPanelBottom 	= 0.1 
    pres@amJust   = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF = 0.01
    	pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 

  gsn_panel(wks,plot,(/2,3/),pres)
  
  legend = create "Legend" legendClass wks 
    "vpXF"                     : 0.4
    "vpYF"                     : 0.125
    "vpWidthF"                 : 0.1  
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.03
    "lgDashIndexes"            : dsh
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : (/"black","black"/)
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
;===================================================================================
;===================================================================================
end


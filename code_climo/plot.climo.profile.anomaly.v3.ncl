load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin	
	
	member    = (/"13","12"/)
	case_name = (/"ZM_0.2","ZM_2.0"/)
	
	clr = (/"black","blue","red"/)
	dsh = (/0,16,0,0/)
	
	rd1 = (/00/)
	
	fig_type = "png"

	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	  
	top_lev = 150.
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_climo/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  num_lev = dimsizes(lev)
  num_t   = (31+30+15+4)*4
  xdim = (/num_m,num_t,num_lev/)
  W   = new(xdim,float)
  S   = new(xdim,float)
  iH  = new(xdim,float)
  H   = new(xdim,float)
  HD  = new(xdim,float)
  WD  = new(xdim,float)
  dpS = new(xdim,float)
  dpH = new(xdim,float)
  Tvals = ispan(2,num_lev-1,1)
  Cvals = ispan(1,num_lev-2,1)
  Bvals = ispan(0,num_lev-3,1)
  P = lev*100.
  dP = new(num_lev,float)
  dP (Cvals) =  P(Bvals)-P(Tvals)
;===================================================================================
;===================================================================================
  fig_file = odir+"climo.profile.anomaly.v3"
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(6,graphic)
  
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame 		= False
  	res@tmXTOn 			= False
  	res@gsnRightString 	= ""
  	res@trYMinF			= 150.
  	;--------------------------------------
  	lres = res
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 2
  	;--------------------------------------
  	;res@xyLineColors					= clr
  	res@xyDashPattern				= 0
  	res@xyLineThicknessF				= 3.0
  	res@tmYLLabelAngleF 				= 45.
  	res@gsnLeftStringFontHeightF 	= 0.03
  	;res@gsnCenterStringFontHeightF 	= 0.03
  	res@trYReverse = True
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
  else
    oname = "DYNAMO_"+member(m)       +"_"+rds+"-"+rde
  end if
  t1 = 0
  if rds.eq."00" then t2 = (31+30+14)*4-1 		end if
  if rds.eq."05" then t2 = (31+30+14)*4-1-5 		end if
  if rds.eq."10" then t2 = (31+30+14)*4-1-10		end if
    ;-------------------------------------------------------------------------
    ifile = idir+"data/"+oname+"/"+oname+".budget.DSE.nc"
    infile = addfile(ifile,"R")
    WdSdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
    infile = addfile(idir+"data/"+oname+"/"+oname+".MSE.nc","R") 
    D = conform(infile->MSE(t1:,:,{lat1:lat2},{lon1:lon2}), dim_avg_n(WdSdp,(/0/)) ,(/2,3/)) *-1.
  	D = where(abs(D).ge.10.,D,D@_FillValue)
    ;-------------------------------------------------------------------------
    var = "MSE"
    infile = addfile(idir+"data/"+oname+"/"+oname+"."+var+".nc","R")
    iH(m,:,:) = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2})  ,(/2,3/))
    H(m,:,:)  = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2})  ,(/2,3/))
    ;HD(m,:,:) = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2})/D ,(/2,3/))
    ;-------------------------------------------------------------------------
    ;var = "DSE"
    ;infile = addfile(idir+"data/"+oname+"/"+oname+"."+var+".nc","R")
    ;S(m,:,:) = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2}) ,(/2,3/))
    ;-------------------------------------------------------------------------
    var = "OMEGA"
    infile = addfile(idir+"data/"+oname+"/"+oname+"."+var+".nc","R")
    W(m,:,:) = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2})    ,(/2,3/))
    WD(m,:,:) = dim_avg_n_Wrap( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2})/D ,(/2,3/))
    ;-------------------------------------------------------------------------
    dpH (m,:,Cvals) = (H (m,:,Bvals)-H (m,:,Tvals))/conform(H (m,:,Cvals),dP(Cvals),1)
    ;dpHD(m,:,Cvals) = (HD(m,:,Bvals)-HD(m,:,Tvals))/conform(HD(m,:,Cvals),dP(Cvals),1)
    ;-------------------------------------------------------------------------
    delete(D)
end do
	
	iH = iH/cpd
	 H =  H/cpd
	;HD = HD/cpd
	
	;Hp   = dim_rmvmean_n_Wrap(H  ,1)
	Wp   = dim_rmvmean_n_Wrap(W  ,1)
	WDp  = dim_rmvmean_n_Wrap(WD ,1)
	WDp  = dim_rmvmean_n_Wrap(WD  ,1)
	dpHp = dim_rmvmean_n_Wrap(dpH,1)

	MSEvi = dim_sum_n(H*conform(iH,dP,2)/g,2)
	MSEvi = (/MSEvi - conform(MSEvi,dim_avg_n(MSEvi,1),0)/)
	TH = 0.
	wcond = conform(H,MSEvi.gt. TH*stddev(MSEvi),(/0,1/))
	dcond = conform(H,MSEvi.lt.-TH*stddev(MSEvi),(/0,1/))
	
	dWp   = where(dcond,Wp ,Wp@_FillValue)
	dWDp  = where(dcond,WDp,Wp@_FillValue)
 	ddpHp = where(dcond,dpHp,dpHp@_FillValue)
 	
 	wWp   = where(wcond,Wp ,Wp@_FillValue)
 	wWDp  = where(wcond,WDp,Wp@_FillValue)
 	wdpHp = where(wcond,dpHp,dpHp@_FillValue)
 		
 		;############################################################
 		;############################################################
 		if True then
		 	V0 = dim_avg_n(- WDp* dpHp,1)
			V1 = dim_avg_n(-dWDp*ddpHp,1)
			V2 = dim_avg_n(-wWDp*wdpHp,1)
			V3 = V0(1,:) - V0(0,:)
			V4 = V1(1,:) - V1(0,:)
			V5 = V2(1,:) - V2(0,:)
			
			V3!0 = "lev"
			V3&lev = lev
			copy_VarCoords(V3,V4)
			copy_VarCoords(V3,V5)
			dP!0 = "lev"
			dP&lev = lev
			fmt = "%6.2f"
			print("")
			print("total column = top + bottom")
			print("")
			print("Tot: "+sprintf(fmt,sum(V3*dP/g))+"  "+\
			       	      sprintf(fmt,sum(V3({500.:})*dP({500.:})/g))+"  "+\
			      		  sprintf(fmt,sum(V3({:500.})*dP({:500.})/g))       )
			print("")
			print("Dry: "+sprintf(fmt,sum(V4*dP/g))+"  "+\
			              sprintf(fmt,sum(V4({500.:})*dP({500.:})/g))+"  "+\
			              sprintf(fmt,sum(V4({:500.})*dP({:500.})/g))       )
			print("")
			print("Wet: "+sprintf(fmt,sum(V5*dP/g))+"  "+\
			              sprintf(fmt,sum(V5({500.:})*dP({500.:})/g))+"  "+\
			      	 	  sprintf(fmt,sum(V5({:500.})*dP({:500.})/g))       )
			print("")
			delete([/V0,V1,V2,V3,V4,V5/])
		end if
		;############################################################
		;############################################################
		
;===================================================================================
; Create plot
;===================================================================================

	
	;X0 = dim_avg_n(- Wp,1)
	;X1 = dim_avg_n(-dWp,1)
	;X2 = dim_avg_n(-wWp,1)
	;X3 = X0 - conform(X0,X0(0,:),(/1/))
	;X4 = X1 - conform(X1,X1(0,:),(/1/))
	;X5 = X2 - conform(X2,X2(0,:),(/1/))
		
    		res1 = res
    		res2 = res
    		res3 = res
    		res4 = res
    		res5 = res
    		res6 = res
		
		res1@gsnLeftString = "Normalized W'"
		res2@gsnLeftString = "Normalized dH'/dp"
		res3@gsnLeftString = "Normalized -W'dH'/dp"
		
		res4@gsnLeftString = res1@gsnLeftString+" Difference"
		res5@gsnLeftString = res2@gsnLeftString+" Difference"
		res6@gsnLeftString = res3@gsnLeftString+" Difference"
		
		res1@xyDashPatterns	= dsh
		res2@xyDashPatterns	= dsh
		res3@xyDashPatterns	= dsh
		
		res4@xyLineColors = clr
		res5@xyLineColors = clr
		res6@xyLineColors = clr
		
		;res4@gsnLeftString = "d"+iv+"'/dp (wet)"
		;res5@gsnLeftString = "W'd"+iv+"'/dp (dry)"
		;res6@gsnLeftString = "W'd"+iv+"'/dp (wet)"
	;---------------------------------------------------------------
	;---------------------------------------------------------------
	V0 = dim_avg_n( Wp,1)
	V1 = dim_avg_n(dWp,1)
	V2 = dim_avg_n(wWp,1)
	V3 = V0 - conform(V0,V0(0,:),(/1/))
	V4 = V1 - conform(V1,V1(0,:),(/1/))
	V5 = V2 - conform(V2,V2(0,:),(/1/))
	res1@trXMinF = min((/V0,V1,V2,V3,V4,V5/))
    res1@trXMaxF = max((/V0,V1,V2,V3,V4,V5/))
	p = 0	
    		res1@xyLineColor = "red"
    plot(p) = gsn_csm_xy(wks,V1,lev,res1)
    		res1@xyLineColor = "blue"
    overlay(plot(p),gsn_csm_xy(wks,V2,lev,res1))
    
    plot(3) = gsn_csm_xy(wks,(/V3(1,:),V4(1,:),V5(1,:)/),lev,res4)
    ;---------------------------------------------------------------
	;---------------------------------------------------------------
	V0 = dim_avg_n( dpHp,1)
	V1 = dim_avg_n(ddpHp,1)
	V2 = dim_avg_n(wdpHp,1)
	V3 = V0 - conform(V0,V0(0,:),(/1/))
	V4 = V1 - conform(V1,V1(0,:),(/1/))
	V5 = V2 - conform(V2,V2(0,:),(/1/))
    res2@trXMinF = min((/V0,V1,V2,V3,V4,V5/))
    res2@trXMaxF = max((/V0,V1,V2,V3,V4,V5/))
    p = 1
    		res2@xyLineColor = "red"
    plot(p) = gsn_csm_xy(wks,V1,lev,res2)
    		res2@xyLineColor = "blue"
    overlay(plot(p),gsn_csm_xy(wks,V2,lev,res2))
    
    plot(4) = gsn_csm_xy(wks,(/V3(1,:),V4(1,:),V5(1,:)/),lev,res5)
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    V0 = dim_avg_n(- WDp* dpHp,1)
	V1 = dim_avg_n(-dWDp*ddpHp,1)
	V2 = dim_avg_n(-wWDp*wdpHp,1)
	V3 = V0 - conform(V0,V0(0,:),(/1/))
	V4 = V1 - conform(V1,V1(0,:),(/1/))
	V5 = V2 - conform(V2,V2(0,:),(/1/))
			
    res3@trXMinF = min((/V0,V1,V2,V3,V4,V5/))
    res3@trXMaxF = max((/V0,V1,V2,V3,V4,V5/))
    p = 2
    plot(p) = gsn_csm_xy(wks,V0,lev,res3)
    		res3@xyLineColor = "red"
    overlay(plot(p),gsn_csm_xy(wks,V1,lev,res3))
    		res3@xyLineColor = "blue"
    overlay(plot(p),gsn_csm_xy(wks,V2,lev,res3))
        
    plot(5) = gsn_csm_xy(wks,(/V3(1,:),V4(1,:),V5(1,:)/),lev,res6)
;===================================================================================
;===================================================================================
end do

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lev*0.,lev,lres))
  end do
  
  	pres = True
  	cr = inttochar(10) 
  	pres@gsnFrame       	= False
  	;pres@gsnPanelBottom 	= 0.1 
    pres@amJust   = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF = 0.01
    	pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 

  gsn_panel(wks,plot,(/2,3/),pres)
  
  legend = create "Legend" legendClass wks 
    "vpXF"                     : 0.4
    "vpYF"                     : 0.15
    "vpWidthF"                 : 0.1  
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.03
    "lgDashIndexes"            : dsh
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : (/"black","black"/)
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
;===================================================================================
;===================================================================================
end


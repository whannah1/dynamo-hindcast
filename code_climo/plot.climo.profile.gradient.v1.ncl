load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin	
	;member = (/"ERAi","12","09","13"/)
	;case_name = (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
	
	;member    = (/"ERAi","12","09","13"/)
	;case_name = (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
	
	member    = (/"ERAi","90","91","09","13"/)
	case_name = (/"ERAi","SP4km","SP2km","ZM_1.0","ZM_0.2"/)
	
	clr = (/"black","purple","pink","blue","green"/)
	dsh = (/1,0,0,0,0/)
	
	sa = "IO"
	
	var  = (/"MSE","OMEGA"/)
	
	norm = False			; Normalization switch to eavluate vertical structure

	
	;rd1 = (/00,05,10/)
	rd1 = (/00/)
	
	fig_type = "png"

	if sa.eq."nsa" then
	  lat1 =  0.
	  lat2 =  6.
	  lon1 =  73.
	  lon2 =  80.
	end if
	if sa.eq."ssa" then
	  lat1 = -7.
	  lat2 =  0.
	  lon1 =  73.
	  lon2 =  80.
	end if	
	
	if sa.eq."IO" then
	  lat1 = -10.
	  lat2 =  10.
	  lon1 =  60.
	  lon2 =  90.
	end if
	
	top_lev = 200.
	
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  num_v = dimsizes(var)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_climo/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ilev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  ilev!0 = "lev"
  ilev&lev = ilev
  lev = ilev({:top_lev})
  num_lev = dimsizes(lev)
  V  = new((/num_m,num_v,num_lev/),float)
  Vg = new((/num_m,num_v,num_lev/),float)
  zt = ispan(2,num_lev-1,1)
  zm = ispan(1,num_lev-2,1)
  zb = ispan(0,num_lev-3,1)
;===================================================================================
;===================================================================================
  fig_file = odir+"climo.profile.gradient.v1"
  ;fig_file = odir+"Q.stddev.profile.v3"
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(num_r*num_v,graphic)
  
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame 		= False
  	res@tmXTOn 			= False
  	res@gsnRightString 	= ""
  	;--------------------------------------
  	lres = res
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 2
  	;--------------------------------------
  	res@xyLineColors					= clr
  	res@xyDashPatterns				= dsh
  	res@xyLineThicknessF				= 3.0
  	res@tmYLLabelAngleF 				= 45.
  	res@gsnLeftStringFontHeightF 	= 0.03
  	res@gsnCenterStringFontHeightF 	= 0.03
  	res@trYReverse = True
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
  else
    oname = "DYNAMO_"+member(m)       +"_"+rds+"-"+rde
  end if
  t1 = 0
  if rds.eq."00" then
    t2 = (31+30+14)*4-1
  end if
  if rds.eq."05" then
    t2 = (31+30+14)*4-1-5
  end if
  if rds.eq."10" then
    t2 = (31+30+14)*4-1-10
  end if
  do v = 0,num_v-1
    ;--------------------------------------------------
    ifile = idir+"data/"+oname+"/"+oname+"."+var(v)+".nc"
    infile = addfile(ifile,"R")
    V(m,v,:) = dim_avg_n_Wrap(dim_avg_n_Wrap( infile->$var(v)$(t1:,{:top_lev},{lat1:lat2},{lon1:lon2}) ,(/2,3/)),(/0/))
    ;--------------------------------------------------
    if var(v).eq."MSE"   then V(m,v,:)  = (/  V(m,v,:)/cpd /)   end if
    if var(v).eq."OMEGA" then V(m,v,:)  = (/  V(m,v,:)*100. /)  end if
    
    ;Vg(m,v,zm) = ( V(m,v,zt) - V(m,v,zb) ) / (lev(zt) - lev(zb))	; Height
    Vg(m,v,zm) = ( V(m,v,zb) - V(m,v,zt) ) / (lev(zb) - lev(zt))		; Pressure
    
    if var(v).eq."OMEGA" then Vg(m,v,:) = V(m,v,:) end if
    
  end do
end do
;===================================================================================
; Create plot
;===================================================================================

  do v = 0,num_v-1
    tres = res
    tres@gsnCenterString  = rds+"-"+rde+" days"
    tres@gsnLeftString   = var(v)
    
    if var(v).eq."MSE" tres@gsnLeftString	= "dH/dz" 		end if
    
	if var(v).eq."MSE" tres@tiXAxisString	= "K /hpa" 		end if
	if var(v).eq."OMEGA" tres@tiXAxisString	= "hPa/s /hpa" 	end if
	if var(v).eq."U" tres@tiXAxisString	    = "m/s /hPa" 	end if
	
	if (var(v).eq."OMEGA").and.norm then 
	  do m = 0,num_m-1
	    Vg(m,v,:) = (/Vg(m,v,:)/abs(min(Vg(m,v,:)))/) 
	  end do
	end if
  		
    plot(r*num_v+v)   = gsn_csm_xy(wks,Vg(:,v,:),lev,tres)
    delete(tres)
  end do
    
;===================================================================================
;===================================================================================
end do

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lev*0.,lev,lres))
  end do
  
  	pres = True
  	cr = inttochar(10) 
  	pres@gsnFrame       	= False
  	;pres@gsnPanelBottom 	= 0.1 
    pres@amJust   = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF = 0.01
    	pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 

  gsn_panel(wks,plot,(/num_r,num_v/),pres)
  
  legend = create "Legend" legendClass wks 
    "vpXF"                     : 0.375
    "vpYF"                     : 0.2
    "vpWidthF"                 : 0.1  
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.02
    "lgDashIndexes"            : dsh
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
;===================================================================================
;===================================================================================
end


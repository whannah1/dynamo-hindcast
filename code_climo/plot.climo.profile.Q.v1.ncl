load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin   
    ;member = (/"ERAi","12","09","13"/)
    ;case_name = (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
    
    member    = (/"ERAi","09","90"/)
    case_name = (/"ERAi","ZM_1.0","SP-CAM"/)
    
    ;member    = (/"ERAi"/)
    ;case_name = (/"ERAi"/)
    
    clr = (/"black","blue","purple"/)
    ;clr = (/"black","blue","red","green","black"/)
    dsh = (/0,0,0,0,1/)
    
    sa = "IO"
    
    var  = (/"Lq","OMEGA"/)

    rd1 = (/00/)
    ;rd1 = (/00,05/)
    ;rd1 = (/00,05,10/)
    
    fig_type = "png"

    if sa.eq."nsa" then
      lat1 =  0.
      lat2 =  6.
      lon1 =  73.
      lon2 =  80.
    end if
    if sa.eq."ssa" then
      lat1 = -7.
      lat2 =  0.
      lon1 =  73.
      lon2 =  80.
    end if  
    
    if sa.eq."IO" then
      lat1 = -10.
      lat2 =  10.
      lon1 =  60.
      lon2 =  90.

      lat1 = -20.
      lat2 =  20.
      lon1 =  50.
      lon2 = 100.
    end if
    
    add_EC = True
    
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  num_v = dimsizes(var)
  idir = "/Users/whannah/Research/DYNAMO/Hindcast/data/repacked/"
  odir = "/Users/whannah/Research/DYNAMO/Hindcast/figs_climo/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  ;lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./)
  lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150./)  
  num_lev = dimsizes(lev)
  V = new((/num_m,num_v,num_lev/),float)
;===================================================================================
;===================================================================================
  fig_file = odir+"climo.profile.Q.v1"
  ;fig_file = odir+"Q.stddev.profile.v3"
  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(num_r*num_v,graphic)
  
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
    res = True
    res@gsnDraw             = False
    res@gsnFrame        = False
    res@tmXTOn          = False
    res@gsnRightString  = ""
    ;--------------------------------------
    lres = res
    lres@xyLineColor        = "black"
    lres@xyLineThicknessF   = 1.
    lres@xyDashPattern      = 2
    ;--------------------------------------
    res@xyLineColors                    = clr
    res@xyDashPatterns              = dsh
    res@xyLineThicknessF                = 4.0
    res@tmYLLabelAngleF                 = 45.
    res@gsnLeftStringFontHeightF    = 0.03
    res@gsnCenterStringFontHeightF  = 0.03
    res@trYReverse = True
    res@trYMinF    = 100.
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
  else
    oname = "DYNAMO_"+member(m)       +"_"+rds+"-"+rde
  end if
  t1 = 0
  if rds.eq."00" then
    t2 = (31+30+14)*4-1
  end if
  if rds.eq."05" then
    t2 = (31+30+14)*4-1-5
  end if
  if rds.eq."10" then
    t2 = (31+30+14)*4-1-10
  end if
  do v = 0,num_v-1
    ;--------------------------------------------------
    ifile = idir+oname+"/"+oname+"."+var(v)+".nc"
    infile = addfile(ifile,"R")
    V(m,v,:) = dim_avg_n_Wrap(dim_avg_n_Wrap( infile->$var(v)$(t1:,{lev},{lat1:lat2},{lon1:lon2}) ,(/2,3/)),(/0/))
    ;--------------------------------------------------
    if var(v).eq."MSE"   then V(m,v,:)  = (/  V(m,v,:)/cpd  /) end if
    if var(v).eq."DSE"   then V(m,v,:)  = (/  V(m,v,:)/cpd  /) end if
    if var(v).eq."OMEGA" then V(m,v,:)  = (/  V(m,v,:)*100. /) end if
  end do
  ;if member(m).eq."ERAi" then
  ;  V(m,v,:) = (/V(m,v,::-1)/)
  ;end if
end do
;===================================================================================
; Create plot
;===================================================================================

  do v = 0,num_v-1
    tres = res
    tres@gsnCenterString  = rds+"-"+rde+" days"
    tres@gsnLeftString   = var(v)
    if var(v).eq."DSE"
      tres@trXMaxF          = 350.
      tres@trXMinF          = 300.
      tres@tiXAxisString        = "K"
    end if
    if var(v).eq."MSE"
      tres@trXMaxF          = 350.
      tres@trXMinF          = 325.
      tres@tiXAxisString        = "K"
    end if
    if var(v).eq."OMEGA"
      tres@trXMaxF          =  2.
      tres@trXMinF          = -6.
      tres@tiXAxisString        = "hPa/s"
    end if
    if var(v).eq."U"
      tres@trXMaxF          = 12.
      tres@trXMinF          =  2.
      tres@tiXAxisString        = "m/s"
    end if
        
    plot(r*num_v+v)   = gsn_csm_xy(wks,V(:,v,:),lev,tres)
    
        if add_EC then
          oname = "DYNAMO_EC_"+rds+"-"+rde
          infile = addfile(idir+oname+"/"+oname+"."+var(v)+".nc","R")
          elev = infile->lev
          etmp = infile->$var(v)$
          etmp = where(etmp.ne.0,etmp,etmp@_FillValue)
          eV = dim_avg_n_Wrap(dim_avg_n_Wrap( etmp(t1:,:,{lat1:lat2},{lon1:lon2}) ,(/2,3/)),(/0/))
          if var(v).eq."MSE"   then eV  = (/  eV/cpd /) end if
          if var(v).eq."OMEGA" then eV  = (/  eV*100. /) end if
                tres@xyMonoLineColor        = True
                tres@xyMonoDashPattern  = True
                tres@xyLineColor        = "red"
                tres@xyDashPattern      = 1
          overlay(plot(r*num_v+v) , gsn_csm_xy(wks,eV,elev,tres))
          delete([/eV,elev/])
        end if
    
    delete(tres)
  end do
    
;===================================================================================
;===================================================================================
end do

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lev*0.,lev,lres))
  end do
  
    pres = True
    cr = inttochar(10) 
    pres@gsnFrame                               = False
    ;pres@gsnPanelBottom                    = 0.1 
    pres@amJust                             = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF   = 0.015
    pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f","g","h"/) 

  gsn_panel(wks,plot,(/num_r,num_v/),pres)
  
  legend = create "Legend" legendClass wks 
    "lgAutoManage"              : False
    ;"vpXF"                     : 0.6       ; 1 panel
    ;"vpYF"                     : 0.55
    ;"vpXF"                     : 0.35          ; 3x2 panel
    ;"vpYF"                     : 0.87
    "vpXF"                     : 0.3        ; 2x2 panel
    "vpYF"                     : 0.75
    "vpWidthF"                 : 0.1 
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    ;"lgItemCount"              : 3
    ;"lgLabelStrings"           : (/"ERAi","CAM5","ECMWF"/)
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.015
    "lgDashIndexes"            : dsh
    ;"lgDashIndexes"            : (/0,0,1/)
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    ;"lgLineColors"             : (/"black","blue","red"/)
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
    
;===================================================================================
;===================================================================================
end


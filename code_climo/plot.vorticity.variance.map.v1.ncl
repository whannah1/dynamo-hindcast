load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin

    ;member = (/"ERAi"/)
    ;case_name = (/"ERAi"/)
    
    member    = (/"ERAi","90","09"/)
    case_name = (/"ERAi","SP-CAM","ZM_1.0","ZM_0.2"/)
    
    rd1 = (/0,5/)
    
    fig_type = "png"

    lat1 = -30.
    lat2 =  30.
    lon1 =  40.
    lon2 = 100.

    plat1 = -20.
    plat2 =  20.
    
    nsmooth = 5
    
    var = "VORT"
    
    lev = 850.
    
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_climo/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  fig_file = odir+"vorticity.variance.map.v1."+rds+"-"+rde
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"ncl_default")
  gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  plot = new(num_m,graphic)
    res = True
    res@gsnDraw                         = False
    res@gsnFrame                    = False
    res@mpCenterLonF                = 180.
    res@mpLimitMode                     = "LatLon"
    res@mpMinLatF                   = plat1
    res@mpMaxLatF                   = plat2
    res@mpMinLonF                   = lon1
    res@mpMaxLonF                   = lon2
    res@gsnAddCyclic                = False
    res@lbLabelFontHeightF          = 0.02
    res@gsnLeftStringFontHeightF    = 0.03
    res@gsnSpreadColors             = True
    res@cnFillOn                    = True
    res@cnLinesOn                   = False
    res@cnLineLabelsOn              = False
    res@lbLabelBarOn                = False
    res@lbTitlePosition             = "Bottom"
    res@lbTitleFontHeightF          = 0.01
    ;res@tmXBLabelFontHeightF           = 0.
    ;res@tmYLLabelFontHeightF           = 0.
    res@tmXTOn      = False
    res@tmYROn      = False
    res@tmXBMinorOn   = False
    res@tmYLMinorOn   = False

    lres = True
    lres@gsnDraw      = False
    lres@gsnFrame       = False
    lres@xyLineColor    = "black"
    lres@xyDashPattern    = 0
    lres@xyLineThicknessF = 1.
    
    res@cnLevelSelectionMode = "ExplicitLevels"
    res@cnLevels = ispan(3,15,2)*1e-10 
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  print(""+oname)
  
  print("  loading U")
  ifile = idir+"data/"+oname+"/"+oname+".U.nc"
  infile = addfile(ifile,"R")
  U = infile->U(:,{lev},{lat1:lat2},{lon1:lon2})
  
  print("  loading V")
  ifile = idir+"data/"+oname+"/"+oname+".V.nc"
  infile = addfile(ifile,"R")
  V = infile->V(:,{lev},{lat1:lat2},{lon1:lon2})

  lat = infile->lat({lat1:lat2})
  lon = infile->lon({lon1:lon2})
  
  Z = uv2vr_cfd(U,V,lat,lon,1)
  
  copy_VarCoords(U,Z)
  Z&lon = lon   
  
  ;print(member(m)+"  avg("+var+") : "+avg(iV))
  
  vZ = dim_variance_n_Wrap(Z,0)
    
    res@gsnLeftString = case_name(m)
    
  plot(m) = gsn_csm_contour_map(wks,vZ,res)

    lres@xyLineThicknessF = 1.

  overlay(plot(m),gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

    lres@xyLineThicknessF = 2.

  overlay(plot(m),gsn_csm_xy(wks,(/90.,90./),(/-10.,10./),lres))
  overlay(plot(m),gsn_csm_xy(wks,(/60.,60./),(/-10.,10./),lres))
  overlay(plot(m),gsn_csm_xy(wks,(/60.,90./),(/ 10., 10./),lres))
  overlay(plot(m),gsn_csm_xy(wks,(/60.,90./),(/-10.,-10./),lres))
  
  delete([/lat,lon,U,V,Z,vZ/])
  
end do 
;end do
;===================================================================================
;===================================================================================

    pres = True
    cr = inttochar(10) 
    pres@txString                           = rds+"-"+rde+" day leads"
    ;pres@gsnPanelBottom                    = 0.1 
    pres@amJust                             = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF   = 0.01
    pres@gsnPanelFigureStrings              = (/"a","b","c","d"/) 
    pres@gsnPanelLabelBar             = True
    pres@lbLabelFontHeightF           = 0.01
    pres@lbLeftMarginF                = .5
    pres@lbRightMarginF               = .5
    pres@lbTopMarginF                 = .2
    pres@lbTitleString                = "W m~S~-2"
    pres@lbTitleFontHeightF         = 0.01
    pres@lbTitlePosition              = "Bottom"

  layout = (/1,num_m/)

  if num_m.eq.4 then layout = (/2,2/) end if

  gsn_panel(wks,plot,layout,pres)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")

end do      
;===================================================================================
;===================================================================================
end

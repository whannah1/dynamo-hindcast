load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions_CESM.ncl"

begin
	mn = (/10,10,10,10,10,10,10,11,11,11,11,11,11/)
	dy = (/01,06,11,16,21,26,31,05,10,15,20,25,30/)
	
	yr = 2011
	yrmndy = toint( yr*10000 + mn*100 + dy )
	member = sprinti("%0.2i",imem)
	case = (/"DYNAMO_"+member+"_f09_"/)+yrmndy
	
	dir  = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/"+case+"/"
	
	nRunAvg = 17
	
	fig_type  = "x11"
	
	colors = (/"blue","cyan","red","orange","green"/)
;====================================================================================================
;====================================================================================================
	num_c = dimsizes(case)
	fig_file = "MSE_budget_figures/MSE_budget.residual.map"
	
	opt  = True
	opt@lat1 = -20.
	opt@lat2 =  20.
	opt@lon1 =   0.
	opt@lon2 = 360.
	lat = LoadHClat(case(0),opt)
	lon = LoadHClon(case(0),opt)
	num_lat = dimsizes(lat)
	num_lon = dimsizes(lon)
	num_t = 20*4
;====================================================================================================
;====================================================================================================
	res = True
	res@gsnDraw  = False
	res@gsnFrame = False
	res@gsnRightStringFontHeightF  = 0.015
	res@gsnLeftStringFontHeightF   = 0.015
	res@gsnCenterStringFontHeightF = 0.015
	res@gsnCenterString = ""
	res@gsnRightString  = ""
	res@gsnLeftString   = ""
	res@tmXBLabelFontHeightF = 0.015
	res@tmYLLabelFontHeightF = 0.015
	res@tmXBMinorLengthF = -0.005
	res@tmXBMajorLengthF = -0.01
	res@tmXTMinorLengthF = -0.005
	res@tmXTMajorLengthF = -0.01
	res@trXMaxF = 360.
	res@vpHeightF = 0.2
	res@xyDashPattern 	= 0
	res@xyLineThicknessF 	= 2.
	res@tiYAxisString = "kg W m~S~-2"

	lres = res
	lres@xyDashPattern 	= 1
	lres@xyLineThicknessF 	= 1.
	lres@xyLineColor 	= "black"


		res@xyLineColors = colors
		res@pmLegendOrthogonalPosF = -1.
	  	res@pmLegendParallelPosF   =  0.35
	  	res@pmLegendWidthF         =  0.05
	  	res@pmLegendHeightF        =  0.08    
	  	;res@lgBoxMinorExtentF      = 0.1      
	  	res@lgLabelFontHeightF     = 0.01 
	  	res@xyExplicitLegendLabels = case_name  
;====================================================================================================
;====================================================================================================
	RFF = new((/num_c,num_t,num_lon/),float)
	RAF = new((/num_c,num_t,num_lon/),float)
	
		RFF!0 = "case"
  		RFF!1 = "time"
  		RFF!2 = "lon"
  		RFF&lon = lon
  		copy_VarCoords(RFF,RAF)
  		RFF@long_name = "MSE Budget Residual (Flux Form)"
  		RAF@long_name = "MSE Budget Residual (Adv. Form)"
;====================================================================================================
;====================================================================================================
do c = 0,num_c-1	
    if case(c).eq."ERAi" then
       yr1 = 0
       yr2 = 9
     else
       yr1 = 2
       yr2 = 8      
     end if  
    ;-------------------------------------------------------------
    ; RHS Forcing terms
    ;-------------------------------------------------------------
    var = "COLQR"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    COLQR = infile->$var$(:,{lat1:lat2},:)
    var = "LHFLX"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    LHFLX = infile->$var$(:,{lat1:lat2},:)
    var = "SHFLX"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    SHFLX = infile->$var$(:,{lat1:lat2},:)
    F = COLQR+LHFLX+SHFLX
      delete([/COLQR,LHFLX,SHFLX/])
    ;-------------------------------------------------------------
    ; Total Tendency
    ;-------------------------------------------------------------
    var = "MSEDT"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    MSEDT = infile->$var$(:,{lat1:lat2},:)
    ;-------------------------------------------------------------
    ; Advection/Convergence terms (flux form)
    ;-------------------------------------------------------------
    var = "delVH"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    delVH = infile->$var$(:,{lat1:lat2},:)
    var = "dHWdp"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    dHWdp = infile->$var$(:,{lat1:lat2},:)
    ;-------------------------------------------------------------
    ; Advection/Convergence terms (advective form)
    ;-------------------------------------------------------------
    var = "VdelH"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    VdelH = infile->$var$(:,{lat1:lat2},:)
    var = "WdHdp"
    infile = addfile(dir(c)+case(c)+".MSE.budget."+var+"."+yr1+"-"+yr2+".nc","r")
    WdHdp = infile->$var$(:,{lat1:lat2},:)
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
    FF = MSEDT + delVH + dHWdp - F
    AF = MSEDT + VdelH + WdHdp - F
    num_t = dimsizes(FF(:,0,0))
    RFF(c,:num_t-1,:) = dim_avg_n(FF,1)
    RAF(c,:num_t-1,:) = dim_avg_n(AF,1)
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
      delete([/FF,AF,MSEDT,delVH,dHWdp,VdelH,WdHdp,F/])
  end do
;====================================================================================================
;====================================================================================================
	  
	  ;if winter then
	  ;; Select Oct.-Apr. Season
	  ;days_per_month = (/31,28,31,30,31,30,31,31,30,31,30,31/)
	  ;do d = sum(days_per_month(:3)),sum(days_per_month(:8))-1
	  ;  days = d+ispan(0,9,1)*365
	  ;  GMS(:,days,:,:) = GMS@_FillValue
	  ;end do
	  ;end if
  
		;----------------------------------------------------------------------------------
		;----------------------------------------------------------------------------------
		;if False then 	
		;do c = 0,num_c-1
		;print("case:"+case(c)+"		num_missing: "+num(ismissing(GMS(c,:,:,:))))
		;print("			num_missing: "+num(ismissing(N1(c,:,:,:))))
		;print("			num_missing: "+num(ismissing(D1(c,:,:,:))))
		;print("")
		;end do
		;end if
		;----------------------------------------------------------------------------------
		;----------------------------------------------------------------------------------
 
;====================================================================================================
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  ;wks@wkPaperWidthF = 6.

	res@pmLegendDisplayMode    = "Always" 
	res@tmXBLabelsOn  = False
	res@gsnRightString = "Mean Residual (flux form)"
  plot(0) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(RFF,1),res)
	res@pmLegendDisplayMode    = "Never" 
	res@gsnRightString = "Mean Residual (adv. form)"
  plot(1) = gsn_csm_xy(wks,lon,dim_avg_n_Wrap(RAF,1),res)
  	res@gsnRightString = "Std. Dev. Residual (flux form)"
  plot(2) = gsn_csm_xy(wks,lon,dim_stddev_n_Wrap(RFF,1),res)
  	res@tmXBLabelsOn  = True
  	res@gsnRightString = "Std. Dev. Residual (adv. form)"
  plot(3) = gsn_csm_xy(wks,lon,dim_stddev_n_Wrap(RAF,1),res)

  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,lon,lon*0,lres))
  end do
;====================================================================================================
;====================================================================================================
  	pres = True
  	pres@gsnPanelFigureStrings 	= (/"a","b","c","d","e","f"/) ; add strings to panel
  	pres@amJust   		   	= "TopLeft"
	pres@gsnPanelBottom 		= 0.05
	pres@gsnPanelFigureStringsFontHeightF	= 0.015
	pres@amJust   = "TopLeft"
	
  
  gsn_panel(wks,plot,(/4,1/),pres)

	print("")
	print(fig_file+"."+fig_type)
	print("")
	
;====================================================================================================
;====================================================================================================
  ;delete([/lat,lon,res,plot1,plot2/])
end



; Similar to v1, but meant to compare the magnitude of the 
; residual for 12hour and 6hour budget data
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin
	dir = "/Users/whannah/DYNAMO/Hindcast/"
	
	sa = "IO"
	
	rd1 = (/00/)
	
	fig_type = "pdf"
	
	if sa.eq."nsa" then
	  member = (/"ERAi","ERAi12","EC"/)
	  memstr = (/"ERAi 6 hour","ERAi 12 hour","ECMWF Analysis"/)
	  mdash  = (/0,16,0/)
	  mcolor = (/"black","black","green"/)
	  ;member = (/"ERAi","LSF","90"/)
	  ;memstr = (/"ERAi","NSA","SP-CAM"/)
	  ;mdash  = (/0,16,0/)
	  ;mcolor = (/"black","black","purple"/)
        lat1 =   0.
		lat2 =   6.
		lon1 =  72.
		lon2 =  80.
	end if
	
	if sa.eq."ssa" then
	  member = (/"ERAi","LSF"/)
	  memstr = (/"ERAi","SSA"/)
	  mdash  = (/0,16/)
	  mcolor = (/"black","black"/)
        lat1 =  -6.
		lat2 =   0.
		lon1 =  72.
		lon2 =  80.
	end if
	
	if sa.eq."IO" then
	  ;member = (/"12","13","ERAi"/)
	  ;memstr = (/"ZM_2.0","ZM_0.2","ERAi"/)
	  ;mdash  = (/0,0,0/)
	  ;mcolor = (/"red","green","black"/)
	  member = (/"ERAi","ERAi12","EC"/)
	  memstr = (/"ERAi 6 hour","ERAi 12 hour","ECMWF Analysis"/)
	  mdash  = (/0,16,0/)
	  mcolor = (/"black","black","green"/)
		lat1 = -15.
		lat2 =  15.
		lon1 =  60.
		lon2 =  80.
	end if
	
	anom = False
	nsmooth = 0;1
	
;====================================================================================================
; Setup plot resources
;====================================================================================================
num_r = dimsizes(rd1)

  if      anom then fig_file = dir+"budget.MSE.timeseries.obs.v3.anom."+sa end if
  if .not.anom then fig_file = dir+"budget.MSE.timeseries.obs.v3."+sa      end if

  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(7*num_r,graphic)
  dum = new((/num_r,dimsizes(plot)/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@vpHeightF 					= 0.2
  	res@xyLineThicknessF				= 2.0
  	res@xyDashPattern				= 0
 	res@tiYAxisString 				= "W m~S~-~S~2"
  	res@tmXBLabelsOn  				= True
  	res@xyLineColor   				= "black"
  	res@gsnLeftStringFontHeightF 	= 0.03
  	res@gsnRightStringFontHeightF 	= 0.03
  	res@xyLineThicknessF				= 4.0
  
  	lres = res
  	lres@xyMonoLineColor	= True
  	lres@xyLineColor   	= "grey50"
  	lres@xyDashPattern 	= 0
  	lres@xyLineThicknessF	= 1.
  	lres@gsnLeftString 	= ""
  	lres@gsnRightString 	= ""
  	lres@pmLegendDisplayMode = "Never"
;====================================================================================================
; Start loops
;====================================================================================================
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  t1    = 0
;====================================================================================================
;====================================================================================================
num_m = dimsizes(member)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
  end if
  case = oname
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  ifile = dir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"r")
  
  if True then
   if member(m).ne."EC" then
    time  = infile->time(t1::4)
   else
    time  = infile->time(t1::4)
   end if
    MSEvi = avg4to1(dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    COLDP = avg4to1(dim_avg_n_Wrap( infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    MSEDT = avg4to1(dim_avg_n_Wrap( infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    VdelH = avg4to1(dim_avg_n_Wrap( infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    WdHdp = avg4to1(dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    LHFLX = avg4to1(dim_avg_n_Wrap( infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    SHFLX = avg4to1(dim_avg_n_Wrap( infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
    COLQR = avg4to1(dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/))) 
   ;end if
   ;if member(m).eq."EC" then
   ; time  = infile->time(t1::2)
   ; MSEvi = (dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; COLDP = (dim_avg_n_Wrap( infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; MSEDT = (dim_avg_n_Wrap( infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; VdelH = (dim_avg_n_Wrap( infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; WdHdp = (dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; LHFLX = (dim_avg_n_Wrap( infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; SHFLX = (dim_avg_n_Wrap( infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   ; COLQR = (dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/))) 
   ;end if
  else
   time  = infile->time;(t1::4)
   MSEvi = (dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   COLDP = (dim_avg_n_Wrap( infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   MSEDT = (dim_avg_n_Wrap( infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   VdelH = (dim_avg_n_Wrap( infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   WdHdp = (dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   LHFLX = (dim_avg_n_Wrap( infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   SHFLX = (dim_avg_n_Wrap( infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
   COLQR = (dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  end if
  
  if member(m).eq."EC" then time  = time/2. end if
  
  if member(m).eq."EC" then
  print(MSEvi+"    "+COLDP+"    "+VdelH+"    "+WdHdp+"    "+LHFLX+"    "+SHFLX+"    "+COLQR)
  end if

  ;if member(m).ne."LSF"    COLQR = avg4to1(dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/))) end if
  ;if member(m).eq."ERAi"   comQR = COLQR end if
  ;;if member(m).eq."ERAi12" comQR = COLQR end if
  ;if member(m).eq."LSF"    COLQR  = comQR(:dimsizes(MSEDT)-1) end if
  
  MSEvi = (/MSEvi/COLDP/cpd/)
  
  	if True
  	do i = 0,nsmooth-1
  	 MSEvi = smooth(MSEvi)
  	 COLDP = smooth(COLDP)
  	 MSEDT = smooth(MSEDT)
  	 VdelH = smooth(VdelH)
  	 WdHdp = smooth(WdHdp)
  	 COLQR = smooth(COLQR)
  	 LHFLX = smooth(LHFLX)
  	 SHFLX = smooth(SHFLX)
	end do
  	end if
  	if anom
  	 MSEvi = dim_rmvmean(MSEvi)
  	 COLDP = dim_rmvmean(COLDP)
  	 MSEDT = dim_rmvmean(MSEDT)
  	 VdelH = dim_rmvmean(VdelH)
  	 WdHdp = dim_rmvmean(WdHdp)
  	 COLQR = dim_rmvmean(COLQR)
  	 LHFLX = dim_rmvmean(LHFLX)
  	 SHFLX = dim_rmvmean(SHFLX)
  	end if
  
  ;R = MSEDT
  ;R = 0.
  
  SRC = COLQR+LHFLX+SHFLX
  ;print(SRC(:))
  
  if m.eq.0 then 
    R = MSEDT+VdelH+WdHdp;-COLQR-LHFLX-SHFLX
    MSEDT0 = ( MSEDT(0::2) + MSEDT(1::2) )/2.
    VdelH0 = ( VdelH(0::2) + VdelH(1::2) )/2.
    WdHdp0 = ( WdHdp(0::2) + WdHdp(1::2) )/2.
  end if
  
  if m.eq.1 then
    ;R = MSEDT0+VdelH0+WdHdp0-COLQR-LHFLX-SHFLX
    R = MSEDT+VdelH+WdHdp;-SRC;COLQR-LHFLX-SHFLX
  end if
  
  if m.eq.2 then
    R = MSEDT+VdelH+WdHdp;-SRC
  end if
  
  VdelH = -VdelH
  ;VdelS = -VdelS
  WdHdp = -WdHdp

	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
	MSEDT@_FillValue = VdelH@_FillValue
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
    ires = res
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat 		= "%D%c"
  	tres@ttmAxis   		= "XB"
  	tres@ttmMajorStride	= 10
  	time_axis_labels(time,ires,tres)
  	ires@trXMinF				= min(time)
  	ires@trXMaxF				= max(time)
  	ires@gsnRightString 		= rds+"-"+rde
  	ires@xyLineColor   		= mcolor(m)
  	ires@xyDashPattern 		= mdash(m)
  	res0 = ires
  	res1 = ires
  	res2 = ires
	res3 = ires
	res4 = ires
	res5 = ires
	res6 = ires
  	res0@gsnLeftString = "Col. Avg. MSE Anomaly"
  	res0@tiYAxisString = "K"
  	;res0@gsnLeftString = "dHdt"
	res1@gsnLeftString 	= "-V*del[h]"
  	res2@gsnLeftString 	= "-W*dh/dp"
  	res3@gsnLeftString 	= "LHF"
  	res4@gsnLeftString 	= "SHF"
  	res5@gsnLeftString 	= "QR"
  	res6@gsnLeftString 	= "value of Budget Residual"
  	
  	res6@vpWidthF  = 1.0
  	res6@vpHeightF = 0.3
	
	if sa.eq."IO" then
	  res1@trYMaxF =   60.
	  res1@trYMinF = - 60.
	  res2@trYMaxF =   60.
	  res2@trYMinF = - 60.
	  res3@trYMaxF =   60.
	  res3@trYMinF = - 60.
	  res4@trYMaxF =   40.
	  res4@trYMinF = - 40.
	  ;res6@trYMaxF =  50.
	  ;res6@trYMinF = -50.
	  res6@trYMaxF = 200;100
	  res6@trYMinF = -200;0
	else
	  if True then
	  res1@trYMaxF =   80.
	  res1@trYMinF = -200.
	  res2@trYMaxF =   80.
	  res2@trYMinF = -200.
	  res3@trYMaxF =  200.
	  res3@trYMinF =   50.
	  res4@trYMaxF =  30.
	  res4@trYMinF =   0.
	  
	  res6@trYMaxF = 180; 150.
	  res6@trYMinF = 0;-100.
	  end if
	end if
	
	V0 = MSEvi
	;V0 = MSEDT
	;V1 = VdelS
	V1 = VdelH
	V2 = WdHdp
	V3 = LHFLX
	V4 = SHFLX
	V5 = COLQR
	V6 = R;abs(R)

  if m.eq.0 then
	plot(0*num_r+r) = gsn_csm_xy(wks,time,V0,res0)
	plot(1*num_r+r) = gsn_csm_xy(wks,time,V1,res1)
	plot(2*num_r+r) = gsn_csm_xy(wks,time,V2,res2)	
	plot(3*num_r+r) = gsn_csm_xy(wks,time,V3,res3)
	plot(4*num_r+r) = gsn_csm_xy(wks,time,V4,res4)
	plot(5*num_r+r) = gsn_csm_xy(wks,time,V5,res5)
	plot(6*num_r+r) = gsn_csm_xy(wks,time,V6,res6)
    do p = 0,6*num_r,num_r
      overlay(plot(p+r),gsn_csm_xy(wks,time,time*0.,lres))
    end do
  end if
  
  	res0@gsnLeftString  = ""
  	res1@gsnLeftString  = ""
  	res2@gsnLeftString  = ""
  	res3@gsnLeftString  = ""
  	res4@gsnLeftString  = ""
  	res5@gsnLeftString  = ""
  	res6@gsnLeftString  = ""
  	res0@gsnRightString = ""
  	res1@gsnRightString = ""
  	res2@gsnRightString = ""
  	res3@gsnRightString = ""
  	res4@gsnRightString = ""
  	res5@gsnRightString = ""
  	res6@gsnRightString = ""

  overlay(plot(0*num_r+r),gsn_csm_xy(wks,time,V0,res0))
  overlay(plot(1*num_r+r),gsn_csm_xy(wks,time,V1,res1))
  overlay(plot(2*num_r+r),gsn_csm_xy(wks,time,V2,res2))
  overlay(plot(3*num_r+r),gsn_csm_xy(wks,time,V3,res3))
  overlay(plot(4*num_r+r),gsn_csm_xy(wks,time,V4,res4))
  overlay(plot(5*num_r+r),gsn_csm_xy(wks,time,V5,res5))
  overlay(plot(6*num_r+r),gsn_csm_xy(wks,time,V6,res6))
  ;if member(m).ne."LSF" then overlay(plot(5+r),gsn_csm_xy(wks,time,V5,res5)) end if
  delete([/V0,V1,V2,V3,V4,V5,V6/])
  delete([/time,cdtime,tres,ires,MSEvi,COLDP,MSEDT,VdelH,WdHdp,COLQR,LHFLX,SHFLX,R/])
  delete(SRC)
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
end do

		;---------------------------------------------
		; Add shading for events
		;---------------------------------------------
		if True then
			pgres = True
			pgres@tfPolyDrawOrder 	= "PreDraw"
			pgres@gsFillOpacityF		= 0.1
			pgres@gsFillColor   		= "grey50"
		  y1 = -1e20
		  y2 =  1e20
		  x1 = 24*(16-1 - rd1(r))
		  x2 = 24*(32-1 - rd1(r))
		  pgx1 = (/x1,x2,x2,x1,x1/)
		  pgy1 = (/y1,y1,y2,y2,y1/)
		  x1 = 24*(31+18-1 - rd1(r))
		  x2 = 24*(31+30-1 - rd1(r))
		  pgx2 = (/x1,x2,x2,x1,x1/)
		  pgy2 = (/y1,y1,y2,y2,y1/)
		  sdum = new((/2,dimsizes(plot)/),graphic)
		  do p = 0,dimsizes(plot)-1
		    sdum(0,p) = gsn_add_polygon(wks,plot(p),pgx1,pgy1,pgres)
		    sdum(1,p) = gsn_add_polygon(wks,plot(p),pgx2,pgy2,pgres)
		  end do
		end if
		;---------------------------------------------
		;---------------------------------------------

  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
end do

  	pres = True	
	pres@gsnFrame       			= False
  	pres@gsnPanelBottom 			= 0.2
  	pres@gsnPanelTop 			= 0.98
  	;pres@gsnPanelFigureStrings				= (/ "a)","b)","c)","d)","e)","f)","g)","h)","i)","j)" /)
    	pres@amJust   							= "TopLeft"
    	pres@gsnPanelFigureStringsFontHeightF	= 0.015
    	;pres@gsnPanelYWhiteSpacePercent 			= 5

  ;gsn_panel(wks,plot,(/dimsizes(plot),num_r/),pres)
  gsn_panel(wks,plot,(/4,2/),pres)

  legend = create "Legend" legendClass wks 
    "lgAutoManage"				: False
    ;"vpXF"                     : 0.4
    ;"vpYF"                     : 0.12
    "vpXF"                     	: 0.05
    "vpYF"                     	: 0.1;0.12
    "vpWidthF"                 	: 0.2   
    "vpHeightF"                	: 0.1   
    "lgPerimOn"                	: True   
    "lgItemCount"              	: num_m
    "lgLabelStrings"           	: memstr(::-1)
    "lgLabelsOn"               	: True     
    "lgLineLabelsOn"           	: False     
    "lgLabelFontHeightF"       	: 0.015    
    "lgDashIndexes"            	: mdash(::-1)
    "lgLineThicknessF"         	: 3.8
    "lgLineColors"             	: mcolor(::-1)
    "lgMonoLineLabelFontColor" 	: True                  
  end create

  draw(legend)
  frame(wks)
  
    delete([/wks,plot,res/])
  
    print("")
    print("  "+fig_file+"."+fig_type)
    print("")
;====================================================================================================
;====================================================================================================
end

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin	

	member = (/"LSF_nsa","LSF_ssa"/);,"ERAi","90","09"/)
	case_name = (/"NSA","SSA","ERAi","SP-CAM","CAM5"/)
	clr = (/"black","black","black","red","blue","green"/)
	
	;member = (/"ERAi","90","09"/)
	;case_name = (/"ERAi","SP-CAM","ZM_1.0"/)
	;clr = (/"black","purple","blue","pink"/)
	
	rd1 = (/00/)

	dsh = (/0,0,0,0,0,0/)
	
	fig_type = "png"

		lat1 = -10.
		lat2 =  10.
		lon1 =  60.
		lon2 =  90.
		
		;lon1 =   0.
		;lon2 = 360.
	
	nsmooth = 3
	
	anom = True

  addErr = False
	
	xvar = "MSEvi"
	if anom then
	  if xvar.eq."MSEvi" then
       bin_min = -4
       bin_max =  4
       bin_spc = 1
      end if
      if xvar.eq."PRECT" then
       ;bin_min = -400
       ;bin_max =  400
       ;bin_spc = 50
       bin_min = -20
       bin_max =  30
       bin_spc =  5
      end if
	else
	  if xvar.eq."PRECT" then
       bin_min = 5
       bin_max = 100
       bin_spc = 5
      end if
      if xvar.eq."MSEvi" then
       bin_min = 330
       bin_max = 340
       bin_spc = 1
      end if
    end if
;===================================================================================
;===================================================================================
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_budget/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
num_r = dimsizes(rd1)
num_m = dimsizes(member)
do r = 0,num_r-1

  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  
  if anom then
    fig_file = odir+"budget.bin.term_vs_MSE.v1."+xvar+"_anom."+rds+"-"+rde
  else
    fig_file = odir+"budget.bin.term_vs_MSE.v1."+rds+"-"+rde
  end if
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"ncl_default")
  plot = new(6,graphic)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame	 		= False
  	;res@vpWidthF 		= 0.4
  	lres = res
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 1

  	res@tmXBMajorOutwardLengthF			= 0.
  	res@tmXBMinorOutwardLengthF			= 0.
  	res@tmYLMajorOutwardLengthF			= 0.
  	res@tmYLMinorOutwardLengthF			= 0.
  	res@gsnLeftStringFontHeightF 	= 0.03
  	res@gsnRightStringFontHeightF 	= 0.03
  	res@tmYLLabelFontHeightF		 	= 0.03
  	res@tmXBLabelFontHeightF		 	= 0.03
  	res@xyLineColors 	             = clr
  	res@xyDashPatterns 	= dsh
  	res@xyLineThicknessF	= 4.
  	
  	if xvar.eq."MSEvi" then res@tiXAxisString = "MSE [K]" end if
  	;if xvar.eq."PRECT" then res@tiXAxisString = "Precip [W m~S~-2~N~]" end if
  	if xvar.eq."PRECT" then res@tiXAxisString = "Precip [mm day~S~-1~N~]" end if
  	res@tiYAxisString = "W m~S~-2~N~"
;===================================================================================
;===================================================================================
  	xbin = ispan(bin_min,bin_max,bin_spc)
  	num_bin = dimsizes(xbin)

  	nvar = 5
 	  	
  	bin_str = new(nvar,string)
  	  	
  	bdim = (/nvar,num_m,num_bin/)
  	bin_val = new(bdim,float)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)
  	bin_tct = new(bdim,float)
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
  t1    = 0
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  MSEvi = infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2})
  COLDP = infile->COLDP(t1:,{lat1:lat2},{lon1:lon2})
  MSEDT = infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2})
  VdelH = infile->VdelH(t1:,{lat1:lat2},{lon1:lon2})
  ;udHdx = infile->udHdx(t1:,{lat1:lat2},{lon1:lon2})
  ;vdHdy = infile->vdHdy(t1:,{lat1:lat2},{lon1:lon2})
  WdHdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
  ;COLQR = infile->COLQR(t1:,{lat1:lat2},{lon1:lon2})
  LHFLX = infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2})
  SHFLX = infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2})
  MSEvi = (/MSEvi/COLDP/cpd/)
  ifile = idir+"data/"+oname+"/"+oname+".budget.DSE.nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  WdSdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
  ;PRECT = infile->PRECT(t1:,{lat1:lat2},{lon1:lon2}) 
  ;if member(m).ne."ERAi" then PRECT = (/PRECT*1000./) end if
  ;PRECT = (/PRECT /1000. /Lv *1000. *(24.*3600.) /)

  ;print(min(PRECT))
  ;print(avg(PRECT))
  ;print(max(PRECT))
  ;exit
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------	
  if anom MSEvi = dim_rmvmean_n(MSEvi,0) end if
  ;if anom PRECT = dim_rmvmean_n(PRECT,0) end if
  ;;if anom PRECT = dim_rmvmed_n(PRECT,0) end if
  	
  	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
  	;PRECT = where(ismissing(PRECT),VdelH@_FillValue,PRECT)
    MSEDT@_FillValue = VdelH@_FillValue

    ;PRECT@_FillValue = VdelH@_FillValue
	
	SFC_FLX = LHFLX
    SFC_FLX = (/SFC_FLX+SHFLX/)
    ;F = COLQR
    ;F = (/F+SFC_FLX/)
    
    ;F = MSEDT+VdelH+WdHdp
    
    ;udHdx = -udHdx
    ;vdHdy = -vdHdy
    VdelH = -VdelH
    WdHdp = -WdHdp
    
    MSEvi@long_name   = "Column Avg. MSE Anomaly"
    MSEDT@long_name   = "Total MSE Tendency"
    ;COLQR@long_name   = "Column Radiative Heating"
    SFC_FLX@long_name = "Surface Fluxes"
    VdelH@long_name   = "-V*del[h]"
    WdHdp@long_name   = "-W*dh/dp"
    ;F@long_name       = "MSE Source"
    WdSdp@long_name   = "-W*ds/dp"
    ;udHdx@long_name   = "-U*dH/dx"
    ;vdHdy@long_name   = "-V*dH/dy"
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
  if xvar.eq."MSEvi" then Vx = MSEvi end if
  if xvar.eq."PRECT" then Vx = PRECT end if
  do v = 0,nvar-1
    ;if v.eq.2 then Vy = udHdx  	end if
    ;if v.eq.1 then Vy = vdHdy  	end if
    
    ;if v.eq.3 then Vy = VdelH    end if
    ;if v.eq.0 then Vy = SFC_FLX end if
    ;if v.eq.1 then Vy = COLQR 	end if
    ;if v.eq.2 then Vy = udHdx   end if
    ;if v.eq.4 then Vy = WdHdp  	end if

    if v.eq.3 then Vy = VdelH    end if
    if v.eq.0 then Vy = SFC_FLX end if
    if v.eq.1 then Vy = VdelH   end if
    if v.eq.2 then Vy = VdelH   end if
    if v.eq.4 then Vy = WdHdp   end if

    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      condition = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      tmp = where(condition,Vy,Vy@_FillValue)
      alt = where(condition.and.(MSEDT.lt.0.),Vy,Vy@_FillValue)
      if all(ismissing(tmp)) then
        print("!!! ALL TMP VALUES ARE MISSING !!!")
        print("  bin_bot: "+bin_bot)
        print("  bin_top: "+bin_top)
        print("")
      else
        bin_val(v,m,b) =    avg(tmp)
        bin_std(v,m,b) = stddev(tmp)
        bin_cnt(v,m,b) = num(condition)
        ;bin_tct(v,m,b) = avg(dim_num_n(condition,0))/4. * avg(dim_num_n(condition,(/1,2/)))
        ; Estimate Degrees of Freedom
        nt = dimsizes(condition(:,0,0))
        tct = new(nt,float)
        tct = 0
        tfac = 4
        sfac = 5
        do t = 0,nt/tfac-1
        do y = 0,dimsizes(condition(0,:,0))/sfac-1
        do x = 0,dimsizes(condition(0,0,:))/sfac-1
          tt = (/ t*tfac , t*tfac+tfac-1 /)
          yy = (/ y*sfac , y*sfac+sfac-1 /)
          xx = (/ x*sfac , x*sfac+sfac-1 /)
          if num(condition(tt(0):tt(1),yy(0):yy(1),xx(0):xx(1))).ge.1 then
            tct(t) = tct(t) + 1
          end if
        end do
        end do
        end do
        bin_tct(v,m,b) = sum(tct)
        delete([/tct,xx,yy,tt/])
        
      end if
      delete([/tmp,alt,condition/])
    end do
    bin_str(v) = Vy@long_name
  end do
  delete([/Vx,Vy/])
    print("		bin: "+xbin+"	"+bin_tct(0,m,:))
    print("")
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
    delete([/MSEvi,COLDP,MSEDT,VdelH,WdHdp,WdSdp/])	
    delete([/LHFLX,SHFLX,SFC_FLX/])	
    delete([/COLQR,udHdx,vdHdy,PRECT,F/]) 
end do

  tval = 1.960	; 95% confidence (2 tail) 
  ;tval = 1.645	; 95% confidence
  ;tval = 2.326	; 99% confidence

  
   ;dof = bin_cnt/100.;(4.*2.)
   dof = bin_tct
   dof = where(dof.gt.1,dof,dof@_FillValue)
   upConf = bin_val(:,:,:) + tval*bin_std(:,:,:)/sqrt(dof-1.)
   dnConf = bin_val(:,:,:) - tval*bin_std(:,:,:)/sqrt(dof-1.)
  
  ;tstat = new((/nvar,num_m-2,num_bin/),float)
  ;mo = 3
  ;do m = 0,num_m-1
   ;mm = m+1
   ;sigma = sqrt( (bin_cnt(:,mo,:)*bin_std(:,mo,:)^2. + bin_cnt(:,mm,:)*bin_std(:,mm,:)^2.) / (bin_cnt(:,mo,:)+bin_cnt(:,mm,:)-2) )
   ;tstat(:,m,:) = ( bin_val(:,mo,:)-bin_val(:,mm,:) ) / (sigma*sqrt( 1/bin_cnt(:,mo,:) + 1/bin_cnt(:,mm,:) ))
  ;end do
;===================================================================================
; Create plot
;===================================================================================
	res@tmXBMode = "Explicit"
	res@tmXBValues = xbin(::1)
  	res@tmXBLabels = xbin(::1)
  	;res@tmXBLabelAngleF = -45.
  	res@trXMinF = min(xbin)
  	res@trXMaxF = max(xbin)
  	res@gsnRightString 	= ""
  	
  	res@xyLineThicknessF = 4.
  
  ; Advection	
  	tres = res
  	tres@trYMinF = -100.
  	tres@trYMaxF =   50.
  p = 3
  	tres@gsnLeftString 	= bin_str(p)
  plot(p) = gsn_csm_xy(wks,xbin,bin_val(p,:,:),tres)
  p = 4
    tres@gsnLeftString  = bin_str(p)
  plot(p) = gsn_csm_xy(wks,xbin,bin_val(p,:,:),tres)
  delete(tres)
  
  ; Surface Fluxes
  p = 0
    tres = res
  	tres@trYMinF =  50.
  	tres@trYMaxF = 200.
  	tres@gsnLeftString 	= bin_str(p)
  plot(p) = gsn_csm_xy(wks,xbin,bin_val(p,:,:),tres)
  delete(tres)
  
  ; Radiative heating 
  p = 1
    tres = res
    tres@trYMinF = -150.
  	tres@trYMaxF =    0.
    tres@gsnLeftString  = bin_str(p)
  plot(p) = gsn_csm_xy(wks,xbin,bin_val(p,:,:),tres)
  delete(tres)
  
  ; ?
  p = 2 
    tres = res
  	;tres@trYMinF = -100.
  	;tres@trYMaxF =   50.
    tres@gsnLeftString  = bin_str(p)
  plot(p) = gsn_csm_xy(wks,xbin,bin_val(p,:,:),tres)

  ;---------------------------------------
  ; Significance test
  ;---------------------------------------
  delete(tres@xyLineColors)	
    tres@gsnLeftString = ""
    	tres@xyLineThicknessF = 4.
    	tres@xyLineColors 	  = clr(1:2)
  ;do p = 0,2
  ;overlay( plot(p) , gsn_csm_xy(wks,xbin,where(abs(tstat(p,:,:)).gt.tval,bin_val(p,1:2,:),bin_val@_FillValue),tres) )
  ;end do
  ;---------------------------------------
  ; Significance test
  ;---------------------------------------
  	delete(tres@xyLineColors)	
  	tres@gsnLeftString 		= ""
    	tres@xyLineThicknessF 	= 4.
    	tres@xyLineColors 	  	= clr
    	tres@xyMarkLineMode		= "Markers"
    	tres@xyMarkerColors 		= clr
    	tres@xyMarkerSizeF		= 0.015
    	ures = tres
    	dres = tres
    	ures@xyMarker			= 8
    	dres@xyMarker			= 7
  if addErr then
  do p = 0,4
    overlay( plot(p) , gsn_csm_xy(wks,xbin,upConf(p,:,:),ures) )
    overlay( plot(p) , gsn_csm_xy(wks,xbin,dnConf(p,:,:),dres) )
  end do
  end if
  ;---------------------------------------
  ;---------------------------------------
  delete(tres)

  	count = bin_cnt(0,:,:)
  	do m = 0,num_m-1 
  	  count(m,:) = bin_cnt(0,m,:)/sum(bin_cnt(0,m,:)) *100.
  	end do
  	res@gsnLeftString = "fraction of occurrence"
  	res@tiYAxisString = "%"
  plot(5) = gsn_csm_xy(wks,xbin,count,res)

  
  do p = 0,dimsizes(plot)-1
    lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 1
    xx = (/-1000.,1000./)
    yy = (/0.,0./)
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    xx = (/0.,0./)
    yy = (/-1000.,1000./)    
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
  end do
;===================================================================================
;===================================================================================
  	pres = True
  	pres@gsnFrame = False
    	pres@gsnPanelFigureStrings				= (/ "a","b","c","d","e","f","g","h", \
    	                                			     "i","j","k","l","m","n","o","p"  /) 
    	pres@amJust   							= "TopLeft"
    	pres@gsnPanelFigureStringsFontHeightF	= 0.015
    	pres@gsnPanelYWhiteSpacePercent 	= 5
    	pres@gsnPanelLabelBar 	= True

  gsn_panel(wks,plot,(/2,3/),pres)
  ;gsn_panel(wks,plot(1:4),(/2,2/),pres)
  
  legend = create "Legend" legendClass wks 
    "lgAutoManage"				: False
    ;"vpXF"                     : 0.4
    ;"vpYF"                     : 0.16
    "vpXF"                     : 0.415
    "vpYF"                     : 0.36
    "vpWidthF"                 : 0.11
    "vpHeightF"                : 0.09   
    "lgPerimOn"                : True
    "lgPerimFill"				: "SolidFill"   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.012   
    "lgDashIndexes"            : dsh
    "lgLineThicknessF"         : 3.8
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
  	
  	delete([/res,lres/])

end do
end


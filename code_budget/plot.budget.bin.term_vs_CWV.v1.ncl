; based on code_budget/plot.budget.bin.term_vs_MSE.v5.ncl
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
load "~/NCL/custom_functions.ncl"
begin       
    member      = (/"ERAi","90","09"/)
    case_name   = (/"ERAi","SP-CAM","CAM5"/)
    clr         = (/"black","blue","red"/)
    fclr        = (/"grey","cyan","pink"/)
    
    rd1 = (/5/)

    fig_type = "png"

    lat1 = -10.
    lat2 =  10.
    lon1 =  50.
    lon2 = 100.

    add_err = True

    verbose = False
;===================================================================================
;===================================================================================
    num_r = dimsizes(rd1)
    num_m = dimsizes(member)
    nvar  = 4

    bins = True
    bins@verbose = False
    bins@bin_min = -15
    bins@bin_max =  15
    bins@bin_spc =  1

    bins@bin_min =  10
    bins@bin_max =  70
    bins@bin_spc =  2

    xbin    = ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bin_str = new(nvar,string)
    bdim   = (/nvar,num_m,num_bin/)
    binval = new(bdim,float)
    binstd = new(bdim,float)
    bincnt = new(bdim,float)

    dsh = (/0,0,0,0,0,0/)

    idir = "~/Research/DYNAMO/Hindcast/data/repacked/"
    odir = "~/Research/DYNAMO/Hindcast/figs_budget/"
;===================================================================================
;===================================================================================

do r = 0,num_r-1

    rds = sprinti("%0.2i",rd1(r))
    rde = sprinti("%0.2i",rd1(r)+5-1)
    
    fig_file = odir+"budget.bin.term_vs_CWV.v1."+rds+"-"+rde
    
    wks = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"ncl_default")
    plot = new(nvar,graphic)
        res = setres_default()
        res@gsnLeftStringFontHeightF    = 0.03
        res@gsnRightStringFontHeightF   = 0.03
        res@tmYLLabelFontHeightF        = 0.02
        res@tmXBLabelFontHeightF        = 0.02
        res@tiXAxisFontHeightF          = 0.03
        res@tiYAxisFontHeightF          = 0.03
        res@xyLineColors                = clr
        res@xyDashPatterns              = dsh
        res@xyLineThicknessF            = 10.
        res@tiYAxisString               = "W m~S~-2~N~"
        res@tiXAxisString               = "MSE [K]"
        res@tmXBLabelAngleF             = -45.

        lres = setres_default()
        lres@xyLineColor                = "black"
        lres@xyLineThicknessF           = 2.
        lres@xyDashPattern              = 1
;===================================================================================
;===================================================================================
do m = 0,num_m-1
    oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
    ;----------------------------------------------------------------------
    ; Load MSE budget data
    ;----------------------------------------------------------------------
    t1    = 0
    ifile = idir+oname+"/"+oname+".budget.MSE.nc"
    print(""+ifile)
    infile = addfile(ifile,"R")
    ;MSEvi = infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2})
    COLDP = infile->COLDP(t1:,{lat1:lat2},{lon1:lon2})
    MSEDT = infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2})
    VdelH = infile->VdelH(t1:,{lat1:lat2},{lon1:lon2})
    udHdx = infile->udHdx(t1:,{lat1:lat2},{lon1:lon2})
    vdHdy = infile->vdHdy(t1:,{lat1:lat2},{lon1:lon2})
    WdHdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
    COLQR = infile->COLQR(t1:,{lat1:lat2},{lon1:lon2})
    LHFLX = infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2})
    SHFLX = infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2})
    ;MSEvi = (/MSEvi/COLDP/cpd/)
    ;MSEvi = dim_rmvmean_n(MSEvi,0)

    ifile = idir+oname+"/"+oname+".CWV.nc"
    print(""+ifile)
    infile = addfile(ifile,"R")
    CWV = infile->CWV(t1:,{lat1:lat2},{lon1:lon2})
    ;CWV = dim_rmvmean_n(CWV,0)

printMAM(CWV)

    MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)  
    MSEDT@_FillValue = VdelH@_FillValue

    SFC_FLX = LHFLX
    SFC_FLX = (/SFC_FLX+SHFLX/)
    
    udHdx = -udHdx
    vdHdy = -vdHdy
    VdelH = -VdelH
    WdHdp = -WdHdp

    CWV@long_name   = "Column Avg. MSE Anomaly"
    ;MSEvi@long_name   = "Column Avg. MSE Anomaly"
    MSEDT@long_name   = "Total MSE Tendency"
    COLQR@long_name   = "Column Radiative Heating"
    SFC_FLX@long_name = "Surface Fluxes"
    VdelH@long_name   = "<-V*del[h]>"
    WdHdp@long_name   = "<-W*dh/dp>"
    udHdx@long_name   = "<-U*dH/dx>"
    vdHdy@long_name   = "<-V*dH/dy>"
    ;---------------------------------------------------------------------- 
    ; Load DSE budget data
    ;---------------------------------------------------------------------- 
    if False then 
    ifile = idir+oname+"/"+oname+".budget.DSE.nc"

    print(""+ifile)
    infile = addfile(ifile,"R")
    WdSdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
    PRECT = infile->PRECT(t1:,{lat1:lat2},{lon1:lon2}) 
    if member(m).ne."ERAi" then PRECT = (/PRECT*1000./) end if
    
    PRECT = (/PRECT /1000. /Lv *1000. *(24.*3600.) /)
    PRECT = where(ismissing(PRECT),VdelH@_FillValue,PRECT)
    PRECT@_FillValue = VdelH@_FillValue

    WdSdp@long_name   = "-W*ds/dp"
    end if
    ;----------------------------------------------------------------------
    ; Bin average the data
    ;----------------------------------------------------------------------
    Vx = CWV
    do v = 0,nvar-1
        if v.eq.0 then Vy = udHdx      end if
        if v.eq.1 then Vy = vdHdy      end if
        ;if v.eq.0 then Vy = SFC_FLX end if
        ;if v.eq.1 then Vy = COLQR   end if
        if v.eq.2 then Vy = VdelH   end if
        if v.eq.3 then Vy = WdHdp   end if
        
        tmp = bin_YbyX(Vy,Vx,bins)
        binval(v,m,:) = tmp
        bincnt(v,m,:) = tmp@cnt
        binstd(v,m,:) = tmp@std
        delete([/tmp/])
        bin_str(v)     = Vy@long_name

    end do
    delete([/Vx,Vy/])
    if verbose then print("     bin: "+xbin+"   "+bincnt(0,m,:)) end if
    print("")
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
    delete([/CWV,COLDP,MSEDT,VdelH,WdHdp,udHdx,vdHdy/])   
    delete([/COLQR,LHFLX,SHFLX,SFC_FLX/])   
    if isvar("WdSdp") then delete(WdSdp) end if
    if isvar("PRECT") then delete(PRECT) end if
end do
;===================================================================================
;===================================================================================
if add_err then
    tval = 1.960    ; 95% confidence (2 tail) 
    
    dof = bincnt/(5.*4.*2.)
    dof = where(dof.gt.1,dof,dof@_FillValue)
    upConf = binval(:,:,:) + tval*binstd(:,:,:)/sqrt(dof-1.)
    dnConf = binval(:,:,:) - tval*binstd(:,:,:)/sqrt(dof-1.)
end if
;===================================================================================
; Create plot
;===================================================================================
        res@tmXBMode        = "Explicit"
        res@tmXBValues      = xbin(::1)
        res@tmXBLabels      = xbin(::1)
        res@trXMinF         = min(xbin)
        res@trXMaxF         = max(xbin)
        res@gsnRightString  = ""

    Ymin = (/50     ,-150   ,-100   ,-100   /)
    Ymax = (/200    ,0      ,50     ,50     /)

    do n = 0,nvar-1
            tres = res
            ;tres@trYMinF = Ymin(n)
            ;tres@trYMaxF = Ymax(n)
            tres@gsnLeftString  = bin_str(n)
        plot(n) = gsn_csm_xy(wks,xbin,binval(n,:,:),tres)
        delete(tres)
    end do
    ;---------------------------------------
    ; Significance test / Error bars
    ;---------------------------------------
    if add_err then
            tres = res
            tres@gsnLeftString      = ""
            tres@xyLineThicknessF   = 4.
            tres@xyLineColors       = clr
        ;do p = 0,nvar-1
        ;overlay( plot(p) , gsn_csm_xy(wks,xbin,where(abs(tstat(p,:,:)).gt.tval,binval(p,:,:),binval@_FillValue),tres) )
        ;end do
            tres@gsnLeftString      = ""
            tres@xyLineThicknessF   = 4.
            tres@xyLineColors       = clr
            tres@xyMarkLineMode     = "Markers"
            tres@xyMarkerColors     = clr
            tres@xyMarkerSizeF      = 0.01
            tres@xyMarkerThicknessF = 10.
            ures = tres
            dres = tres
            ures@xyMarker           = 8
            dres@xyMarker           = 7
            lres@xyDashPattern      = 0
            lres@xyLineThicknessF   = 5.

            tlres = lres
            tlres@gsnXYFillOpacities   = 0.8
            tlres@xyLineColor           = -1 
            shadeArea = new((/2,num_bin/),float)

        splot = new(bdim,graphic)
        do p = 0,nvar-1
            ;overlay( plot(p) , gsn_csm_xy(wks,xbin,binval(p,:,:),tres) )
            ;overlay( plot(p) , gsn_csm_xy(wks,xbin,upConf(p,:,:),ures) )
            ;overlay( plot(p) , gsn_csm_xy(wks,xbin,dnConf(p,:,:),dres) )
            do m = 0,num_m-1 
            do b = 0,num_bin-1
                lres@xyLineColor        = clr(m)
                ;overlay( plot(p) , gsn_csm_xy(wks,(/xbin(b),xbin(b)/),(/dnConf(p,m,b),upConf(p,m,b)/),lres) )
                
                shadeArea(0,:) = dnConf(p,m,:)
                shadeArea(1,:) = upConf(p,m,:)
                ;tlres@gsnXYAboveFillColors  = clr(m)
                ;tlres@gsnXYBelowFillColors  = clr(m)
                tlres@gsnXYFillColors       = fclr(m)
                ;overlay( plot(p) , gsn_csm_xy(wks,xbin,shadeArea,tlres) )
                splot(p,m,v) = gsn_csm_xy(wks,xbin,shadeArea(::1,:),tlres)
                overlay( plot(p) , splot(p,m,v))
            end do
            end do
        end do
    end if
    ;---------------------------------------
    ; replace last plot with distribution
    ;---------------------------------------
    count = bincnt(0,:,:)
    do m = 0,num_m-1 
        count(m,:) = bincnt(0,m,:)/sum(bincnt(0,m,:)) *100.
    end do
        res@gsnLeftString = "fraction of occurrence"
        res@tiYAxisString = "%"
    plot(nvar-1) = gsn_csm_xy(wks,xbin,count,res)
    ;---------------------------------------
    ; add lines
    ;---------------------------------------
    do p = 0,dimsizes(plot)-1
        lres@xyLineColor        = "black"
        lres@xyLineThicknessF   = 1.
        lres@xyDashPattern      = 1
        xx = (/-1000.,1000./)
        yy = (/0.,0./)
        overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
        xx = (/0.,0./)
        yy = (/-1000.,1000./)    
        overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    end do
;===================================================================================
; Finalize plot
;===================================================================================
    pres = True
    pres@gsnFrame                           = False
    ;pres@gsnPanelFigureStrings                 = (/ "a","b","c","d","e","f"/) 
    pres@amJust                             = "TopLeft"
    pres@gsnPanelFigureStringsFontHeightF   = 0.015

    gsn_panel(wks,plot,(/2,2/),pres)
    
    legend = create "Legend" legendClass wks 
        "lgAutoManage"              : False
        "vpXF"                     : 0.62
        "vpYF"                     : 0.2
        "vpWidthF"                 : 0.11
        "vpHeightF"                : 0.09   
        "lgPerimOn"                : True
        "lgPerimFill"               : "SolidFill"   
        "lgItemCount"              : num_m
        "lgLabelStrings"           : case_name
        "lgLabelsOn"               : True     
        "lgLineLabelsOn"           : False     
        "lgLabelFontHeightF"       : 0.012   
        "lgDashIndexes"            : dsh
        "lgLineThicknessF"         : 8
        "lgLineColors"             : clr
        "lgMonoLineLabelFontColor" : True                  
    end create

    draw(legend)
    frame(wks)
    
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    
    ;trimPNG(fig_file)

    delete([/res,lres/])

end do
end


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin
	dir = "/Users/whannah/DYNAMO/Hindcast/"
	
	;member = (/"09","12","13"/)
	member = (/"ERAi","12","09","13"/)
	
	;rd1 = (/00,05,10,15/)
	rd1 = (/00/)
	
	nsmooth = 3
	
	fig_type = "x11"
	
	mdash  = (/0,0,0,0/)
	mcolor = (/"black","blue","red","green"/)
	
	lat1 =  0.
	lat2 =  7.
	lon1 = 70.
	lon2 = 80.
	
	anom = False
;====================================================================================================
;====================================================================================================
num_r = dimsizes(rd1)
do r = 0,num_r-1
	rds = sprinti("%0.2i",rd1(r))
  	rde = sprinti("%0.2i",rd1(r)+5-1)
  	if anom then
	  fig_file = dir+"budget.MSE.timeseries.v4.anom."+rds+"-"+rde
	else
	  fig_file = dir+"budget.MSE.timeseries.v4."+rds+"-"+rde
	end if

  ifile = "/Users/whannah/DYNAMO/ERAi/ERAi.budget.MSE.nc"
  infile = addfile(ifile,"R")
  t1    = (30+rd1(r))*4
  t2 	= t1+(13*5)*4-1
  time  = infile->time(t1:t2:4)
  oMSEvi = avg4to1(dim_avg_n_Wrap( infile->MSEvi(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oCOLDP = avg4to1(dim_avg_n_Wrap( infile->COLDP(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oMSEDT = avg4to1(dim_avg_n_Wrap( infile->MSEDT(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oVdelH = avg4to1(dim_avg_n_Wrap( infile->VdelH(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oudHdx = avg4to1(dim_avg_n_Wrap( infile->udHdx(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  ovdHdy = avg4to1(dim_avg_n_Wrap( infile->vdHdy(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oWdHdp = avg4to1(dim_avg_n_Wrap( infile->WdHdp(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oCOLQR = avg4to1(dim_avg_n_Wrap( infile->COLQR(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oLHFLX = avg4to1(dim_avg_n_Wrap( infile->LHFLX(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oSHFLX = avg4to1(dim_avg_n_Wrap( infile->SHFLX(t1:t2,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  oMSEvi = (/oMSEvi/oCOLDP/cpd/)
  	if True
  	do i = 0,nsmooth-1
  	 oMSEvi = smooth(oMSEvi)
  	 oCOLDP = smooth(oCOLDP)
  	 oMSEDT = smooth(oMSEDT)
  	 oVdelH = smooth(oVdelH)
  	 oudHdx = smooth(oudHdx)
  	 ovdHdy = smooth(ovdHdy)
  	 oWdHdp = smooth(oWdHdp)
  	 oCOLQR = smooth(oCOLQR)
  	 oLHFLX = smooth(oLHFLX)
  	 oSHFLX = smooth(oSHFLX)
  	end do
  	end if
  	if anom
  	 oMSEvi = dim_rmvmean(oMSEvi)
  	 oCOLDP = dim_rmvmean(oCOLDP)
  	 oMSEDT = dim_rmvmean(oMSEDT)
  	 oVdelH = dim_rmvmean(oVdelH)
  	 oudHdx = dim_rmvmean(oudHdx)
  	 ovdHdy = dim_rmvmean(ovdHdy)
  	 oWdHdp = dim_rmvmean(oWdHdp)
  	 oCOLQR = dim_rmvmean(oCOLQR)
  	 oLHFLX = dim_rmvmean(oLHFLX)
  	 oSHFLX = dim_rmvmean(oSHFLX)
  	end if
  oVdelH = -oVdelH
  oWdHdp = -oWdHdp
  oudHdx = -oudHdx
  ovdHdy = -ovdHdy
  oF = oCOLQR
  oF = (/oF+oLHFLX+oSHFLX/)
  oSFC_FLX = oLHFLX
  oSFC_FLX = (/oSFC_FLX+oSHFLX/)
;====================================================================================================
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  	res = True
  	res@gsnDraw 		= False
  	res@gsnFrame 		= False
  	res@vpHeightF 		= 0.3
  	res@xyLineThicknessF	= 3.0
  	res@xyDashPattern	= 0
  	;res@tmXBLabelAngleF	= -45.
  	res@trXMinF		= min(time)
  	res@trXMaxF		= max(time)
 	res@tiYAxisString 	= "W m~S~-~S~2"
  	res@tmXBLabelsOn  = True
  	res@xyLineColor   = "black"
  	res@gsnLeftStringFontHeightF = 0.03
  	if anom then
  	res@trYMinF = -100.
  	res@trYMaxF =  100.
  	else
  	res@trYMinF = -200.
  	res@trYMaxF =  200.
  	end if
  
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat 		= "%D%c"
  	tres@ttmAxis   		= "XB"
  	tres@ttmMajorStride	= 10
  	time_axis_labels(time,res,tres)
  	
  	ires = res
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  	res0 = ires
  	res0@gsnLeftString = "Column Avg. MSE"
  	res0@tiYAxisString = "K"
  	if anom then
  	res0@trYMinF = -3.
  	res0@trYMaxF =  3.
  	else
  	res0@trYMinF = 332.
  	res0@trYMaxF = 341.
  	end if
  plot(0) = gsn_csm_xy(wks,time,oMSEvi,res0)
  ;----------------------------------------------------------------------------
  	res1 = ires
  	res1@gsnLeftString 	= "-V*del[h]"
  plot(1) = gsn_csm_xy(wks,time,oVdelH,res1)
  ;----------------------------------------------------------------------------
  	res2 = ires
  	res2@gsnLeftString 	= "-W*dh/dp"
  plot(2) = gsn_csm_xy(wks,time,oWdHdp,res2)
  ;----------------------------------------------------------------------------
  	res3 = ires
  	res3@gsnLeftString 	= "QR+LHF+SHF"
  plot(3) = gsn_csm_xy(wks,time,oF,res3)
  ;----------------------------------------------------------------------------
  ;	res4 = ires
  ;	res4@tmXBLabelsOn  	= True
  ;	res4@gsnLeftString 	= "Surface Fluxes (LHF+SHF)"
  ;plot(4) = gsn_csm_xy(wks,time,oSFC_FLX,res4)
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  ;	res5 = ires
  ;	res5@tmXBLabelsOn  	= True
  ;	res5@gsnLeftString 	= "Column radiate heating (QR)"
  ;plot(5) = gsn_csm_xy(wks,time,oCOLQR,res5)
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
;====================================================================================================
;====================================================================================================
num_m = dimsizes(member)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  ;oname = "DYNAMO_xx_"+rds+"-"+rde
  case = oname
;====================================================================================================
;====================================================================================================
  ifile = dir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"r")
  t1    = 0
  ;time  = infile->time(t1::4)
  MSEvi = avg4to1(dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  COLDP = avg4to1(dim_avg_n_Wrap( infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  MSEDT = avg4to1(dim_avg_n_Wrap( infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  VdelH = avg4to1(dim_avg_n_Wrap( infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  ;udHdx = avg4to1(dim_avg_n_Wrap( infile->udHdx(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  ;vdHdy = avg4to1(dim_avg_n_Wrap( infile->vdHdy(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  WdHdp = avg4to1(dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  COLQR = avg4to1(dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  LHFLX = avg4to1(dim_avg_n_Wrap( infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  SHFLX = avg4to1(dim_avg_n_Wrap( infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  MSEvi = (/MSEvi/COLDP/cpd/)
  ;WdHdp = (/-WdHdp/)
  	if True
  	do i = 0,nsmooth-1
  	 MSEvi = smooth(MSEvi)
  	 COLDP = smooth(COLDP)
  	 MSEDT = smooth(MSEDT)
  	 VdelH = smooth(VdelH)
  	 ;udHdx = smooth(udHdx)
  	 ;vdHdy = smooth(vdHdy)
  	 WdHdp = smooth(WdHdp)
  	 COLQR = smooth(COLQR)
  	 LHFLX = smooth(LHFLX)
  	 SHFLX = smooth(SHFLX)
	end do
  	end if
  	if anom
  	 MSEvi = dim_rmvmean(MSEvi)
  	 COLDP = dim_rmvmean(COLDP)
  	 MSEDT = dim_rmvmean(MSEDT)
  	 VdelH = dim_rmvmean(VdelH)
  	 ;udHdx = dim_rmvmean(udHdx)
  	 ;vdHdy = dim_rmvmean(vdHdy)
  	 WdHdp = dim_rmvmean(WdHdp)
  	 COLQR = dim_rmvmean(COLQR)
  	 LHFLX = dim_rmvmean(LHFLX)
  	 SHFLX = dim_rmvmean(SHFLX)
  	end if
  SFC_FLX = LHFLX+SHFLX
  F = COLQR
  F = (/F+SFC_FLX/)
  
  RTEND = VdelH
  RWdHp = VdelH
  RESID = VdelH
  RTEND = (/-WdHdp-VdelH+COLQR+SFC_FLX		 /)
  RWdHp =-(/-MSEDT-VdelH+COLQR+SFC_FLX		 /)
  RESID = (/ MSEDT+VdelH+WdHdp-COLQR-SFC_FLX	 /)
  RTEND@long_name = "MSEDT as residual"
  RWdHp@long_name = "WdHdp as residual"
  RESID@long_name = "MSE Budget Residual"
  
  VdelH = -VdelH
  WdHdp = -WdHdp
  ;udHdx = -udHdx
  ;vdHdy = -vdHdy
	
	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
	MSEDT@_FillValue = VdelH@_FillValue
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  	res0@xyLineColor   = mcolor(m)
  	res0@xyDashPattern = mdash(m)
  overlay(plot(0),gsn_csm_xy(wks,time,MSEvi,res0))
  ;----------------------------------------------------------------------------
  	res1@xyLineColor   = mcolor(m)
  	res1@xyDashPattern = mdash(m)
  overlay(plot(1),gsn_csm_xy(wks,time,VdelH,res1))
  ;----------------------------------------------------------------------------
  	res2@xyLineColor   = mcolor(m)
  	res2@xyDashPattern = mdash(m)
  overlay(plot(2),gsn_csm_xy(wks,time,WdHdp,res2))
  ;----------------------------------------------------------------------------
  	res3@xyLineColor   = mcolor(m)
  	res3@xyDashPattern = mdash(m)
  overlay(plot(3),gsn_csm_xy(wks,time,F,res3))
  ;----------------------------------------------------------------------------
  ;	res4@xyLineColor   = mcolor(m)
  ;	res4@xyDashPattern = mdash(m)
  ;overlay(plot(4),gsn_csm_xy(wks,time,SFC_FLX,res4))
  ;----------------------------------------------------------------------------
  ;	res5@xyLineColor   = mcolor(m)
  ;	res5@xyDashPattern = mdash(m)
  ;overlay(plot(5),gsn_csm_xy(wks,time,COLQR,res5))
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
end do
;====================================================================================================
;====================================================================================================
  	res@xyMonoLineColor	= True
  	res@xyLineColor   	= "gray"
  	res@xyDashPattern 	= 0
  	res@xyLineThicknessF	= 1.
  	res@gsnLeftString 	= ""
  	res@pmLegendDisplayMode = "Never"
  do p = 0,dimsizes(plot)-1
    overlay(plot(p),gsn_csm_xy(wks,time,time*0.,res))
  end do

  	pres = True	
	pres@gsnFrame       	= False
  	pres@gsnPanelBottom 	= 0.18 
  	if anom then
  	  pres@txString 		= "Hindcast lead time "+rds+"-"+rde+" days  (anomalies at DYNAMO array)"
  	else
  	  pres@txString 		= "Hindcast lead time "+rds+"-"+rde+" days"
  	end if
  	pres@amJust   = "TopLeft"
    	if rds.eq."00" then
    	  pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)"/) 
    	end if
  	if rds.eq."05" then
    	  pres@gsnPanelFigureStrings= (/"e)","f)","g)","h)"/) 
    	end if
    	if rds.eq."10" then
    	  pres@gsnPanelFigureStrings= (/"e)","f)","g)","h)"/) 
    	end if
  	
  gsn_panel(wks,plot,(/2,2/),pres)
  
  legend = create "Legend" legendClass wks 
    "vpXF"                     : 0.4
    "vpYF"                     : 0.17
    "vpWidthF"                 : 0.2   
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m+1
    "lgLabelStrings"           : (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.015    
    "lgDashIndexes"            : (/0,0,0,0/) 
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : (/"black","blue","red","green"/)
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
    delete([/wks,plot,res/])
  
    print("")
    print("  "+fig_file+"."+fig_type)
    print("")
;====================================================================================================
;====================================================================================================
end do
end

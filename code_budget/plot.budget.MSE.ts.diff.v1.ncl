load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin
	dir = "/Users/whannah/DYNAMO/Hindcast/"
	
	member = (/"12","13"/)
	
	rd1 = (/00,05/)
	
	nsmooth = 3
	
	fig_type = "png"
	
	mdash  = (/0,0,0/)
	mcolor = (/"red","green"/)
	
        lat1 =   0.
		lat2 =   6.
		lon1 =  72.
		lon2 =  80.
	
	sa = "nsa"
	
	anom = False
	
;====================================================================================================
; Setup plot resources
;====================================================================================================
  fig_file = dir+"budget.MSE.ts.diff.v2."+sa  
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  dum = new((/2,dimsizes(plot)/),graphic)
  	res = True
  	res@gsnDraw 						= False
  	res@gsnFrame 					= False
  	res@vpHeightF 					= 0.3
  	res@xyLineThicknessF				= 2.0
  	res@xyDashPattern				= 0
 	res@tiYAxisString 				= "W m~S~-~S~2"
  	res@tmXBLabelsOn  				= True
  	res@xyLineColor   				= "black"
  	res@gsnLeftStringFontHeightF 	= 0.03
  	res@gsnRightStringFontHeightF 	= 0.03
  	;if anom then
  	;  res@trYMinF = -100.
  	;  res@trYMaxF =  100.
  	;else
  	;  res@trYMinF = -200.
  	;  res@trYMaxF =  200.
  	;end if
  
  	lres = res
  	lres@xyMonoLineColor	= True
  	lres@xyLineColor   	= "gray"
  	lres@xyDashPattern 	= 0
  	lres@xyLineThicknessF	= 1.
  	lres@gsnLeftString 	= ""
  	lres@gsnRightString 	= ""
  	lres@pmLegendDisplayMode = "Never"
  	
  	res@xyLineThicknessF	= 2.0
;====================================================================================================
; Start loops
;====================================================================================================
num_r = dimsizes(rd1)
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  t1    = 0
;====================================================================================================
;====================================================================================================
num_m = dimsizes(member)
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
  end if
  case = oname
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  ifile = dir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"r")
  time  = infile->time(t1::4)
  MSEvi = avg4to1(dim_avg_n_Wrap( infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  COLDP = avg4to1(dim_avg_n_Wrap( infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  MSEDT = avg4to1(dim_avg_n_Wrap( infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  VdelH = avg4to1(dim_avg_n_Wrap( infile->VdelH(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  WdHdp = avg4to1(dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  LHFLX = avg4to1(dim_avg_n_Wrap( infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  SHFLX = avg4to1(dim_avg_n_Wrap( infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  
  if member(m).eq."LSF"
    COLQR = MSEvi * MSEvi@_FillValue
  else
    COLQR = avg4to1(dim_avg_n_Wrap( infile->COLQR(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  end if
  
  ifile = dir+"data/"+oname+"/"+oname+".budget.DSE.nc"
  infile = addfile(ifile,"r")
  DSEvi = avg4to1(dim_avg_n_Wrap( infile->DSEvi(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  WdSdp = avg4to1(dim_avg_n_Wrap( infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)))
  
  MSEvi = (/MSEvi/COLDP/cpd/)
  DSEvi = (/DSEvi/COLDP/cpd/)
  
  	if True
  	do i = 0,nsmooth-1
  	 MSEvi = smooth(MSEvi)
  	 DSEvi = smooth(DSEvi)
  	 COLDP = smooth(COLDP)
  	 MSEDT = smooth(MSEDT)
  	 VdelH = smooth(VdelH)
  	 WdHdp = smooth(WdHdp)
  	 COLQR = smooth(COLQR)
  	 LHFLX = smooth(LHFLX)
  	 SHFLX = smooth(SHFLX)
  	 WdSdp = smooth(WdSdp)
	end do
  	end if
  	if anom
  	 MSEvi = dim_rmvmean(MSEvi)
  	 DSEvi = dim_rmvmean(DSEvi)
  	 COLDP = dim_rmvmean(COLDP)
  	 MSEDT = dim_rmvmean(MSEDT)
  	 VdelH = dim_rmvmean(VdelH)
  	 WdHdp = dim_rmvmean(WdHdp)
  	 COLQR = dim_rmvmean(COLQR)
  	 LHFLX = dim_rmvmean(LHFLX)
  	 SHFLX = dim_rmvmean(SHFLX)
  	 WdSdp = dim_rmvmean(WdSdp)
  	end if
  F = COLQR
  F = (/F+LHFLX+SHFLX/)
  
  ;if any( member(m).eq.(/"LSF","ERAi"/) ) then
    ;F = F*0.
    F = MSEDT+VdelH+WdHdp
  ;end if
  
  VdelH = -VdelH
  WdHdp = -WdHdp
  ;WdSdp = -WdSdp
	
	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
	MSEDT@_FillValue = VdelH@_FillValue
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
    ires = res
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat 		= "%D%c"
  	tres@ttmAxis   		= "XB"
  	tres@ttmMajorStride	= 10
  	time_axis_labels(time,ires,tres)
  	ires@trXMinF				= min(time)
  	ires@trXMaxF				= max(time)
  	ires@gsnRightString 		= rds+"-"+rde
  	res0 = ires
  	res0@gsnLeftString = "Column Avg. MSE"
  	res0@tiYAxisString = "K"
  	if anom then
  	  res0@trYMinF = -3.
  	  res0@trYMaxF =  3.
  	else
  	  res0@trYMinF = 332.
  	  res0@trYMaxF = 341.
  	end if
  	res1 = ires
	res1@gsnLeftString 	= "";"-V*del[h]"
	res2 = ires
  	res2@gsnLeftString 	= "";"-W*dh/dp"
	res3 = ires
  	res3@gsnLeftString 	= "";"QR+LHF+SHF"
  	
  	;res0@xyLineColor   = mcolor(m)
  	;res1@xyLineColor   = mcolor(m)
	;res2@xyLineColor   = mcolor(m)
	;res3@xyLineColor   = mcolor(m)
	;res0@xyDashPattern = mdash(m)
	;res1@xyDashPattern = mdash(m)
	;res2@xyDashPattern = mdash(m)
	;res3@xyDashPattern = mdash(m)
	
	res1@xyMonoDashPattern	= False
	res1@xyLineColors		= (/"red","black","black","black"/)
	res1@xyDashPatterns 		= (/0,0,1,2/)
	
	res0@xyLineColors   = mcolor
	res0@xyDashPatterns = mdash
	
	  res1@trYMaxF =  210.
	  res1@trYMinF = -210.
	  
	  ;res2@trYMaxF =  100.
	  ;res2@trYMinF = -180.
	  
	  ;res3@trYMaxF =  240.
	  ;res3@trYMinF =  -80.
	
  if m.eq.0 then
    iMSEDT = MSEDT
    iMSEvi = MSEvi
    iVdelH = VdelH
    iWdHdp = WdHdp
    iF     = F
  else
    dMSEvi = (iMSEvi - MSEvi)*100.
    dMSEDT = iMSEDT - MSEDT
    dVdelH = iVdelH - VdelH
    dWdHdp = iWdHdp - WdHdp
    dF     = iF - F
	plot(0+r) = gsn_csm_xy(wks,time,(/iMSEvi,MSEvi/),res0)
	plot(2+r) = gsn_csm_xy(wks,time,(/dMSEvi,dVdelH,dWdHdp,dF/)	,res1)
  end if
  
    delete([/cdtime,tres,ires,MSEvi,COLDP,MSEDT,VdelH,WdHdp,COLQR,LHFLX,SHFLX,F,DSEvi,WdSdp/])
  
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
end do


		;---------------------------------------------
		; Add shading for events
		;---------------------------------------------
		if True then
			pgres = True
			pgres@tfPolyDrawOrder 	= "PreDraw"
			pgres@gsFillOpacityF		= 0.1
			pgres@gsFillColor   		= "grey"
		  y1 = -1e20
		  y2 =  1e20
		  x1 = 24*(16-1 - rd1(r))
		  x2 = 24*(32-1 - rd1(r))
		  pgx1 = (/x1,x2,x2,x1,x1/)
		  pgy1 = (/y1,y1,y2,y2,y1/)
		  x1 = 24*(31+18-1 - rd1(r))
		  x2 = 24*(31+30-1 - rd1(r))
		  pgx2 = (/x1,x2,x2,x1,x1/)
		  pgy2 = (/y1,y1,y2,y2,y1/)
		  do p = 0,2,2
		    dum(0,p+r) = gsn_add_polygon(wks,plot(p+r),pgx1,pgy1,pgres)
		    dum(1,p+r) = gsn_add_polygon(wks,plot(p+r),pgx2,pgy2,pgres)
		  end do
		end if
		;---------------------------------------------
		;---------------------------------------------

  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
end do

	do p = 0,dimsizes(plot)-1
      overlay(plot(p),gsn_csm_xy(wks,time,time*0.,lres))
    end do

  	pres = True	
	pres@gsnFrame       			= False
  	;pres@gsnPanelBottom 			= 0.13
  	;pres@gsnPanelTop 			= 0.98
  	pres@amJust   				= "TopLeft"
    	pres@gsnPanelFigureStrings	= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 

  gsn_panel(wks,plot,(/2,2/),pres)

  legend1 = create "Legend" legendClass wks 
    ;"vpXF"                     : 0.2
    ;"vpYF"                     : 0.2
    ;"vpWidthF"                 : 0.2   
    ;"vpHeightF"                : 0.1   
    "vpXF"                     : 0.395
    "vpYF"                     : 0.74
    "vpWidthF"                 : 0.08   
    "vpHeightF"                : 0.05
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : (/"ZM_2.0","ZM_0.2"/)
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.015    
    "lgDashIndexes"            : (/0,0,0,0/) 
    "lgLineThicknessF"         : 3.8
    "lgLineColors"             : (/"red","green"/)
    "lgMonoLineLabelFontColor" : True                  
  end create
  
  legend2 = create "Legend" legendClass wks 
    "vpXF"                     : 0.425;0.6
    "vpYF"                     : 0.2
    "vpWidthF"                 : 0.25   
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : 4
    "lgLabelStrings"           : (/"MSE diff. (x100)","VdelH diff.","WdHdp diff.","F diff."/)
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.015    
    "lgDashIndexes"            : (/0,0,1,2/) 
    "lgLineThicknessF"         : 3.8
    "lgLineColors"             : (/"red","black","black","black"/)
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend1)
  draw(legend2)
  frame(wks)
  
    delete([/wks,plot,res/])
  
    print("")
    print("  "+fig_file+"."+fig_type)
    print("")
;====================================================================================================
;====================================================================================================
end

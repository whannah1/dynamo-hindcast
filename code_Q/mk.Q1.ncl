load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
;load "$NCARG_ROOT/custom_functions_obs.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"

begin

	;mn = (/10,10,10,10,10,10,10,11,11,11,11,11,11,12,12,12/)
	;dy = (/01,06,11,16,21,26,31,05,10,15,20,25,30,05,10,15/)
	mn = (/12,12,12/)
	dy = (/05,10,15/)
	
	imem = 12
	yr = 2011
	yrmndy = toint( yr*10000 + mn*100 + dy )
	member = sprinti("%0.2i",imem)
	case = (/"DYNAMO_"+member+"_f09_"/)+yrmndy
	
	var = "Q1"
	odir  = "/Users/whannah/DYNAMO/Hindcast/data/"+case+"/"	
;====================================================================================================
;====================================================================================================
num_c = dimsizes(case)
do c = 0,num_c-1
  ;=========================================================
  ; coordinate variables
  ;=========================================================
      
      opt  = True
      opt@lat1 = -15.
      opt@lat2 =  15.
      opt@lon1 =   0.
      opt@lon2 = 360.
      lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./)
      lat = LoadHClat(case(c),opt)
      lon = LoadHClon(case(c),opt)
        print("")
        print("  case: "+case(c))
        print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:  "+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:  "+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+sprintf("%6.4g",max(lev))+"	:  "+sprintf("%6.4g",min(lev)))
        print("")
      latsz = dimsizes(lat)
      lonsz = dimsizes(lon)
      levsz = dimsizes(lev)
      num_t = 20*4
  ;=========================================================
  ; Load Data
  ;=========================================================
        T     = LoadHC4D(case(c),"T"    ,lev,opt)
        Z     = LoadHC4D(case(c),"Z3"   ,lev,opt)
        QR    = LoadHCQR(case(c),"QR"   ,lev,opt)
		;Q     = LoadHC4D(case(c),"Q"    ,lev,opt)
		U     = LoadHC4D(case(c),"U"    ,lev,opt)
        V     = LoadHC4D(case(c),"V"    ,lev,opt)
        W     = LoadHC4D(case(c),"OMEGA",lev,opt)
		;LHFLX = LoadHC  (case(c),"LHFLX",lev,opt)
		;SHFLX = LoadHC  (case(c),"SHFLX",lev,opt)
		;Ps    = LoadHC  (case(c),"PS"   ,lev,opt)
	
	P  = conform(T,lev,1)*100.
	
	S = cpd*T + g*Z
    	 
        	S!0 = "time"
        	S!1 = "lev"
        	S!2 = "lat"
        	S!3 = "lon"
        	S&lev = lev
        	S&lat = lat
        	S&lon = lon        

          delete([/T,Z/])
  ;=========================================================
  ; Calculate Q1
  ;=========================================================
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;----------------------------------------------------------- 
        	xvals = ispan(0,lonsz-1,1)
		Rvals = (xvals+lonsz+1)%lonsz
		Lvals = (xvals+lonsz-1)%lonsz
		yvals = ispan(0,latsz-1,1)
		Nvals = yvals(2:latsz-1)
		Cvals = yvals(1:latsz-2)
		Svals = yvals(0:latsz-3)

        S_dx = new((/num_t,levsz,latsz-2,lonsz/),float)
        S_dy = new((/num_t,levsz,latsz-2,lonsz/),float)
            
        deltax = tofloat( (lon(2)-lon(0))*111000.*conform(S_dx,cos(lat(Cvals)*3.14159/180.),2) )
        deltay = tofloat( (lat(2)-lat(0))*111000. )
        
        S_dx  = (  S(:,:,Cvals,Rvals) -  S(:,:,Cvals,Lvals) )/deltax
        S_dy  = (  S(:,:,Nvals,:)     -  S(:,:,Svals,:)     )/deltay
        
			S_dx!2     = "lat"
			S_dx&lat   = lat(Cvals)
			S_dy!2     = "lat"
			S_dy&lat   = lat(Cvals)
      ;-----------------------------------------------------------
      ; Total Tendency   
      ;-----------------------------------------------------------   
        dt = 6.*3600.
        dSdt = new((/num_t,levsz,latsz-2,lonsz/),"float")
        dSdt(1:num_t-2,:,:,:) = ( S(2:num_t-1,:,Cvals,:) - S(0:num_t-3,:,Cvals,:) ) / dt
      ;-----------------------------------------------------------
      ; Horizontal advection/convergence
      ;-----------------------------------------------------------
        VdelS = U(:,:,Cvals,:)*S_dx + V(:,:,Cvals,:)*S_dy
          delete([/S_dx,S_dy,U,V/])
      ;-----------------------------------------------------------
      ; Vertical advection/convergence
      ;-----------------------------------------------------------
        dSdp   = new((/num_t,levsz,latsz,lonsz/),float)
        Zvals  = ispan(1,levsz-2,1) 
        altdP  = P(:,0:levsz-3,:,:) - P(:,2:levsz-1,:,:)
        dSdp(:,Zvals,:,:)   = (S(:,0:levsz-3,:,:) - S(:,2:levsz-1,:,:) ) / altdP
        WdSdp  = W(:,:,Cvals,:)*dSdp(:,:,Cvals,:)
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        Q1 = dSdt + VdelS + WdSdp
        
    unit_str = "J/kg/s"
	Q1!0 = "time"
	Q1!1 = "lev"
        Q1!2 = "lat"
        Q1!3 = "lon"
        Q1&lev = lev
        Q1&lat = lat(Cvals)
        Q1&lon = lon
        Q1@long_name = "Apparent Heat Source"
        Q1@units     = unit_str
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        ofile = odir(c)+case(c)+"."+var+".nc"
        if isfilepresent(ofile) then
          system("rm "+ofile) 
        end if
        outfile = addfile(ofile,"c")
        outfile->$var$ =  Q1
      
      print(outfile)
      print("")
      print("  "+ofile)
      print("")
  ;=========================================================
  ;=========================================================
end do
;====================================================================================================
;====================================================================================================
end

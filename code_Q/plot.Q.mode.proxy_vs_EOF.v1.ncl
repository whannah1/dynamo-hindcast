load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin	
	
	;member = (/"ERAi","12","09","13"/)
	member = (/"ERAi","LSF","12","09","13"/)
	;member = (/"ERAi"/)
	
	sa = "nsa"
	
	case_name = (/"ERAi","LSF_NSA","ZM_2.0","ZM_1.0","ZM_0.2"/)
	;case_name = (/"ctrl"/)
	
	clr = (/"red","blue","black","red","blue"/)
	dsh = (/0,0,2,1,1/)

	rd1 = (/00/)
	
	fig_type = "x11"

	lat1 =  0.
	lat2 =  6.
	lon1 = 72.
	lon2 = 80.
	
	nsmooth = 11
	
	var = "Q1_theta"
	
	anom = True
	
;===================================================================================
;===================================================================================
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_Q/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  lev =(/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,75./) 
  lev@units = "hPa"
  lev!0 = "lev"
  lev&lev = lev
  num_lev = dimsizes(lev)
  dP = new(num_lev,float)
  dP(1:num_lev-2) = (lev(0:num_lev-3)+lev(1:num_lev-2))/2. - (lev(1:num_lev-2)+lev(2:num_lev-1))/2.
  dP!0 = "lev"
  dP&lev = lev
  
  elev_bot = 975.
  elev_top = 100.
  elev = lev({elev_bot:elev_top})
  nelev = dimsizes(elev)
;===================================================================================
; begin loop
;===================================================================================
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  fig_file = odir+"Q.mode.proxy_vs_EOF.v1."+rds+"-"+rde
  num_s = toint(systemfunc("ls -d1 "+idir+"data/DYNAMO_09_f09_2011* | wc -w"))
  num_t = 4*5*num_s
  time = new((/num_t/),double)
    time = tofloat(ispan(0,(num_t-1)*6,6))/24.
    time!0 = "time"
    time&time = time
    time@units = "days since 2011-10-"+sprinti("%0.2i",1+rd1(r))
  iQ1 = new((/num_m,num_t,num_lev/),float)
  iQ2 = new((/num_m,num_t,num_lev/),float)
  Q1 = new((/num_m,num_t/4,num_lev/),float)
  Q2 = new((/num_m,num_t/4,num_lev/),float)
    iQ1!0 = "case"
  	iQ1!1 = "time"
  	iQ1!2 = "lev"
  	iQ1&time = time
  	iQ1&lev  = lev
;  	copy_VarCoords(iQ1,iQ2)
  	Q1!0 = "case"
  	Q1!1 = "time"
  	Q1!2 = "lev"
  	Q1&time = time(::4)
  	Q1&lev  = lev
;  	copy_VarCoords(Q1,Q2)
  
  
  
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"BlueYellowRed")
  plot = new(num_m*2,graphic)
  	res = True
  	res@gsnDraw 				= False
  	res@gsnFrame 			= False
  	res@gsnLeftStringFontHeightF 	= 0.02
  	res@gsnRightStringFontHeightF 	= 0.02
  	res@gsnLeftString 		= ""
  	res@gsnRightString		= ""
  	res@tmXTOn 				= False
  	res@tmYLLabelAngleF 		= 45.
  	res@vpHeightF 			= 0.3
  	;-----------------------------------
  	xres = res
  	cres = res
  	;-----------------------------------
  	xres@trYMaxF 			=  5.
  	xres@trYMinF 			= -5.
  	xres@xyLineThicknessF	= 3.
  	xres@xyLineColors		= clr
  	xres@xyDashPatterns		= dsh
  	;-----------------------------------
  	cres@tmYROn 					= False
  	cres@tmYLMinorOn				= False
  	cres@tmYLMode				= "Manual"
  	cres@tmYLTickStartF			= 1000.
  	cres@tmYLTickEndF			= 100.
  	cres@tmYLTickSpacingF		= 100.
  	cres@gsnSpreadColors 		= True
  	cres@cnFillOn 				= True
  	cres@cnLinesOn 				= False
  	cres@cnLineLabelsOn			= False
  	cres@cnInfoLabelOn      		= False
  	cres@lbLabelBarOn			= False
  	cres@lbLabelFontHeightF 		= 0.02
  	cres@lbLabelAngleF			= -45.
  	cres@lbLabelStride			= 2
  	cres@trYReverse				= True
  	cres@trYLog					= False
  	cres@cnLevelSelectionMode	= "ManualLevels"
	cres@cnMinLevelValF  		= -18.
  	cres@cnMaxLevelValF  		=  18.
 	cres@cnLevelSpacingF 		=  3.
   	;-----------------------------------
  	cdtime = cd_calendar(time,2)
  	tres = True
  	tres@ttmFormat = "%D%c"
  	tres@ttmAxis = "XB"
;===================================================================================
; Load Hindcast data
;===================================================================================
do m = 0,num_m-1
  if member(m).eq."LSF" then
    oname = "DYNAMO_"+member(m)+"_"+sa+"_"+rds+"-"+rde
    ;ifile = idir+"data/LSF/"+oname+"."+var+".nc"
  else
    oname = "DYNAMO_"+member(m)       +"_"+rds+"-"+rde
  end if
  ifile = idir+"data/"+oname+"/"+oname+"."+var+".nc"
  t1 = 0
  ;--------------------------------------------------
  infile = addfile(ifile,"R")
  if member(m).eq."LSF" then
    t2 = (31+30+14)*4-1
;print(ifile)
;printVarSummary(infile->$var$(t1:,:,0,0))
;printVarSummary(iQ1(m,:t2,:))
    iQ1(m,:t2,:) = (/ infile->$var$(t1:,:,0,0) /)
  else
;print(infile)
    iQ1(m,:,:) = (/ dim_avg_n( infile->$var$(t1:,:,{lat1:lat2},{lon1:lon2}) ,(/2,3/)) /)
  end if
  ;--------------------------------------------------
  if member(m).eq."ERAi" then
    iQ1(m,:,:) = (/iQ1(m,:,::-1)/)
  end if
end do
  
  if anom then
	iQ1 = dim_rmvmean_n_Wrap(iQ1,1)
  end if
	
  
  do m = 0,num_m-1
  do n = 0,nsmooth-1
    iQ1(m,:,:) = (/ smooth(iQ1(m,:,:)) /)
  end do
  end do
  
;===================================================================================
; average to daily values
;===================================================================================
if True then
  do m = 0,num_m-1
    Q1(m,:,:) = avg4to1(iQ1(m,:,:))
  end do
end if
;===================================================================================
; Set variable metadata
;===================================================================================
  Q1 = (/ Q1*3600.*24. /)
  Q1@units = "K/day"
  Q2@units = "K/day"
;===================================================================================  
; Define vertical mode indices
;===================================================================================    
  pbot = 975.
  ptop = 200.
  pmid = 500.
  dPtop = conform(Q1(:,:,{pmid:pbot}),dP({pmid:pbot}),2)
  dPbot = conform(Q1(:,:,{ptop:pmid}),dP({ptop:pmid}),2)
  Qctop = dim_avg_n(Q1(:,:,{pmid:pbot})*dPtop,2) / dim_avg_n(dPtop,2)
  Qcbot = dim_avg_n(Q1(:,:,{ptop:pmid})*dPbot,2) / dim_avg_n(dPbot,2)
  Qc1 = ( Qctop + Qcbot )/2.
  Qc2 = ( Qctop - Qcbot )/2.
  Qc1 = Qc1/stddev(Qc1)
  Qc2 = Qc2/stddev(Qc2)
;==================================================================================
; Calcualte EOFs
;==================================================================================
    neof = 5
    tQ1 = Q1(:,:,{elev_bot:elev_top})
    opt      = True
	opt@jopt = 0
	do m= 0,num_m-1
     eof  = eofunc   (tQ1(case|m,lev|:,time|:) ,neof,opt) 
     ;--------------------------------------------
	 do e = 0,2
	   if avg(eof(e,:10)).lt.0.0 then
	     eof(e,:) = (/-1.0*eof(e,:)/)
	   end if
	 end do
	 ;--------------------------------------------
     pc   = eofunc_ts(tQ1(case|m,lev|:,time|:),eof,False)
     ;reof = eofunc_varimax(eof,-1)
     if (m.eq.0) then
       PC = new((/num_m,neof,num_t/4/),float)
         PC!0 = "case"
         PC!1 = "mode"
         PC!2 = "time"
       pcvar = new((/num_m,neof/),float)
     end if
     PC(m,:,:)  = pc/stddev(pc)
     pcvar(m,:) = eof@pcvar
       delete([/eof,pc/])
    end do
;===================================================================================
; Create plots
;===================================================================================
  time_axis_labels(time,cres,tres)
  time_axis_labels(time,xres,tres)
do m = 0,num_m-1
	cres@gsnLeftString 	= case_name(m)
	xres@gsnLeftString 	= case_name(m)
  	cres@gsnRightString = "Q1"
  plot(0+m*2) = gsn_csm_pres_hgt(wks,Q1(case|m,lev|:,time|:),cres)
  	xres@gsnRightString = "Q1 vertical mode indices"
  plot(1+m*2) = gsn_csm_xy(wks,time(::4),(/Qc1(m,:),Qc2(m,:),Qc1(m,:)*0.,PC(m,0,:),PC(m,1,:)/),xres)
  cc1 = esccr(Qc1(m,:),PC(m,0,:),0)
  cc2 = esccr(Qc2(m,:),PC(m,1,:),0)
  print(member(m)+"	"+cc1+"		"+cc2)
end do
;===================================================================================
; Remove Right Y-Axis Label
;===================================================================================
  getvalues plot@contour                                                     
  "pmAnnoManagers" : am_ids                                                
  end getvalues                                                              
  
  index = ind(NhlName(am_ids).eq."right_axis")  
  if(.not.ismissing(index)) then                                             
    NhlRemoveAnnotation(plot@contour,am_ids(index))                          
  end if
;===================================================================================
; Create panel plot
;===================================================================================
  	pres = True
  	cr = inttochar(10) 
    	pres@txString = rds+"-"+rde+"   "+lat1+":"+lat2+"N / "+lon1+":"+lon2+"E"
    	pres@gsnPanelLabelBar    		= True
    	pres@gsnPanelXWhiteSpacePercent = 5.
    	pres@gsnPanelXWhiteSpacePercent = 5.

  ;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  gsn_panel(wks,plot,(/num_m,2/),pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete([/res,Q1,Qc1,Qc2/])
;===================================================================================
;===================================================================================
end do
end


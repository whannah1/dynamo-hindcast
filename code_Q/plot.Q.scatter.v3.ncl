load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin	

	member = (/"ERAi","12","09","13"/)
	
	;rd1 = (/00,05/)
	rd1 = (/05/)
	
	;clrs = (/"red","blue","green"/)
  	clrs = "blue"
	
	fig_type = "eps"

		lat1 = -5.
		lat2 =  5.
		lon1 = 60.
		lon2 = 90.
	
	anom = False
	
	lev = (/70.,100.,125.,150.,175.,200.,250.,300.,400.,500.,600.,650.,700.,750.,800.,825.,850.,875.,900.,925.,950.,975./)
;===================================================================================
;===================================================================================
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/"
  
  fig_file = odir+"Q.scatter.v3.IO"
  
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
	num_r = dimsizes(rd1)
	num_m = dimsizes(member)
	;num_x = num_r*num_m
	xdim = (/num_r,num_m/)
	X1 = new(xdim,float)
	X2 = new(xdim,float)
	Y1 = new(xdim,float)
	Y2 = new(xdim,float)
	Y3 = new(xdim,float)
	Y4 = new(xdim,float)
do m = 0,num_m-1
do r = 0,num_r-1
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  xx = r+m*num_r
  ;===================================================================================
  ; Load MSE budget terms
  ;===================================================================================
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  infile = addfile(ifile,"R")
  t1    = 0
  ;time  = infile->time(t1::4)
  MSEvi = avg4to1(infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2}) )
  COLDP = avg4to1(infile->COLDP(t1:,{lat1:lat2},{lon1:lon2}) )
  MSEvi = (/MSEvi/COLDP/cpd/)
  ;===================================================================================
  ;===================================================================================
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  t1 = 0
  ;--------------------------------------------------
  ifile = idir+"data/"+oname+"/"+oname+".Q1.nc"
  infile = addfile(ifile,"R")
  lev = infile->lev
  Q1 = dim_avg_n_Wrap( infile->Q1(t1:,:,{lat1:lat2},{lon1:lon2}) ,(/2,3/)) 
  ;--------------------------------------------------
  Q1 = (/ Q1*3600.*24./cpd /)
  Q1@units = "K/day"
  ;===================================================================================
  ;===================================================================================	
  Q1!0 = "time"
  Q1!1 = "lev"
  Q1&lev = lev
  if member(m).eq."ERAi" then
    Q1 = (/Q1(:,::-1)/)
    Q1&lev = lev(::-1)
  end if
  
    ;if r.eq.0 then
  	;  printVarSummary(Q1)
  	;end if
  	
    
  ;Qc1 = dim_sum_n(Q1(:,{600.:800.}),1) + dim_sum_n(Q1(:,{200.:400.}),1) 
  ;Qc2 = dim_sum_n(Q1(:,{600.:800.}),1) - dim_sum_n(Q1(:,{200.:400.}),1)  
  
  Qc1 = ( dim_avg_n(Q1(:,{600.:800.}),1) + dim_avg_n(Q1(:,{200.:400.}),1) );/2.
  Qc2 = ( dim_avg_n(Q1(:,{600.:800.}),1) - dim_avg_n(Q1(:,{200.:400.}),1) );/2.

  Qc3 = Q1(:,{700.}) + Q1(:,{300.})
  Qc4 = Q1(:,{700.}) - Q1(:,{300.})
  
	  X1(r,m) = stddev(MSEvi)
	  X2(r,m) = stddev(MSEvi)
	  ;X2(r,m) = stddev(DSEvi)
	  ;X2(r,m) = stddev(SFC_FLX)
	  
	  Y1(r,m) = avg(Qc1)
	  Y2(r,m) = avg(Qc2)
	  Y3(r,m) = avg(Qc3)
	  Y4(r,m) = avg(Qc4)
	  
	  delete([/Q1,Qc1,Qc2,Qc3,Qc4/])
	  delete([/MSEvi,COLDP/])
  
end do
end do 

;===================================================================================
;===================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame 		= False
  	res@tmXTOn			= False
  	res@tmYROn 			= False
  	res@xyDashPattern 	= 0
  	res@gsnLeftStringFontHeightF	= 0.02
  	
	X1s = "MSE"
	X2s = "MSE"
	;X2s = "DSE"
	;X2s = "LHFLX"
	
	Y1s = "Q1 mode 1"
	Y2s = "Q1 mode 2"
	Y3s = "Q1 mode 1 (alt)"
	Y4s = "Q1 mode 2 (alt)"
	
	mkrs = (/16,4,6,15/)
	
	;res@xyLineColors = clrs
	res@xyLineColor = "white"
	
	res1 = res
	res2 = res
	res3 = res
	res4 = res

		res1@gsnLeftString	= Y1s+" / "+X1s
		res2@gsnLeftString 	= Y2s+" / "+X1s
		res3@gsnLeftString 	= Y3s+" / "+X2s
		res4@gsnLeftString 	= Y4s+" / "+X2s
		
		sigma = "~F8~s~F21~"
		res1@tiXAxisString 	= sigma+X1s+" (K)"
		res2@tiXAxisString 	= sigma+X1s+" (K)"
		res3@tiXAxisString 	= sigma+X2s+" (K)"
		res4@tiXAxisString 	= sigma+X2s+" (K)"
		
		opstr = "mean "
		
		res1@tiYAxisString 	= opstr+Y1s+" (K/day)"
		res2@tiYAxisString 	= opstr+Y2s+" (K/day)"
		res3@tiYAxisString 	= opstr+Y3s+" (K/day)"
		res4@tiYAxisString 	= opstr+Y4s+" (K/day)"
		
		
	plot(0) = gsn_csm_xy(wks,X1,Y1,res1)
  	plot(1) = gsn_csm_xy(wks,X1,Y2,res2)
  	plot(2) = gsn_csm_xy(wks,X2,Y3,res3)
  	plot(3) = gsn_csm_xy(wks,X2,Y4,res4)
  	
  	dum = new((/4,num_r,num_m/),graphic)
  	
  	delete([/res1,res2,res3,res4/])
  	
  	gres = True
  	gres@gsMarkerThicknessF 	= 2.0
  	gres@gsMarkerSizeF 		= 0.02
  	
  	res1 = gres
  	res2 = gres
  	res3 = gres
  	res4 = gres
  	
  	do m = 0,num_m-1
 	do r = 0,num_r-1
 		res1@gsMarkerIndex  	= mkrs(m)
 		res2@gsMarkerIndex  	= mkrs(m)
 		res3@gsMarkerIndex  	= mkrs(m)
 		res4@gsMarkerIndex  	= mkrs(m)
 		res1@gsMarkerColor 	= clrs(r)
 		res2@gsMarkerColor 	= clrs(r)
 		res3@gsMarkerColor 	= clrs(r)
 		res4@gsMarkerColor 	= clrs(r)
 	  dum(0,r,m) = gsn_add_polymarker(wks,plot(0),X1(r,m),Y1(r,m),res1)
 	  dum(1,r,m) = gsn_add_polymarker(wks,plot(1),X1(r,m),Y2(r,m),res1)
 	  dum(2,r,m) = gsn_add_polymarker(wks,plot(2),X2(r,m),Y3(r,m),res1)
 	  dum(3,r,m) = gsn_add_polymarker(wks,plot(3),X2(r,m),Y4(r,m),res1)
  	end do
  	end do
	
  	pres = True
  	pres@gsnFrame       	= False
  	pres@gsnPanelBottom 	= 0.18 
    	pres@txString = lat1+":"+lat2+"N / "+lon1+":"+lon2+"E"
    ;pres@amJust   = "TopLeft"
    	;pres@gsnPanelFigureStrings= (/"a)","b)","c)","d)","e)","f)","g)","h)"/) 
    	pres@gsnPanelXWhiteSpacePercent = 5
    	pres@gsnPanelYWhiteSpacePercent = 5

  gsn_panel(wks,plot(:1),(/1,2/),pres)
  ;gsn_panel(wks,plot,(/2,2/),pres)
  
  legend1 = create "Legend" legendClass wks 
    "vpXF"                      	: 0.2
    "vpYF"                      	: 0.2
    "vpWidthF"                  	: 0.2   
    "vpHeightF"                 	: 0.15   
    "lgPerimOn"                 	: True   
    "lgItemCount"               	: num_m
    "lgLabelStrings"            	: (/"ERAi","ZM_2.0","ZM_1.0","ZM_0.2"/)
    "lgItemType"					: "Markers"
    "lgLabelsOn"                	: True     
    "lgMarkerIndexes"		    	: mkrs   
    "lgMarkerSizeF"				: 0.02
    "lgLabelFontHeightF"        	: 0.015    
    "lgMarkerColor"				: "black"
    "lgMonoLineLabelFontColor"  	: True                  
  end create

  draw(legend1)
  
  legend2 = create "Legend" legendClass wks 
    "vpXF"                      	: 0.6
    "vpYF"                      	: 0.2
    "vpWidthF"                  	: 0.3   
    "vpHeightF"                 	: 0.1   
    "lgPerimOn"                 	: True   
    "lgItemCount"               	: num_r
    ;"lgLabelStrings"            	: (/"0-4","5-9"/)+" day lead"
    "lgLabelStrings"            	: (/"5-9"/)+" day lead"
    "lgItemType"					: "Lines"
    "lgLineLabelsOn"				: False
    "lgLineColors"				: clrs
    "lgLineThicknessF"			: 2.
    "lgMonoDashIndex"			: True
    "lgDashIndex"				: 0
    "lgLabelsOn"                	: True     
    "lgLabelFontHeightF"        	: 0.02    
    "lgMonoLineLabelFontColor"  	: True                  
  end create

  ;draw(legend2)
  
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete(res)
;===================================================================================
;===================================================================================
end


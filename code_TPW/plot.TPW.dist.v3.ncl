; Similar to v2, but also adds dashed lines to 
; approximate the change of the shape of the TPW 
; distribution that occurs with MJO phase.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
;load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    member = (/"EC","90","09"/)


    clr = (/"black","blue","red","black"/)

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/Hindcast/figs_TPW/TPW.dist.v3"

    lat1 = -20
    lat2 =  20
    lon1 =  40
    lon2 = 180

    idir = "~/Research/DYNAMO/Hindcast/data/repacked/"
    
;===================================================================================
;===================================================================================
    num_m = dimsizes(member)
    nvar = 4
    bins = True
    bins@verbose = False
    bins@bin_min =  5
    bins@bin_max = 65
    bins@bin_spc =  2

    xbin    = ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim    = (/num_m,nvar,num_bin/)
    ;bin_str = new(bdim,string)
    ;bin_val = new(bdim,float)
    bin_cnt = new(bdim,float)

    nt = 10
    case_name = new(num_m,string)
    case_name = where(member.eq."90","SP-CAM",case_name)
    case_name = where(member.eq."09","CAM5"  ,case_name)
    case_name = where(member.eq."EC","ECMWF" ,case_name)
    ;-------------------------------------------------------------
    ; Set up workspace
    ;-------------------------------------------------------------
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(2,graphic)
        res = True
        res@gsnDraw  = False
        res@gsnFrame = False
        res@tmXBLabelFontHeightF        = 0.01
        res@tmYLLabelFontHeightF        = 0.01
        res@tiXAxisFontHeightF          = 0.015
        res@tiYAxisFontHeightF          = 0.015
        res@tiXAxisOn                   = True
        res@tmYLMinorOn                 = False
        res@tmXBMinorOn                 = False
        res@tmYROn                      = False
        res@tmXTOn                      = False
        ;res@tmXBLabelAngleF             = -45.
        res@tmXBMajorOutwardLengthF     = 0.0
        res@tmYLMajorOutwardLengthF     = 0.0
        res@tmXBMinorOutwardLengthF     = 0.0
        res@tmYLMinorOutwardLengthF     = 0.0
        res@vpHeightF                   = 0.3
        res@gsnRightStringFontHeightF   = 0.015
        res@gsnLeftStringFontHeightF    = 0.015
        res@gsnRightString              = ""
        res@gsnLeftString               = ""
        res@tiXAxisString               = "TPW [mm]"
;===================================================================================
;===================================================================================    
do m = 0,num_m-1    
    ;-----------------------------------------------------
    ; Load TPW data from budget calculation
    ;-----------------------------------------------------
    ;ifile1 = idir+"DYNAMO_"+member(m)+"_00-04/DYNAMO_"+member(m)+"_00-04.budget.Lq.nc"
    ;ifile2 = idir+"DYNAMO_"+member(m)+"_05-09/DYNAMO_"+member(m)+"_05-09.budget.Lq.nc"
    ifile1 = idir+"DYNAMO_"+member(m)+"_00-04/DYNAMO_"+member(m)+"_00-04.CWV.nc"
    ifile2 = idir+"DYNAMO_"+member(m)+"_05-09/DYNAMO_"+member(m)+"_05-09.CWV.nc"
    infile1 = addfile(ifile1,"r")
    infile2 = addfile(ifile2,"r")
    ;num_c   = dimsizes(infile1->time)/(5*4)
    num_c   = dimsizes(infile1->CWV(:,0,0)) / (5*4)
    if True then
        ;TPW1 = block_avg( infile1->LQvi(:,{lat1:lat2},{lon1:lon2}) ,4)
        ;TPW2 = block_avg( infile2->LQvi(:,{lat1:lat2},{lon1:lon2}) ,4)
        TPW1 = block_avg( infile1->CWV(:,{lat1:lat2},{lon1:lon2}) ,4)
        TPW2 = block_avg( infile2->CWV(:,{lat1:lat2},{lon1:lon2}) ,4)
        lat = infile1->lat({lat1:lat2})
        lon = infile1->lon({lon1:lon2}) 
        num_lat = dimsizes(lat)
        num_lon = dimsizes(lon)
        delete([/lat,lon/])
    else
        if m.eq.0 then 
            lat = infile1->lat({lat1:lat2})
            lon = infile1->lon({lon1:lon2})
        end if
        iTPW1 = block_avg2( infile1->LQvi(:,:,:) ,4)
        iTPW2 = block_avg2( infile2->LQvi(:,:,:) ,4)
        olat1 = infile1->lat
        olat2 = infile2->lat
        olon1 = infile1->lon
        olon2 = infile2->lon
        TPW1 = area_hi2lores_Wrap(olon1,olat1,iTPW1,False,1.,lon,lat,False)
        TPW2 = area_hi2lores_Wrap(olon2,olat2,iTPW2,False,1.,lon,lat,False)
        delete([/olat1,olat2,olon1,olon2,iTPW1,iTPW2/])
    end if

    TPW = new((/num_c,10,num_lat,num_lon/),float)
    do n = 0,4
        TPW(:,n+0,:,:) = TPW1(n::5,:,:)
        TPW(:,n+5,:,:) = TPW2(n::5,:,:)
    end do
    ;TPW  = (/TPW /Lv/)
    ;TPW1 = (/TPW1/Lv/)
    ;TPW2 = (/TPW2/Lv/)
    ;-----------------------------------------------------
    ; Calculate TPW distribution
    ;-----------------------------------------------------

    wetdays = ind(dim_avg_n(TPW(:,0,:,:),(/1,2/)).ge.45.)
    drydays = ind(dim_avg_n(TPW(:,0,:,:),(/1,2/)).lt.45.)

    print(wetdays)
    print(drydays)

    if m.eq.0 then nvar = 4 end if
    if m.ne.0 then nvar = 2 end if

    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        if v.eq.0 then Vy = TPW(:,0:1,:,:)  end if
        if v.eq.1 then Vy = TPW(:,8:9,:,:)  end if
        if v.eq.2 then Vy = TPW(wetdays,0:2,:,:) end if
        if v.eq.3 then Vy = TPW(drydays,0:2,:,:) end if
        Vx = Vy
        tmp = bin_YbyX(Vy,Vx,bins)
        ;bin_val(m,v,:) = tmp
        bin_cnt(m,v,:) = tmp@count /sum(tmp@count)*100.
        delete([/Vy,Vx,tmp/])
    end do
    ;-------------------------------------------------------------
    ;-------------------------------------------------------------
    delete([/TPW,TPW1,TPW2,wetdays,drydays/])
end do 
;===================================================================================
;===================================================================================
        ores = res
        ores@xyDashPattern          = 1
        ores@xyLineThicknessF       = 2.
        ores@xyLineColor            = "black"

        res@gsnLeftString           = "Column Humidity Distribution"
        res@xyDashPattern           = 0
        res@xyLineThicknessF        = 4.
        res@xyLineColors            = clr
        res@tiYAxisString           = "% of observations"
        res@tiXAxisString           = "[mm]"
        res@trYMaxF                 = 12.
        res@trYMinF                 = 0.

    do v = 0,1;nvar-1
        if v.eq.0 then res@gsnRightString = "days 0-1" end if
        if v.eq.1 then res@gsnRightString = "days 8-9" end if
        ;if v.eq.0 then res@gsnRightString = "day 0" end if
        ;if v.eq.1 then res@gsnRightString = "day 9" end if
        plot(v) = gsn_csm_xy(wks,xbin,bin_cnt(:,v,:),res)
        overlay(plot(v) , gsn_csm_xy(wks,xbin,bin_cnt(:,2,:),ores) )
        overlay(plot(v) , gsn_csm_xy(wks,xbin,bin_cnt(:,3,:),ores) )
    end do

;===================================================================================
;===================================================================================
        pres = True
        pres@gsnFrame                           = False
        pres@amJust                             = "TopLeft"
        pres@gsnPanelXWhiteSpacePercent         = 3.
        pres@gsnPanelFigureStringsFontHeightF   = 0.015
        pres@gsnPanelFigureStrings              = (/"a","b","c","d"/) 

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
    
        legend = create "Legend" legendClass wks 
        "lgAutoManage"             : False
        "vpXF"                      : 0.2
        "vpYF"                      : 0.9
        "vpWidthF"                  : 0.1  
        "vpHeightF"                 : 0.07   
        "lgPerimOn"                 : True   
        "lgItemCount"               : num_m
        "lgLabelStrings"            : case_name
        "lgLabelsOn"                : True     
        "lgLineLabelsOn"            : False     
        "lgLabelFontHeightF"        : 0.015
        "lgMonoDashIndex"           : True
        "lgDashIndex"               : 0
        "lgLineThicknessF"          : 3.
        "lgLineColors"              : clr
        "lgMonoLineLabelFontColor"  : True                  
    end create
    
    draw(legend)
    frame(wks)

    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


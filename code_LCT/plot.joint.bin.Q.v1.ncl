load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    case = (/"09","90"/)
    ;case = (/"09"/)
    rd = "00-04"

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/Hindcast/figs_LCT/joint.bin.Q.v1"
    
    reduceRes   = False
    dailyAvg    = False
    add_obs     = True
    debug       = False

    qvar = "Q1_dse"
    ;qvar = "Q2"    ; has limited lat/lon range

    blev = 600
    tlev = 500
;====================================================================================================
;====================================================================================================
    case_str = case
    case_str = where(case.eq."09","CAM5"    ,case_str)
    case_str = where(case.eq."90","SP-CAM"  ,case_str)

    lat1 := -10
    lat2 :=  10
    lon1 :=  60
    lon2 :=  90
    
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.5
        res@tmXTOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@tmYRMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.02
        res@gsnCenterStringFontHeightF      = 0.02
        res@gsnRightStringFontHeightF       = 0.02
        res@tmXBLabelFontHeightF            = 0.015
        res@tmYLLabelFontHeightF            = 0.015
        res@tiXAxisFontHeightF              = 0.02
        res@tiYAxisFontHeightF              = 0.02
        ;res@lbLabelFontheightF             = 0.01
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        ;res@tmXBLabelAngleF                    = -50.
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"

    num_c = dimsizes(case)

    nvar = 2
    
    binsy = True
    binsy@verbose = False
    binsy@bin_min = -21
    binsy@bin_max =  27
    binsy@bin_spc =   3

    ;if dailyAvg then
        binsy@bin_min = -10
        binsy@bin_max =  16
    ;end if

    binsx = True
    binsx@verbose = False
    binsx@bin_min = 30
    binsx@bin_max = 63
    binsx@bin_spc =  3
    
    ybin    = ispan(binsy@bin_min,binsy@bin_max,binsy@bin_spc)
    xbin    = ispan(binsx@bin_min,binsx@bin_max,binsx@bin_spc)
    nbiny   = dimsizes(ybin)
    nbinx   = dimsizes(xbin)
    bdim    = (/num_c,nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    binavg = new((/num_c,nvar/),float)

    obdim   = (/nvar,nbiny,nbinx/)
    obinval = new(obdim,float)
    obincnt = new(obdim,float)
    obinavg = new((/nvar/),float)
;====================================================================================================
; Load ECMWF LCT Data
;====================================================================================================   
if add_obs then
    ;tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    infile = addfile(tfile,"r")
    EClat = infile->lat({lat1:lat2})
    EClon = infile->lon({lon1:lon2})
    iECTPW = infile->ECTPW;(:,{lat1:lat2},{lon1:lon2})
    ;iECDDT = infile->ECDDT;(:,{lat1:lat2},{lon1:lon2})
    iECLCT = infile->ECCPR;(:,{lat1:lat2},{lon1:lon2})
    ;copy_VarCoords(iECTPW,iECDDT)
    copy_VarCoords(iECTPW,iECLCT)
    ECTPW = iECTPW(:,{lat1:lat2},{lon1:lon2})
    ;ECDDT = iECDDT(:,{lat1:lat2},{lon1:lon2})
    ECLCT = iECLCT(:,{lat1:lat2},{lon1:lon2})

    ECLCT@long_name = "ECMWF LCT"
    printMAM(ECLCT)
end if
;====================================================================================================
; Load ECLHF
;====================================================================================================
if False then
    printline()
    print("Loading ECLHF...")
    infile = addfile("~/Data/DYNAMO/ECMWF/data/12hour.00-12.DYNAMO.ECMWF.LHF.nc","r")
    EClat = infile->lat({lat1:lat2})
    EClon = infile->lon({lon1:lon2})
    t1 = 365 - (31+30+31)
    t2 = 365 - 1

    ECLHF = block_avg( infile->LHF(:,{lat1:lat2},{lon1:lon2}) ,2)
    ECLHF = (/ECLHF/Lv*86400*-1./)
    ECLHF@long_name     = "Evaporation"
    ECLHF@units         = "mm/day"

    ECLHF = dim_rmvmean_n_Wrap(ECLHF,0)
end if
;====================================================================================================
;====================================================================================================
if add_obs then
    infile = addfile("~/Data/DYNAMO/ECMWF/data/EC.q1q2.nc","r")
    EClev = infile->lev;({lev})

    ECQ1 = infile->q1(:,:,{lat1:lat2},{lon1:lon2})
    ;ECQ2 = infile->q2(:,:,{lat1:lat2},{lon1:lon2})

    ilat = ECQ1&lat
    ilon = ECQ1&lon
    ECQ1 := area_hi2lores_Wrap(ilon,ilat,ECQ1,False,1.,EClon,EClat,False)
    
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
    ps = infile->Ps(:,{lat1:lat2},{lon1:lon2}) 
    ps := area_hi2lores_Wrap(ilon,ilat,ps,False,1.,EClon,EClat,False) 
    p = ECQ1&lev*100.
    p@units = "Pa"
    p!0 = "lev"
    p&lev = p
    dp = calc_dP(p,ps)
    copy_VarCoords(ECQ1,dp)
    ;Qtop = dim_sum_n(ECQ1(:,{:500},:,:)*dp,1)
    ;Qbot = dim_sum_n(ECQ1(:,{600:},:,:)*dp,1)
    ;Qtop = dim_sum_n(ECQ1(:,{:tlev},:,:)*dp(:,{:tlev},:,:),1)
    ;Qbot = dim_sum_n(ECQ1(:,{blev:},:,:)*dp(:,{blev:},:,:),1)
    Qtop := dim_sum_n(ECQ1(:,{:tlev},:,:)*dp(:,{:tlev},:,:),1) / dim_sum_n(dp(:,{:tlev},:,:),1)
    Qbot := dim_sum_n(ECQ1(:,{blev:},:,:)*dp(:,{blev:},:,:),1) / dim_sum_n(dp(:,{blev:},:,:),1)
    
    modeIndex2 = Qbot - Qtop
    modeIndex1 = Qbot + Qtop

    ;modeIndex1 = (/ modeIndex1/stddev(modeIndex1) /)
    ;modeIndex2 = (/ modeIndex2/stddev(modeIndex2) /)

    ;printline()
    ;printVarSummary(ECQ1)
    printline()

    ;-----------------------------------------------------
    ; Bin ECMWF
    ;-----------------------------------------------------
    do v = 0,nvar-1
        ;print("  c = "+c+"    v = "+v+"    v%nvar = "+v%nvar)
        if v.eq.0 then Vz := modeIndex1 end if
        if v.eq.1 then Vz := modeIndex2 end if
        Vy := ECLCT
        Vx := ECTPW      
        
        if dailyAvg then
            Vx := block_avg(Vx,4)
            Vy := block_avg(Vy,4)
            Vz := block_avg(Vz,4)
        end if

        if any(dimsizes(Vz).ne.dimsizes(Vx)) then
            printDim(Vz)
            printDim(Vy)
            printDim(Vx)
        end if
        
        tmp := bin_ZbyYX(Vz,Vy,Vx,binsy,binsx,0)
        
        obinval(v,:,:) = tmp
        obincnt(v,:,:) = tmp@pct
        obinavg(v) = avg(Vx)

        delete([/Vz,Vy,Vx,tmp/])
    end do
    ;-----------------------------------------------------
    ;-----------------------------------------------------
end if

;====================================================================================================
; Load Hindcast Data
;====================================================================================================
do c = 0,num_c-1
    tcase = "DYNAMO_"+case(c)+"_"+rd
    ifile  := "~/Data/DYNAMO/Hindcast/repacked/"+tcase+"/"+tcase+".CWV.nc"
    infile := addfile(ifile,"r")
    lat = infile->lat({lat1:lat2})
    lon = infile->lon({lon1:lon2})
    TPW = infile->CWV(:,{lat1:lat2},{lon1:lon2})

    ifile  := "~/Data/DYNAMO/Hindcast/repacked/"+tcase+"/"+tcase+".budget.Lq.nc"
    infile := addfile(ifile,"r")
    DDT = infile->DLQDT(:,{lat1:lat2},{lon1:lon2})
    ADV = infile->VdelQ(:,{lat1:lat2},{lon1:lon2})
    DDT = (/DDT/Lv*86400./)
    ADV = (/ADV/Lv*86400./)
    LCT = DDT + ADV
    copy_VarCoords(TPW,LCT)


LCT@long_name   = case(c)+" LCT"
printMAM(LCT)

    ifile  := "~/Data/DYNAMO/Hindcast/repacked/"+tcase+"/"+tcase+"."+qvar+".nc"
    infile := addfile(ifile,"r")
    Q1 = infile->$qvar$(:,:,{lat1:lat2},{lon1:lon2})
    Q1 = (/Q1*86400./cpd/)

    if c.eq.0 then print(Q1&lev) end if

    ifile  := "~/Data/DYNAMO/Hindcast/repacked/"+tcase+"/"+tcase+".PS.nc"
    infile := addfile(ifile,"r")
    ps := infile->PS(:,{lat1:lat2},{lon1:lon2})  
    ps@units = "Pa"
    p := Q1&lev*100.
    p@units = "Pa"
    p!0 = "lev"
    p&lev = p
    dp := calc_dP(p,ps)
    copy_VarCoords(Q1,dp)
    ; Note coord variable is reversed in CAM data
    ;Qbot = dim_sum_n(Q1(:,{:600},:,:),1)
    ;Qtop = dim_sum_n(Q1(:,{500:},:,:),1)

    ;Qbot := dim_sum_n(Q1(:,{:blev},:,:)*dp(:,{:blev},:,:),1)
    ;Qtop := dim_sum_n(Q1(:,{tlev:},:,:)*dp(:,{tlev:},:,:),1)
    Qtop := dim_sum_n(Q1(:,{tlev:},:,:)*dp(:,{tlev:},:,:),1) / dim_sum_n(dp(:,{tlev:},:,:),1)
    Qbot := dim_sum_n(Q1(:,{:blev},:,:)*dp(:,{:blev},:,:),1) / dim_sum_n(dp(:,{:blev},:,:),1)
    
    modeIndex2 := Qbot - Qtop
    modeIndex1 := Qbot + Qtop

    ;modeIndex1 = (/ modeIndex1/stddev(modeIndex1) /)
    ;modeIndex2 = (/ modeIndex2/stddev(modeIndex2) /)

    ;-----------------------------------------------------
    ;-----------------------------------------------------
    if False then
        lev := Q1&lev 
        nlev = dimsizes(lev)
        RC   := new((/2,nlev/),float)
        tval := new((/2,nlev/),float)
        npts := new((/2,nlev/),float)
        
        do k = 0,dimsizes(lev)-1
            rx = ndtooned( modeIndex1 )
            ry = ndtooned( Q1(:,k,:,:) )
            tmp = regCoef(rx,ry)
            RC(0,k) = tmp
            npts(0,k) = tmp@nptxy
            tval(0,k) = tmp@tval
            
            rx = ndtooned( modeIndex2 )
            ry = ndtooned( Q1(:,k,:,:) )
            tmp = regCoef(rx,ry)
            RC(1,k) = tmp
            npts(1,k) = tmp@nptxy
            tval(1,k) = tmp@tval
        end do
        ;print(tval+"        "+RC)

        qplot_xy(RC,lev)
        ;exit
    end if
    ;-----------------------------------------------------
    ; Bin hindcast data
    ;-----------------------------------------------------
    do v = 0,nvar-1
        print("  c = "+c+"    v = "+v+"    v%nvar = "+v%nvar)
        if v.eq.0 then Vz := modeIndex1 end if
        if v.eq.1 then Vz := modeIndex2 end if
        Vy := LCT
        Vx := TPW      
        
        if dailyAvg then
            Vx := block_avg(Vx,4)
            Vy := block_avg(Vy,4)
            Vz := block_avg(Vz,4)
        end if

        if any(dimsizes(Vz).ne.dimsizes(Vx)) then
            printDim(Vz)
            printDim(Vy)
            printDim(Vx)
        end if
        
        tmp := bin_ZbyYX(Vz,Vy,Vx,binsy,binsx,0)
        
        binval(c,v,:,:) = tmp
        bincnt(c,v,:,:) = tmp@pct
        binavg(c,v) = avg(Vx)

        delete([/Vz,Vy,Vx,tmp/])
    end do
    ;-----------------------------------------------------
    ;-----------------------------------------------------

end do

    printline()
    bincnt!0 = "case"
    bincnt!1 = "var"
    bincnt!2 = "ybin"
    bincnt!3 = "xbin"
    bincnt&ybin = ybin
    bincnt&xbin = xbin

    obincnt!0 = "var"
    obincnt!1 = "ybin"
    obincnt!2 = "xbin"
    obincnt&ybin = ybin
    obincnt&xbin = xbin
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    ;plot = new(nvar*2,graphic)
    num_p = num_c*nvar
    if add_obs then num_p = num_c*nvar+nvar end if
    plot = new(num_p,graphic)
        cres = res
        cres@cnFillOn           = True
        cres@cnLinesOn          = False
        ;cres@lbOrientation     = "vertical"
        ;cres@cnFillPalette     = ""
        cres@tiYAxisString      = "Lagrangian Tendency [mm day~S~-1~N~]"
        cres@tiXAxisString      = "CWV [mm]"
        cres@gsnRightString     = ""
        cres@pmLabelBarOrthogonalPosF   = 0.02
        cres@tiXAxisOffsetYF            = .115
        
        cres@cnLevelSelectionMode   = "ExplicitLevels"
        ;cres@cnLevels               = ispan(-95,95,5)/1e2
        ;cres@cnLevels               = ispan(-60,60,5)/1e2

        ;cres@gsnLeftString      = "ECMWF"
        cres@lbLabelFontHeightF = 0.015

        tres := res
        tres@cnFillOn           = False
        tres@cnLinesOn          = True
        tres@cnLineLabelsOn     = False
        tres@cnInfoLabelOn      = False
        tres@cnLineThicknessF   = 3.
        tres@gsnContourZeroLineThicknessF   = 6.
        tres@gsnContourNegLineDashPattern   = 1
        
    
    do v = 0,nvar-1
        if v.eq.0 then cres@cnLevels := ispan(-20,20,2)  end if
        if v.eq.1 then cres@cnLevels := ispan(-60,60,5)/1e1 end if
    do c = 0,num_c-1
        pc = c*nvar+v
        cres@gsnLeftString      = case_str(c)
        cres@gsnRightString     = "M~B~"+(v+1)+"~N~"
        plot(pc) = gsn_csm_contour(wks,binval(c,v,:,:),cres)
        overlay(plot(pc) , gsn_csm_contour(wks,bincnt(c,v,:,:),tres))
        overlay(plot(pc) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
        overlay(plot(pc) , gsn_csm_xy(wks,(/1.,1./)*binavg(c,v),(/-1e3,1e3/),lres)) 
    end do
    if add_obs then
        pc = num_c*nvar+v
        cres@gsnLeftString      = "ECMWF"
        cres@gsnRightString     = "M~B~"+(v+1)+"~N~"
        plot(pc) = gsn_csm_contour(wks,obinval(v,:,:),cres)
        overlay(plot(pc) , gsn_csm_contour(wks,obincnt(v,:,:),tres))
        overlay(plot(pc) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
        overlay(plot(pc) , gsn_csm_xy(wks,(/1.,1./)*obinavg(v),(/-1e3,1e3/),lres)) 
    end if
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@gsnFrame                           = True
        pres@gsnPanelYWhiteSpacePercent         = 5
        ;pres@amJust                             = "TopLeft"
        ;pres@gsnPanelFigureStringsFontHeightF   = 0.01
        ;pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f"/)

    ;gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
    layout = (/num_c,nvar/)
    if add_obs then layout = (/num_c+1,nvar/) end if
    gsn_panel(wks,plot,layout,pres)

    trimPNG(fig_file)
;====================================================================================================
;====================================================================================================
end

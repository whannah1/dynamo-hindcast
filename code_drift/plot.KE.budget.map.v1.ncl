; Plots the drift signal of terms from the EKE budget
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin
	
  	member    = (/"ERAi","90","09"/)
  	case_name = (/"ERAi","SP-CAM","CAM5"/)
  	
    lev   = 700.
    
    ndayrun = 5
    
    addvc = True
    
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/Hindcast/figs_drift/EKE.budget.map.v1"
;===================================================================================
;===================================================================================
  idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/repacked/"
  num_m = dimsizes(member)	
  num_t = 10*4  
  
  num_v = 2

	lat1 = -40.
	lat2 =  40.
	lon1 =   0.
	lon2 = 360.
	
	plat1 = -35.
  	plat2 =  35.
	
	olat1 = lat1
	olat2 = lat2
	
	opt  = True
  	opt@lat1 = lat1
  	opt@lat2 = lat2
  	opt@lon1 = lon1
  	opt@lon2 = lon2
  
  fig_type@wkWidth  = 2048
  fig_type@wkHeight = 2048
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")
  plot = new((num_m-1)*num_v,graphic)
  	res = True
  	res@gsnDraw 					= False
  	res@gsnFrame 					= False
  	res@gsnSpreadColors 			= True
  	res@cnFillOn 					= True
  	res@cnLinesOn 					= False
  	res@cnInfoLabelOn				= False
  	res@cnLineLabelsOn 				= False
  	res@tmXTOn						= False
  	res@tmYROn						= False
  	res@lbLabelBarOn 				= True
  	res@mpMinLatF					= plat1
  	res@mpMaxLatF					= plat2
  	;res@mpMinLonF					= 
  	;res@mpMaxLonF					= 
  	res@mpCenterLonF				= 150.
  	
  	FontHeight = 0.015
  	res@gsnRightStringFontHeightF	= 0.015
  	res@gsnLeftStringFontHeightF	= 0.015
  	res@tiYAxisFontHeightF			= 0.015
  	res@tmXBLabelFontHeightF		= 0.01
  	res@tmYLLabelFontHeightF		= 0.01
  	res@lbLabelFontHeightF			= 0.01
  	res@tmXBMajorOutwardLengthF		= 0.0
  	res@tmXBMinorOutwardLengthF		= 0.0
  	res@tmYLMajorOutwardLengthF		= 0.0
  	res@tmYLMinorOutwardLengthF		= 0.0
  	res@lbTopMarginF				= 0.05 + 0.05
  	res@lbBottomMarginF				= 0.05 - 0.05
  	
  	vres = True
  	vres@gsnDraw 					= False
  	vres@gsnFrame 					= False
  	vres@vcRefAnnoOn				= False
  	vres@vcRefMagnitudeF            = 5.             	; define vector ref mag
  	vres@vcRefLengthF               = 0.045           	; define length of vec ref
  	vres@vcRefAnnoOrthogonalPosF    = -.5            	; move ref vector
  	vres@vcRefAnnoArrowLineColor    = "black"         	; change ref vector color
  	vres@vcRefAnnoArrowUseVecColor 	= False           	; don't use vec color for ref
  	vres@vcGlyphStyle              	= "CurlyVector"     ; turn on curley vectors
  	vres@vcLineArrowColor           = "black"           ; change vector color
  	vres@vcLineArrowThicknessF      = 1.0               ; change vector thickness
  	vres@vcVectorDrawOrder          = "PostDraw"        ; draw vectors last
  	vres@vcMinMagnitudeF			= 1.
  	vres@gsnLeftString  			= ""
  	vres@gsnRightString  			= ""
  	vres@gsnCenterString  			= ""
  	vres@vcMinDistanceF 			= 0.017
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  tmember = member(m)
  oname = "DYNAMO_"+tmember+(/"_00-04","_05-09"/)
  ;--------------------------------------------------------------------------------
  ; Load data
  ;--------------------------------------------------------------------------------
  		ifile   = idir+oname+"/"+oname+".U.nc"
    	infile1 = addfile(ifile(0),"R")
    	infile2 = addfile(ifile(1),"R")
    	u1 = infile1->U(:,{lev},{lat1:lat2},{lon1:lon2})
    	u2 = infile2->U(:,{lev},{lat1:lat2},{lon1:lon2})

    	ifile   = idir+oname+"/"+oname+".V.nc"
    	infile1 = addfile(ifile(0),"R")
    	infile2 = addfile(ifile(1),"R")
    	v1 = infile1->V(:,{lev},{lat1:lat2},{lon1:lon2})
    	v2 = infile2->V(:,{lev},{lat1:lat2},{lon1:lon2})
    	
    	ifile   = idir+oname+"/"+oname+".OMEGA.nc"
    	infile1 = addfile(ifile(0),"R")
    	infile2 = addfile(ifile(1),"R")
    	w1 = infile1->OMEGA(:,{lev},{lat1:lat2},{lon1:lon2})
    	w2 = infile2->OMEGA(:,{lev},{lat1:lat2},{lon1:lon2})
    	
    	ifile   = idir+oname+"/"+oname+".T.nc"
    	infile1 = addfile(ifile(0),"R")
    	infile2 = addfile(ifile(1),"R")
    	T1 = infile1->T(:,{lev},{lat1:lat2},{lon1:lon2})
    	T2 = infile2->T(:,{lev},{lat1:lat2},{lon1:lon2})
    	
    	lat  = infile1->lat({lat1:lat2})
  		lon  = infile1->lon({lon1:lon2})
    	nlat = dimsizes(lat)
    	nlon = dimsizes(lon)
    	
    	;KE1 = ( u1^2. + v1^2. )/2.
    	;KE2 = ( u2^2. + v2^2. )/2.
		
		ub1 = runave_n_Wrap(u1,ndayrun,0,0)
		ub2 = runave_n_Wrap(u2,ndayrun,0,0)
		vb1 = runave_n_Wrap(v1,ndayrun,0,0)
		vb2 = runave_n_Wrap(v2,ndayrun,0,0)
		wb1 = runave_n_Wrap(w1,ndayrun,0,0)
		wb2 = runave_n_Wrap(w2,ndayrun,0,0)
		Tb1 = runave_n_Wrap(T1,ndayrun,0,0)
		Tb2 = runave_n_Wrap(T2,ndayrun,0,0)
		
    	up1 = u1 - ub1
    	up2 = u2 - ub2
    	vp1 = v1 - vb1
    	vp2 = v2 - vb2
    	wp1 = w1 - wb1
    	wp2 = w2 - wb2
    	Tp1 = T1 - Tb1
    	Tp2 = T2 - Tb2
    	
    	EKE1 = ( up1^2. + vp1^2. )/2.
    	EKE2 = ( up2^2. + vp2^2. )/2.
  ;--------------------------------------------------------------------------------
  ; Calculate EKE budget terms
  ;--------------------------------------------------------------------------------	
	xvals = ispan(0,nlon-1,1)
	dxv = xvals(1) - xvals(0)
	Rvals = where((xvals+1).le.max(xvals),xvals+1,xvals+1-(max(xvals)+1))
	Xvals = xvals
	Lvals = where((xvals-1).ge.min(xvals),xvals-1,xvals-1+(max(xvals)+1))
	yvals = ispan(0,nlat-1,1)
	Nvals = yvals(2:nlat-1)
	Cvals = yvals(1:nlat-2)
	Svals = yvals(0:nlat-3)

  	dlon  = lon(1)-lon(0)
	dx = conform(ub1(:,Cvals,Xvals), tofloat( (lon+dlon  +lon(Xvals))/2. -(lon-dlon  +lon(Xvals))/2. ) ,2)
	dy = conform(ub1(:,Cvals,Xvals), tofloat( (lat(Nvals)+lat(Cvals))/2. -(lat(Svals)+lat(Cvals))/2. ) ,1)
	dx = dx*111000.*conform(ub1(:,Cvals,Xvals),tofloat(cos(lat(Cvals)*3.14159/180.)),1)
	dy = dy*111000.        
	
	do i = 0,1
		if i.eq.0 then 
		  ub = ub1 
		  up = up1
		  vb = vb1
		  vp = vp1
		  wp = wp1
		  Tp = Tp1
		else
		  ub = ub2 
		  up = up2
		  vb = vb2
		  vp = vp2
		  wp = wp2
		  Tp = Tp2
		end if
		
		ddim   = dimsizes(up)
		dubdx  = new(ddim,float)
		dubdy  = new(ddim,float)
		dvbdx  = new(ddim,float)
		dvbdy  = new(ddim,float)
		
		dubdx(:,Cvals,Xvals) = ( (ub(:,Cvals,Rvals)+ub(:,Cvals,Xvals))/2. - (ub(:,Cvals,Lvals)+ub(:,Cvals,Xvals))/2. )/dx
		dubdy(:,Cvals,Xvals) = ( (ub(:,Nvals,Xvals)+ub(:,Cvals,Xvals))/2. - (ub(:,Svals,Xvals)+ub(:,Cvals,Xvals))/2. )/dy
		dvbdx(:,Cvals,Xvals) = ( (ub(:,Cvals,Rvals)+vb(:,Cvals,Xvals))/2. - (vb(:,Cvals,Lvals)+vb(:,Cvals,Xvals))/2. )/dx
		dvbdy(:,Cvals,Xvals) = ( (ub(:,Nvals,Xvals)+vb(:,Cvals,Xvals))/2. - (vb(:,Svals,Xvals)+vb(:,Cvals,Xvals))/2. )/dy
		;dubdp(:,Cvals,Xvals) = 
		;dvbdp(:,Cvals,Xvals) = 

		uudxu = runave_n_Wrap( up * up * dubdx ,ndayrun,0,0)
		uvdyu = runave_n_Wrap( up * vp * dubdy ,ndayrun,0,0)

		vudxv = runave_n_Wrap( vp * up * dvbdx ,ndayrun,0,0)
		vvdyv = runave_n_Wrap( vp * vp * dvbdy ,ndayrun,0,0)
		
		;uwdpu = up * wp * dubdp
		;vwdpv = vp * wp * dvbdp
		
		tmp = uudxu + uvdyu + vudxv + vvdyv ;+ uwdpu + vwdpv 
	
		if i.eq.0 then BT1 = tmp end if
		if i.eq.1 then BT2 = tmp end if
		
		tmp = runave_n_Wrap( (Rd/(lev*100.))*(wp*Tp)  ,ndayrun,0,0)
		
		if i.eq.0 then BC1 = tmp end if
		if i.eq.1 then BC2 = tmp end if
		
		delete([/tmp,uudxu,uvdyu,vudxv,vvdyv/])
		delete([/dubdx,dubdy,dvbdx,dvbdy/])
		delete([/ub,up,vb,vp/])
    end do
    delete([/xvals,Rvals,Xvals,Lvals,yvals,Nvals,Cvals,Svals,dx,dy,dlon,dxv/])
  ;--------------------------------------------------------------------------------
  ; Isolate Drift Signal
  ;--------------------------------------------------------------------------------
  	lat = infile1->lat({lat1:lat2})
  	lon = infile1->lon({lon1:lon2})
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)
    xdim = (/num_t,nlat,nlon/)
    BT = new(xdim,float)
    BC = new(xdim,float)
    U = new(xdim,float)
    V = new(xdim,float)
  
    dstride = 5*4
    do d = 0,num_t/2-1
      d1 = d
      d2 = d+num_t/2
      BT(d1,:,:) = dim_avg_n(BT1(d::dstride,:,:),0)
      BT(d2,:,:) = dim_avg_n(BT2(d::dstride,:,:),0)
      BC(d1,:,:) = dim_avg_n(BC1(d::dstride,:,:),0)
      BC(d2,:,:) = dim_avg_n(BC2(d::dstride,:,:),0)
      
      U(d1,:,:) = dim_avg_n(u1(d::dstride,:,:),0)
      U(d2,:,:) = dim_avg_n(u2(d::dstride,:,:),0)
      V(d1,:,:) = dim_avg_n(v1(d::dstride,:,:),0)
      V(d2,:,:) = dim_avg_n(v2(d::dstride,:,:),0)
    end do
    delete([/u1,u2,v1,v2/])
    delete([/ub1,ub2,vb1,vb2/])
    delete([/up1,up2,vp1,vp2/])
    
    U!0 = "time"
    U!1 = "lat"
    U!2 = "lon"
    U&lat = lat
    U&lon = lon
    
    copy_VarCoords(U,V)
    copy_VarCoords(U,BT)
    copy_VarCoords(U,BC)

    aBT = avg4to1(BT)
    aBC = avg4to1(BC)
    aU = avg4to1(U)
    aV = avg4to1(V)
    
    if m.eq.0 then 
      aBT0  = conform(aBT,dim_avg_n_Wrap(aBT,0),(/1,2/))
      aBC0  = conform(aBC,dim_avg_n_Wrap(aBC,0),(/1,2/))  
      aU0  = conform(aU,dim_avg_n_Wrap(aU,0),(/1,2/)) 
      aV0  = conform(aV,dim_avg_n_Wrap(aV,0),(/1,2/)) 
      lon0 = lon
      lat0 = lat
    end if
    
 	aBT = (/ aBT - linint2(lon0,lat0,aBT0,True,lon,lat,1) /)
 	aBC = (/ aBC - linint2(lon0,lat0,aBC0,True,lon,lat,1) /)
 	aU = (/ aU - linint2(lon0,lat0,aU0,True,lon,lat,1) /)
 	aV = (/ aV - linint2(lon0,lat0,aV0,True,lon,lat,1) /)
    
    ltime = (tofloat(ispan(0,9,1)))
    ltime@units = "lead time [days]"
    
    aBT&time = ltime
    aBC&time = ltime
    aU&time = ltime 
    aV&time = ltime  
  ;--------------------------------------------------------------------------------
  ; Plot Map 
  ;--------------------------------------------------------------------------------
  vname = new(num_v,string)
  vname(0) = "BT "+lev(:num_v-1)
  vname(1) = "BC "+lev(:num_v-1)  
  
    	tres = res
  		tres@gsnLeftString  		= case_name(m)
  		tres@gsnRightString 		= vname
  		tres@cnLevelSelectionMode	= "ExplicitLevels"

  		if lev.eq.700. then tres@cnLevels = ispan( -4, 4, 1)*1e-4 end if

  do v = 0,num_v-1
  	
  	p = m-1+(num_m-1)*v
    
    if m.ne.0.and.v.eq.0 then plot(p) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(aBT(8:,:,:),0),tres) end if
    if m.ne.0.and.v.eq.1 then plot(p) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(aBC(8:,:,:),0),tres) end if
    
    if addvc.and.(m.ne.0) then overlay(plot(p),gsn_csm_vector(wks,dim_avg_n_Wrap(aU(8:,:,:),0),dim_avg_n_Wrap(aV(8:,:,:),0),vres)) end if
    
  end do
  ;--------------------------------------------------------------------------------
  ;--------------------------------------------------------------------------------
    delete([/tres,lat,lon/])
    delete([/BT,aBT,BC,aBC/])
    delete([/U,V,aU,aV/])
    delete([/BT1,BT2,BC1,BC2/])
    delete([/EKE1,EKE2/])
    ;delete([/KE1,KE2/])
end do
;===================================================================================
;===================================================================================
  	pres = True
    	;pres@txString = case_stub
    	;pres@gsnPanelBottom 					= 0.06
    	;pres@gsnPanelYWhiteSpacePercent		= 5.
    	pres@gsnPanelFigureStrings				= (/"a","b","c","d","e","f","g","h","i"/)
    	pres@gsnPanelFigureStringsFontHeightF	= 0.01
    	pres@amJust								= "TopLeft"
    	
  
  layout = (/num_m-1,num_v/)
  ;layout = (/num_v,num_m-1/)
  
  gsn_panel(wks,plot,layout,pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end



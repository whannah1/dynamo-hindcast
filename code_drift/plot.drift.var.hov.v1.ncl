; Plots a hovmoller of the systematic drift of a variable
; Note that OLR is done with a separate script plot.drift.OLR.hov.v1.ncl
; so NOAA OLR can be used as a reference
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    member    = (/"EC","90","09"/)
    case_name = (/"ECMWF","SP-CAM","CAM5"/)
    
    fig_type = "png"
    
    plat1 = -15.
    plat2 =  15.

    if True then
        var   = (/"U","U"/)
        lev   = (/850,200/)
        vname = var+lev
        lon2 = 360.
    else
        var   = (/"CWV"/)
        lev   = (/0/)
        vname = var
        lon2 = 180.
    end if
    
    altFlag = False
    recalc  = True
    
    fig_file = "~/Research/DYNAMO/Hindcast/figs_drift/drift.var.hov.v1."+var(0)

    if      altFlag then fig_file = fig_file+".alt" end if
;===================================================================================
;===================================================================================
    idir = "~/Data/DYNAMO/Hindcast/repacked/"
    num_m = dimsizes(member)    
    num_v = dimsizes(var)
    num_t = 10;*4 

    dof = 16.*4.
    
    vname = where(var.eq."FLUT","OLR",vname)

    lat1 = -15.
    lat2 =  15.
    lon1 =   0.
    ;lon2 = 360.
    
    opt  = True
    opt@lat1 = lat1
    opt@lat2 = lat2
    opt@lon1 = lon1
    opt@lon2 = lon2
    
    wks = gsn_open_wks(fig_type,fig_file)
    ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
    gsn_define_colormap(wks,"BlueWhiteOrangeRed")
    plot = new(num_m*num_v,graphic)
        res = True
        res@gsnDraw                 = False
        res@gsnFrame                = False
        res@gsnSpreadColors         = True
        res@cnFillOn                = True
        res@cnLinesOn               = False
        ;res@vpWidthF               = 0.3
        res@vpHeightF               = 0.4
        res@tmXTOn                  = False
        res@tmYROn                  = False
        res@cnLineLabelsOn          = False
        ;res@tmYLLabelAngleF        = 45.
        res@tiYAxisString           = "Lead Time [days]"
        ;res@trYMaxF                = num_t/4
        res@trYMinF                 = 0.
        res@trYReverse              = True
        res@lbLabelBarOn            = True
        res@cnLevelSelectionMode    = "ExplicitLevels"
        res@cnInfoLabelOn           = False
        
        FontHeight = 0.03
        res@gsnRightStringFontHeightF   = FontHeight
        res@gsnLeftStringFontHeightF    = FontHeight
        res@tiYAxisFontHeightF          = FontHeight
        res@tmXBLabelFontHeightF        = FontHeight
        res@tmYLLabelFontHeightF        = FontHeight
        res@lbLabelFontHeightF          = FontHeight
        res@tmXBMajorOutwardLengthF     = 0.0
        res@tmXBMinorOutwardLengthF     = 0.0
        res@tmYLMajorOutwardLengthF     = 0.0
        res@tmYLMinorOutwardLengthF     = 0.0
        ;res@tmXBLabelAngleF             = -30.
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do m = 0,num_m-1
    oname = "DYNAMO_"+member(m)+(/"_00-04","_05-09"/)
    tfile   = idir+oname(0)+"/"+oname(0)+"."+var(v)+"."+lev(v)+".drift.var.hov.v1.nc"
    if recalc then
        ;---------------------------------------------------
        ; Load data
        ;---------------------------------------------------
        ifile   = idir+oname+"/"+oname+"."+var(v)+".nc"
        infile1 = addfile(ifile(0),"R")
        infile2 = addfile(ifile(1),"R")    
        ndim    = dimsizes(dimsizes(infile1->$var(v)$))

        if ndim.eq.4 then
            iV1 = infile1->$var(v)$(:,{lev(v)},{lat1:lat2},{lon1:lon2})
            iV2 = infile2->$var(v)$(:,{lev(v)},{lat1:lat2},{lon1:lon2})
        else
            iV1 = infile1->$var(v)$(:,{lat1:lat2},{lon1:lon2})
            iV2 = infile2->$var(v)$(:,{lat1:lat2},{lon1:lon2})
        end if

        iV1 := block_avg( iV1 ,4)
        iV2 := block_avg( iV2 ,4)

        lat = infile1->lat({lat1:lat2})
        lon = infile1->lon({lon1:lon2})
        nlat = dimsizes(lat)
        nlon = dimsizes(lon)
        xdim = (/num_t,nlat,nlon/)
        D = new(xdim,float)
        S = new(xdim,float)
        ;S = new((/num_t,nlon/),float)
        ;---------------------------------------------------
        ; Isolate Drift
        ;---------------------------------------------------
        dstride = 5;*4
        do d = 0,num_t/2-1
            d1 = d
            d2 = d+num_t/2
            D(d1,:,:) = dim_avg_n(iV1(d::dstride,:,:),0)
            D(d2,:,:) = dim_avg_n(iV2(d::dstride,:,:),0)
            S(d1,:,:) = dim_stddev_n(iV1(d::dstride,:,:),0)
            S(d2,:,:) = dim_stddev_n(iV2(d::dstride,:,:),0)
        end do
        
        D!0 = "time"
        D!1 = "lat"
        D!2 = "lon"
        D&lat = lat
        D&lon = lon
        copy_VarCoords(D,S)

        aS = block_avg( dim_avg_n_Wrap(S(:,{plat1:plat2},:),1) ,1)
        aD = block_avg( dim_avg_n_Wrap(D(:,{plat1:plat2},:),1) ,1)
        
        SE = aS / sqrt(dof-1)

        if m.eq.0 then 
            aD0  = conform(aD,dim_avg_n(aD,0),1) 
            SE0  = SE
            lon0 = lon
        end if

        if .not.altFlag then 
            aD =     (/ aD   - linint1(lon0,aD0,True,lon,1)   /) 
            ;SE = sqrt( SE^2. + linint1(lon0,SE0,True,lon,1)^2. )
        end if

        ltime = (tofloat(ispan(0,9,1)))
        ltime@units = "lead time [days]"
        
        aD&time = ltime 
        ;---------------------------------------------------
        ; write temporary file
        ;---------------------------------------------------
        copy_VarCoords(aD,aS)
        copy_VarCoords(aD,SE)
        if isfilepresent(tfile) then system("rm "+tfile) end if
        outfile = addfile(tfile,"c")
        outfile->aD = aD
        outfile->aS = aS
        outfile->SE = SE
        delete([/lat,lon,D,S,iV1,iV2/])
    else
        infile = addfile(tfile,"r")
        aD = infile->aD
        aS = infile->aS
        SE = infile->SE
    end if
    aD&lon@units = "degrees east"
    ;---------------------------------------------------
    ; significance test
    ;---------------------------------------------------

    ;tcrit = 2.131   ; 0.05 dof = 16
    tcrit = 1.96    ; 0.05 dof = inf
    ;tcrit = 1.
    ;res@gsnCenterString = "!!!!"

    tstat = aD / SE
    tstat@long_name = "t-statistic "+member(m)
    
    sig = where(abs(tstat).gt.tcrit,1.,0.)
    copy_VarCoords(aD,sig)

        printline()
        ;printMAM(tstat)
        printMAM( tcrit * SE  )
        printMAM( abs(aD) )
        n1 = num(sig.eq.1)
        n2 = product(dimsizes(sig))
        print((tofloat(n1)/tofloat(n2)*100.)+"%     ("+n1+"/"+n2+")")
        printline()
    ;---------------------------------------------------
    ; Create plot
    ;---------------------------------------------------
        tres = res
        tres@gsnLeftString              = case_name(m)
        tres@cnLevelSelectionMode       = "ExplicitLevels"
        tres@gsnRightString             = vname(v)
    
    
    if var(v).eq."U".and.lev(v).eq.850. then tres@cnLevels := fspan(-3.3,3.3,10) end if
    if var(v).eq."U".and.lev(v).eq.200. then tres@cnLevels := fspan(-7.5,7.5,10) end if

    if var(v).eq."CWV" then tres@cnLevels := ispan(-3,3,1) end if
    
    if altFlag then
        if var(v).eq."U".and.lev(v).eq.850. then tres@cnLevels := ispan(-10,10,2) end if
        if var(v).eq."U".and.lev(v).eq.200. then tres@cnLevels := ispan(-13,13,2) end if
    end if
    
    plot(m+num_m*v) = gsn_csm_contour(wks,aD,tres)
    ;plot(m+num_m*v) = gsn_csm_contour(wks,aS,tres)

        sres = setres_contour()
        sres@lbLabelBarOn           = False
        sres@cnLineLabelsOn         = False
        sres@cnInfoLabelOn          = False
        sres@cnFillPattern          = 17
        sres@cnFillScaleF           = 0.5
        sres@cnFillDotSizeF         = 0.0015
    if .not.all(sig.eq.0.) then
        overlay(plot(m+num_m*v),gsn_csm_contour(wks,sig,sres))

        opt = True
        opt@gsnShadeFillType = "pattern"        ; color is the default
        opt@gsnShadeLow  = -1
        opt@gsnShadeHigh = sres@cnFillPattern
        plot(m+num_m*v) = gsn_contour_shade(plot(m+num_m*v),0.5,0.5,opt)

    end if
    
    delete([/tres,aD,aS,SE,tstat,sig/])
    ;---------------------------------------------------
    ;---------------------------------------------------
end do
end do
;===================================================================================
;===================================================================================
        pres = True
        pres@gsnPanelBottom                     = 0.06
        pres@gsnPanelYWhiteSpacePercent         = 5.
        pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f","g","h","i"/)
        pres@gsnPanelFigureStringsFontHeightF   = 0.01
        pres@amJust                             = "TopLeft"
    
    layout = (/num_v,num_m/)
    
    gsn_panel(wks,plot,layout,pres)
    
    trimPNG(fig_file)
;===================================================================================
;===================================================================================
end


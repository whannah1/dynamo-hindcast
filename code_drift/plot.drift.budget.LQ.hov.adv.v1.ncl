; Plots a hovmoller of Column Lqv budget term drift as a function of Lead time
; 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
;===================================================================================
;===================================================================================
function cload(var)
local
begin

	ifile   = idir+oname+"/"+oname+"."+var+".nc"
	infile1 = addfile(ifile(0),"R")
	infile2 = addfile(ifile(1),"R")
	V1 = infile1->$var$(:,{lat1:lat2},{lon1:lon2})
	V2 = infile2->$var$(:,{lat1:lat2},{lon1:lon2})

end
;===================================================================================
;===================================================================================
begin
		;member = (/"EC","90","09"/)
		member = (/"EC"/)
		case_name = (/"EC","SP-CAM","CAM5"/)
		
		fig_type = "png"
		
		plat1 = -20.
		plat2 =  20.

		recalc = True 
		
		fig_file = "~/Research/DYNAMO/Hindcast/figs_drift/drift.budget.LQ.adv.hov.v1"
;===================================================================================
;===================================================================================
	setfileoption("nc","Format","NetCDF4")
	idir = "~/Research/DYNAMO/Hindcast/data/repacked/"
	num_m = dimsizes(member)  
	num_v = 5
	num_t = 10*4 

		lat1 = -20.
		lat2 =  20.
		lon1 =   0.
		lon2 = 180.
		
		olat1 = lat1
		olat2 = lat2
		
		opt  = True
		opt@lat1 = lat1
		opt@lat2 = lat2
		opt@lon1 = lon1
		opt@lon2 = lon2
	
	fig_type@wkHeight = 2048
	fig_type@wkWidth  = 2048
	wks = gsn_open_wks(fig_type,fig_file)
	;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	gsn_define_colormap(wks,"BlueWhiteOrangeRed")
	plot = new(num_m*num_v,graphic)
		res = True
		res@gsnDraw                     = False
		res@gsnFrame                    = False
		res@gsnSpreadColors             = True
		res@cnFillOn                    = True
		res@cnLinesOn                   = False
		;res@vpWidthF                   = 0.3
		;res@vpHeightF                  = 0.4
		res@tmXTOn                      = False
		res@tmYROn                      = False
		res@cnLineLabelsOn              = False
		;res@tmYLLabelAngleF            = 45.
		res@tiYAxisString               = "Lead Time [days]"
		;res@trYMaxF                    = num_t/4
		res@trYMinF                     = 0.
		res@trYReverse                  = True
		res@lbLabelBarOn                = True
		res@cnLevelSelectionMode        = "ExplicitLevels"
		res@cnInfoLabelOn               = False
		
		FontHeight = 0.03
		res@gsnRightStringFontHeightF   = FontHeight
		res@gsnLeftStringFontHeightF    = FontHeight
		res@tiYAxisFontHeightF          = FontHeight
		res@tmXBLabelFontHeightF        = FontHeight
		res@tmYLLabelFontHeightF        = FontHeight
		res@lbLabelFontHeightF          = FontHeight
		res@tmXBMajorOutwardLengthF     = 0.0
		res@tmXBMinorOutwardLengthF     = 0.0
		res@tmYLMajorOutwardLengthF     = 0.0
		res@tmYLMinorOutwardLengthF     = 0.0
		res@tmXBLabelAngleF             = -30.
		res@gsnRightString              = ""
		res@gsnLeftString               = ""
;===================================================================================
;===================================================================================
do m = 0,num_m-1
	oname = "DYNAMO_"+member(m)+(/"_00-04","_05-09"/)
	tfile = idir+"Q.adv."+oname+".nc"
	;---------------------------------------------------------------------------
	;---------------------------------------------------------------------------
	if recalc then 
		;---------------------------------------------------
		; Load data
		;---------------------------------------------------
		ifile   = idir+oname+"/"+oname+".Q.nc"
		infile1 = addfile(ifile(0),"R")
		infile2 = addfile(ifile(1),"R")
		Q1 = infile1->Q(:,{lat1:lat2},{lon1:lon2})
		Q2 = infile2->Q(:,{lat1:lat2},{lon1:lon2})

		ifile   = idir+oname+"/"+oname+".U.nc"
		infile1 = addfile(ifile(0),"R")
		infile2 = addfile(ifile(1),"R")
		U1 = infile1->U(:,{lat1:lat2},{lon1:lon2})
		U2 = infile2->U(:,{lat1:lat2},{lon1:lon2})

		ifile   = idir+oname+"/"+oname+".V.nc"
		infile1 = addfile(ifile(0),"R")
		infile2 = addfile(ifile(1),"R")
		V1 = infile1->V(:,{lat1:lat2},{lon1:lon2})
		V2 = infile2->V(:,{lat1:lat2},{lon1:lon2})

		ifile   = idir+oname+"/"+oname+".PS.nc"
		infile1 = addfile(ifile(0),"R")
		infile2 = addfile(ifile(1),"R")
		Ps1 = infile1->PS(:,{lat1:lat2},{lon1:lon2})
		Ps2 = infile2->PS(:,{lat1:lat2},{lon1:lon2})

		dP1 = calc_dP(Q&lev,PS1)
		dP2 = calc_dP(Q&lev,PS2)
		;---------------------------------------------------
		; Isolate Drift signal
		;---------------------------------------------------
		lev = Q&lev
		lat = Q&lat
		lon = Q&lon
		num_lat = dimsizes(lat)
		num_lon = dimsizes(lon)
		nuim_c = 16
		
		xdim = (/num_c,10,num_lev,num_lat,num_lon/)
		Q  = new(xdim,float)
		U  = new(xdim,float)
		V  = new(xdim,float)
		dP = new(xdim,float)
		do n = 0,4
			Q (:,n+0,:,:,:) = Q1(n::5,:,:,:)
			Q (:,n+5,:,:,:) = Q2(n::5,:,:,:)

			U (:,n+0,:,:,:) = U1(n::5,:,:,:)
			U (:,n+5,:,:,:) = U2(n::5,:,:,:)

			V (:,n+0,:,:,:) = V1(n::5,:,:,:)
			V (:,n+5,:,:,:) = V2(n::5,:,:,:)

			dP(:,n+0,:,:,:) = dP1(n::5,:,:,:)
			dP(:,n+5,:,:,:) = dP2(n::5,:,:,:)
		end do
		delete([/Q1,Q2,U1,U2,V1,V2,dP1,dP2,PS1,PS2/])
		;---------------------------------------------------
		;---------------------------------------------------
		navg = 3
		opt  = 0
		ndim = 4
		xdim = (/0,1,2,3/)
		;Qb = runave_n_Wrap(Q,navg,opt,dim)
		Qb = conform(Q,dim_avg_n(Q,dim),xdim)
		Ub = conform(U,dim_avg_n(U,dim),xdim)
		Vb = conform(V,dim_avg_n(V,dim),xdim)

		Qp = Q - Qb
		Up = U - Ub
		Vp = V - Vb
		
		UdQdx = dim_sum_n( U * calc_ddx(Q) *dP/g,2)
		VdQdx = dim_sum_n( V * calc_ddy(Q) *dP/g,2)
		delete([/Q,U,V/])

		UbdQbdx = dim_sum_n( Ub * calc_ddx(Qb) *dP/g,2)
		VbdQbdx = dim_sum_n( Vb * calc_ddy(Qb) *dP/g,2)
		UpdQpdx = dim_sum_n( Up * calc_ddx(Qp) *dP/g,2)
		VpdQpdx = dim_sum_n( Vp * calc_ddy(Qp) *dP/g,2)
		delete([/dP,Qb,Ub,Vb,Qp,Up,Vp/])
		;---------------------------------------------------
		;---------------------------------------------------
		if isfilepresent(tfile) then system("rm "+tfile) end if
		outfile = addfile(tfile,"c")
		outfile->UdQdx   = UdQdx
		outfile->VdQdx   = VdQdx
		outfile->UbdQbdx = UbdQbdx
		outfile->VbdQbdx = VbdQbdx
		outfile->UpdQpdx = UpdQpdx
		outfile->VpdQpdx = VpdQpdx
		;---------------------------------------------------
		;---------------------------------------------------
	else
		infile = addfile(tfile,"r")
		UdQdx   = UdQdx
		VdQdx   = VdQdx
		UbdQbdx = UbdQbdx
		VbdQbdx = VbdQbdx
		UpdQpdx = UpdQpdx
		VpdQpdx = VpdQpdx
	end if
	;---------------------------------------------------------------------------
	;---------------------------------------------------------------------------
	nvar = 3 

	xdim = (/nvar,10,num_lat,num_lon/)
	D = new(xdim,float)

	dstride = 5*4
	do d = 0,num_t/2-1
	d1 = d
	d2 = d+num_t/2
	do v = 0,nvar-1
		if v.eq.0 then D(v,d1,:,:) = dim_avg_n(wdQdp1(d::dstride,:,:),0) end if
		if v.eq.0 then D(v,d2,:,:) = dim_avg_n(wdQdp2(d::dstride,:,:),0) end if
		if v.eq.1 then D(v,d1,:,:) = dim_avg_n(LHFLX1(d::dstride,:,:),0) end if
		if v.eq.1 then D(v,d2,:,:) = dim_avg_n(LHFLX2(d::dstride,:,:),0) end if
	end do
	end do
	
	D!0 = "var"
	D!1 = "time"
	D!2 = "lat"
	D!3 = "lon"
	D&lat = lat
	D&lon = lon
    ;D&var = (/"<U*dQ/dx>","<V*dQ/dy>","","","",""/) 
	;---------------------------------------------------------------------------
	;---------------------------------------------------------------------------
		aD = new((/nvar,num_t/4,nlon/),float)

		do v = 0,nvar-1 aD(v,:,:) = block_avg2( dim_avg_n_Wrap(D(v,:,{plat1:plat2},:),1) ,4) end do
		
		if m.eq.0 then aD0 = dim_avg_n(aD(:,:,:),1) end if
		if m.eq.0 then lon0 = lon end if
		do t = 0,dimsizes(aD(0,:,0))-1
			aD(:,t,:) = (/ aD(:,t,:) - linint1(lon0,aD0,True,lon,1) /)
		end do
		
		ltime = (tofloat(ispan(0,9,1)))
		ltime@units = "lead time [days]"
		aD&time = ltime 
		
			tres = res
			tres@gsnLeftString          = case_name(m)
			tres@cnLevelSelectionMode   = "ExplicitLevels"

		do v = 0,nvar-1
				tres@cnLevels   = ispan(-90,90,15)
				tres@gsnRightString     = D&var(v)
			plot(m+num_m*v) = gsn_csm_contour(wks,aD(v,:,:),tres)   
			delete(tres@cnLevels)
		end do
		;delete([/tres/])
		delete([/lat,lon/])
end do
;===================================================================================
;===================================================================================
		pres = True
		pres@gsnPanelBottom                     = 0.06
		pres@gsnPanelYWhiteSpacePercent         = 5.
		pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f","g","h","i","j","k","l","m","n"/)
		pres@gsnPanelFigureStringsFontHeightF   = 0.005
		pres@amJust                             = "TopLeft"
	
	layout = (/num_v,num_m/)
	
	gsn_panel(wks,plot,layout,pres)
	
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


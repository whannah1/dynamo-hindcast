; Plots the drift signal of a variable in lead time vs. longitude
; drift is calculated relative to the first member (obs)
; v1 is shows a conventional hovmoller of the drift signal
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin
	;member    = (/"ERAi","90"/)
  	member    = (/"ERAi","90","09"/)
  	case_name = (/"ERAi","SP-CAM","CAM5"/)
    
    rd = (/5/)
    
    lev = 700.
    
    ndayrun = 11
    
    num_v = 2
    
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/Hindcast/figs_drift/drift.reg.v2"
;===================================================================================
;===================================================================================
  idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
  num_m = dimsizes(member)
  num_r = dimsizes(rd)
  num_t = 10*4 
  
  ;vname = var+lev(:num_v-1) 
  ;vname = where(var.eq."FLUT","OLR",vname)
	
	; Input data range
	lat1 = -15.
	lat2 =  35.
	lon1 =   0.
	lon2 = 360.
	
	; plot range
	plat1 = -10.
  	plat2 =  30.
  	plon1 =  45.
  	plon2 =  90.
  	
  	; box for index
  	clat1 = 14.
  	clat2 = 18.
  	clon1 = 64.
  	clon2 = 68.
	
	opt  = True
  	opt@lat1 = lat1
  	opt@lat2 = lat2
  	opt@lon1 = lon1
  	opt@lon2 = lon2
  
  fig_type@wkWidth  = 2048*2.
  fig_type@wkHeight = 2048*2.
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")
  ;plot = new((num_m)*num_r,graphic)
  plot = new((num_m)*num_v,graphic)
  	res = True
  	res@gsnDraw 					= False
  	res@gsnFrame 					= False
  	res@gsnSpreadColors 			= True
  	res@cnFillOn 					= True
  	res@cnLinesOn 					= False
  	res@cnInfoLabelOn				= False
  	res@cnLineLabelsOn 				= False
  	res@lbLabelBarOn 				= True
  	res@mpMinLatF					= plat1
  	res@mpMaxLatF					= plat2
  	res@mpMinLonF					= plon1
  	res@mpMaxLonF					= plon2
  	;res@mpCenterLonF				= 150.
  	
  	res@gsnRightStringFontHeightF	= 0.015
  	res@gsnLeftStringFontHeightF	= 0.015
  	res@tiYAxisFontHeightF			= 0.015
  	res@tmXBLabelFontHeightF		= 0.015
  	res@tmYLLabelFontHeightF		= 0.015
  	res@lbLabelFontHeightF			= 0.015
  	res@tmXBMajorOutwardLengthF		= 0.0
  	res@tmXBMinorOutwardLengthF		= 0.0
  	res@tmYLMajorOutwardLengthF		= 0.0
  	res@tmYLMinorOutwardLengthF		= 0.0
  	
  	vres = True
  	vres@gsnDraw 					= False
  	vres@gsnFrame 					= False
  	vres@vcRefMagnitudeF            = 5.             	; define vector ref mag
  	vres@vcRefLengthF               = 0.045           	; define length of vec ref
  	vres@vcRefAnnoOrthogonalPosF    = -.5            	; move ref vector
  	vres@vcRefAnnoArrowLineColor    = "black"         	; change ref vector color
  	vres@vcRefAnnoArrowUseVecColor 	= False           	; don't use vec color for ref
  	vres@vcGlyphStyle              	= "CurlyVector"     ; turn on curley vectors
  	vres@vcLineArrowColor           = "black"           ; change vector color
  	vres@vcLineArrowThicknessF      = 1.0               ; change vector thickness
  	vres@vcVectorDrawOrder          = "PostDraw"        ; draw vectors last
  	vres@vcMinMagnitudeF			= 1.
  	vres@gsnLeftString  			= ""
  	vres@gsnRightString  			= ""
  	vres@gsnCenterString  			= ""
  	vres@vcMinDistanceF 			= 0.017
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do r = 0,num_r-1
do m = 0,num_m-1
  tmember = member(m)
  rds = sprinti("%0.2i",rd(r))
  rde = sprinti("%0.2i",rd(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  ;oname = "DYNAMO_"+tmember+(/"_00-04","_05-09"/)
  print("-------------------------------------------------------------------")
  print(""+oname)
  ;---------------------------------------------------
  ; Load the data
  ;---------------------------------------------------
  		ifile   = idir+"data/"+oname+"/"+oname+".U.nc"
    	infile1 = addfile(ifile(0),"R")
    	U = infile1->U(:,{lev},{lat1:lat2},{lon1:lon2})

    	ifile   = idir+"data/"+oname+"/"+oname+".V.nc"
    	infile1 = addfile(ifile(0),"R")
    	V = infile1->V(:,{lev},{lat1:lat2},{lon1:lon2})
    	
		lat  = infile1->lat({lat1:lat2})
  		lon  = infile1->lon({lon1:lon2})
    	nlat = dimsizes(lat)
    	nlon = dimsizes(lon)
    	
    	if m.eq.0 then 
    	  Uo   = U 
    	  Vo   = V 
    	  lat0 = lat
    	  lon0 = lon
    	end if
		
		Ub = linint2(lon0,lat0, runave_n_Wrap(Uo,ndayrun,0,0) ,True,lon,lat,1)
		Vb = linint2(lon0,lat0, runave_n_Wrap(Vo,ndayrun,0,0) ,True,lon,lat,1) 
		
    	Up = U - Ub 
		Vp = V - Vb
		
		Ud = dim_avg_n(U,0) - dim_avg_n( linint2(lon0,lat0,Uo ,True,lon,lat,1) ,0)
		Vd = dim_avg_n(V,0) - dim_avg_n( linint2(lon0,lat0,Vo ,True,lon,lat,1) ,0)
		
		Ud!0 = "lat"
		Ud!1 = "lon"
		Ud&lat = lat
		Ud&lon = lon
		copy_VarCoords(Ud,Vd)
  ;---------------------------------------------------
  ; Calculate EKE and EVR
  ;---------------------------------------------------    
		xvals = ispan(0,nlon-1,1)
        dxv = xvals(1) - xvals(0)
        Rvals = where((xvals+1).le.max(xvals),xvals+1,xvals+1-(max(xvals)+1))
        Xvals = xvals
        Lvals = where((xvals-1).ge.min(xvals),xvals-1,xvals-1+(max(xvals)+1))
		yvals = ispan(0,nlat-1,1)
		Nvals = yvals(2:nlat-1)
		Cvals = yvals(1:nlat-2)
		Svals = yvals(0:nlat-3)
		dUdx  = new(dimsizes(Up),float)
		dUdy  = new(dimsizes(Up),float)
		dVdx  = new(dimsizes(Up),float)
		dVdy  = new(dimsizes(Up),float)
		dUpdy = new(dimsizes(Up),float)
		dVpdx = new(dimsizes(Up),float)
		dlon  = lon(1)-lon(0)
		dx = conform(Up(:,Cvals,Xvals), tofloat( (lon+dlon  +lon(Xvals))/2. -(lon-dlon  +lon(Xvals))/2. ) ,2)
        dy = conform(Up(:,Cvals,Xvals), tofloat( (lat(Nvals)+lat(Cvals))/2. -(lat(Svals)+lat(Cvals))/2. ) ,1)
        dx = dx*111000.*conform(dUpdy(:,Cvals,Xvals),tofloat(cos(lat(Cvals)*3.14159/180.)),1)
        dy = dy*111000.        
    
    dUdx(:,Cvals,Xvals) = ( (Ub(:,Cvals,Rvals)+Ub(:,Cvals,Xvals))/2.  \
                           -(Ub(:,Cvals,Lvals)+Ub(:,Cvals,Xvals))/2.   )/dx
    dUdy(:,Cvals,Xvals) = ( (Ub(:,Nvals,Xvals)+Ub(:,Cvals,Xvals))/2.  \
                           -(Ub(:,Svals,Xvals)+Ub(:,Cvals,Xvals))/2.   )/dy
	dVdx(:,Cvals,Xvals) = ( (Vb(:,Cvals,Rvals)+Vb(:,Cvals,Xvals))/2.  \
                           -(Vb(:,Cvals,Lvals)+Vb(:,Cvals,Xvals))/2.   )/dx
	dVdy(:,Cvals,Xvals) = ( (Vb(:,Nvals,Xvals)+Vb(:,Cvals,Xvals))/2.  \
                           -(Vb(:,Svals,Xvals)+Vb(:,Cvals,Xvals))/2.   )/dy
	
	UVdUdy = -1.*runave_n_Wrap(Up*Vp,ndayrun,0,0) * dUdy
	UUdUdx = -1.*runave_n_Wrap(Up*Up,ndayrun,0,0) * dUdx
	UVdVdx = -1.*runave_n_Wrap(Up*Vp,ndayrun,0,0) * dVdx
	VVdVdy = -1.*runave_n_Wrap(Vp*Vp,ndayrun,0,0) * dVdy
	
	BT  = UVdUdy + UUdUdx + UVdVdx + VVdVdy
	
    dUpdy(:,Cvals,Xvals) = ( (Up(:,Nvals,Xvals)+Up(:,Cvals,Xvals))/2.  \
                            -(Up(:,Svals,Xvals)+Up(:,Cvals,Xvals))/2.   )/dy
	dVpdx(:,Cvals,Xvals) = ( (Vp(:,Cvals,Rvals)+Vp(:,Cvals,Xvals))/2.  \
                            -(Vp(:,Cvals,Lvals)+Vp(:,Cvals,Xvals))/2.   )/dx

	EVR = ( dVpdx - dUpdy )
	EKE = ( Up^2. + Vp^2. )/2.
	
	
	EKE!0 = "time"
	EKE!1 = "lat"
	EKE!2 = "lon"
	EKE&lat = lat
	EKE&lon = lon
	copy_VarCoords(EKE,EVR)
	copy_VarCoords(EKE,BT)
  ;---------------------------------------------------
  ; Calcualte Regression Map
  ;---------------------------------------------------
	index = dim_avg_n_Wrap(EKE(:,{clat1:clat2},{clon1:clon2}),(/1,2/))
	;index = dim_avg_n_Wrap(EVR(:,{clat1:clat2},{clon1:clon2}),(/1,2/))
	
	comp = new((/num_v,nlat,nlon/),float)
	
  	comp(0,:,:) = regCoef(index/stddev(index),EKE(lat|:,lon|:,time|:))
  	comp(1,:,:) = regCoef(index/stddev(index),BT(lat|:,lon|:,time|:))
  
  	comp!0 = "var"
  	comp!1 = "lat"
  	comp!2 = "lon"
  	comp&lat = lat
  	comp&lon = lon
  ;---------------------------------------------------
  ; Create Plot
  ;---------------------------------------------------  
  	vname = (/"EKE","UVdUdy","UUdUdx","UVdVdx","VVdVdy"/)
  	do i = 0,num_v-1
  		tres = res
  		tres@gsnLeftString  		= case_name(m)+" "+rds+"-"+rde
  		tres@gsnRightString 		= vname(i)
  		tres@cnLevelSelectionMode	= "ExplicitLevels"
		if i.eq.0 then tres@cnLevels = ispan(-70,70,5) end if
		;if i.ne.0 then tres@cnLevels = ispan(-40,40,5)*1e-5 end if
	  print(i+"	"+min(comp(i,:,:))+"	-	"+max(comp(i,:,:)))  
   	  p = m+(num_m)*i
      plot(p) = gsn_csm_contour_map(wks,comp(i,:,:),tres)
      overlay(plot(p),gsn_csm_vector(wks,Ud,Vd,vres))
      delete(tres)
    end do
  ;---------------------------------------------------
  ;---------------------------------------------------
    delete([/lat,lon/])
    delete([/EKE,EVR,comp/])
    delete([/U,V,Up,Ub,Vb,Ud,Vd/])
    delete([/Vp,dUpdy,dVpdx,dlon,dx,dy/])
    delete([/dUdx,dUdy,dVdx,dVdy/])
    delete([/UVdUdy,UUdUdx,UVdVdx,VVdVdy/])
    delete([/xvals,Rvals,Xvals,Lvals/])
    delete([/yvals,Nvals,Cvals,Svals/])
end do
end do
end do
print("-------------------------------------------------------------------")
  ;---------------------------------------------------
  ; Add box around index region
  ;---------------------------------------------------
	  gres                 	= True
	  gres@gsLineColor		= "black"
	  gres@gsLineThicknessF = 2.0

  blat = (/clat1,clat2,clat2,clat1,clat1/)
  blon = (/clon1,clon1,clon2,clon2,clon1/)
  dum = new(dimsizes(plot),graphic)
  do p = 0,dimsizes(plot)-1
    dum(p) = gsn_add_polyline(wks,plot(p),blon,blat,gres)
  end do
;===================================================================================
;===================================================================================
  	pres = True
    pres@gsnPanelFigureStrings				= (/"a","b","c","d","e","f"/)
    pres@gsnPanelFigureStringsFontHeightF	= 0.01
    pres@amJust								= "TopRight"
  
  layout = (/num_v,num_m/)
  
  gsn_panel(wks,plot,layout,pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end



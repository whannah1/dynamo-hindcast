; Plots a 1D timeseries of Column MSE budget term drift as a function of Lead time
; v3 is similar to v2, but includes meridional eddy advection
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_misc.ncl"
begin
    member = (/"ERAi","90","09"/)
    case_name = (/"ERAi","SP-CAM","ZM_1.0"/)

    clr = (/"black","purple","blue","green"/)
    
    fig_type = "png"
    
    lat1 = -12.
    lat2 =  12.
    lon1 =  70.
    lon2 =  80.

    if True then
      ilat1 = lat1
      ilat2 = lat2
      ilon1 = lon1
      ilon2 = lon2
    else
      ilat1 = -10.
      ilat2 =  10.
      ilon1 =  70.
      ilon2 =  80.
    end if
    
    fig_file = "~/DYNAMO/Hindcast/drift.budget.1D.v3"
;===================================================================================
;===================================================================================
  idir = "~/DYNAMO/Hindcast/"
  num_m = dimsizes(member)  
  num_v = 2
  num_t = 10*4 
  
  tlev = ispan(1000,100,25)
  num_lev = dimsizes(tlev)

  vname = new(num_v,string)
    
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2
  
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")
  plot = new(num_v,graphic)
    res = True
    res@gsnDraw             = False
    res@gsnFrame            = False
    res@vpWidthF           = 0.4
    res@tmXTOn              = False
    res@tmYROn              = False
    res@tiXAxisString       = "Lead Time [days]"
    res@tiYAxisString       = ""
    
    FontHeight = 0.02
    res@gsnRightStringFontHeightF   = 0.03;FontHeight
    res@gsnLeftStringFontHeightF    = 0.03;FontHeight
    res@tiYAxisFontHeightF          = FontHeight
    res@tmXBLabelFontHeightF        = FontHeight
    res@tmYLLabelFontHeightF        = FontHeight
    res@tmXBMajorOutwardLengthF     = 0.0
    res@tmXBMinorOutwardLengthF     = 0.0
    res@tmYLMajorOutwardLengthF     = 0.0
    res@tmYLMinorOutwardLengthF     = 0.0
    res@tmXBMinorOn                 = False
    res@tmYLMinorOn                 = False
    res@tmXBAutoPrecision           = False
    res@tmYLAutoPrecision           = False
    res@tmXBPrecision               = 2
    res@tmYLPrecision               = 2

    lres = res
    lres@gsnLeftString          = ""
    lres@gsnRightString         = ""
    lres@xyDashPattern          = 1
    lres@xyLineThicknessF       = 1.0
    lres@xyLineColor            = "black"
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+(/"_00-04","_05-09"/)
  ;---------------------------------------------------
  ; Load MSE budget
  ;---------------------------------------------------
    ifile   = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")    

    MSEvi1 = infile1->MSEvi(:,{lat1:lat2},{lon1:lon2})
    COLDP1 = infile1->COLDP(:,{lat1:lat2},{lon1:lon2})
    MSEDT1 = infile1->MSEDT(:,{lat1:lat2},{lon1:lon2})
    VdelH1 = infile1->VdelH(:,{lat1:lat2},{lon1:lon2})
    udHdx1 = infile1->udHdx(:,{lat1:lat2},{lon1:lon2})
    vdHdy1 = infile1->vdHdy(:,{lat1:lat2},{lon1:lon2})
    wdHdp1 = infile1->WdHdp(:,{lat1:lat2},{lon1:lon2})
    COLQR1 = infile1->COLQR(:,{lat1:lat2},{lon1:lon2})
    LHFLX1 = infile1->LHFLX(:,{lat1:lat2},{lon1:lon2})
    SHFLX1 = infile1->SHFLX(:,{lat1:lat2},{lon1:lon2})
    
    MSEvi2 = infile2->MSEvi(:,{lat1:lat2},{lon1:lon2})
    COLDP2 = infile2->COLDP(:,{lat1:lat2},{lon1:lon2})
    MSEDT2 = infile2->MSEDT(:,{lat1:lat2},{lon1:lon2})
    VdelH2 = infile2->VdelH(:,{lat1:lat2},{lon1:lon2})
    udHdx2 = infile2->udHdx(:,{lat1:lat2},{lon1:lon2})
    vdHdy2 = infile2->vdHdy(:,{lat1:lat2},{lon1:lon2})
    wdHdp2 = infile2->WdHdp(:,{lat1:lat2},{lon1:lon2})
    COLQR2 = infile2->COLQR(:,{lat1:lat2},{lon1:lon2})
    LHFLX2 = infile2->LHFLX(:,{lat1:lat2},{lon1:lon2})
    SHFLX2 = infile2->SHFLX(:,{lat1:lat2},{lon1:lon2})
    
    MSEvi1 = (/ MSEvi1/COLDP1/cpd /)
    MSEvi2 = (/ MSEvi2/COLDP2/cpd /)
  ;---------------------------------------------------
  ; Calculate MSE budget Residual
  ;---------------------------------------------------
    RESIDUAL1 = MSEDT1*0.
    RESIDUAL2 = MSEDT2*0.
    RESIDUAL1 = RESIDUAL1@_FillValue
    RESIDUAL2 = RESIDUAL2@_FillValue
    
    SFFLX1 = LHFLX1 + SHFLX1 
    SFFLX2 = LHFLX2 + SHFLX2
    
    SRC1 = COLQR1 + SFFLX1
    SRC2 = COLQR2 + SFFLX2
    
    RESIDUAL1 = MSEDT1 + udHdx1 + vdHdy1 + wdHdp1 - COLQR1 - SFFLX1
    RESIDUAL2 = MSEDT2 + udHdx2 + vdHdy2 + wdHdp2 - COLQR2 - SFFLX2
    
    udHdx1 = -udHdx1
    udHdx2 = -udHdx2
    vdHdy1 = -vdHdy1
    vdHdy2 = -vdHdy2
    wdHdp1 = -wdHdp1
    wdHdp2 = -wdHdp2
    VdelH1 = -VdelH1
    VdelH2 = -VdelH2
  ;---------------------------------------------------
    ifile = idir+"data/"+oname+"/"+oname+".Ps.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    Ps1 = infile1->PS(:,{lat1:lat2},{lon1:lon2})
    Ps2 = infile2->PS(:,{lat1:lat2},{lon1:lon2})

    ifile = idir+"data/"+oname+"/"+oname+".MSE.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    H1 = linint1_n_Wrap(infile1->lev,infile1->MSE(:,:,{lat1:lat2},{lon1:lon2}),False,tlev,0,1)
    H2 = linint1_n_Wrap(infile2->lev,infile2->MSE(:,:,{lat1:lat2},{lon1:lon2}),False,tlev,0,1)
  
    ifile = idir+"data/"+oname+"/"+oname+".V.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    V1 = linint1_n_Wrap(infile1->lev,infile1->V(:,:,{lat1:lat2},{lon1:lon2}),False,tlev,0,1)
    V2 = linint1_n_Wrap(infile2->lev,infile2->V(:,:,{lat1:lat2},{lon1:lon2}),False,tlev,0,1)

    P  = conform(V1,tlev,1)*100.
    dP1 = new(dimsizes(V1),float)
    dP2 = new(dimsizes(V1),float)
    Ps13D = conform(P,Ps1,(/0,2,3/))
    Ps23D = conform(P,Ps2,(/0,2,3/))
    dP1(:,1:num_lev-2,:,:) = (P(:,0:num_lev-3,:,:)+P(:,1:num_lev-2,:,:))/2. - (P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:))/2.
    dP1(:,0,:,:)  = where( Ps1.gt.P(:,0,:,:) , Ps1 - (P(:,0,:,:)+P(:,1,:,:))/2. , dP1@_FillValue)
    dP1(:,1:num_lev-2,:,:) = where( (Ps13D(:,1:num_lev-2,:,:).gt.P(:,1:num_lev-2,:,:)).and.	\
                                    (Ps13D(:,1:num_lev-2,:,:).lt.P(:,0:num_lev-3,:,:)), 	\
                                     Ps13D(:,1:num_lev-2,:,:)-(P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:))/2., \
                                       dP1(:,1:num_lev-2,:,:) ) 
    dP2(:,1:num_lev-2,:,:) = (P(:,0:num_lev-3,:,:)+P(:,1:num_lev-2,:,:))/2. - (P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:))/2.
    dP2(:,0,:,:)  = where( Ps2.gt.P(:,0,:,:) , Ps2 - (P(:,0,:,:)+P(:,1,:,:))/2. , dP2@_FillValue)
    dP2(:,1:num_lev-2,:,:) = where( (Ps23D(:,1:num_lev-2,:,:).gt.P(:,1:num_lev-2,:,:)).and.	\
                                    (Ps23D(:,1:num_lev-2,:,:).lt.P(:,0:num_lev-3,:,:)), 	\
                                     Ps23D(:,1:num_lev-2,:,:)-(P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:))/2., \
                                       dP2(:,1:num_lev-2,:,:) ) 

    lat = V1&lat
    lon = V1&lon 
    num_lat = dimsizes(lat)
    num_lon = dimsizes(lon)
    nt = dimsizes(H1(:,0,0,0))

    ndaywgt = 5

    H1 = H1 - runave_n_Wrap(H1,ndaywgt*4,0,0)
    H2 = H2 - runave_n_Wrap(H2,ndaywgt*4,0,0)
    V1 = V1 - runave_n_Wrap(V1,ndaywgt*4,0,0)
    V2 = V2 - runave_n_Wrap(V2,ndaywgt*4,0,0)

    xvals = ispan(0,num_lon-1,1)
    Rvals = xvals(2:num_lon-1)
    Xvals = xvals(1:num_lon-2)
    Lvals = xvals(0:num_lon-3)
    yvals = ispan(0,num_lat-1,1)
    Nvals = yvals(2:num_lat-1)
    Cvals = yvals(1:num_lat-2)
    Svals = yvals(0:num_lat-3)

    tdP1 = dP1(:,:,Cvals,:)
    tdP2 = dP2(:,:,Cvals,:)
    tV1  =  V1(:,:,Cvals,:)
    tV2  =  V2(:,:,Cvals,:)
    delete([/dP1,V1/])
    delete([/dP2,V2/])
    dP1 = tdP1
    dP2 = tdP2
    V1  = tV1
    V2  = tV2
    delete([/tdP1,tV1/])
    delete([/tdP2,tV2/])
  
    ;H1_dx  = new((/num_t,num_lev,num_lat-2,num_lon-2/),float)
    H1_dy  = new((/nt,num_lev,num_lat-2,num_lon/),float)
    H2_dy  = new((/nt,num_lev,num_lat-2,num_lon/),float)

    ;dx = tofloat( (lon(2)-lon(0))*111000.*conform(S_dx,cos(lat(Cvals)*3.14159/180.),2) )
    dy = tofloat( (lat(2)-lat(0))*111000. )
  	;H1_dx  = (  H1(:,:,Cvals,Rvals)- S(:,:,Cvals,Lvals) )/dx
    H1_dy  = (  H1(:,:,Nvals,:)- H1(:,:,Svals,:) )/dy
    H2_dy  = (  H2(:,:,Nvals,:)- H2(:,:,Svals,:) )/dy

    vpdHpdy1 = dim_sum_n( (V1*H1_dy)*dP1/g,1)
    vpdHpdy2 = dim_sum_n( (V2*H2_dy)*dP2/g,1)

    vpdHpdy1 = -vpdHpdy1
    vpdHpdy2 = -vpdHpdy2

    vpdHpdy1!0 = "time"
    vpdHpdy1!1 = "lat"
    vpdHpdy1!2 = "lon"
    vpdHpdy1&lat = lat(Cvals)
    vpdHpdy1&lon = lon
    copy_VarCoords(vpdHpdy1,vpdHpdy2)

    delete([/P,dy/])
    delete([/H1,V1,Ps1,Ps13D,dP1,H1_dy/])
    delete([/H2,V2,Ps2,Ps23D,dP2,H2_dy/])
  ;---------------------------------------------------
  ; Isolate Drift signal
  ;---------------------------------------------------
    udHdx1@long_name = "-<u dh/dx>"
    vdHdy1@long_name = "-<v dh/dy>"
    wdHdp1@long_name = "-<w dh/dp>"
    COLQR1@long_name = "<Q~B~R~N~>"

    vpdHpdy1@long_name = "-<v'dh'/dy>"

    lat = infile1->lat({lat1:lat2})
    lon = infile1->lon({lon1:lon2})
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)
    xdim = (/2,num_v,num_t,nlat-2,nlon/)
    D = new(xdim,float)

    ;index1 = conform(MSEDT1,dim_avg_n(MSEDT1(:,{ilat1:ilat2},{ilon1:ilon2}),(/1,2/)),0)
    ;index2 = conform(MSEDT2,dim_avg_n(MSEDT2(:,{ilat1:ilat2},{ilon1:ilon2}),(/1,2/)),0)

    index1 = MSEDT1(:,Cvals,:)
    index2 = MSEDT2(:,Cvals,:)

    ;do n = 0,0
      index1 = (/ run_wgt_mean_2D(index1,lat(Cvals),lon) /)
      index2 = (/ run_wgt_mean_2D(index2,lat(Cvals),lon) /)
    ;end do

    dstride = 5*4
    do v = 0,num_v-1
    do d = 0,num_t/2-1
      d1 = d
      d2 = d+num_t/2
      if v.eq.0 then tmp1 = vdHdy1(:,Cvals,:) end if
      if v.eq.0 then tmp2 = vdHdy2(:,Cvals,:) end if
      if v.eq.1 then tmp1 = vpdHpdy1 end if
      if v.eq.1 then tmp2 = vpdHpdy2 end if   
      if v.eq.2 then tmp1 = wdHdp1 end if
      if v.eq.2 then tmp2 = wdHdp2 end if
      if v.eq.3 then tmp1 = COLQR1 end if
      if v.eq.3 then tmp2 = COLQR2 end if
      ttmp1 = where(index1.gt.0,tmp1,tmp1@_FillValue)
      ttmp2 = where(index2.gt.0,tmp2,tmp2@_FillValue)
      D(0,v,d1,:,:) = dim_avg_n(ttmp1(d::dstride,:,:),0)
      D(0,v,d2,:,:) = dim_avg_n(ttmp2(d::dstride,:,:),0)
      ttmp1 = where(index1.lt.0,tmp1,tmp1@_FillValue)
      ttmp2 = where(index2.lt.0,tmp2,tmp2@_FillValue)
      D(1,v,d1,:,:) = dim_avg_n(ttmp1(d::dstride,:,:),0)
      D(1,v,d2,:,:) = dim_avg_n(ttmp2(d::dstride,:,:),0)
      vname(v) = tmp1@long_name
      delete([/tmp1,tmp2,ttmp1,ttmp2/])
    end do
    end do
    
    D!0 = "seg"
    D!1 = "var"
    D!2 = "time"
    D!3 = "lat"
    D!4 = "lon"
    D&lat = lat(Cvals)
    D&lon = lon

    delete([/index1,index2/])
  ;---------------------------------------------------
  ;---------------------------------------------------
    num_t2 = num_t/4
    aD = new((/2,num_v,num_t2/),float)
    do s = 0,1
    do v = 0,num_v-1
      aD(s,v,:) = avg4to1(dim_avg_n_Wrap(D(s,v,:,:,:),(/1,2/)))
      ;aD(v,:) = dim_avg_n_Wrap(D(v,:,:,:),(/1,2/))
    end do
    end do
    
    aD0 = aD(:,:,0)
    if m.eq.0 then aD0 = dim_avg_n(aD(:,:,:),2) end if

    ;do v = 0,num_v-1
    ;do t = 0,num_t2-1
    ;  aD(v,t) = (/ aD(v,t) - aD0(v) /)
    ;  ;aD(v,t,:) = (/ aD(v,t,:) - linint1(lon0,aD0,True,lon,1) /)
    ;end do
    ;end do
    
    if num_t2.eq.num_t then ltime = tofloat(ispan(0,40-1,1))/4 end if
    if num_t2.ne.num_t then ltime = tofloat(ispan(0,10-1,1)) end if
    ltime@units = "lead time [days]"
    aD!0 = "seg"
    aD!1 = "var"
    aD!2 = "time"
    aD&time = ltime 
    
    
    do v = 0,num_v-1
        tres = res
        tres@gsnLeftString          = ""
        tres@gsnRightString         = ""
        tres@xyLineColor            = clr(m)
        tres@xyLineThicknessF       = 4.
        tres@trXMaxF                = max(ltime)
        tres@trYMinF                = -150.
        tres@trYMaxF                =  100.
        ;if v.eq.3 then tres@trYMaxF =  50. end if
      m1 = 0
      if m.eq.m1 then tres@gsnRightString       = vname(v) end if
      if m.eq.m1 then         plot(v) = gsn_csm_xy(wks,ltime,aD(0,v,:) ,tres)  end if
      if m.gt.m1 then overlay(plot(v) , gsn_csm_xy(wks,ltime,aD(0,v,:) ,tres)) end if

        tres@gsnLeftString          = ""
        tres@gsnRightString         = ""
        tres@xyDashPattern          = 16
      if m.eq.m1 then overlay(plot(v) , gsn_csm_xy(wks,ltime,aD(1,v,:) ,tres)) end if
      if m.gt.m1 then overlay(plot(v) , gsn_csm_xy(wks,ltime,aD(1,v,:) ,tres)) end if

      if m.eq.m1 then overlay(plot(v) , gsn_csm_xy(wks,ltime,ltime*0.,lres)) end if
        delete(tres)
    end do

    delete([/D,aD,aD0/])
    delete([/MSEvi1,COLDP1,MSEDT1,VdelH1,udHdx1,vdHdy1,wdHdp1/])
    delete([/MSEvi2,COLDP2,MSEDT2,VdelH2,udHdx2,vdHdy2,wdHdp2/])
    delete([/COLQR1,LHFLX1,SHFLX1,SFFLX1,SRC1,RESIDUAL1/])
    delete([/COLQR2,LHFLX2,SHFLX2,SFFLX2,SRC2,RESIDUAL2/])
    delete([/lat,lon/])
    delete([/vpdHpdy1,vpdHpdy2/])
    delete([/xvals,yvals,Rvals,Xvals,Lvals,Nvals,Cvals,Svals/])
    
end do
;===================================================================================
;===================================================================================
    pres = True
    pres@gsnFrame                           = False
    ;pres@gsnPanelYWhiteSpacePercent        = 5.
    pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f","g","h","i","j","k","l","m","n"/)
    pres@gsnPanelFigureStringsFontHeightF   = 0.01
    pres@amJust                             = "TopLeft"
  
  layout = (/1,num_v/)
  
  gsn_panel(wks,plot,layout,pres)

      legend = create "Legend" legendClass wks 
    "lgAutoManage"                   : False
    ;"vpXF"                     : 0.12
    ;"vpYF"                     : 0.72
    ;"vpXF"                     : 0.09
    ;"vpYF"                     : 0.55
    "vpXF"                     : 0.25
    "vpYF"                     : 0.75
    "vpWidthF"                 : 0.1;0.11   
    "vpHeightF"                : 0.08;0.08   
    "lgPerimOn"                : True   
    "lgPerimFill"              : "SolidFill"
    "lgPerimFillColor"         : "white"
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.02   
    ;"lgItemType"                  : "MarkLines"
    ;"lgMarkerIndexes"             : (/m1,m1,m1,m2,m2,m2/)
    "lgDashIndexes"            : ispan(0,num_m-1,1)*0
    "lgLineThicknessF"         : 3.
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


; Plots a hovmoller of Column MSE budget term drift as a function of Lead time
; similar to v1, but v1 only plots colmn MSE
; v2 was designed to plot the MSE Source terms
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin
	member = (/"ERAi","90","09","13"/)
	case_name = (/"ERAi","SP-CAM","ZM_1.0","ZM_0.2"/)
	
	fig_type = "png"
	
    plat1 = -15.
  	plat2 =  15.
	
	fig_file = "~/DYNAMO/Hindcast/drift.budget.hov.v3"
;===================================================================================
;===================================================================================
  idir = "~/DYNAMO/Hindcast/"
  num_m = dimsizes(member)	
  num_v = 3
  num_t = 10*4 

	lat1 = -15.
	lat2 =  15.
	lon1 =   0.
	lon2 = 360.
	
	olat1 = lat1
	olat2 = lat2
	
	opt  = True
  	opt@lat1 = lat1
  	opt@lat2 = lat2
  	opt@lon1 = lon1
  	opt@lon2 = lon2
  
  wks = gsn_open_wks(fig_type,fig_file)
  ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
  gsn_define_colormap(wks,"BlueWhiteOrangeRed")
  plot = new(num_m*num_v,graphic)
  	res = True
  	res@gsnDraw 					= False
  	res@gsnFrame 				= False
  	res@gsnSpreadColors 			= True
  	res@cnFillOn 				= True
  	res@cnLinesOn 				= False
  	;res@vpWidthF 				= 0.3
  	res@vpHeightF				= 0.4
  	res@tmXTOn					= False
  	res@tmYROn					= False
  	res@cnLineLabelsOn 			= False
  	;res@tmYLLabelAngleF 		= 45.
  	res@tiYAxisString			= "Lead Time [days]"
  	;res@trYMaxF 					= num_t/4
  	res@trYMinF 					= 0.
  	res@trYReverse				= True
  	res@lbLabelBarOn 			= True
  	res@cnLevelSelectionMode		= "ExplicitLevels"
  	res@cnInfoLabelOn			= False
  	
  	FontHeight = 0.03
  	res@gsnRightStringFontHeightF	= FontHeight
  	res@gsnLeftStringFontHeightF		= FontHeight
  	res@tiYAxisFontHeightF			= FontHeight
  	res@tmXBLabelFontHeightF			= FontHeight
  	res@tmYLLabelFontHeightF			= FontHeight
  	res@lbLabelFontHeightF			= FontHeight
  	res@tmXBMajorOutwardLengthF		= 0.0
  	res@tmXBMinorOutwardLengthF		= 0.0
  	res@tmYLMajorOutwardLengthF		= 0.0
  	res@tmYLMinorOutwardLengthF		= 0.0
  	res@tmXBLabelAngleF				= -30.
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+(/"_00-04","_05-09"/)
  ;---------------------------------------------------
  ;---------------------------------------------------
    ifile   = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")    

    COLQR1 = infile1->COLQR(:,{lat1:lat2},{lon1:lon2})
    LHFLX1 = infile1->LHFLX(:,{lat1:lat2},{lon1:lon2})
    SHFLX1 = infile1->SHFLX(:,{lat1:lat2},{lon1:lon2})
    
    COLQR2 = infile2->COLQR(:,{lat1:lat2},{lon1:lon2})
    LHFLX2 = infile2->LHFLX(:,{lat1:lat2},{lon1:lon2})
    SHFLX2 = infile2->SHFLX(:,{lat1:lat2},{lon1:lon2})
  
  	lat = infile1->lat
  	lon = infile1->lon
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)
    xdim = (/num_t,nlat,nlon/)
    D1 = new(xdim,float)
    D2 = new(xdim,float)
    D3 = new(xdim,float)
  
    dstride = 5*4
    do d = 0,num_t/2-1
      d1 = d
      d2 = d+num_t/2
      D1(d1,:,:) = dim_avg_n(COLQR1(d::dstride,:,:),0)
      D1(d2,:,:) = dim_avg_n(COLQR2(d::dstride,:,:),0)
      
      D2(d1,:,:) = dim_avg_n(LHFLX1(d::dstride,:,:),0)
      D2(d2,:,:) = dim_avg_n(LHFLX2(d::dstride,:,:),0)
      
      D3(d1,:,:) = dim_avg_n(SHFLX1(d::dstride,:,:),0)
      D3(d2,:,:) = dim_avg_n(SHFLX2(d::dstride,:,:),0)
    end do
    
    D1!0 = "time"
    D1!1 = "lat"
    D1!2 = "lon"
    D1&lat = lat
    D1&lon = lon
    copy_VarCoords(D1,D2)
    copy_VarCoords(D1,D3)
    
    aD1 = avg4to1(dim_avg_n_Wrap(D1(:,{plat1:plat2},:),1))
    aD2 = avg4to1(dim_avg_n_Wrap(D2(:,{plat1:plat2},:),1))
    aD3 = avg4to1(dim_avg_n_Wrap(D3(:,{plat1:plat2},:),1))
    
    aD10 = aD1(0,:)
    aD20 = aD2(0,:)
    aD30 = aD3(0,:)
    ;do t = 0,dimsizes(aD1(:,0))-1
    ;  aD1(t,:) = (/ aD1(t,:) - aD10 /)
    ;  aD2(t,:) = (/ aD2(t,:) - aD20 /)
    ;  aD3(t,:) = (/ aD3(t,:) - aD30 /)
    ;end do
    
    ltime = (tofloat(ispan(0,9,1)))
    ltime@units = "lead time [days]"
    
    aD1&time = ltime 
    
    		tres = res
  		tres@gsnLeftString  			= case_name(m)
  		tres@cnLevelSelectionMode	= "ExplicitLevels"
  		;tres@cnLevels				= fspan(-50.,50.,11)
  		
  		tres1 = tres
  		tres2 = tres
  		tres3 = tres
  		tres1@gsnRightString 			= "<QR>"
  		tres2@gsnRightString 			= "LHF"
  		tres3@gsnRightString 			= "SHF"
  		
  		tres1@cnLevels				= fspan(-160.,-60.,11)
  		tres2@cnLevels				= fspan(  60.,160.,11)
  		tres3@cnLevels				= fspan(5.,50.,11)
  	
    plot(m+num_m*0) = gsn_csm_contour(wks,aD1,tres1)
    plot(m+num_m*1) = gsn_csm_contour(wks,aD2,tres2)
    plot(m+num_m*2) = gsn_csm_contour(wks,aD3,tres3)
    
    delete([/tres,tres1,tres2,tres3/])
    delete([/D1,D2,D3,aD1,aD2,aD3,aD10,aD20,aD30/])
    delete([/COLQR1,LHFLX1,SHFLX1/])
    delete([/COLQR2,LHFLX2,SHFLX2/])
    delete([/lat,lon/])
end do
;===================================================================================
;===================================================================================
  	pres = True
    	;pres@txString = case_stub
    	pres@gsnPanelBottom 						= 0.06
    	pres@gsnPanelYWhiteSpacePercent			= 5.
    	pres@gsnPanelFigureStrings				= (/"a)","b)","c)","d)","e)","f)","g)","h)","i)"/)
    	pres@gsnPanelFigureStringsFontHeightF	= 0.015
    	pres@amJust								= "TopLeft"
  
  layout = (/num_v,num_m/)
  
  gsn_panel(wks,plot,layout,pres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


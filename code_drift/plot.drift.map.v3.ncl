; Plots a map of the drift pattern of a variable
; drift is calculated relative to the obs (ERAi or ECMWF)
; v2 shows...
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    member    = (/"EC","90","09"/)
    case_name = (/"ECMWF","SP-CAM","CAM5"/)

    member    = (/"ERAi","90","09"/)
    case_name = (/"ERAi","SP-CAM","CAM5"/)

    if False then
        var   = (/"KE"/)+""
        lev   = (/700/)
        vname = "700hPa Kinetic Energy Drift"
    else
        var   = (/"FLUT"/)+""
        lev   = (/700/)
        vname = "OLR"
    end if

    cvar  = "CWV"
    
    
    
    addvc = False
    debug = False
        
    fig_type = "png"
    fig_file = "~/Research/DYNAMO/Hindcast/figs_drift/drift.map.v3."+var(0) 
;===================================================================================
;===================================================================================
    ;idir = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/repacked/"
    idir = "~/Data/DYNAMO/Hindcast/repacked/"
    num_m = dimsizes(member)    
    num_v = dimsizes(var)
    num_t = 10;*4 
    
    dof = 16.*4.

    ndayrun = 5

    lat1 = -30.
    lat2 =  30.
    lon1 =   0.
    lon2 = 180.

    if debug then
        lat1 = -10.
        lat2 =  10.
        lon1 =  60.
        lon2 =  90.
    end if
    
    plat1 = lat1
    plat2 = lat2
    
    olat1 = lat1
    olat2 = lat2
    
    opt  = True
    opt@lat1 = lat1
    opt@lat2 = lat2
    opt@lon1 = lon1
    opt@lon2 = lon2
    
    fig_type@wkWidth  = 2048
    fig_type@wkHeight = 2048
    wks = gsn_open_wks(fig_type,fig_file)
    ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
    gsn_define_colormap(wks,"BlueWhiteOrangeRed")
    plot = new((num_m-1)*num_v,graphic)
        res = True
        res@gsnDraw                     = False
        res@gsnFrame                    = False
        res@gsnSpreadColors             = True
        res@cnFillOn                    = True
        res@cnLinesOn                   = False
        res@cnInfoLabelOn               = False
        res@cnLineLabelsOn              = False
        res@tmXTOn                      = False
        res@tmYROn                      = False
        res@lbLabelBarOn                = True
        cres = res
        cres@cnFillOn               = False
        cres@cnLinesOn              = True
        cres@cnLineLabelsOn         = False
        cres@cnLineThicknessF       = 2.
        cres@cnLevelSelectionMode   = "ExplicitLevels"

        res@mpMinLatF                   = plat1
        res@mpMaxLatF                   = plat2
        res@mpMinLonF                   = lon1
        res@mpMaxLonF                   = lon2
        ;res@mpCenterLonF               = 150.
        res@gsnAddCyclic                = False
        
        FontHeight = 0.015
        res@gsnRightStringFontHeightF   = 0.015
        res@gsnLeftStringFontHeightF    = 0.015
        res@tiYAxisFontHeightF          = 0.015
        res@tmXBLabelFontHeightF        = 0.01
        res@tmYLLabelFontHeightF        = 0.01
        res@lbLabelFontHeightF          = 0.01
        res@tmXBMajorOutwardLengthF     = 0.0
        res@tmXBMinorOutwardLengthF     = 0.0
        res@tmYLMajorOutwardLengthF     = 0.0
        res@tmYLMinorOutwardLengthF     = 0.0
        res@lbTopMarginF                = 0.05 + 0.04
        res@lbBottomMarginF             = 0.05 - 0.04
        
        vres = True
        vres@gsnDraw                    = False
        vres@gsnFrame                   = False
        vres@vcRefAnnoOn                = False
        vres@vcRefMagnitudeF            = 5.                ; define vector ref mag
        vres@vcRefLengthF               = 0.045             ; define length of vec ref
        vres@vcRefAnnoOrthogonalPosF    = -.5               ; move ref vector
        vres@vcRefAnnoArrowLineColor    = "black"           ; change ref vector color
        vres@vcRefAnnoArrowUseVecColor  = False             ; don't use vec color for ref
        vres@vcGlyphStyle               = "CurlyVector"     ; turn on curley vectors
        vres@vcLineArrowColor           = "black"           ; change vector color
        vres@vcLineArrowThicknessF      = 1.0               ; change vector thickness
        vres@vcVectorDrawOrder          = "PostDraw"        ; draw vectors last
        vres@vcMinMagnitudeF            = 1.
        vres@gsnLeftString              = ""
        vres@gsnRightString             = ""
        vres@gsnCenterString            = ""
        vres@vcMinDistanceF             = 0.017
;===================================================================================
;===================================================================================
do v = 0,num_v-1
do m = 0,num_m-1
    tmember = member(m)
    oname = "DYNAMO_"+tmember+(/"_00-04","_05-09"/)
    ;---------------------------------------------------
    ;---------------------------------------------------
    ifile   = idir+oname+"/"+oname+"."+cvar(v)+".nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    C1 = infile1->$cvar(v)$(:,{lat1:lat2},{lon1:lon2})
    C2 = infile2->$cvar(v)$(:,{lat1:lat2},{lon1:lon2})

    ifile   = idir+oname+"/"+oname+".U.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    u1 = infile1->U(:,{lev(v)},{lat1:lat2},{lon1:lon2})
    u2 = infile2->U(:,{lev(v)},{lat1:lat2},{lon1:lon2})

    ifile   = idir+oname+"/"+oname+".V.nc"
    infile1 = addfile(ifile(0),"R")
    infile2 = addfile(ifile(1),"R")
    v1 = infile1->V(:,{lev(v)},{lat1:lat2},{lon1:lon2})
    v2 = infile2->V(:,{lev(v)},{lat1:lat2},{lon1:lon2})
    
    lat  = infile1->lat({lat1:lat2})
    lon  = infile1->lon({lon1:lon2})
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)
    
    if any(var(v).eq.(/"KE","EN","PKE","PEN"/)) then 

        if var(v).eq."KE" then
            iV1 = ( u1^2. + v1^2. )/2.
            iV2 = ( u2^2. + v2^2. )/2.
        end if

        if var(v).eq."EN" then
            ilat = infile1->lat({lat1:lat2})
            ilon = infile1->lon({lon1:lon2})

            toname = "DYNAMO_90"+(/"_00-04","_05-09"/)
            ifile  = idir+toname+"/"+toname+".U.nc"
            infile = addfile(ifile(0),"R")
            tlat   = infile->lat({lat1:lat2})
            tlon   = infile->lon({lon1:lon2})
            delete([/lat,lon/])
            lat = tlat
            lon = tlon

            tu1 = linint2(ilon,ilat,u1,True,tlon,tlat,1)
            tu2 = linint2(ilon,ilat,u2,True,tlon,tlat,1)
            tv1 = linint2(ilon,ilat,v1,True,tlon,tlat,1)
            tv2 = linint2(ilon,ilat,v2,True,tlon,tlat,1)

            tC1 = linint2(ilon,ilat,C1,True,tlon,tlat,1)
            tC2 = linint2(ilon,ilat,C2,True,tlon,tlat,1)
            tu1 = linint2(ilon,ilat,u1,True,tlon,tlat,1)
            tu2 = linint2(ilon,ilat,u2,True,tlon,tlat,1)
            tv1 = linint2(ilon,ilat,v1,True,tlon,tlat,1)
            tv2 = linint2(ilon,ilat,v2,True,tlon,tlat,1)
            delete([/C1,C2,u1,u2,v1,v2/])
            C1 = tC1
            C2 = tC2
            u1 = tu1
            u2 = tu2
            v1 = tv1
            v2 = tv2
            
            iV1 = uv2vr_cfd(tu1,tv1,tlat,tlon,1) ^2. /2.
            iV2 = uv2vr_cfd(tu2,tv2,tlat,tlon,1) ^2. /2.
            delete([/ilat,ilon/])
            delete([/tC1,tC2,tu1,tu2,tv1,tv2,tlat,tlon/])
        end if
        
        if var(v).eq."PKE" then
            eu1 = u1 - runave_n_Wrap(u1,ndayrun,0,0)
            eu2 = u2 - runave_n_Wrap(u2,ndayrun,0,0)
            ev1 = v1 - runave_n_Wrap(v1,ndayrun,0,0)
            ev2 = v2 - runave_n_Wrap(v2,ndayrun,0,0)
            iV1 = ( eu1^2. + ev1^2. )/2.
            iV2 = ( eu2^2. + ev2^2. )/2.
            delete([/eu1,eu2,ev1,ev2/])
        end if
        
        if var(v).eq."PEN" then
            eu1 = u1 - runave_n_Wrap(u1,ndayrun,0,0)
            eu2 = u2 - runave_n_Wrap(u2,ndayrun,0,0)
            ev1 = v1 - runave_n_Wrap(v1,ndayrun,0,0)
            ev2 = v2 - runave_n_Wrap(v2,ndayrun,0,0)
            ilat = infile1->lat({lat1:lat2})
            ilon = infile1->lon({lon1:lon2})
            iV1 = uv2vr_cfd(eu1,ev1,ilat,ilon,1) ^2. /2.
            iV2 = uv2vr_cfd(eu2,ev2,ilat,ilon,1) ^2. /2.
            delete([/ilat,ilon/])
            delete([/eu1,eu2,ev1,ev2/])
        end if
        
    end if
    ;---------------------------------------------------
    ;---------------------------------------------------
    if (var(v).eq."FLUT").and.(member(m).eq."ERAi") then tmember   = "NOAA" end if
    if (var(v).eq."FLUT").and.(member(m).eq."ERAi") then case_name(v) = "NOAA" end if
    if .not.isvar("iV1") then 
        ifile   = idir+oname+"/"+oname+"."+var(v)+".nc"
        infile1 = addfile(ifile(0),"R")
        infile2 = addfile(ifile(1),"R")    
        ndim    = dimsizes(dimsizes(infile1->$var(v)$))
    
        if ndim.eq.4 then
            iV1 = infile1->$var(v)$(:,{lev(v)},{lat1:lat2},{lon1:lon2})
            iV2 = infile2->$var(v)$(:,{lev(v)},{lat1:lat2},{lon1:lon2})
        else
            iV1 = infile1->$var(v)$(:,{lat1:lat2},{lon1:lon2})
            iV2 = infile2->$var(v)$(:,{lat1:lat2},{lon1:lon2})
        end if
    end if
    ;---------------------------------------------------
    ;---------------------------------------------------
    iV1 := block_avg( iV1 ,4)
    iV2 := block_avg( iV2 ,4)
    C1  := block_avg(  C1 ,4)
    C2  := block_avg(  C2 ,4)
    u1  := block_avg(  u1 ,4)
    u2  := block_avg(  u2 ,4)
    v1  := block_avg(  v1 ,4)
    v2  := block_avg(  v2 ,4)

    ;lat = infile1->lat({lat1:lat2})
    ;lon = infile1->lon({lon1:lon2})
    nlat = dimsizes(lat)
    nlon = dimsizes(lon)
    xdim = (/num_t,nlat,nlon/)
    ;D = new(xdim,float)
    ;S = new(xdim,float)
    ;C = new(xdim,float)
    U = new(xdim,float)
    V = new(xdim,float)

    D = new((/num_t,dimsizes(iV1&lat),dimsizes(iV1&lon)/),float)
    S = new((/num_t,dimsizes(iV1&lat),dimsizes(iV1&lon)/),float)
    C = new((/num_t,dimsizes(C1&lat),dimsizes(C1&lon)/),float)
    
    dstride = 5;*4
    do d = 0,num_t/2-1
        d1 = d
        d2 = d+num_t/2
        D(d1,:,:) = dim_avg_n(iV1(d::dstride,:,:),0)
        D(d2,:,:) = dim_avg_n(iV2(d::dstride,:,:),0)

        S(d1,:,:) = dim_stddev_n(iV1(d::dstride,:,:),0)
        S(d2,:,:) = dim_stddev_n(iV2(d::dstride,:,:),0)

        C(d1,:,:) = dim_avg_n(C1(d::dstride,:,:),0)
        C(d2,:,:) = dim_avg_n(C2(d::dstride,:,:),0)
        
        U(d1,:,:) = dim_avg_n(u1(d::dstride,:,:),0)
        U(d2,:,:) = dim_avg_n(u2(d::dstride,:,:),0)
        
        V(d1,:,:) = dim_avg_n(v1(d::dstride,:,:),0)
        V(d2,:,:) = dim_avg_n(v2(d::dstride,:,:),0)
    end do
    delete([/u1,u2,v1,v2/])

    if debug then
        printline()
        printMAM(D)
        printline()
    end if
    
    if (member(m).eq."ERAi").and.(var(v).eq."FLUT") then D = (/ D*-1. /) end if
    ;---------------------------------------------------
    ;---------------------------------------------------
    D!0 = "time"
    D!1 = "lat"
    D!2 = "lon"
    D&lat = iV1&lat
    D&lon = iV1&lon
    
    copy_VarCoords(D,U)
    copy_VarCoords(D,V)
    copy_VarCoords(D,C)
    copy_VarCoords(D,S)
    
    aD = block_avg(D,1)
    aC = block_avg(C,1)
    aU = block_avg(U,1)
    aV = block_avg(V,1)
    
    if m.eq.0 then 
        aD0  = conform(aD,dim_avg_n_Wrap(aD,0),(/1,2/)) 
        aC0  = conform(aC,dim_avg_n_Wrap(aC,0),(/1,2/)) 
        aU0  = conform(aU,dim_avg_n_Wrap(aU,0),(/1,2/)) 
        aV0  = conform(aV,dim_avg_n_Wrap(aV,0),(/1,2/)) 
        lon0 = aD&lon
        lat0 = aD&lat
    end if
    
    aD = (/ aD - linint2(lon0,lat0,aD0,True,aD&lon,aD&lat,1) /)
    ;aC = (/ aC - linint2(lon0,lat0,aC0,True,lon,lat,1) /)
    ;aU = (/ aU - linint2(lon0,lat0,aU0,True,aD&lon,aD&lat,1) /)
    ;aV = (/ aV - linint2(lon0,lat0,aV0,True,aD&lon,aD&lat,1) /)
        
    ltime = (tofloat(ispan(0,9,1)))
    ltime@units = "lead time [days]"
    
    aD&time = ltime
    ;aU&time = ltime 
    ;aV&time = ltime  
    ;---------------------------------------------------
    ; significance test
    ;---------------------------------------------------
    SE = S / sqrt(dof-1)

    SE = where(SE.ne.0,SE,SE@_FillValue)

    ;tcrit = 2.131   ; 0.05 dof = 16
    ;tcrit = 2.04    ; 0.05 dof = 32
    tcrit = 1.96    ; 0.05 dof = inf

    tstat = aD / SE
    tstat@long_name = "t-statistic "+member(m)
    
    sig = where(abs(tstat).gt.tcrit,1.,0.)
    copy_VarCoords(aD,sig)

        printline()
        ;printMAM(tstat)
        printMAM( tcrit * SE  )
        printMAM( abs(aD) )
        n1 = num(sig.eq.1)
        n2 = product(dimsizes(sig))
        print((tofloat(n1)/tofloat(n2)*100.)+"%     ("+n1+"/"+n2+")")
        printline()
    ;---------------------------------------------------
    ; Create plot
    ;---------------------------------------------------

        tres = res
        tres@gsnLeftString          = case_name(m)
        tres@gsnRightString         = vname(v)
        tres@cnLevelSelectionMode   = "ExplicitLevels"

        ospc = 8
        if var(v).eq."FLUT"                  then tres@cnLevels = ispan( -60, 60,20) end if
        if var(v).eq."V"                     then tres@cnLevels = fspan(  -5,  5,21) end if
        if var(v).eq."U"                     then tres@cnLevels = ispan( -10, 10,1) end if
        if var(v).eq."KE".and.lev(v).eq.700. then tres@cnLevels = ispan( -30, 30, 5) end if
        if var(v).eq."KE".and.lev(v).eq.500. then tres@cnLevels = ispan(-100,100,10) end if
        if var(v).eq."KE".and.lev(v).eq.200. then tres@cnLevels = ispan(-200,200,20) end if

        if var(v).eq."EN".and.lev(v).eq.700. then tres@cnLevels = ispan(-8,8,2)*1e-10 end if
        if var(v).eq."EN".and.lev(v).eq.500. then tres@cnLevels = ispan(-50,50,5)*1e-11 end if
        if var(v).eq."EN".and.lev(v).eq.200. then tres@cnLevels = ispan(-50,50,5)*1e-11 end if

        if var(v).eq."PKE".and.lev(v).eq.700. then tres@cnLevels = ispan( -20, 20,4)/1e1 end if
        if var(v).eq."PEN".and.lev(v).eq.700. then tres@cnLevels = ispan(-6,6,2)*1e-11 end if

    p = m-1+(num_m-1)*v
    
    if m.ne.0 then 
        plot(p) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(aD(8:,:,:),0),tres) 

        cres@cnLineThicknessF       = 3.
        cres@cnLineDashPattern      = 0
        cres@cnLevels               = ispan(45,75,5)
        overlay( plot(p) , gsn_csm_contour(wks,dim_avg_n_Wrap(aC,0),cres) )
        delete(cres@cnLevels)

        cres@cnLineDashPattern      = 1
        cres@cnLevels               = ispan(5,35,5)
        overlay( plot(p) , gsn_csm_contour(wks,dim_avg_n_Wrap(aC,0),cres) )
        delete(cres@cnLevels)

        cres@cnLineThicknessF       = 6.
        cres@cnLineDashPattern      = 0
        cres@cnLevels               = (/45/)
        overlay( plot(p) , gsn_csm_contour(wks,dim_avg_n_Wrap(aC,0),cres) )
        delete(cres@cnLevels)

            sres = setres_contour()
            sres@lbLabelBarOn           = False
            sres@cnLineLabelsOn         = False
            sres@cnInfoLabelOn          = False
            sres@cnFillPattern          = 17
            sres@cnFillScaleF           = 0.8
            sres@cnFillDotSizeF         = 0.0015
        if .not.all(sig.eq.0.) then
            ;overlay(plot(p),gsn_csm_contour(wks,sig(8,:,:),sres))
            overlay(plot(p),gsn_csm_contour(wks,dim_avg_n_Wrap(sig(8:,:,:),0),sres))
            opt = True
            opt@gsnShadeFillType = "pattern"        ; color is the default
            opt@gsnShadeLow  = -1
            opt@gsnShadeHigh = sres@cnFillPattern
            plot(p) = gsn_contour_shade(plot(p),0.8,0.8,opt)
        end if

    end if
    
    if addvc.and.(m.ne.0) then overlay(plot(p),gsn_csm_vector(wks,dim_avg_n_Wrap(aU(8:,:,:),0),dim_avg_n_Wrap(aV(8:,:,:),0),vres)) end if
    ;---------------------------------------------------
    ;---------------------------------------------------
        delete([/tres,lat,lon/])
        delete([/D,aD,C,aC/])
        delete([/U,V,aU,aV/])
        delete([/S,SE,tstat,sig/])
        delete([/iV1,iV2,C1,C2/])
end do
end do
;if debug then exit end if
;===================================================================================
;===================================================================================
        pres = True
            ;pres@txString = case_stub
            ;pres@gsnPanelBottom                    = 0.06
            ;pres@gsnPanelYWhiteSpacePercent        = 5.
            pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f","g","h","i"/)
            pres@gsnPanelFigureStringsFontHeightF   = 0.015
            pres@amJust                             = "TopLeft"
            
    
    layout = (/num_m-1,1/)
    ;layout = (/num_v,num_m-1/)
    
    gsn_panel(wks,plot,layout,pres)
    
        print("")
        print(" "+fig_file+"."+fig_type)
        print("")
        
        if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;===================================================================================
;===================================================================================
end


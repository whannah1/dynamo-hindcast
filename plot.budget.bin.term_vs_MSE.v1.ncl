load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin	

	member = (/"ERAi","12","13","90","91"/)
	case_name = (/"ERAi","ZM_2.0","ZM_0.2","SP4k","SP2k"/)
	
	;member = (/"ERAi","12","13"/)
	;case_name = (/"ERAi","ZM_2.0","ZM_0.2"/)
	
	rd1 = (/00/)
	;rd1 = (/00/)
	
	clr = (/"black","red","green","purple","pink"/)
	dsh = (/0,0,0,0,0,0/)
	
	fig_type = "png"

		lat1 = -10.
		lat2 =  10.
		lon1 =  60.
		lon2 =  90.
		
		;lon1 =   0.
		;lon2 = 360.
	
	nsmooth = 3
	
	anom = True
	
	xvar = "MSEvi"
	if anom then
	  if xvar.eq."MSEvi" then
       bin_min = -5
       bin_max =  5
       bin_spc = 1
      end if
      if xvar.eq."PRECT" then
       bin_min = -400
       bin_max =  400
       bin_spc = 50
      end if
	else
      bin_min = 330
      bin_max = 340
      bin_spc = 1
    end if
;===================================================================================
;===================================================================================
  idir = "/Users/whannah/DYNAMO/Hindcast/"
  odir = "/Users/whannah/DYNAMO/Hindcast/figs_budget/"
  opt  = True
  opt@lat1 = lat1
  opt@lat2 = lat2
  opt@lon1 = lon1
  opt@lon2 = lon2 
;===================================================================================
;===================================================================================
num_r = dimsizes(rd1)
num_m = dimsizes(member)
do r = 0,num_r-1

  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  
  if anom then
    fig_file = odir+"budget.bin.term_vs_MSE.v1."+xvar+"_anom."+rds+"-"+rde
  else
    fig_file = odir+"budget.bin.term_vs_MSE.v1."+rds+"-"+rde
  end if
  
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"ncl_default")
  plot = new(6,graphic)
  	res = True
  	res@gsnDraw 			= False
  	res@gsnFrame	 		= False
  	;res@vpWidthF 		= 0.4
  	lres = res
  	lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 1

  	res@gsnLeftStringFontHeightF 	= 0.028
  	res@gsnRightStringFontHeightF 	= 0.028
  	res@xyLineColors 	= clr
  	res@xyDashPatterns 	= dsh
  	res@xyLineThicknessF	= 3.
  	
  	if xvar.eq."MSEvi" then res@tiXAxisString = "MSE [K]" end if
  	if xvar.eq."PRECT" then res@tiXAxisString = "Precip [W m~S~-2~N~]" end if
  	res@tiYAxisString = "W m~S~-2~N~"
;===================================================================================
;===================================================================================
  	xbin = ispan(bin_min,bin_max,bin_spc)
  	num_bin = dimsizes(xbin)

  	nvar = 5
 	  	
  	bin_str = new(nvar,string)
  	  	
  	bdim = (/nvar,num_m,num_bin/)
  	bin_val = new(bdim,float)
  	bin_std = new(bdim,float)
  	bin_cnt = new(bdim,float)
;===================================================================================
;===================================================================================
do m = 0,num_m-1
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
  t1    = 0
  ifile = idir+"data/"+oname+"/"+oname+".budget.MSE.nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  MSEvi = infile->MSEvi(t1:,{lat1:lat2},{lon1:lon2})
  COLDP = infile->COLDP(t1:,{lat1:lat2},{lon1:lon2})
  MSEDT = infile->MSEDT(t1:,{lat1:lat2},{lon1:lon2})
  VdelH = infile->VdelH(t1:,{lat1:lat2},{lon1:lon2})
  WdHdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
  COLQR = infile->COLQR(t1:,{lat1:lat2},{lon1:lon2})
  LHFLX = infile->LHFLX(t1:,{lat1:lat2},{lon1:lon2})
  SHFLX = infile->SHFLX(t1:,{lat1:lat2},{lon1:lon2})
  MSEvi = (/MSEvi/COLDP/cpd/)
  ifile = idir+"data/"+oname+"/"+oname+".budget.DSE.nc"
  print(""+ifile)
  infile = addfile(ifile,"R")
  WdSdp = infile->WdHdp(t1:,{lat1:lat2},{lon1:lon2})
  PRECT = infile->PRECT(t1:,{lat1:lat2},{lon1:lon2}) 
  if member(m).ne."ERAi" then PRECT = (/PRECT*1000./) end if
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------	
  if anom MSEvi = dim_rmvmean_n(MSEvi,0) end if
  ;if anom PRECT = dim_rmvmean_n(PRECT,0) end if
  if anom PRECT = dim_rmvmed_n(PRECT,0) end if
  	
  	MSEDT = where(ismissing(MSEDT),VdelH@_FillValue,MSEDT)
  	PRECT = where(ismissing(PRECT),VdelH@_FillValue,PRECT)
    MSEDT@_FillValue = VdelH@_FillValue
    PRECT@_FillValue = VdelH@_FillValue
	
	SFC_FLX = LHFLX
    SFC_FLX = (/SFC_FLX+SHFLX/)
    F = COLQR
    F = (/F+SFC_FLX/)
    
    F = MSEDT+VdelH+WdHdp
    
    VdelH = -VdelH
    WdHdp = -WdHdp
    
    MSEvi@long_name   = "Column Avg. MSE Anomaly"
    MSEDT@long_name   = "Total MSE Tendency"
    COLQR@long_name   = "QR"
    SFC_FLX@long_name = "Surface Fluxes"
    VdelH@long_name   = "-V*del[h]"
    WdHdp@long_name   = "-W*dh/dp"
    F@long_name       = "MSE Source"
    WdSdp@long_name   = "-W*ds/dp"
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
  if xvar.eq."MSEvi" then Vx = MSEvi end if
  if xvar.eq."PRECT" then Vx = PRECT end if
  do v = 0,nvar-1
    if v.eq.0 then Vy = MSEDT  	end if
    if v.eq.1 then Vy = VdelH  	end if
    if v.eq.2 then Vy = WdHdp  	end if
    ;if v.eq.3 then Vy = F     	end if
    ;if v.eq.4 then Vy = PRECT 	end if
    if v.eq.3 then Vy = SFC_FLX end if
    if v.eq.4 then Vy = COLQR 	end if
    ;if v.eq.4 then Vy = WdSdp 	end if
    do b = 0,num_bin-1
      bin_bot = bin_min + bin_spc*(b  )
      bin_top = bin_min + bin_spc*(b+1)
      condition = (Vx.ge.bin_bot).and.(Vx.lt.bin_top)
      tmp = where(condition,Vy,Vy@_FillValue)
      alt = where(condition.and.(MSEDT.lt.0.),Vy,Vy@_FillValue)
      if all(ismissing(tmp)) then
        print("!!! ALL TMP VALUES ARE MISSING !!!")
        print("  bin_bot: "+bin_bot)
        print("  bin_top: "+bin_top)
        print("")
      else
        bin_val(v,m,b) =    avg(tmp)
        ;bin_alt(v,m,b) =    avg(alt)
        bin_std(v,m,b) = stddev(tmp)
        bin_cnt(v,m,b) = num(condition)
      end if
      delete([/tmp,alt,condition/])
    end do
    bin_str(v) = Vy@long_name
  end do
  delete([/Vx,Vy/])
    print("		bin: "+xbin+"	"+bin_cnt(0,m,:))
    print("")
  ;----------------------------------------------------------------------
  ;----------------------------------------------------------------------
    delete([/MSEvi,COLDP,MSEDT,VdelH,WdHdp,COLQR,LHFLX,SHFLX,SFC_FLX,F,PRECT,WdSdp/])	
end do

	;bin_val = bin_alt
	
;===================================================================================
; Create plot
;===================================================================================
	res@tmXBMode = "Explicit"
	res@tmXBValues = xbin(::1)
  	res@tmXBLabels = xbin(::1)
  	res@tmXBLabelAngleF = -45.
  	res@trXMinF = min(xbin)
  	res@trXMaxF = max(xbin)
  	res@gsnRightString 	= ""
  	
  	res@gsnLeftString 	= bin_str(0)
  	res0 = res
  	
  	res0@trYMaxF = 30.
  plot(0) = gsn_csm_xy(wks,xbin,bin_val(0,:,:),res0)
  
  	res@gsnLeftString 	= bin_str(1)
  	res0 = res
  	res0@trYMaxF = 0.
  plot(1) = gsn_csm_xy(wks,xbin,bin_val(1,:,:),res0)
  
  	res@gsnLeftString 	= bin_str(2)
  plot(2) = gsn_csm_xy(wks,xbin,bin_val(2,:,:),res)
  
  	res@gsnLeftString 	= bin_str(3)
  plot(3) = gsn_csm_xy(wks,xbin,bin_val(3,:,:),res)
  
  	;res@gsnLeftString 	= "Precipitation"
  	res@gsnLeftString 	= bin_str(4)
  plot(4) = gsn_csm_xy(wks,xbin,bin_val(4,:,:),res)
  
  	
  
  
  	count = bin_cnt(0,:,:)
  	do m = 0,num_m-1 
  	  count(m,:) = bin_cnt(0,m,:)/sum(bin_cnt(0,m,:)) *100.
  	end do
  	res@gsnLeftString = "fraction of occurrence"
  	res@tiYAxisString = "%"
  plot(5) = gsn_csm_xy(wks,xbin,count,res)

  
  do p = 0,dimsizes(plot)-1
    lres@xyLineColor 		= "black"
  	lres@xyLineThicknessF 	= 1.
  	lres@xyDashPattern		= 1
    xx = (/-1000.,1000./)
    yy = (/0.,0./)
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
    xx = (/0.,0./)
    yy = (/-1000.,1000./)    
    overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
  end do
;===================================================================================
;===================================================================================

  	pres = True
  	pres@gsnFrame = False
    	pres@gsnPanelFigureStrings				= (/ "a)","b)","c)","d)","e)","f)","g)","h)", \
    	                                			     "i)","j)","k)","l)","m)","n)","o)","p)"  /) 
    	pres@amJust   							= "TopLeft"
    	pres@gsnPanelFigureStringsFontHeightF	= 0.015
    	pres@gsnPanelYWhiteSpacePercent 	= 5
    	pres@gsnPanelLabelBar 	= True

  gsn_panel(wks,plot,(/2,3/),pres)
  
  legend = create "Legend" legendClass wks 
    "vpXF"                     : 0.4
    "vpYF"                     : 0.16
    "vpWidthF"                 : 0.2   
    "vpHeightF"                : 0.1   
    "lgPerimOn"                : True   
    "lgItemCount"              : num_m
    "lgLabelStrings"           : case_name
    "lgLabelsOn"               : True     
    "lgLineLabelsOn"           : False     
    "lgLabelFontHeightF"       : 0.015    
    "lgDashIndexes"            : dsh
    "lgLineThicknessF"         : 3.8
    "lgLineColors"             : clr
    "lgMonoLineLabelFontColor" : True                  
  end create

  draw(legend)
  frame(wks)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  	
  	delete([/res,lres/])

end do
end


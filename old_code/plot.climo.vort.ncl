load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
begin
	dir = "/maloney-scratch/whannah/DYNAMO/Hindcast/"
	
	member = (/"09","13"/)
	;member = (/"09"/)
	
	rd1 = (/00/)
	
	fig_type = "x11"
	fig_file = dir+"climo.vorticity"
	
	lat1 = -20.
	lat2 =  20.
	lon1 =   0.
	lon2 = 360.
  
;====================================================================================================
;====================================================================================================
      ;opt  = True
      ;opt@lat1 = -15.
      ;opt@lat2 =  15.
      ;opt@lon1 =   0.
      ;opt@lon2 = 360.
      ;lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,70./)
      ;lat = LoadHClat(opt)
      ;lon = LoadHClon(opt)	
      ;  print("")
      ;  print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:  "+sprintf("%6.4g",max(lat)))
      ;  print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:  "+sprintf("%6.4g",max(lon)))
      ;  print("    lev = 	"+sprintf("%6.4g",max(lev))+"	:  "+sprintf("%6.4g",min(lev)))
      ;  print("")
      ;num_lat  = dimsizes(lat)
      ;num_lon  = dimsizes(lon)
      ;num_lev  = dimsizes(lev)

  t1 = (30)*4
  t2 = t1+(31+30)*4-1  
  
  num_r = dimsizes(rd1)
  num_m = dimsizes(member)
;====================================================================================================
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"ncl_default")
  plot = new(num_m,graphic)
  	res = True
  	res@gsnDraw 		= False
  	res@gsnFrame 		= False
  	res@gsnSpreadColors 	= True
  	res@cnFillOn 		= True
  	res@cnLinesOn 		= False
  	res@cnLineLabelsOn 	= False
  	res@gsnAddCyclic 	= False
  	res@mpCenterLonF = 180.
	res@mpLimitMode = "LatLon"
	res@mpMinLatF = lat1
	res@mpMaxLatF = lat2
  	res@lbLabelFontHeightF 		= 0.01
  	res@gsnLeftStringFontHeightF	= 0.01
;====================================================================================================
;====================================================================================================  
do m = 0,num_m-1
do r = 0,num_r-1  
  rds = sprinti("%0.2i",rd1(r))
  rde = sprinti("%0.2i",rd1(r)+5-1)
  oname = "DYNAMO_"+member(m)+"_"+rds+"-"+rde
  ifile = dir+"data/"+oname+"/"+oname+".vars.nc"
  infile = addfile(ifile,"R")
  
  U = infile->U(:,{500.},:,:)
  V = infile->V(:,{500.},:,:)
  
  lat = infile->lat
  lon = infile->lon
  
  Z = uv2vr_cfd(U,V,lat,lon,1)
  
  copy_VarCoords(U,Z)
  Z&lon = lon  	
  ;----------------------------------------------------------------------------
  ;----------------------------------------------------------------------------
  	res@gsnLeftString = "Vorticity"
  plot(m) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(Z,0),res)
end do	
end do
;====================================================================================================  
;====================================================================================================  
  	pres = True
  	
  gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  
    delete([/wks,plot,res/])
  
    print("")
    print("  "+fig_file+"."+fig_type)
    print("")
;====================================================================================================
;====================================================================================================
end

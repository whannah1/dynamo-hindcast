load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"

begin
	yr = 2011
	;mn = (/10,10,10,10,10,10,10,11,11,11,11,11,11/)
	;dy = (/01,06,11,16,21,26,31,05,10,15,20,25,30/)
	mn = (/10/)
	dy = (/01/)
	yrmndy = toint( yr*10000 + mn*100 + dy )
	
	member = "xx"

	case = (/"DYNAMO_"+member+"_NCEP_"/)+yrmndy
	
	lat1 = -15.
	lat2 =  15.
	lon1 =   0.
	lon2 = 360.
	
	fac  = 1		; Samples per day
	harm = 2		;
	
	recalc = True
	mkplot = True
	
	debug = False
	dbx = 90.
	dby = 0.
;===================================================================================
; Coordinates
;===================================================================================
 
  num_c = 1;dimsizes(case)
  
  num_t   = 20*fac
  time    = (tofloat(ispan(0,num_t-1,1))*(24./fac))/24.
  time@units = "days since "+yr(0)+"-"+mn(0)+"-"+dy(0)+""
  
  ifile  = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.U850.1980-2011.nc"
  infile = addfile(ifile,"r")
  olat = infile->lat
  olon = infile->lon
  lat = olat
  lon = olon
  num_lat = dimsizes(lat)
  num_lon = dimsizes(lon)
;===================================================================================
; Load historical NCEP long-term mean
;===================================================================================
  ifile = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.RMM.climo.1980-2010.nc"
  infile = addfile(ifile,"r")
  meanU850 = infile->U850({lat1:lat2},{lon1:lon2})
  meanU200 = infile->U200({lat1:lat2},{lon1:lon2})
  meanOLR  = infile->OLR ({lat1:lat2},{lon1:lon2})
;===================================================================================
; Load seasonal cycle data
;===================================================================================
      ifile = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.RMM.seasonal_cycle.1980-2010.nc"
      infile = addfile(ifile,"r")
      tHU850 = dim_sum_n_Wrap(infile->HU850(:harm,0:,{lat1:lat2},{lon1:lon2}),0)
      tHU200 = dim_sum_n_Wrap(infile->HU200(:harm,0:,{lat1:lat2},{lon1:lon2}),0)
      tHOLR  = dim_sum_n_Wrap(infile->HOLR (:harm,0:,{lat1:lat2},{lon1:lon2}),0)
;===================================================================================
; Load data used for 120-day mean
;===================================================================================
  ; To remove the 120-day mean on Oct1 we need data back to Jun3
  ; Since this is the previous 119 days
  if True then
    mt1 = 31*365 ;+ 31+28+31+30+31+2
    ;----------------------------------------
    print("  loading NCEP data for 120-day mean...(U850)")
    ifile  = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.U850.1980-2011.nc"
    infile = addfile(ifile,"r")
    itmp = infile->U(mt1*4:,:,:)
    tmp = (itmp(0::4,:,:)+itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:))/4.
    copy_VarCoords(itmp(0::4,:,:),tmp)
    ncepU850 = tmp(:,{lat1:lat2},{lon1:lon2})
      delete([/itmp,tmp/])
    ;----------------------------------------
    print("  loading NCEP data for 120-day mean...(U200)")
    ifile  = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.U200.1980-2011.nc"
    infile = addfile(ifile,"r")
    itmp = infile->U(mt1*4:,:,:)
    tmp = (itmp(0::4,:,:)+itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:))/4.
    copy_VarCoords(itmp(0::4,:,:),tmp)
    ncepU200 = tmp(:,{lat1:lat2},{lon1:lon2})
      delete([/itmp,tmp/])
    ;----------------------------------------
    print("  loading NCEP data for 120-day mean...(OLR)")
    ifile = "/data/whannah/obs/OLR/olr.daily.1980-2011.nc"
    infile = addfile(ifile,"r")
    tmp = infile->OLR(mt1:,:,:)
    ncepOLR = tmp(:,{lat1:lat2},{lon1:lon2})
    ncepOLR!0 = "timeOLR"
      delete([/tmp/])
    ;----------------------------------------
    print("  done")
    ofile = "/maloney-scratch/whannah/DYNAMO/NCEP/temp_alt_RMM_data.NCEP.nc"
    if isfilepresent(ofile) then
      system("rm "+ofile)
    end if
    outfile = addfile(ofile,"c")
    outfile->ncepU850 = ncepU850
    outfile->ncepU200 = ncepU200
    outfile->ncepOLR  = ncepOLR
  else
    ifile = "/maloney-scratch/whannah/DYNAMO/NCEP/temp_alt_RMM_data.NCEP.nc"
    infile = addfile(ifile,"r")
    ncepU850 = infile->ncepU850
    ncepU200 = infile->ncepU200
    ncepOLR  = infile->ncepOLR
  end if
  ;----------------------------------------
  ; Remove long term mean 
  ;----------------------------------------
      ncepU850 = ncepU850(:,:,:)  - (/ conform(ncepU850,meanU850,(/1,2/)) /) - tHU850
      ncepU200 = ncepU200(:,:,:)  - (/ conform(ncepU200,meanU200,(/1,2/)) /) - tHU200
      ncepOLR  = ncepOLR (:,:,:)  - (/ conform(ncepOLR ,meanOLR ,(/1,2/)) /) - tHOLR
;===================================================================================
; Load historical RMM data for normalizing (std.dev.)
;===================================================================================  
    ;data = asciiread("/maloney-scratch/whannah/DYNAMO/RMM.no_leap.txt",(/365*31,7/),"float")
    data = asciiread("/maloney-scratch/whannah/DYNAMO/RMM_DYNAMO.txt",(/31+30+31,7/),"float")    
	oRMM1 = data(:,3)
	oRMM2 = data(:,4)
	delete([/data/])
;===================================================================================
; Caclulate RMM
;===================================================================================
do c = 0,num_c-1
  ;if recalc then
    ;================================================================
    ; Load simulation data
    ;================================================================
      mt1 = 31*365 + 31+28+31+30+31+30+31+31+30
      ;----------------------------------------
      print("  loading hindcast simulations...(U850)")
      ifile  = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.U850.1980-2011.nc"
      infile = addfile(ifile,"r")
      itmp = infile->U(mt1*4:mt1*4+20*4-1,{lat1:lat2},{lon1:lon2})
      tmp = (itmp(0::4,:,:)+itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:))/4.
      copy_VarCoords(itmp(0::4,:,:),tmp)
      mU850 = tmp(:,{lat1:lat2},{lon1:lon2})
        delete([/itmp,tmp/])
      ;----------------------------------------
      print("  loading hindcast simulations...(U200)")
      ifile  = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.U200.1980-2011.nc"
      infile = addfile(ifile,"r")
      itmp = infile->U(mt1*4:mt1*4+20*4-1,{lat1:lat2},{lon1:lon2})
      tmp = (itmp(0::4,:,:)+itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:))/4.
      copy_VarCoords(itmp(0::4,:,:),tmp)
      mU200 = tmp(:,{lat1:lat2},{lon1:lon2})
        delete([/itmp,tmp/])
      ;----------------------------------------
      print("  loading hindcast simulations...(OLR)")
      ifile = "/data/whannah/obs/OLR/olr.daily.1980-2011.nc"
      infile = addfile(ifile,"r")
      tmp = infile->OLR(mt1:mt1+20-1,{lat1:lat2},{lon1:lon2})
      mOLR = tmp(:,{lat1:lat2},{lon1:lon2})
        delete([/tmp/])
      ;----------------------------------------
      print("  done.")
    ;================================================================
    ; Remove mean
    ;================================================================
      mU850 = mU850 - (/ conform(mU850,meanU850,(/1,2/)) /)
      mU200 = mU200 - (/ conform(mU200,meanU200,(/1,2/)) /)
      mOLR  = mOLR  - (/ conform(mOLR ,meanOLR ,(/1,2/)) /)
    ;================================================================
    ; Remove observed climatological seaonal cycle
    ;================================================================
      print("  Removing seasonal cycle...")
      ;----------------------------------------
      ; Load NCEP seasonal cycle climatology
      ;----------------------------------------
      ht1 = 31+28+31+30+31+30+31+31+30 +(mn(c)-10)*31 +(dy(c)-1)
      ifile = "/maloney-scratch/whannah/DYNAMO/NCEP/NCEP.RMM.seasonal_cycle.1980-2010.nc"
      infile = addfile(ifile,"r")
      HU850 = dim_sum_n_Wrap(infile->HU850(:harm,ht1:ht1+19,{lat1:lat2},{lon1:lon2}),0)
      HU200 = dim_sum_n_Wrap(infile->HU200(:harm,ht1:ht1+19,{lat1:lat2},{lon1:lon2}),0)
      HOLR  = dim_sum_n_Wrap(infile->HOLR (:harm,ht1:ht1+19,{lat1:lat2},{lon1:lon2}),0)
      ;----------------------------------------
      ; Subtract seasonal cycle from hindcast
      ;----------------------------------------
      mU850 = (/ mU850 - HU850/)
      mU200 = (/ mU200 - HU200 /)
      mOLR  = (/ mOLR  - HOLR  /)
      print("  done.")
    ;================================================================
    ; Remove 120-mean
    ;================================================================
      d1 = 31+28+31+30+31+30+31+31+30 -119 ;+(mn(c)-10)*31+dy(c)-1
      do t = 0,20*fac-1
        mU850(t,:,:) = (/ mU850(t,:,:) - ( mU850(t,:,:)+dim_sum_n(ncepU850(d1+t:d1+118,:,:),0) )/120. /)
        mU200(t,:,:) = (/ mU200(t,:,:) - ( mU200(t,:,:)+dim_sum_n(ncepU200(d1+t:d1+118,:,:),0) )/120. /)
        mOLR (t,:,:) = (/ mOLR (t,:,:) - ( mOLR (t,:,:)+dim_sum_n(ncepOLR (d1+t:d1+118,:,:),0) )/120. /)
        ;mU850(t,:,:) = (/ mU850(t,:,:) - ( dim_sum_n(ncepU850(d1+t:d1+119,:,:),0) )/120. /)
        ;mU200(t,:,:) = (/ mU200(t,:,:) - ( dim_sum_n(ncepU200(d1+t:d1+119,:,:),0) )/120. /)
        ;mOLR (t,:,:) = (/ mOLR (t,:,:) - ( dim_sum_n(ncepOLR (d1+t:d1+119,:,:),0) )/120. /)
      end do
    ;================================================================
    ; Average Across the Equator
    ;================================================================
      eU850 = dim_avg_n(mU850,1) /  1.81
      eU200 = dim_avg_n(mU200,1) /  4.81
      eOLR  = dim_avg_n(mOLR ,1) / 15.1
  ;================================================================
  ; Create Vector for projection
  ;================================================================
    V = new((/20*fac,3*num_lon/),float)
    i = 0
    V(:,i:i+num_lon-1) = eOLR
    i = num_lon
    V(:,i:i+num_lon-1) = eU850
    i = num_lon*2
    V(:,i:i+num_lon-1) = eU200
  ;================================================================
  ; Project onto EOFs
  ;================================================================
  ifile = "/maloney-scratch/whannah/DYNAMO/Hindcast/RMM_EOF_Structures.txt"
  data = asciiread(ifile,23+144*2*3,"float")
  EOF1 = data(23::2) 
  EOF2 = data(24::2) 
  
  RMM1 = dim_sum_n(V*conform(V,EOF1,1),1) / sqrt(55.43719)
  RMM2 = dim_sum_n(V*conform(V,EOF2,1),1) / sqrt(52.64146)
  
  	print("")
  	if isvar("tmp") then
  	  delete(tmp)
  	end if 
  	t = 0
  	tmp = eOLR(t,:)
  	i = 0
  	A1 = sum(tmp*EOF1(i*144:(i+1)*144-1))^2. + sum(tmp*EOF2(i*144:(i+1)*144-1))^2.
  	tmp = eU850(t,:)
  	i = 1
  	A2 = sum(tmp*EOF1(i*144:(i+1)*144-1))^2. + sum(tmp*EOF2(i*144:(i+1)*144-1))^2.
  	tmp = eU200(t,:)
  	i = 2
  	A3 = sum(tmp*EOF1(i*144:(i+1)*144-1))^2. + sum(tmp*EOF2(i*144:(i+1)*144-1))^2.
  
  mS =  RMM1(0)^2.+ RMM2(0)^2.	;(A1+A2+A3)
  oS = oRMM1(0)^2.+oRMM2(0)^2.	;(0.1213689   +   0.2738290   +   0.6048022)
  pr = 5
  fmt = "%4.4f"
  print("")
  print("OLR:  "+sprintf(fmt,(A1/mS))+"		"+sprintf(fmt,(A1/oS))+"	(0.1213689)"+"		"+sprintf(fmt,A1)+"		"+sprintf(fmt,(oS*0.1213689)))
  print("U850: "+sprintf(fmt,(A2/mS))+"		"+sprintf(fmt,(A2/oS))+"	(0.2738290)"+"		"+sprintf(fmt,A2)+"		"+sprintf(fmt,(oS*0.2738290)))
  print("U200: "+sprintf(fmt,(A3/mS))+"		"+sprintf(fmt,(A3/oS))+"	(0.6048022)"+"		"+sprintf(fmt,A3)+"		"+sprintf(fmt,(oS*0.6048022)))
  print("")
  print("mAMP	"+mS)
  print("oAMP	"+oS)
  

		; YR			   MN          DY  OLR            U850           U200
		;2011          10           1  0.1213689      0.2738290      0.6048022    
        ;2011          10           2  0.1085826      0.3140527      0.5773646    
        ;2011          10           3  4.1612018E-02  0.3668695      0.5915186    
        ;2011          10           4  1.3241430E-02  0.3016072      0.6851514    
        ;2011          10           5  0.1290348      0.2722498      0.5987154    
        ;2011          10           6  0.1474295      0.3561696      0.4964008    
        ;2011          10           7  0.1215252      0.4320058      0.4464689    
        ;2011          10           8  0.1518189      0.3589070      0.4892741    
        ;2011          10           9  0.1432370      0.3090606      0.5477024    
        ;2011          10          10  0.1414620      0.4157303      0.4428077  

  ;================================================================
  ; plotting code for sanity test
  ;================================================================
	if mkplot then
	  d1 = 0;31+28+31+30+31+30+31+30+30
	  ot1 = 0  +d1
	  ot2 = 19 +d1
	  mst = fac
	  wks = gsn_open_wks("x11","test")
	  plot = new(4,graphic)
		res = True
		res@gsnDraw = False
		res@gsnFrame = False
	  oAMP = sqrt(oRMM1(ot1:ot2)^2.+oRMM2(ot1:ot2)^2.)
	  mAMP = sqrt( RMM1(::mst)^2.  + RMM2(::mst)^2.)
	  oPHS = atan2(oRMM2(ot1:ot2),oRMM1(ot1:ot2))
	  mPHS = atan2( RMM2(::mst)  , RMM1(::mst))
	  res@gsnRightString = "AMP"
	  plot(0) = gsn_csm_y(wks,(/mAMP,oAMP/),res)
	  res@gsnRightString = "PHS"
	  plot(1) = gsn_csm_y(wks,(/mPHS,oPHS/),res)
	  res@gsnRightString = "RMM1"
	  plot(2) = gsn_csm_y(wks,(/RMM1(::mst),oRMM1(ot1:ot2)/),res)
	  res@gsnRightString = "RMM2"
	  plot(3) = gsn_csm_y(wks,(/RMM2(::mst),oRMM2(ot1:ot2)/),res)
	  gsn_panel(wks,plot,(/2,2/),False)
	end if
	;----------------------------------------------
	;----------------------------------------------
	if False then
	  n = 288
	  wks = gsn_open_wks("x11","test")
	  plot = new(3,graphic)
		res = True
		res@gsnDraw = False
		res@gsnFrame = False
	  plot(0) = gsn_csm_xy(wks,lon,(/EOF1(n*0:n*1-1),EOF1(n*1:n*2-1),EOF1(n*2:n*3-1)/),res)
	  plot(1) = gsn_csm_xy(wks,lon,(/EOF2(n*0:n*1-1),EOF2(n*1:n*2-1),EOF2(n*2:n*3-1)/),res)
	  plot(2) = gsn_csm_xy(wks,lon,(/V (0,n*0:n*1-1), V(0,n*1:n*2-1), V(0,n*2:n*3-1)/),res)
	  ;plot(2) = gsn_csm_xy(wks,lon,(/V (0,n*0:n*1-1)*EOF1(n*0:n*1-1), V(0,n*1:n*2-1)*EOF1(n*1:n*2-1),V(0,n*2:n*3-1)*EOF1(n*2:n*3-1)/),res)
	  gsn_panel(wks,plot,(/2,2/),False)
	end if
  ;================================================================
  ; Write model RMM to file
  ;================================================================  
  exit
  ofile = "/maloney-scratch/whannah/DYNAMO/Hindcast/data/"+case(c)+"/"+case(c)+".RMM.nc"
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")
  outfile->RMM1 = RMM1
  outfile->RMM2 = RMM2
  
  	print("")
  	print("	"+ofile)
  	print("")
end do
;===================================================================================
;===================================================================================
end
